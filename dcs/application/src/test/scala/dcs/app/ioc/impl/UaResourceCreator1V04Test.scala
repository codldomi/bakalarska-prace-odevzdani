package dcs.app.ioc.impl

import dcs.app.ioc.model.{UaResourceConf, UaSecurityConf, UaServerConf}
import dcs.app.test.tag.{IntegrationTest, NeedsRunningServer}

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scopcua.data.UaId
import scopcua.data.security.UaSecurityPolicy
import scopcua.data.value.UaString
import scopcua.data.value.number.integer.UaUInteger.UaUInt32

import net.liftweb.json._


class UaResourceCreator1V04Test extends AnyFunSuite with Matchers {

  private val creator = new UaResourceCreator1V04


  private def compare(result: String, expected: String): Unit = {

    val testableResult = parse(result)
    val testableExpected = parse(expected)

    testableResult shouldBe testableExpected
  }


  test("Test configuration for real localhost servers: Eclipse Milo Demo Server, open62541 test server.", IntegrationTest, NeedsRunningServer) {

    val config =
      UaResourceConf(Seq(
        UaServerConf(
          id = 1,
          url = "opc.tcp://127.0.0.1:4840",
          security = None,
          models = Seq("nodeset/Opc.Ua.IEC61850-7-3.NodeSet2.xml", "nodeset/Opc.Ua.IEC61850-7-4.NodeSet2.xml", "nodeset/ModemTec.PDM1.NodeSet2.xml"),
          statusCodes = "StatusCode.csv"),
        UaServerConf(
          id = 2,
          url = "opc.tcp://127.0.0.1:6254/milo",
          security = Some(
            UaSecurityConf(
              alias = "secureClient",
              username = "user1",
              password = "password",
              appName = "Secure Scalable Client",
              appUri = "urn:secure:scalable:client",
              policy = UaSecurityPolicy.Basic256Sha256,
              serverCert = "client/pki/trusted/certs/milo_demo_server.crt",
              trustDir = "src/test/resources/client/pki",
              keyStorePassword = "password",
              keyStore = "client/ks.p12",
              timeout = 60000)),
          models = Seq.empty,
          statusCodes = "StatusCode.csv")))

    val resources = creator.create(config)

    val open = resources(key = 1)
    val milo = resources(key = 2)

    // Read Namespace Array - node with i=2255;ns=“http://opcfoundation.org/UA/”

    val miloResult = milo.readValue(id = UaId(UaUInt32(2255), UaString("http://opcfoundation.org/UA/")))
    val miloExpected = s"""["http://opcfoundation.org/UA/", "urn:eclipse:milo:opcua:server:db520941-be4e-4934-8367-2b2a899762f1", "urn:eclipse:milo:opcua:server:demo"]"""

    compare(miloResult._1, miloExpected)

    // Read data of BehaviourModeKing[IEC74] - node with i=6002;ns=http://www.modemtec.cz/PD/

    val openResult = open.readValue(id = UaId(UaUInt32(6002), UaString("http://www.modemtec.cz/PD/")))
    val openExpected = "{ \"value\": 3, \"name\": \"test\" }"

    compare(openResult._1, openExpected)
  }

}

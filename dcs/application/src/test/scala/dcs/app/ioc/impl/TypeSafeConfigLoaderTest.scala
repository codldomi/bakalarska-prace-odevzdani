package dcs.app.ioc.impl

import dcs.app.ioc.model.{UaResourceConf, UaSecurityConf, UaServerConf}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import scopcua.data.security.UaSecurityPolicy


class TypeSafeConfigLoaderTest extends AnyFunSuite with Matchers {

  test("Load OPC UA Resource") {

    val loader = new TypeSafeConfigLoader("application.conf")

    val result = loader.loadUaResource()

    val expected =
      UaResourceConf(Seq(
        UaServerConf(
          id = 1,
          url = "opc.tcp://127.0.0.1:6254/milo",
          security = Some(
            UaSecurityConf(
              alias = "secureClient",
              username = "user1",
              password = "password",
              appName = "Secure Scalable Client",
              appUri = "urn:secure:scalable:client",
              policy = UaSecurityPolicy.Basic256Sha256,
              serverCert = "client/pki/trusted/certs/milo_demo_server.crt",
              trustDir = "src/main/resources/client/pki",
              keyStorePassword = "password",
              keyStore = "client/ks.p12",
              timeout = 60000)),
          models = Seq.empty,
          statusCodes = "StatusCode.csv"),
        UaServerConf(
          id = 2,
          url = "opc.tcp://127.0.0.1:4840",
          security = None,
          models = Seq("nodeset/Opc.Ua.IEC61850-7-3.NodeSet2.xml", "nodeset/Opc.Ua.IEC61850-7-4.NodeSet2.xml", "nodeset/ModemTec.PDM1.NodeSet2.xml"),
          statusCodes = "StatusCode.csv")))

    result shouldBe expected
  }

}
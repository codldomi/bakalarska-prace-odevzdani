package dcs.app.test.tag

import org.scalatest.Tag

/** A test uses other components without their mock or stub.
  */
object IntegrationTest extends Tag("IntegrationTest")

package dcs.app.test.tag

import org.scalatest.Tag


/** A test needs running OPC UA server.
  */
object NeedsRunningDb extends Tag("NeedsRunningDb")

package dcs.app.layer.dl.repository.impl

import dcs.app.layer.dl.db.Tables
import dcs.app.layer.dl.entity.TaskState.{Created, Success}
import dcs.app.layer.dl.entity.{TaskResult, TaskState, UpdateTask, VariableValue}

import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import java.sql.Timestamp

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global


class SlickTaskRepositoryTest extends AnyFunSuite with Matchers with BeforeAndAfter {

  private val config = DatabaseConfig.forConfig[JdbcProfile]("postgres")
  private val profile: JdbcProfile = config.profile
  private val db = config.db
  private val repo = new SlickTaskRepository(profile, db, waitTime = 2)

  import profile.api._
  import Tables._

  /** The current time.
    */
  private val timeB = Timestamp.valueOf("2022-12-12 00:00:00")

  /** The fixed time. Older than now.
    */
  private val timeA = Timestamp.valueOf("2020-12-12 00:00:00")

  /** Synchronously executes an action.
    */
  private def exec[T](action: DBIO[T]): T = Await.result(db.run(action), 2.seconds)

  /** Inserts rows with default values into tables.
    *
    * @return Server ID, Node ID
    */
  private def init(): (Int, Int) = {

    val action = for {

      objectTreeId <- {
        val row = TObjectTreeRow(id = 0, cAlias = "ObjectTree", cTimestamp = timeB)
        val objectTrees = TableQuery[TObjectTree]
        objectTrees returning objectTrees.map(_.id) += row
      }

      serverId <- {
        val row = TServerRow(id = 0, cAlias = "Server", cUrl = "URL", tObjectTreeId = objectTreeId)
        val servers = TableQuery[TServer]
        servers returning  servers.map(_.id) += row
      }

      namespaceId <- {
        val row = TNamespaceRow(id = 0, cUri = "Namespace", cTimestamp = timeB)
        val namespaces = TableQuery[TNamespace]
        namespaces returning  namespaces.map(_.id) += row
      }

      nodeId <- {
        val row = TIdRow(id = 0, cIndex = 10, tNamespaceId = namespaceId)
        val ids = TableQuery[TId]
        ids returning  ids.map(_.id) += row
      }

      dataTypeId <- {
        val row = TIdRow(id = 0, cIndex = 100, tNamespaceId = namespaceId)
        val ids = TableQuery[TId]
        ids returning ids.map(_.id) += row
      }

      _ <- {
        val row =
          TNodeRow(
            cBrowseName = "BrowseName",
            cDisplayName = "DisplayName",
            cDescription = None,
            cType = 1,
            tNodeId = None,
            tObjectTreeId = objectTreeId,
            id = nodeId)
        TableQuery[TNode] += row
      }

      _ <- {
        val row = TDatatypeRow(cIsAbstract = false, cType = 1, cName = "DataType", tDatatypeId = None, id = dataTypeId)
        TableQuery[TDatatype] += row
      }

      _ <- {
        val row =
          TVariableRow(
            cIsHistorizing = true,
            cMinimumSamplingInterval = 0,
            cDefaultValue = None,
            cAccessLevel = 2,
            tNodeId = nodeId,
            tDatatypeId = dataTypeId)
        TableQuery[TVariable] += row
      }

    } yield (serverId, nodeId)

    exec(action)
  }


  //  Deletes rows before and after each test
  before {
    exec(
      TTaskState.delete
      andThen TResult.delete
      andThen TTask.delete
      andThen TVariableValue.delete
      andThen TVariable.delete
      andThen TDatatype.delete
      andThen TNode.delete
      andThen TId.delete
      andThen TNamespace.delete
      andThen TServer.delete
      andThen TObjectTree.delete)
  }

  after {
    exec(
      TTaskState.delete
      andThen TResult.delete
      andThen TTask.delete
      andThen TVariableValue.delete
      andThen TVariable.delete
      andThen TDatatype.delete
      andThen TNode.delete
      andThen TId.delete
      andThen TNamespace.delete
      andThen TServer.delete
      andThen TObjectTree.delete)
  }


  test("Create UpdateTask") {

    val (serverId, nodeId) = init()

    val task =
      UpdateTask(
        id = 0,
        state = TaskState.Created(id = 0, description = None, timestamp = timeB),
        serverId,
        nodeId,
        result = None)

    repo.createUpdateTask(task)
  }


  test("Read task (with result)") {

    val (serverId, nodeId) = init()
    val (taskId, stateId, variableId) = {

      val action = for {

        taskId <- {
          val tasks = TableQuery[TTask]
          val row = TTaskRow(id = 0, cType = 1, tNodeId = nodeId, tServerId = serverId)
          tasks returning tasks.map(_.id) += row
        }

        _ <- {
          val states = TableQuery[TTaskState]
          val row = TTaskStateRow(id = 0, cType = 1, cTimestamp = timeA, cDescription = None, tTaskId = taskId)
          states += row
        }

        stateId <- {
          val states = TableQuery[TTaskState]
          val row = TTaskStateRow(id = 0, cType = 4, cTimestamp = timeB, cDescription = None, tTaskId = taskId)
          states returning states.map(_.id) += row
        }

        variableId <- {
          val values = TableQuery[TVariableValue]
          val row =
            TVariableValueRow(
              id = 0,
              cSourceTimestamp = timeA,
              cTimestamp = timeA,
              cValue = "null",
              tServerId = serverId,
              tVariableId = nodeId)
          values returning values.map(_.id) += row
        }

        _ <- {
          val results = TableQuery[TResult]
          val row = TResultRow(tTaskId = taskId, tVariableValueId = Some(variableId))
          results += row
        }

      } yield (taskId, stateId, variableId)

      exec(action)
    }

    val result = repo.read(taskId)

    val expected =
      UpdateTask(
        id = taskId,
        state = Success(id = stateId, description = None, timestamp = timeB),
        serverId,
        nodeId,
        Some(TaskResult(VariableValue(id = variableId, sourceTimestamp = timeA, timestamp = timeA, value = "null"))))

    result shouldBe expected
  }


  test("Read task (without result)") {

    val (serverId, nodeId) = init()
    val (taskId, stateId) = {

      val action = for {

        taskId <- {
          val tasks = TableQuery[TTask]
          val row = TTaskRow(id = 0, cType = 1, tNodeId = nodeId, tServerId = serverId)
          tasks returning tasks.map(_.id) += row
        }

        stateId <- {
          val states = TableQuery[TTaskState]
          val row = TTaskStateRow(id = 0, cType = 1, cTimestamp = timeA, cDescription = None, tTaskId = taskId)
          states returning states.map(_.id) += row
        }

      } yield (taskId, stateId)

      exec(action)
    }

    val result = repo.read(taskId)

    val expected =
      UpdateTask(
        id = taskId,
        state = Created(id = stateId, description = None, timestamp = timeA),
        serverId,
        nodeId,
        result = None)

    result shouldBe expected
  }

}

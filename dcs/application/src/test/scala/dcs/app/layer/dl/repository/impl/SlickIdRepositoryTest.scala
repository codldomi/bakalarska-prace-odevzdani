package dcs.app.layer.dl.repository.impl

import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import slick.jdbc.JdbcProfile
import slick.basic.DatabaseConfig

import dcs.app.layer.dl.db.Tables
import dcs.app.test.tag.NeedsRunningDb

import scopcua.data.UaId
import scopcua.data.value.UaString
import scopcua.data.value.number.integer.UaUInteger.UaUInt32

import java.sql.Timestamp
import java.time.Instant

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt


class SlickIdRepositoryTest extends AnyFunSuite with Matchers with BeforeAndAfter {

  private val config = DatabaseConfig.forConfig[JdbcProfile]("postgres")
  private val profile: JdbcProfile = config.profile
  private val db = config.db
  private val repo = new SlickIdRepository(profile, db, waitTime = 2)

  import profile.api._
  import Tables._

  private def now: Timestamp = Timestamp.from(Instant.now())

  //  Synchronously executes an action.
  private def exec[T](action: DBIO[T]): T = Await.result(db.run(action), 2.seconds)

  //  Deletes rows before and after each test
  before {
    exec(TId.delete andThen TNamespace.delete)
  }

  after {
    exec(TId.delete andThen TNamespace.delete)
  }


  test("Read OPC UA identifier", NeedsRunningDb) {

    val namespace = TNamespaceRow(id = 1, cUri = "Namespace", cTimestamp = now)
    val namespaces = TableQuery[TNamespace]
    val namespaceId = exec(namespaces returning namespaces.map(_.id)  += namespace)

    val id = TIdRow(id = 1, cIndex = 10, tNamespaceId = namespaceId)
    val ids = TableQuery[TId]
    val idId = exec(ids returning ids.map(_.id) += id)

    val result = repo.read(id = idId)
    val expected = UaId(UaUInt32(10), UaString("Namespace"))

    result shouldBe expected
  }


  test("Read database identifier", NeedsRunningDb) {

    val namespace = TNamespaceRow(id = 1, cUri = "Namespace", cTimestamp = now)
    val namespaces = TableQuery[TNamespace]
    val namespaceId = exec(namespaces returning namespaces.map(_.id)  += namespace)

    val id = TIdRow(id = 1, cIndex = 10, tNamespaceId = namespaceId)
    val ids = TableQuery[TId]
    val idId = exec(ids returning ids.map(_.id) += id)

    val result = repo.readId(UaId(UaUInt32(10), UaString("Namespace")))
    val expected = idId

    result shouldBe expected
  }

}

package dcs.app.layer.bl.service.impl

import dcs.app.clock.Clock
import dcs.app.layer.bl.service.impl.util.resource.UaResourceManager
import dcs.app.layer.dl.entity.{TaskResult, TaskState, UpdateTask, VariableValue}
import dcs.app.layer.dl.repository.{IdRepository, TaskRepository}
import dcs.app.logger.Logger
import dcs.app.test.tag.UnitTest

import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory

import scopcua.data.UaId
import scopcua.data.value.UaString
import scopcua.data.value.number.integer.UaUInteger.UaUInt32

import java.sql.Timestamp


class SyncTaskServiceTest extends AnyFunSuite with MockFactory {

  private val nodeId =
    UaId(
      index = UaUInt32(1),
      namespaceUri = UaString("namespaceUri"))

  private val varValue =
    VariableValue(
      id = 0,
      sourceTimestamp = Timestamp.valueOf("2022-01-01 09:02:15"),
      timestamp = Timestamp.valueOf("2022-01-01 09:02:45"),
      value = "value")

  private val result: TaskResult = TaskResult(varValue)

  private val createdTask =
    UpdateTask(
      id = 123,
      state =
        TaskState.Created(
          id = 123,
          description = None,
          timestamp = Timestamp.valueOf("2022-01-01 09:01:15")),
      serverId = 456,
      nodeId = 789,
      result = None)

  private val processingState =
    TaskState.Processing(
      id = 123,
      description = Some("The application accepted and started to process Task."),
      timestamp = Timestamp.valueOf("2022-01-01 09:02:15"))

  private val successState =
    TaskState.Success(
      id = 123,
      description = Some("The application successfully processed Task."),
      timestamp = Timestamp.valueOf("2022-01-01 09:03:15"))

  private val processingTask =
    UpdateTask(
      id = 123,
      state = processingState,
      serverId = 456,
      nodeId = 789,
      result = None)

  private val successTask =
    UpdateTask(
      id = 123,
      state = successState,
      serverId = 456,
      nodeId = 789,
      result = Some(result))


  test("createUpdateTask method executes properly", UnitTest) {

    val taskRepoMock = mock[TaskRepository]
    val idRepoMock = mock[IdRepository]
    val resourcesMock = mock[UaResourceManager]
    val loggerMock = mock[Logger]
    val clockMock = mock[Clock]

    // Expected calls to IdRepository
    (idRepoMock.read _).expects(createdTask.nodeId).returning(nodeId)

    // Expected calls to Logger
    (loggerMock.logTask _).expects(createdTask)
    (loggerMock.logTask _).expects(processingTask)
    (loggerMock.logTask _).expects(successTask)

    // Expected calls to ResourceManager
    (resourcesMock.readValue _).expects(processingTask.serverId, nodeId)
      .returning(varValue.value, varValue.sourceTimestamp)

    // Expected calls to Clock
    inSequence {
      (clockMock.now _).expects().returning(Timestamp.valueOf("2022-01-01 09:02:15"))
      (clockMock.now _).expects().returning(Timestamp.valueOf("2022-01-01 09:02:45"))
      (clockMock.now _).expects().returning(Timestamp.valueOf("2022-01-01 09:03:15"))
    }

    // Expected calls to TaskRepository
    inSequence {
      (taskRepoMock.createUpdateTask _).expects(createdTask)
        .returning(createdTask)

      (taskRepoMock.createTaskState _).expects(createdTask.id, processingState)
        .returning(processingState)

      (taskRepoMock.createTaskResult _).expects(processingTask.id, processingTask.nodeId, processingTask.serverId, result)
        .returning(result)

      (taskRepoMock.createTaskState _).expects(processingTask.id, successState)
        .returning(successState)
    }

    val syncTaskService = new SyncTaskService(taskRepoMock, idRepoMock, resourcesMock, loggerMock, clockMock)
    val expectedTask = syncTaskService.createUpdateTask(createdTask)

    assert(true, expectedTask == successTask)
  }


  test("createUpdateTask method where UaResourceManager::readValue throws exception", UnitTest) {

    val taskRepoMock = mock[TaskRepository]
    val idRepoMock = mock[IdRepository]
    val resourcesMock = mock[UaResourceManager]
    val loggerMock = mock[Logger]
    val clockMock = mock[Clock]

    val errorState =
      TaskState.Error(
        id = 123,
        description = Some("The application terminated Task with an error message: Read value not found"),
        timestamp =  Timestamp.valueOf("2022-01-01 09:02:45"))

    val errorTask =
      UpdateTask(
        id = 123,
        state = errorState,
        serverId = 456,
        nodeId = 789,
        result = None)

    // Expected calls to IdRepository
    (idRepoMock.read _).expects(createdTask.nodeId).returning(nodeId)

    // Expected calls to Logger
    (loggerMock.logTask _).expects(createdTask)
    (loggerMock.logTask _).expects(processingTask)
    (loggerMock.logTask _).expects(errorTask)

    // Expected calls to ResourceManager
    (resourcesMock.readValue _).expects(processingTask.serverId, nodeId)
      .throwing(new Exception("Read value not found"))

    // Expected calls to Clock
    inSequence {
      (clockMock.now _).expects().returning(Timestamp.valueOf("2022-01-01 09:02:15"))
      (clockMock.now _).expects().returning(Timestamp.valueOf("2022-01-01 09:02:45"))
    }

    // Expected calls to TaskRepository
    inSequence {
      (taskRepoMock.createUpdateTask _).expects(createdTask).returning(createdTask)

      (taskRepoMock.createTaskState _).expects(createdTask.id, processingState).returning(processingState)

      (taskRepoMock.createTaskState _).expects(processingTask.id, errorState).returning(errorState)
    }

    val syncTaskService = new SyncTaskService(taskRepoMock, idRepoMock, resourcesMock, loggerMock, clockMock)
    val expectedTask = syncTaskService.createUpdateTask(createdTask)

    assert(true, expectedTask == errorTask)
  }


  test("createUpdateTask method database doesn't support current task", UnitTest) {

    val taskRepoMock = mock[TaskRepository]
    val idRepoMock = mock[IdRepository]
    val resourcesMock = mock[UaResourceManager]
    val loggerMock = mock[Logger]
    val clockMock = mock[Clock]

    val errorState =
      TaskState.Error(
        id = 123,
        description = Some("The application terminated Task with an error message: Database doesn't support this task"),
        timestamp =  Timestamp.valueOf("2022-01-01 09:03:45"))

    val errorTask =
      UpdateTask(
        id = 123,
        state = errorState,
        serverId = 456,
        nodeId = 789,
        result = None)

    // Expected calls to IdRepository
    (idRepoMock.read _).expects(createdTask.nodeId).returning(nodeId)

    // Expected calls to Logger
    (loggerMock.logTask _).expects(createdTask)
    (loggerMock.logTask _).expects(processingTask)
    (loggerMock.logTask _).expects(errorTask)

    // Expected calls to ResourceManager
    (resourcesMock.readValue _).expects(processingTask.serverId, nodeId)
      .returning(varValue.value, varValue.sourceTimestamp)

    // Expected calls to Clock
    inSequence {
      (clockMock.now _).expects().returning(Timestamp.valueOf("2022-01-01 09:02:15"))
      (clockMock.now _).expects().returning(Timestamp.valueOf("2022-01-01 09:02:45"))
      (clockMock.now _).expects().returning(Timestamp.valueOf("2022-01-01 09:03:15"))
      (clockMock.now _).expects().returning(Timestamp.valueOf("2022-01-01 09:03:45"))
    }

    // Expected calls to TaskRepository
    inSequence {
      (taskRepoMock.createUpdateTask _).expects(createdTask).returning(createdTask)

      (taskRepoMock.createTaskState _).expects(createdTask.id, processingState)
        .returning(processingState)

      (taskRepoMock.createTaskResult _).expects(processingTask.id, processingTask.nodeId, processingTask.serverId, result)
        .returning(result)

      (taskRepoMock.createTaskState _).expects(processingTask.id, successState)
        .throwing(new Exception("Database doesn't support this task"))

      (taskRepoMock.createTaskState _).expects(processingTask.id, errorState)
        .returning(errorState)
    }

    val syncTaskService = new SyncTaskService(taskRepoMock, idRepoMock, resourcesMock, loggerMock, clockMock)
    val expectedTask = syncTaskService.createUpdateTask(createdTask)

    assert(true, expectedTask == errorTask)
  }


  test("createUpdateTask method IdRepository::read method couldn't find nodeId", UnitTest) {

    val taskRepoMock = mock[TaskRepository]
    val idRepoMock = mock[IdRepository]
    val resourcesMock = mock[UaResourceManager]
    val loggerMock = mock[Logger]
    val clockMock = mock[Clock]

    // Expected calls to IdRepository
    (idRepoMock.read _).expects(createdTask.nodeId).throwing(new Exception("NodeId doesn't exist in database"))

    // Expected calls to Logger
    (loggerMock.logTask _).expects(createdTask)

    // Expected calls to TaskRepository
    (taskRepoMock.createUpdateTask _).expects(createdTask).returning(createdTask)

    val syncTaskService = new SyncTaskService(taskRepoMock, idRepoMock, resourcesMock, loggerMock, clockMock)

    assertThrows[Exception] {
      syncTaskService.createUpdateTask(createdTask)
    }
  }

}

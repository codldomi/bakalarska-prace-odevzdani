package dcs.app.layer.dl.repository

import scopcua.data.UaId


/** Encapsulates the logic required to access data source - identifier Id.
  */
trait IdRepository {

  /** Reads OPC UA identifier.
    *
    * @param id Entity's identifier.
    * @return OPC UA identifier.
    */
  def read(id: Int): UaId

  /** Reads Entity identifier.
    *
    * @param id OPC UA identifier.
    * @return Entity identifier.
    */
  def readId(id: UaId): Int

}

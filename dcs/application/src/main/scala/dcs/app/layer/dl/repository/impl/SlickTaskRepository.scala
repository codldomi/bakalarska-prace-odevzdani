package dcs.app.layer.dl.repository.impl

import dcs.app.layer.dl.db.Tables
import dcs.app.layer.dl.entity.{Task, TaskResult, TaskState, UpdateTask, VariableValue}
import dcs.app.layer.dl.repository.TaskRepository

import slick.dbio.DBIOAction
import slick.jdbc.JdbcProfile

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt


/** TaskRepository implemented via Slick library.
  *
  * @param profile Database Driver.
  * @param db Database.
  * @param waitTime A maximum time interval for waiting on a response from a database.
  */
class SlickTaskRepository(
  val profile: JdbcProfile,
  val db: JdbcProfile#Backend#Database,
  val waitTime: Int
) extends TaskRepository {

  import profile.api._
  import Tables._

  /** Synchronously executes an action.
    */
  def exec[T](action: DBIO[T]): T = Await.result(db.run(action), waitTime.seconds)

  /** Reads Task from the data source.
    *
    * @param id Task identifier.
    * @return Read Task.
    */
  override def read(id: Int): Task = {

    val action = for {

      row <- {
        TableQuery[TTask]
          .filter(_.id === id)
          .result
          .head
      }

      task <- row.cType match {
        case 1 => readUpdateTask(id, row.tNodeId, row.tServerId)
        case _ => throw new IllegalArgumentException("Unsupported Task.")
      }

    } yield task

    exec(action.transactionally)
  }

  /** Creates TaskState with the selected data.
    *
    * @param taskId Task identifier.
    * @param state Data.
    * @return Created TaskState with assigned identifiers.
    */
  override def createTaskState(taskId: Int, state: TaskState): TaskState = {

    val action = createState(taskId, state)
    exec(action.transactionally)
  }

  /** Creates TaskResult with the selected data.
    *
    * @param taskId Task identifier.
    * @param nodeId Node identifier.
    * @param serverId Server identifier.
    * @param result Data.
    * @return Created TaskResult with assigned identifiers.
    */
  override def createTaskResult(taskId: Int, nodeId: Int, serverId: Int, result: TaskResult): TaskResult = {

    val action = createResult(nodeId, serverId, taskId, result)
    exec(action.transactionally)
  }

  /** Creates UpdateTask with the selected data.
    *
    * @param task Data.
    * @return Created UpdateTask with assigned identifiers.
    */
  override def createUpdateTask(task: UpdateTask): UpdateTask = {

    val action = for {

      taskId <- {
        val row = TTaskRow(task.id, 1, task.serverId, task.nodeId)
        val jobs = TableQuery[TTask]
        jobs returning jobs.map(_.id) += row
      }

      result <- task.result match {
        case Some(r) => createResult(task.nodeId, task.serverId, taskId, r).map(createdResult => Some(createdResult))
        case None => DBIOAction.successful(None)
      }

      state <- createState(taskId, task.state)

    } yield task.copy(id = taskId, state = state, result = result)

    exec(action.transactionally)
  }


  private def createResult(nodeId: Int, serverId: Int, taskId: Int, result: TaskResult): DBIO[TaskResult] = for {

    variableValue <- result.value match {
      case Some(value) => createVariableValue(nodeId, serverId, value).map(v => Some(v))
      case None => DBIOAction.successful(None)
    }

    _ <- {
      val row =
        TResultRow(
          taskId,
          variableValue match {
            case Some(v) => Some(v.id)
            case None => None
          })

      val results = TableQuery[TResult]
      results += row
    }

  } yield result.copy(value = variableValue)


  private def createVariableValue(nodeId: Int, serverId: Int, value: VariableValue): DBIO[VariableValue] = {

    val row = TVariableValueRow(value.id, value.sourceTimestamp, value.timestamp, value.value, serverId, nodeId)

    val variableValues = TableQuery[TVariableValue]
    val action = variableValues returning variableValues.map(_.id) += row

    action.map(returnedId => value.copy(id = returnedId))
  }


  private def createState(taskId: Int, state: TaskState): DBIO[TaskState] = {

    val cType = state match {
      case _: TaskState.Created    => 1
      case _: TaskState.Processing => 2
      case _: TaskState.Error      => 3
      case _: TaskState.Success    => 4
    }

    val row = TTaskStateRow(state.id, cType, state.timestamp, state.description, taskId)

    val states = TableQuery[TTaskState]
    val action = states returning states.map(_.id) += row

    action
      .map(returnedId =>
        state match {
          case c: TaskState.Created    => c.copy(id = returnedId)
          case p: TaskState.Processing => p.copy(id = returnedId)
          case e: TaskState.Error      => e.copy(id = returnedId)
          case s: TaskState.Success    => s.copy(id = returnedId)
        })
  }


  private def readLastState(jobId: Int): DBIO[TaskState] = {

    TableQuery[TTaskState]
      .filter(_.tTaskId === jobId)
      .sortBy(_.cTimestamp.desc)
      .take(1)
      .result
      .head
      .map(row => row.cType match {
        case 1 => TaskState.Created(row.id, row.cDescription, row.cTimestamp)
        case 2 => TaskState.Processing(row.id, row.cDescription, row.cTimestamp)
        case 3 => TaskState.Error(row.id, row.cDescription, row.cTimestamp)
        case 4 => TaskState.Success(row.id, row.cDescription, row.cTimestamp)
        case _ => throw new IllegalArgumentException("Unsupported TaskState.")
      })
  }


  private def readResult(taskId: Int): DBIO[Option[TaskResult]] = for {

    optionRow <- {
      TableQuery[TResult]
        .filter(_.tTaskId === taskId)
        .result
        .headOption
    }

    result <- optionRow match {
      case Some(row) if row.tVariableValueId.isDefined =>
        val valueId = row.tVariableValueId.get
        readVariableValue(valueId).map(value => Some(TaskResult(value)))

      case Some(row) if row.tVariableValueId.isEmpty =>
        DBIOAction.successful(Some(TaskResult(value = None)))

      case None => DBIOAction.successful(None)
    }

  } yield result


  private def readVariableValue(id: Int): DBIO[VariableValue] = {

    TableQuery[TVariableValue]
      .filter(_.id === id)
      .result
      .head
      .map(row => VariableValue(row.id, row.cSourceTimestamp, row.cTimestamp, row.cValue))
  }


  private def readUpdateTask(id: Int, nodeId: Int, serverId: Int): DBIO[UpdateTask] = for {

    state <- readLastState(id)
    result <- readResult(id)

  } yield UpdateTask(id, state, serverId, nodeId, result)

}


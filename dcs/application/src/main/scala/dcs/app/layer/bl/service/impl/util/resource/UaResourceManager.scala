package dcs.app.layer.bl.service.impl.util.resource

import scopcua.data.UaId

import java.sql.Timestamp


/** Encapsulates methods on OPC UA servers.
  */
trait UaResourceManager {

  /** Reads value from the selected server and node.
    *
    * @param serverId The selected server.
    * @param id The node's identifier.
    * @return Encoded value and source timestamp.
    */
  def readValue(serverId: Int, id: UaId): (String, Timestamp)

}

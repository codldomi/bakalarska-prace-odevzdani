package dcs.app.layer.pl.cli.view


/** Provides the CLI application entry point.
  */
trait View {

  /** Starts the program cycle.
    */
  def run(): Unit

}

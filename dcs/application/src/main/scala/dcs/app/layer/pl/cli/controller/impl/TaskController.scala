package dcs.app.layer.pl.cli.controller.impl

import dcs.app.clock.Clock
import dcs.app.layer.bl.service.TaskService
import dcs.app.layer.dl.entity.{Task, TaskState, UpdateTask}
import dcs.app.layer.dl.repository.TaskRepository
import dcs.app.layer.pl.cli.controller.Controller

import scala.util.Try


/** Implements CLI Controller for the task processing.
  *
  * @param taskRepo Task Repository.
  * @param service Task Service.
  * @param clock Time provider.
  */
class TaskController(
  val taskRepo: TaskRepository,
  val service: TaskService,
  val clock: Clock
) extends Controller {

  /** Processes new UpdateTask.
    *
    * @param serverId Server's identifier.
    * @param nodeId Node's identifier.
    * @return The current state of the newly created UpdateTask.
    */
  override def processUpdateTask(serverId: Int, nodeId: Int): Try[UpdateTask] = Try {

    val task =
      UpdateTask(
        id = 0,
        TaskState.Created(id = 0, description = None, timestamp = clock.now),
        serverId,
        nodeId,
        result = None)

    service.createUpdateTask(task)
  }

  /** Reads information of the selected task.
    *
    * @param id Task's identifier.
    * @return The selected task.
    */
  override def readTask(id: Int): Try[Task] = Try {

    taskRepo.read(id)
  }

}
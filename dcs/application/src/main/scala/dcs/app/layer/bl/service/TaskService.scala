package dcs.app.layer.bl.service

import dcs.app.layer.dl.entity.UpdateTask


/** Provides a task processing.
  */
trait TaskService {

  /** Creates an UpdateTask.
    *
    * @param task Data for a creation of the task.
    * @return Created task in the current state.
    */
  def createUpdateTask(task: UpdateTask): UpdateTask

}

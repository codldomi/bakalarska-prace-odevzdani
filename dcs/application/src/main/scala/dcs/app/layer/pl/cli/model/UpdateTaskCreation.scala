package dcs.app.layer.pl.cli.model


case class UpdateTaskCreation(serverId: Int, nodeId: Int)

package dcs.app.layer.dl.entity

import java.sql.Timestamp


sealed trait TaskState {

  def id: Int

  def description: Option[String]

  def timestamp: Timestamp

}


object TaskState {

  case class Created(id: Int, description: Option[String], timestamp: Timestamp) extends TaskState

  case class Processing(id: Int, description: Option[String], timestamp: Timestamp) extends TaskState

  case class Error(id: Int, description: Option[String], timestamp: Timestamp) extends TaskState

  case class Success(id: Int, description: Option[String], timestamp: Timestamp) extends TaskState

}

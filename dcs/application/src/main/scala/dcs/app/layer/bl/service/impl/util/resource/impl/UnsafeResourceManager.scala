package dcs.app.layer.bl.service.impl.util.resource.impl

import dcs.app.layer.bl.service.impl.util.resource.UaResourceManager
import dcs.app.layer.bl.service.impl.util.resource.impl.util.UaResource
import scopcua.data.UaId

import java.sql.Timestamp


/** Doesn't manage connections to OPC UA servers (Resource) - only provides a mapping between server's ID and Resource.
  *
  * @param resources A mapping Server's ID -> Resource.
  */
class UnsafeResourceManager(
  val resources: Map[Int, UaResource],
) extends UaResourceManager {

  /** Reads value from the selected server and node.
    *
    * @param serverId The selected server.
    * @param id The node's identifier.
    * @return Encoded value and source timestamp.
    */
  override def readValue(serverId: Int, id: UaId): (String, Timestamp) = {

    val resource = resources(serverId)
    resource.readValue(id)
  }

}

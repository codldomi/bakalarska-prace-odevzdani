package dcs.app.layer.bl.service.impl.util.resource.impl.util

import scopcua.data.UaId
import scopcua.sdk.client.UaClient
import scopcua.util.jsonencoder.UaJsonEncoder
import scopcua.conversion.UaValueConversion.uaDateTimeToTimestamp

import java.sql.Timestamp


/** Encapsulates operations on an OPC UA client. Provides connecting, disconnecting and value encoding.
  *
  * @param client An OPC UA client.
  * @param encoder An encoder to the JSON format.
  */
class UaResource(client: UaClient, encoder: UaJsonEncoder) {

  private def session[A](operation: => A): A = {

    client.connect()
    try {
      operation
    } finally {
      client.disconnect()
    }
  }

  /** Reads value as JSON and its source timestamp of the selected node.
    *
    * @note Resource firstly connects to the server, then does an operation and finally disconnects from the server.
    * @param id The node's identifier.
    * @return Encoded value into JSON and source timestamp.
    */
  def readValue(id: UaId): (String, Timestamp) = session {

    val (value, dateTime) = client.readValue(id)

    val json = encoder.encode(value)
    val timestamp: Timestamp = dateTime

    (json, timestamp)
  }

}

package dcs.app.layer.bl.service.impl

import dcs.app.clock.Clock
import dcs.app.layer.bl.service.TaskService
import dcs.app.layer.bl.service.impl.util.resource.UaResourceManager
import dcs.app.layer.dl.entity.{TaskResult, TaskState, UpdateTask, VariableValue}
import dcs.app.layer.dl.repository.{IdRepository, TaskRepository}
import dcs.app.logger.Logger


/** Synchronously processes a task.
  *
  * @param taskRepo A repository managing tasks.
  * @param idRepo A repository managing ids.
  * @param resources A resource manager managing resources (e.g. provides searching by server ID).
  * @param logger A logger supporting Task's logging.
  */
class SyncTaskService(
  val taskRepo: TaskRepository,
  val idRepo: IdRepository,
  val resources: UaResourceManager,
  val logger: Logger,
  val clock: Clock
) extends TaskService {

  /** Creates an UpdateTask and processes it.
    *
    * @note Persists and logs an each state of the task.
    * @param createdTask Data for a creation of the task.
    * @return Processed task with a result.
    */
  override def createUpdateTask(createdTask: UpdateTask): UpdateTask = {

    var task = taskRepo.createUpdateTask(createdTask)
    logger.logTask(task)

    val nodeId = idRepo.read(task.nodeId)

    try {
      val created =
        taskRepo.createTaskState(
          task.id,
          TaskState.Processing(
            task.id,
            description = Some("The application accepted and started to process Task."),
            clock.now))

      task = task.copy(state = created)
      logger.logTask(task)

      val (value, sourceTimestamp) = resources.readValue(task.serverId, nodeId)
      val variableValue = VariableValue(id = 0, sourceTimestamp, clock.now, value)

      var result = TaskResult(variableValue)
      result = taskRepo.createTaskResult(task.id, task.nodeId, task.serverId, result)

      val success =
        taskRepo.createTaskState(
          task.id,
          TaskState.Success(
            task.id,
            description = Some("The application successfully processed Task."),
            clock.now))

      task = task.copy(state = success, result = Some(result))
      logger.logTask(task)
    } catch {
      case t: Throwable =>
        val error =
          taskRepo.createTaskState(
            task.id,
            TaskState.Error(
              task.id,
              description = Some(s"The application terminated Task with an error message: ${t.getMessage}"),
              clock.now))

        task = task.copy(state = error)
        logger.logTask(task)
    }

    task
  }

}

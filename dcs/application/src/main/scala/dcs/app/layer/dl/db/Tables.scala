package dcs.app.layer.dl.db

// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.jdbc.PostgresProfile
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.jdbc.JdbcProfile
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Array(TArgument.schema, TDatatype.schema, TEnumField.schema, TId.schema, TMethod.schema, TNamespace.schema, TNode.schema, TObject.schema, TObjectTree.schema, TResult.schema, TServer.schema, TStructField.schema, TTask.schema, TTaskState.schema, TVariable.schema, TVariableValue.schema).reduceLeft(_ ++ _)
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table TArgument
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param cIsInput Database column c_is_input SqlType(bool)
   *  @param cName Database column c_name SqlType(text)
   *  @param tMethodId Database column t_method_id SqlType(int4)
   *  @param tDatatypeId Database column t_datatype_id SqlType(int4) */
  case class TArgumentRow(id: Int, cIsInput: Boolean, cName: String, tMethodId: Int, tDatatypeId: Int)
  /** GetResult implicit for fetching TArgumentRow objects using plain SQL queries */
  implicit def GetResultTArgumentRow(implicit e0: GR[Int], e1: GR[Boolean], e2: GR[String]): GR[TArgumentRow] = GR{
    prs => import prs._
    TArgumentRow.tupled((<<[Int], <<[Boolean], <<[String], <<[Int], <<[Int]))
  }
  /** Table description of table t_argument. Objects of this class serve as prototypes for rows in queries. */
  class TArgument(_tableTag: Tag) extends profile.api.Table[TArgumentRow](_tableTag, Some("s_server"), "t_argument") {
    def * = (id, cIsInput, cName, tMethodId, tDatatypeId) <> (TArgumentRow.tupled, TArgumentRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(cIsInput), Rep.Some(cName), Rep.Some(tMethodId), Rep.Some(tDatatypeId))).shaped.<>({r=>import r._; _1.map(_=> TArgumentRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column c_is_input SqlType(bool) */
    val cIsInput: Rep[Boolean] = column[Boolean]("c_is_input")
    /** Database column c_name SqlType(text) */
    val cName: Rep[String] = column[String]("c_name")
    /** Database column t_method_id SqlType(int4) */
    val tMethodId: Rep[Int] = column[Int]("t_method_id")
    /** Database column t_datatype_id SqlType(int4) */
    val tDatatypeId: Rep[Int] = column[Int]("t_datatype_id")

    /** Foreign key referencing TDatatype (database name t_datatype_fk) */
    lazy val tDatatypeFk = foreignKey("t_datatype_fk", tDatatypeId, TDatatype)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
    /** Foreign key referencing TMethod (database name t_method_fk) */
    lazy val tMethodFk = foreignKey("t_method_fk", tMethodId, TMethod)(r => r.tNodeId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table TArgument */
  lazy val TArgument = new TableQuery(tag => new TArgument(tag))

  /** Entity class storing rows of table TDatatype
   *  @param cIsAbstract Database column c_is_abstract SqlType(bool)
   *  @param cType Database column c_type SqlType(int4)
   *  @param cName Database column c_name SqlType(text)
   *  @param tDatatypeId Database column t_datatype_id SqlType(int4), Default(None)
   *  @param id Database column id SqlType(int4), PrimaryKey */
  case class TDatatypeRow(cIsAbstract: Boolean, cType: Int, cName: String, tDatatypeId: Option[Int] = None, id: Int)
  /** GetResult implicit for fetching TDatatypeRow objects using plain SQL queries */
  implicit def GetResultTDatatypeRow(implicit e0: GR[Boolean], e1: GR[Int], e2: GR[String], e3: GR[Option[Int]]): GR[TDatatypeRow] = GR{
    prs => import prs._
    TDatatypeRow.tupled((<<[Boolean], <<[Int], <<[String], <<?[Int], <<[Int]))
  }
  /** Table description of table t_datatype. Objects of this class serve as prototypes for rows in queries. */
  class TDatatype(_tableTag: Tag) extends profile.api.Table[TDatatypeRow](_tableTag, Some("s_server"), "t_datatype") {
    def * = (cIsAbstract, cType, cName, tDatatypeId, id) <> (TDatatypeRow.tupled, TDatatypeRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(cIsAbstract), Rep.Some(cType), Rep.Some(cName), tDatatypeId, Rep.Some(id))).shaped.<>({r=>import r._; _1.map(_=> TDatatypeRow.tupled((_1.get, _2.get, _3.get, _4, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column c_is_abstract SqlType(bool) */
    val cIsAbstract: Rep[Boolean] = column[Boolean]("c_is_abstract")
    /** Database column c_type SqlType(int4) */
    val cType: Rep[Int] = column[Int]("c_type")
    /** Database column c_name SqlType(text) */
    val cName: Rep[String] = column[String]("c_name")
    /** Database column t_datatype_id SqlType(int4), Default(None) */
    val tDatatypeId: Rep[Option[Int]] = column[Option[Int]]("t_datatype_id", O.Default(None))
    /** Database column id SqlType(int4), PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.PrimaryKey)

    /** Foreign key referencing TDatatype (database name t_datatype_id_fk) */
    lazy val tDatatypeFk = foreignKey("t_datatype_id_fk", tDatatypeId, TDatatype)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing TId (database name t_id_fk) */
    lazy val tIdFk = foreignKey("t_id_fk", id, TId)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  }
  /** Collection-like TableQuery object for table TDatatype */
  lazy val TDatatype = new TableQuery(tag => new TDatatype(tag))

  /** Entity class storing rows of table TEnumField
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param cName Database column c_name SqlType(text)
   *  @param cValue Database column c_value SqlType(int4)
   *  @param tDatatypeId Database column t_datatype_id SqlType(int4) */
  case class TEnumFieldRow(id: Int, cName: String, cValue: Int, tDatatypeId: Int)
  /** GetResult implicit for fetching TEnumFieldRow objects using plain SQL queries */
  implicit def GetResultTEnumFieldRow(implicit e0: GR[Int], e1: GR[String]): GR[TEnumFieldRow] = GR{
    prs => import prs._
    TEnumFieldRow.tupled((<<[Int], <<[String], <<[Int], <<[Int]))
  }
  /** Table description of table t_enum_field. Objects of this class serve as prototypes for rows in queries. */
  class TEnumField(_tableTag: Tag) extends profile.api.Table[TEnumFieldRow](_tableTag, Some("s_server"), "t_enum_field") {
    def * = (id, cName, cValue, tDatatypeId) <> (TEnumFieldRow.tupled, TEnumFieldRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(cName), Rep.Some(cValue), Rep.Some(tDatatypeId))).shaped.<>({r=>import r._; _1.map(_=> TEnumFieldRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column c_name SqlType(text) */
    val cName: Rep[String] = column[String]("c_name")
    /** Database column c_value SqlType(int4) */
    val cValue: Rep[Int] = column[Int]("c_value")
    /** Database column t_datatype_id SqlType(int4) */
    val tDatatypeId: Rep[Int] = column[Int]("t_datatype_id")

    /** Foreign key referencing TDatatype (database name t_datatype_fk) */
    lazy val tDatatypeFk = foreignKey("t_datatype_fk", tDatatypeId, TDatatype)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table TEnumField */
  lazy val TEnumField = new TableQuery(tag => new TEnumField(tag))

  /** Entity class storing rows of table TId
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param cIndex Database column c_index SqlType(int4)
   *  @param tNamespaceId Database column t_namespace_id SqlType(int4) */
  case class TIdRow(id: Int, cIndex: Int, tNamespaceId: Int)
  /** GetResult implicit for fetching TIdRow objects using plain SQL queries */
  implicit def GetResultTIdRow(implicit e0: GR[Int]): GR[TIdRow] = GR{
    prs => import prs._
    TIdRow.tupled((<<[Int], <<[Int], <<[Int]))
  }
  /** Table description of table t_id. Objects of this class serve as prototypes for rows in queries. */
  class TId(_tableTag: Tag) extends profile.api.Table[TIdRow](_tableTag, Some("s_server"), "t_id") {
    def * = (id, cIndex, tNamespaceId) <> (TIdRow.tupled, TIdRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(cIndex), Rep.Some(tNamespaceId))).shaped.<>({r=>import r._; _1.map(_=> TIdRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column c_index SqlType(int4) */
    val cIndex: Rep[Int] = column[Int]("c_index")
    /** Database column t_namespace_id SqlType(int4) */
    val tNamespaceId: Rep[Int] = column[Int]("t_namespace_id")

    /** Foreign key referencing TNamespace (database name t_namespace_fk) */
    lazy val tNamespaceFk = foreignKey("t_namespace_fk", tNamespaceId, TNamespace)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table TId */
  lazy val TId = new TableQuery(tag => new TId(tag))

  /** Entity class storing rows of table TMethod
   *  @param cIsExecutable Database column c_is_executable SqlType(bool)
   *  @param tNodeId Database column t_node_id SqlType(int4), PrimaryKey */
  case class TMethodRow(cIsExecutable: Boolean, tNodeId: Int)
  /** GetResult implicit for fetching TMethodRow objects using plain SQL queries */
  implicit def GetResultTMethodRow(implicit e0: GR[Boolean], e1: GR[Int]): GR[TMethodRow] = GR{
    prs => import prs._
    TMethodRow.tupled((<<[Boolean], <<[Int]))
  }
  /** Table description of table t_method. Objects of this class serve as prototypes for rows in queries. */
  class TMethod(_tableTag: Tag) extends profile.api.Table[TMethodRow](_tableTag, Some("s_server"), "t_method") {
    def * = (cIsExecutable, tNodeId) <> (TMethodRow.tupled, TMethodRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(cIsExecutable), Rep.Some(tNodeId))).shaped.<>({r=>import r._; _1.map(_=> TMethodRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column c_is_executable SqlType(bool) */
    val cIsExecutable: Rep[Boolean] = column[Boolean]("c_is_executable")
    /** Database column t_node_id SqlType(int4), PrimaryKey */
    val tNodeId: Rep[Int] = column[Int]("t_node_id", O.PrimaryKey)

    /** Foreign key referencing TNode (database name t_node_fk) */
    lazy val tNodeFk = foreignKey("t_node_fk", tNodeId, TNode)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  }
  /** Collection-like TableQuery object for table TMethod */
  lazy val TMethod = new TableQuery(tag => new TMethod(tag))

  /** Entity class storing rows of table TNamespace
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param cUri Database column c_uri SqlType(text)
   *  @param cTimestamp Database column c_timestamp SqlType(timestamp) */
  case class TNamespaceRow(id: Int, cUri: String, cTimestamp: java.sql.Timestamp)
  /** GetResult implicit for fetching TNamespaceRow objects using plain SQL queries */
  implicit def GetResultTNamespaceRow(implicit e0: GR[Int], e1: GR[String], e2: GR[java.sql.Timestamp]): GR[TNamespaceRow] = GR{
    prs => import prs._
    TNamespaceRow.tupled((<<[Int], <<[String], <<[java.sql.Timestamp]))
  }
  /** Table description of table t_namespace. Objects of this class serve as prototypes for rows in queries. */
  class TNamespace(_tableTag: Tag) extends profile.api.Table[TNamespaceRow](_tableTag, Some("s_server"), "t_namespace") {
    def * = (id, cUri, cTimestamp) <> (TNamespaceRow.tupled, TNamespaceRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(cUri), Rep.Some(cTimestamp))).shaped.<>({r=>import r._; _1.map(_=> TNamespaceRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column c_uri SqlType(text) */
    val cUri: Rep[String] = column[String]("c_uri")
    /** Database column c_timestamp SqlType(timestamp) */
    val cTimestamp: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("c_timestamp")

    /** Uniqueness Index over (cUri) (database name t_namespace_uri_uq) */
    val index1 = index("t_namespace_uri_uq", cUri, unique=true)
  }
  /** Collection-like TableQuery object for table TNamespace */
  lazy val TNamespace = new TableQuery(tag => new TNamespace(tag))

  /** Entity class storing rows of table TNode
   *  @param cBrowseName Database column c_browse_name SqlType(text)
   *  @param cDisplayName Database column c_display_name SqlType(text)
   *  @param cDescription Database column c_description SqlType(text), Default(None)
   *  @param cType Database column c_type SqlType(int4)
   *  @param tNodeId Database column t_node_id SqlType(int4), Default(None)
   *  @param tObjectTreeId Database column t_object_tree_id SqlType(int4)
   *  @param id Database column id SqlType(int4), PrimaryKey */
  case class TNodeRow(cBrowseName: String, cDisplayName: String, cDescription: Option[String] = None, cType: Int, tNodeId: Option[Int] = None, tObjectTreeId: Int, id: Int)
  /** GetResult implicit for fetching TNodeRow objects using plain SQL queries */
  implicit def GetResultTNodeRow(implicit e0: GR[String], e1: GR[Option[String]], e2: GR[Int], e3: GR[Option[Int]]): GR[TNodeRow] = GR{
    prs => import prs._
    TNodeRow.tupled((<<[String], <<[String], <<?[String], <<[Int], <<?[Int], <<[Int], <<[Int]))
  }
  /** Table description of table t_node. Objects of this class serve as prototypes for rows in queries. */
  class TNode(_tableTag: Tag) extends profile.api.Table[TNodeRow](_tableTag, Some("s_server"), "t_node") {
    def * = (cBrowseName, cDisplayName, cDescription, cType, tNodeId, tObjectTreeId, id) <> (TNodeRow.tupled, TNodeRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(cBrowseName), Rep.Some(cDisplayName), cDescription, Rep.Some(cType), tNodeId, Rep.Some(tObjectTreeId), Rep.Some(id))).shaped.<>({r=>import r._; _1.map(_=> TNodeRow.tupled((_1.get, _2.get, _3, _4.get, _5, _6.get, _7.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column c_browse_name SqlType(text) */
    val cBrowseName: Rep[String] = column[String]("c_browse_name")
    /** Database column c_display_name SqlType(text) */
    val cDisplayName: Rep[String] = column[String]("c_display_name")
    /** Database column c_description SqlType(text), Default(None) */
    val cDescription: Rep[Option[String]] = column[Option[String]]("c_description", O.Default(None))
    /** Database column c_type SqlType(int4) */
    val cType: Rep[Int] = column[Int]("c_type")
    /** Database column t_node_id SqlType(int4), Default(None) */
    val tNodeId: Rep[Option[Int]] = column[Option[Int]]("t_node_id", O.Default(None))
    /** Database column t_object_tree_id SqlType(int4) */
    val tObjectTreeId: Rep[Int] = column[Int]("t_object_tree_id")
    /** Database column id SqlType(int4), PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.PrimaryKey)

    /** Foreign key referencing TId (database name t_id_fk) */
    lazy val tIdFk = foreignKey("t_id_fk", id, TId)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
    /** Foreign key referencing TNode (database name t_node_id_fk) */
    lazy val tNodeFk = foreignKey("t_node_id_fk", tNodeId, TNode)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing TObjectTree (database name t_object_tree_fk) */
    lazy val tObjectTreeFk = foreignKey("t_object_tree_fk", tObjectTreeId, TObjectTree)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table TNode */
  lazy val TNode = new TableQuery(tag => new TNode(tag))

  /** Entity class storing rows of table TObject
   *  @param cEventNotifier Database column c_event_notifier SqlType(int4)
   *  @param tNodeId Database column t_node_id SqlType(int4), PrimaryKey */
  case class TObjectRow(cEventNotifier: Int, tNodeId: Int)
  /** GetResult implicit for fetching TObjectRow objects using plain SQL queries */
  implicit def GetResultTObjectRow(implicit e0: GR[Int]): GR[TObjectRow] = GR{
    prs => import prs._
    TObjectRow.tupled((<<[Int], <<[Int]))
  }
  /** Table description of table t_object. Objects of this class serve as prototypes for rows in queries. */
  class TObject(_tableTag: Tag) extends profile.api.Table[TObjectRow](_tableTag, Some("s_server"), "t_object") {
    def * = (cEventNotifier, tNodeId) <> (TObjectRow.tupled, TObjectRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(cEventNotifier), Rep.Some(tNodeId))).shaped.<>({r=>import r._; _1.map(_=> TObjectRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column c_event_notifier SqlType(int4) */
    val cEventNotifier: Rep[Int] = column[Int]("c_event_notifier")
    /** Database column t_node_id SqlType(int4), PrimaryKey */
    val tNodeId: Rep[Int] = column[Int]("t_node_id", O.PrimaryKey)

    /** Foreign key referencing TNode (database name t_node_fk) */
    lazy val tNodeFk = foreignKey("t_node_fk", tNodeId, TNode)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  }
  /** Collection-like TableQuery object for table TObject */
  lazy val TObject = new TableQuery(tag => new TObject(tag))

  /** Entity class storing rows of table TObjectTree
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param cAlias Database column c_alias SqlType(text)
   *  @param cTimestamp Database column c_timestamp SqlType(timestamp) */
  case class TObjectTreeRow(id: Int, cAlias: String, cTimestamp: java.sql.Timestamp)
  /** GetResult implicit for fetching TObjectTreeRow objects using plain SQL queries */
  implicit def GetResultTObjectTreeRow(implicit e0: GR[Int], e1: GR[String], e2: GR[java.sql.Timestamp]): GR[TObjectTreeRow] = GR{
    prs => import prs._
    TObjectTreeRow.tupled((<<[Int], <<[String], <<[java.sql.Timestamp]))
  }
  /** Table description of table t_object_tree. Objects of this class serve as prototypes for rows in queries. */
  class TObjectTree(_tableTag: Tag) extends profile.api.Table[TObjectTreeRow](_tableTag, Some("s_server"), "t_object_tree") {
    def * = (id, cAlias, cTimestamp) <> (TObjectTreeRow.tupled, TObjectTreeRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(cAlias), Rep.Some(cTimestamp))).shaped.<>({r=>import r._; _1.map(_=> TObjectTreeRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column c_alias SqlType(text) */
    val cAlias: Rep[String] = column[String]("c_alias")
    /** Database column c_timestamp SqlType(timestamp) */
    val cTimestamp: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("c_timestamp")

    /** Uniqueness Index over (cAlias) (database name t_object_tree_alias_uq) */
    val index1 = index("t_object_tree_alias_uq", cAlias, unique=true)
  }
  /** Collection-like TableQuery object for table TObjectTree */
  lazy val TObjectTree = new TableQuery(tag => new TObjectTree(tag))

  /** Entity class storing rows of table TResult
   *  @param tTaskId Database column t_task_id SqlType(int4), PrimaryKey
   *  @param tVariableValueId Database column t_variable_value_id SqlType(int4), Default(None) */
  case class TResultRow(tTaskId: Int, tVariableValueId: Option[Int] = None)
  /** GetResult implicit for fetching TResultRow objects using plain SQL queries */
  implicit def GetResultTResultRow(implicit e0: GR[Int], e1: GR[Option[Int]]): GR[TResultRow] = GR{
    prs => import prs._
    TResultRow.tupled((<<[Int], <<?[Int]))
  }
  /** Table description of table t_result. Objects of this class serve as prototypes for rows in queries. */
  class TResult(_tableTag: Tag) extends profile.api.Table[TResultRow](_tableTag, Some("s_task"), "t_result") {
    def * = (tTaskId, tVariableValueId) <> (TResultRow.tupled, TResultRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(tTaskId), tVariableValueId)).shaped.<>({r=>import r._; _1.map(_=> TResultRow.tupled((_1.get, _2)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column t_task_id SqlType(int4), PrimaryKey */
    val tTaskId: Rep[Int] = column[Int]("t_task_id", O.PrimaryKey)
    /** Database column t_variable_value_id SqlType(int4), Default(None) */
    val tVariableValueId: Rep[Option[Int]] = column[Option[Int]]("t_variable_value_id", O.Default(None))

    /** Foreign key referencing TTask (database name t_task_fk) */
    lazy val tTaskFk = foreignKey("t_task_fk", tTaskId, TTask)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
    /** Foreign key referencing TVariableValue (database name t_variable_value_fk) */
    lazy val tVariableValueFk = foreignKey("t_variable_value_fk", tVariableValueId, TVariableValue)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.SetNull)
  }
  /** Collection-like TableQuery object for table TResult */
  lazy val TResult = new TableQuery(tag => new TResult(tag))

  /** Entity class storing rows of table TServer
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param cAlias Database column c_alias SqlType(text)
   *  @param cUrl Database column c_url SqlType(text)
   *  @param tObjectTreeId Database column t_object_tree_id SqlType(int4) */
  case class TServerRow(id: Int, cAlias: String, cUrl: String, tObjectTreeId: Int)
  /** GetResult implicit for fetching TServerRow objects using plain SQL queries */
  implicit def GetResultTServerRow(implicit e0: GR[Int], e1: GR[String]): GR[TServerRow] = GR{
    prs => import prs._
    TServerRow.tupled((<<[Int], <<[String], <<[String], <<[Int]))
  }
  /** Table description of table t_server. Objects of this class serve as prototypes for rows in queries. */
  class TServer(_tableTag: Tag) extends profile.api.Table[TServerRow](_tableTag, Some("s_server"), "t_server") {
    def * = (id, cAlias, cUrl, tObjectTreeId) <> (TServerRow.tupled, TServerRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(cAlias), Rep.Some(cUrl), Rep.Some(tObjectTreeId))).shaped.<>({r=>import r._; _1.map(_=> TServerRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column c_alias SqlType(text) */
    val cAlias: Rep[String] = column[String]("c_alias")
    /** Database column c_url SqlType(text) */
    val cUrl: Rep[String] = column[String]("c_url")
    /** Database column t_object_tree_id SqlType(int4) */
    val tObjectTreeId: Rep[Int] = column[Int]("t_object_tree_id")

    /** Foreign key referencing TObjectTree (database name t_object_tree_fk) */
    lazy val tObjectTreeFk = foreignKey("t_object_tree_fk", tObjectTreeId, TObjectTree)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)

    /** Uniqueness Index over (cAlias) (database name t_server_alias_uq) */
    val index1 = index("t_server_alias_uq", cAlias, unique=true)
  }
  /** Collection-like TableQuery object for table TServer */
  lazy val TServer = new TableQuery(tag => new TServer(tag))

  /** Entity class storing rows of table TStructField
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param cName Database column c_name SqlType(text)
   *  @param cIsArray Database column c_is_array SqlType(bool)
   *  @param cIsOptional Database column c_is_optional SqlType(bool)
   *  @param tDatatypeId Database column t_datatype_id SqlType(int4)
   *  @param tOfDatatypeId Database column t_of_datatype_id SqlType(int4) */
  case class TStructFieldRow(id: Int, cName: String, cIsArray: Boolean, cIsOptional: Boolean, tDatatypeId: Int, tOfDatatypeId: Int)
  /** GetResult implicit for fetching TStructFieldRow objects using plain SQL queries */
  implicit def GetResultTStructFieldRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Boolean]): GR[TStructFieldRow] = GR{
    prs => import prs._
    TStructFieldRow.tupled((<<[Int], <<[String], <<[Boolean], <<[Boolean], <<[Int], <<[Int]))
  }
  /** Table description of table t_struct_field. Objects of this class serve as prototypes for rows in queries. */
  class TStructField(_tableTag: Tag) extends profile.api.Table[TStructFieldRow](_tableTag, Some("s_server"), "t_struct_field") {
    def * = (id, cName, cIsArray, cIsOptional, tDatatypeId, tOfDatatypeId) <> (TStructFieldRow.tupled, TStructFieldRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(cName), Rep.Some(cIsArray), Rep.Some(cIsOptional), Rep.Some(tDatatypeId), Rep.Some(tOfDatatypeId))).shaped.<>({r=>import r._; _1.map(_=> TStructFieldRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column c_name SqlType(text) */
    val cName: Rep[String] = column[String]("c_name")
    /** Database column c_is_array SqlType(bool) */
    val cIsArray: Rep[Boolean] = column[Boolean]("c_is_array")
    /** Database column c_is_optional SqlType(bool) */
    val cIsOptional: Rep[Boolean] = column[Boolean]("c_is_optional")
    /** Database column t_datatype_id SqlType(int4) */
    val tDatatypeId: Rep[Int] = column[Int]("t_datatype_id")
    /** Database column t_of_datatype_id SqlType(int4) */
    val tOfDatatypeId: Rep[Int] = column[Int]("t_of_datatype_id")

    /** Foreign key referencing TDatatype (database name t_datatype_fk) */
    lazy val tDatatypeFk1 = foreignKey("t_datatype_fk", tDatatypeId, TDatatype)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
    /** Foreign key referencing TDatatype (database name t_of_datatype_fk) */
    lazy val tDatatypeFk2 = foreignKey("t_of_datatype_fk", tOfDatatypeId, TDatatype)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table TStructField */
  lazy val TStructField = new TableQuery(tag => new TStructField(tag))

  /** Entity class storing rows of table TTask
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param cType Database column c_type SqlType(int4)
   *  @param tServerId Database column t_server_id SqlType(int4)
   *  @param tNodeId Database column t_node_id SqlType(int4) */
  case class TTaskRow(id: Int, cType: Int, tServerId: Int, tNodeId: Int)
  /** GetResult implicit for fetching TTaskRow objects using plain SQL queries */
  implicit def GetResultTTaskRow(implicit e0: GR[Int]): GR[TTaskRow] = GR{
    prs => import prs._
    TTaskRow.tupled((<<[Int], <<[Int], <<[Int], <<[Int]))
  }
  /** Table description of table t_task. Objects of this class serve as prototypes for rows in queries. */
  class TTask(_tableTag: Tag) extends profile.api.Table[TTaskRow](_tableTag, Some("s_task"), "t_task") {
    def * = (id, cType, tServerId, tNodeId) <> (TTaskRow.tupled, TTaskRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(cType), Rep.Some(tServerId), Rep.Some(tNodeId))).shaped.<>({r=>import r._; _1.map(_=> TTaskRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column c_type SqlType(int4) */
    val cType: Rep[Int] = column[Int]("c_type")
    /** Database column t_server_id SqlType(int4) */
    val tServerId: Rep[Int] = column[Int]("t_server_id")
    /** Database column t_node_id SqlType(int4) */
    val tNodeId: Rep[Int] = column[Int]("t_node_id")

    /** Foreign key referencing TNode (database name t_node_fk) */
    lazy val tNodeFk = foreignKey("t_node_fk", tNodeId, TNode)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
    /** Foreign key referencing TServer (database name t_server_fk) */
    lazy val tServerFk = foreignKey("t_server_fk", tServerId, TServer)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table TTask */
  lazy val TTask = new TableQuery(tag => new TTask(tag))

  /** Entity class storing rows of table TTaskState
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param cType Database column c_type SqlType(int4)
   *  @param cTimestamp Database column c_timestamp SqlType(timestamp)
   *  @param cDescription Database column c_description SqlType(text), Default(None)
   *  @param tTaskId Database column t_task_id SqlType(int4) */
  case class TTaskStateRow(id: Int, cType: Int, cTimestamp: java.sql.Timestamp, cDescription: Option[String] = None, tTaskId: Int)
  /** GetResult implicit for fetching TTaskStateRow objects using plain SQL queries */
  implicit def GetResultTTaskStateRow(implicit e0: GR[Int], e1: GR[java.sql.Timestamp], e2: GR[Option[String]]): GR[TTaskStateRow] = GR{
    prs => import prs._
    TTaskStateRow.tupled((<<[Int], <<[Int], <<[java.sql.Timestamp], <<?[String], <<[Int]))
  }
  /** Table description of table t_task_state. Objects of this class serve as prototypes for rows in queries. */
  class TTaskState(_tableTag: Tag) extends profile.api.Table[TTaskStateRow](_tableTag, Some("s_task"), "t_task_state") {
    def * = (id, cType, cTimestamp, cDescription, tTaskId) <> (TTaskStateRow.tupled, TTaskStateRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(cType), Rep.Some(cTimestamp), cDescription, Rep.Some(tTaskId))).shaped.<>({r=>import r._; _1.map(_=> TTaskStateRow.tupled((_1.get, _2.get, _3.get, _4, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column c_type SqlType(int4) */
    val cType: Rep[Int] = column[Int]("c_type")
    /** Database column c_timestamp SqlType(timestamp) */
    val cTimestamp: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("c_timestamp")
    /** Database column c_description SqlType(text), Default(None) */
    val cDescription: Rep[Option[String]] = column[Option[String]]("c_description", O.Default(None))
    /** Database column t_task_id SqlType(int4) */
    val tTaskId: Rep[Int] = column[Int]("t_task_id")

    /** Foreign key referencing TTask (database name t_task_fk) */
    lazy val tTaskFk = foreignKey("t_task_fk", tTaskId, TTask)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table TTaskState */
  lazy val TTaskState = new TableQuery(tag => new TTaskState(tag))

  /** Entity class storing rows of table TVariable
   *  @param cIsHistorizing Database column c_is_historizing SqlType(bool)
   *  @param cMinimumSamplingInterval Database column c_minimum_sampling_interval SqlType(float8)
   *  @param cDefaultValue Database column c_default_value SqlType(json), Length(2147483647,false), Default(None)
   *  @param cAccessLevel Database column c_access_level SqlType(int4)
   *  @param tNodeId Database column t_node_id SqlType(int4), PrimaryKey
   *  @param tDatatypeId Database column t_datatype_id SqlType(int4) */
  case class TVariableRow(cIsHistorizing: Boolean, cMinimumSamplingInterval: Double, cDefaultValue: Option[String] = None, cAccessLevel: Int, tNodeId: Int, tDatatypeId: Int)
  /** GetResult implicit for fetching TVariableRow objects using plain SQL queries */
  implicit def GetResultTVariableRow(implicit e0: GR[Boolean], e1: GR[Double], e2: GR[Option[String]], e3: GR[Int]): GR[TVariableRow] = GR{
    prs => import prs._
    TVariableRow.tupled((<<[Boolean], <<[Double], <<?[String], <<[Int], <<[Int], <<[Int]))
  }
  /** Table description of table t_variable. Objects of this class serve as prototypes for rows in queries. */
  class TVariable(_tableTag: Tag) extends profile.api.Table[TVariableRow](_tableTag, Some("s_server"), "t_variable") {
    def * = (cIsHistorizing, cMinimumSamplingInterval, cDefaultValue, cAccessLevel, tNodeId, tDatatypeId) <> (TVariableRow.tupled, TVariableRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(cIsHistorizing), Rep.Some(cMinimumSamplingInterval), cDefaultValue, Rep.Some(cAccessLevel), Rep.Some(tNodeId), Rep.Some(tDatatypeId))).shaped.<>({r=>import r._; _1.map(_=> TVariableRow.tupled((_1.get, _2.get, _3, _4.get, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column c_is_historizing SqlType(bool) */
    val cIsHistorizing: Rep[Boolean] = column[Boolean]("c_is_historizing")
    /** Database column c_minimum_sampling_interval SqlType(float8) */
    val cMinimumSamplingInterval: Rep[Double] = column[Double]("c_minimum_sampling_interval")
    /** Database column c_default_value SqlType(json), Length(2147483647,false), Default(None) */
    val cDefaultValue: Rep[Option[String]] = column[Option[String]]("c_default_value", O.Length(2147483647,varying=false), O.Default(None))
    /** Database column c_access_level SqlType(int4) */
    val cAccessLevel: Rep[Int] = column[Int]("c_access_level")
    /** Database column t_node_id SqlType(int4), PrimaryKey */
    val tNodeId: Rep[Int] = column[Int]("t_node_id", O.PrimaryKey)
    /** Database column t_datatype_id SqlType(int4) */
    val tDatatypeId: Rep[Int] = column[Int]("t_datatype_id")

    /** Foreign key referencing TDatatype (database name t_datatype_fk) */
    lazy val tDatatypeFk = foreignKey("t_datatype_fk", tDatatypeId, TDatatype)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
    /** Foreign key referencing TNode (database name t_node_fk) */
    lazy val tNodeFk = foreignKey("t_node_fk", tNodeId, TNode)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  }
  /** Collection-like TableQuery object for table TVariable */
  lazy val TVariable = new TableQuery(tag => new TVariable(tag))

  /** Entity class storing rows of table TVariableValue
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param cSourceTimestamp Database column c_source_timestamp SqlType(timestamp)
   *  @param cTimestamp Database column c_timestamp SqlType(timestamp)
   *  @param cValue Database column c_value SqlType(json), Length(2147483647,false)
   *  @param tServerId Database column t_server_id SqlType(int4)
   *  @param tVariableId Database column t_variable_id SqlType(int4) */
  case class TVariableValueRow(id: Int, cSourceTimestamp: java.sql.Timestamp, cTimestamp: java.sql.Timestamp, cValue: String, tServerId: Int, tVariableId: Int)
  /** GetResult implicit for fetching TVariableValueRow objects using plain SQL queries */
  implicit def GetResultTVariableValueRow(implicit e0: GR[Int], e1: GR[java.sql.Timestamp], e2: GR[String]): GR[TVariableValueRow] = GR{
    prs => import prs._
    TVariableValueRow.tupled((<<[Int], <<[java.sql.Timestamp], <<[java.sql.Timestamp], <<[String], <<[Int], <<[Int]))
  }
  /** Table description of table t_variable_value. Objects of this class serve as prototypes for rows in queries. */
  class TVariableValue(_tableTag: Tag) extends profile.api.Table[TVariableValueRow](_tableTag, Some("s_server"), "t_variable_value") {
    def * = (id, cSourceTimestamp, cTimestamp, cValue, tServerId, tVariableId) <> (TVariableValueRow.tupled, TVariableValueRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(cSourceTimestamp), Rep.Some(cTimestamp), Rep.Some(cValue), Rep.Some(tServerId), Rep.Some(tVariableId))).shaped.<>({r=>import r._; _1.map(_=> TVariableValueRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column c_source_timestamp SqlType(timestamp) */
    val cSourceTimestamp: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("c_source_timestamp")
    /** Database column c_timestamp SqlType(timestamp) */
    val cTimestamp: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("c_timestamp")
    /** Database column c_value SqlType(json), Length(2147483647,false) */
    val cValue: Rep[String] = column[String]("c_value", O.Length(2147483647,varying=false))
    /** Database column t_server_id SqlType(int4) */
    val tServerId: Rep[Int] = column[Int]("t_server_id")
    /** Database column t_variable_id SqlType(int4) */
    val tVariableId: Rep[Int] = column[Int]("t_variable_id")

    /** Foreign key referencing TServer (database name t_server_fk) */
    lazy val tServerFk = foreignKey("t_server_fk", tServerId, TServer)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
    /** Foreign key referencing TVariable (database name t_variable_fk) */
    lazy val tVariableFk = foreignKey("t_variable_fk", tVariableId, TVariable)(r => r.tNodeId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table TVariableValue */
  lazy val TVariableValue = new TableQuery(tag => new TVariableValue(tag))
}

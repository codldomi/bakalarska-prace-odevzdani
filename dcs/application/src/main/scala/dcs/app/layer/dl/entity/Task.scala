package dcs.app.layer.dl.entity


trait Task {

  def id: Int

  def state: TaskState

  def serverId: Int

}

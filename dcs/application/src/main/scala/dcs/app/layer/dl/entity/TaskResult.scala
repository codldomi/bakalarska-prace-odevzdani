package dcs.app.layer.dl.entity


case class TaskResult(value: Option[VariableValue])


object TaskResult {

  def apply(value: VariableValue): TaskResult = new TaskResult(Some(value))

}
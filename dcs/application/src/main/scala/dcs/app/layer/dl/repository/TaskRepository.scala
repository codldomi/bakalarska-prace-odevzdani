package dcs.app.layer.dl.repository

import dcs.app.layer.dl.entity.{Task, TaskResult, TaskState, UpdateTask}


/** Encapsulates the logic required to access data source - Task and its state and result.
  */
trait TaskRepository {

  /** Creates UpdateTask with the selected data.
    *
    * @param task Data.
    * @return Created UpdateTask with assigned identifiers.
    */
  def createUpdateTask(task: UpdateTask): UpdateTask

  /** Creates TaskResult with the selected data.
    *
    * @param taskId Task identifier.
    * @param nodeId Node identifier.
    * @param serverId Server identifier.
    * @param result Data.
    * @return Created TaskResult with assigned identifiers.
    */
  def createTaskResult(taskId: Int, nodeId: Int, serverId: Int, result: TaskResult): TaskResult

  /** Creates TaskState with the selected data.
    *
    * @param taskId Task identifier.
    * @param state Data.
    * @return Created TaskState with assigned identifiers.
    */
  def createTaskState(taskId: Int, state: TaskState): TaskState

  /** Reads Task from the data source.
    *
    * @param id Task identifier.
    * @return Read Task.
    */
  def read(id: Int): Task

}

package dcs.app.layer.dl.repository.impl

import dcs.app.layer.dl.db.Tables
import dcs.app.layer.dl.repository.IdRepository

import scopcua.data.UaId
import scopcua.data.value.UaString
import scopcua.data.value.number.integer.UaUInteger.UaUInt32

import slick.jdbc.JdbcProfile

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt


/** IdRepository implemented via Slick library.
  *
  * @param profile Database Driver.
  * @param db Database.
  * @param waitTime A maximum time interval for waiting on a response from a database.
  */
class SlickIdRepository(
  val profile: JdbcProfile,
  val db: JdbcProfile#Backend#Database,
  val waitTime: Int
) extends IdRepository  {

  import profile.api._
  import Tables._

  /** Synchronously executes an action.
    */
  def exec[T](action: DBIO[T]): T = Await.result(db.run(action), waitTime.seconds)

  /** Selects all ids with their namespaces.
    */
  private def ids = for {
    (i, n) <- TId join TNamespace on (_.tNamespaceId === _.id)
  } yield (i.id, i.cIndex, n.cUri)

  /** Reads OPC UA identifier.
    *
    * @param id Entity's identifier.
    * @return OPC UA identifier.
    */
  override def read(id: Int): UaId = {

    val action =
      ids
        .filter { case(cId, _, _) => cId === id }
        .result
        .head
        .map { case (_, index, uri) => UaId(UaUInt32(index), UaString(uri)) }

    exec(action)
  }

  /** Reads Entity identifier.
    *
    * @param id OPC UA identifier.
    * @return Entity identifier.
    */
  override def readId(id: UaId): Int = {

    val action =
      ids
        .filter { case (_, cIndex, _) => cIndex === id.index.value.toInt }
        .filter { case (_, _, cUri) => cUri === id.namespaceUri.value }
        .result
        .head
        .map { case (dbId, _, _) => dbId }

    exec(action)
  }

}

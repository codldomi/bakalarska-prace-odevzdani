package dcs.app.layer.dl.entity


case class UpdateTask(
  id: Int,
  state: TaskState,
  serverId: Int,
  nodeId: Int,
  result: Option[TaskResult]
) extends Task

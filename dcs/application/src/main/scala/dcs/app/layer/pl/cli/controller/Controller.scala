package dcs.app.layer.pl.cli.controller

import dcs.app.layer.dl.entity.{Task, UpdateTask}

import scala.util.Try


/** Provides reactions to user interaction.
  */
trait Controller {

  /** Processes new UpdateTask.
    *
    * @param serverId Server's identifier.
    * @param nodeId Node's identifier.
    * @return The current state of the newly created UpdateTask.
    */
  def processUpdateTask(serverId: Int, nodeId: Int): Try[UpdateTask]

  /** Reads information of the selected task.
    *
    * @param id Task's identifier.
    * @return The selected task.
    */
  def readTask(id: Int): Try[Task]

}

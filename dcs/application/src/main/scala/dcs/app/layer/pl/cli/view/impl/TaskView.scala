package dcs.app.layer.pl.cli.view.impl

import dcs.app.layer.dl.entity.Task
import dcs.app.layer.pl.cli.controller.Controller
import dcs.app.layer.pl.cli.model.UpdateTaskCreation
import dcs.app.layer.pl.cli.view.View

import net.liftweb.json.parse
import net.liftweb.json.{DefaultFormats, Serialization}

import scala.io.StdIn
import scala.util.{Failure, Success}


/** Provides the CLI application entry point.
  *
  * @param controller Controller for this view.
  */
class TaskView(val controller: Controller) extends View {

  /** Starts the program cycle.
    */
  override def run(): Unit = {

    println("Write Your command.")

    var isRunning = true

    while (isRunning) {

      val args =
        StdIn
          .readLine()
          .split("\\s+")
          .toList

      args match {
        case "create" :: "update" :: tail =>
          processUpdateTask(tail.mkString(" "))
        case "read" :: data :: Nil =>
          processReadInfoTask(data)
        case "stop" :: Nil =>
          processStop()
          isRunning = false
        case "help" :: Nil =>
          processHelp()
        case _ =>
          processUnknown()
      }
    }
  }


  private def processUpdateTask(json: String): Unit = {

    implicit val formats: DefaultFormats.type = DefaultFormats

    val data = try {
      parse(json).extract[UpdateTaskCreation]
    } catch {
        case _: Throwable =>
          processUnknown()
          return
    }

    val result = controller.processUpdateTask(data.serverId, data.nodeId)

    result match {
      case Failure(exception) => println(s"Creating of Update ask ended with exception message: ${exception.getMessage}")
      case Success(value) => printTask(value)
    }
  }


  private def processReadInfoTask(textualId: String): Unit = {

    val id = textualId.toInt
    val result = controller.readTask(id)

    result match {
      case Failure(exception) => println(s"Reading of Task ended with exception message: ${exception.getMessage}")
      case Success(value) => printTask(value)
    }
  }


  private def printTask(task: Task): Unit = {

    implicit val formats: DefaultFormats.type = DefaultFormats

    val json = Serialization.writePretty(task)
    val text =
      s"""result =
         |$json
         |""".stripMargin

    print(text)
  }


  private def processStop(): Unit = println(s"Stopping the application.")


  private def processHelp(): Unit = {

    val text =
      s"""Data Collection Application
         |
         |Commands:
         |
         |”help“ – shows this help page
         |"stop" - stops the program
         |"read [id]" - shows information about the selected task
         |"create update { "serverId" : [number], "nodeId" : [number] }" - creates UpdateTask and processes it
         |""".stripMargin

    print(text)
  }


  private def processUnknown(): Unit = println("The command is unknown. Use \"help\" to show all supported commands.")


}

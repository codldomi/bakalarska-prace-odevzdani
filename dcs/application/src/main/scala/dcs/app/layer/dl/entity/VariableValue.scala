package dcs.app.layer.dl.entity

import java.sql.Timestamp


case class VariableValue(
  id: Int,
  sourceTimestamp: Timestamp,
  timestamp: Timestamp,
  value: String
)

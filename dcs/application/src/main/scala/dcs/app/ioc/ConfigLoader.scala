package dcs.app.ioc

import dcs.app.ioc.model.{DbSettingsConf, UaResourceConf}

import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile


trait ConfigLoader {

  def loadDatabase(): DatabaseConfig[JdbcProfile]

  def loadDatabaseSettings(): DbSettingsConf

  def loadUaResource(): UaResourceConf

}

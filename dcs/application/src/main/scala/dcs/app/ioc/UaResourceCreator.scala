package dcs.app.ioc

import dcs.app.ioc.model.UaResourceConf
import dcs.app.layer.bl.service.impl.util.resource.impl.util.UaResource


/** Provides creating of OPC UA resources.
  */
trait UaResourceCreator {

  /** Creates mapping serverId -> resource from the OPC UA resource configuration.
    *
    * @param config OPC UA resource configuration.
    * @return Mapping serverId -> resource.
    */
  def create(config: UaResourceConf): Map[Int, UaResource]

}

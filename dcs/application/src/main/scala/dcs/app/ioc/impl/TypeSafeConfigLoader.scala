package dcs.app.ioc.impl

import dcs.app.ioc.ConfigLoader
import dcs.app.ioc.model.{DbSettingsConf, UaResourceConf}

import pureconfig.generic.ProductHint
import pureconfig._
import pureconfig.generic.auto._
import pureconfig.generic.semiauto._

import scopcua.data.security.UaSecurityPolicy

import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile


class TypeSafeConfigLoader(file: String) extends ConfigLoader {

  private val _source = ConfigSource.resources(file)

  private implicit def hint[A]: ProductHint[A] = ProductHint[A](ConfigFieldMapping(CamelCase, CamelCase))

  private implicit val securityConfig: ConfigReader[UaSecurityPolicy] = deriveEnumerationReader[UaSecurityPolicy](ConfigFieldMapping(PascalCase, PascalCase))


  def loadDatabase(): DatabaseConfig[JdbcProfile] = {

    DatabaseConfig.forConfig[JdbcProfile]("dbConnection")
  }


  def loadDatabaseSettings(): DbSettingsConf = {

    _source
      .at("dbSettings")
      .loadOrThrow[DbSettingsConf]
  }


  def loadUaResource(): UaResourceConf = {

    _source
      .at("uaResource")
      .loadOrThrow[UaResourceConf]
  }

}

package dcs.app.ioc.impl

import dcs.app.ioc.UaResourceCreator
import dcs.app.ioc.model.{UaResourceConf, UaSecurityConf, UaServerConf}
import dcs.app.layer.bl.service.impl.util.resource.impl.util.UaResource

import scopcua.data.security.UaSecurityConfig
import scopcua.data.{UaDataType, UaId}
import scopcua.sdk.client.UaClient
import scopcua.sdk.client.impl.UaMiloClient
import scopcua.util.binarydecoder.UaBinaryDecoder
import scopcua.util.binarydecoder.impl.UaStdBinaryDecoder
import scopcua.util.datatypeparser.impl.UaDataTypeParser1v04
import scopcua.util.jsonencoder.UaJsonEncoder
import scopcua.util.jsonencoder.impl.UaSmplJsonEncoder
import scopcua.util.statuscodeparser.impl.UaCsvStatusCodeParser

import java.security.cert.X509Certificate
import java.security.{KeyPair, KeyStore, PrivateKey}

import scala.io.Source


/** Implements UaResourceCreator for the OPC UA of version 1.04.
  */
class UaResourceCreator1V04() extends UaResourceCreator {

  /** Creates mapping serverId -> resource from the OPC UA resource configuration.
    *
    * @param config OPC UA resource configuration.
    * @return Mapping serverId -> resource.
    */
  override def create(config: UaResourceConf): Map[Int, UaResource] = {

    config
      .servers
      .foldLeft(Map.empty[Int, UaResource])((result, server) => {

        val decoder = createBinaryDecoder(server)
        val client = server.security match {
          case None => UaMiloClient.from(server.url, decoder)
          case Some(security) => createSecureClient(server, security, decoder)
        }

        val encoder = createJsonEncoder(client)
        val resource = new UaResource(client, encoder)

        result + (server.id -> resource)
      })
  }


  private def createJsonEncoder(client: UaClient): UaJsonEncoder = {

    client.connect()
    val namespaces = client.readNamespaces()
    client.disconnect()

    new UaSmplJsonEncoder(namespaces)
  }


  private def createBinaryDecoder(server: UaServerConf): UaBinaryDecoder = {

    val dataTypeParser = new UaDataTypeParser1v04()
    val statusCodeParser = new UaCsvStatusCodeParser()

    val types =
      server
        .models
        .foldLeft(Map.empty[UaId, UaDataType])((result, model) => {

          val text = Source.fromResource(model).mkString
          val dataTypes = dataTypeParser.parse(text)

          result ++ dataTypes
        })

    val text = Source.fromResource(server.statusCodes).mkString
    val statusCodes = statusCodeParser.parse(text)

    new UaStdBinaryDecoder(types ++ UaDataType.builtIns, statusCodes)
  }


  private def createSecureClient(server: UaServerConf, security: UaSecurityConf, decoder: UaBinaryDecoder): UaClient = {

    val keyStore = KeyStore.getInstance("PKCS12")
    val is = getClass.getClassLoader.getResourceAsStream(security.keyStore)
    keyStore.load(is, security.keyStorePassword.toCharArray)

    val certificate =
      keyStore
        .getCertificate(security.alias)
        .asInstanceOf[X509Certificate]

    val chain =
      keyStore
        .getCertificateChain(security.alias)
        .map(_.asInstanceOf[X509Certificate])

    val clientPrivateKey = keyStore.getKey(security.alias, security.keyStorePassword.toCharArray)
    val serverPublicKey = certificate.getPublicKey
    val keyPair = new KeyPair(serverPublicKey, clientPrivateKey.asInstanceOf[PrivateKey])

    val config =
      UaSecurityConfig(
        security.username,
        security.password,
        security.appName,
        security.appUri,
        keyPair,
        certificate,
        chain,
        security.policy,
        security.trustDir)

    UaMiloClient.from(server.url, config, decoder)
  }

}

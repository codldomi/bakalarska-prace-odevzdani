package dcs.app.ioc.model

import scopcua.data.security.UaSecurityPolicy


case class UaSecurityConf(
  username: String,
  password: String,
  appName: String,
  appUri: String,
  alias: String,
  policy: UaSecurityPolicy,
  serverCert: String,
  trustDir: String,
  keyStorePassword: String,
  keyStore: String,
  timeout: Int
)

package dcs.app.ioc.model


case class UaServerConf(
  id: Int,
  url: String,
  security: Option[UaSecurityConf],
  models: Seq[String],
  statusCodes: String
)

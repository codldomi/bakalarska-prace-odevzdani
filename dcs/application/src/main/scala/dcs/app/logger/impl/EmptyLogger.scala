package dcs.app.logger.impl

import dcs.app.layer.dl.entity.Task
import dcs.app.logger.Logger


object EmptyLogger extends Logger {

  override def logTask(task: Task): Unit = {}

}

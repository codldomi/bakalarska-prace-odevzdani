package dcs.app.logger

import dcs.app.layer.dl.entity.Task


trait Logger {

  def logTask(task: Task): Unit

}

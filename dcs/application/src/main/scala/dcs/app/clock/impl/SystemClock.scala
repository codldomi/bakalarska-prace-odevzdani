package dcs.app.clock.impl

import dcs.app.clock.Clock

import java.sql.Timestamp
import java.time.Instant


/** Provides System's date and time.
  */
object SystemClock extends Clock {

  override def now: Timestamp = Timestamp.from(Instant.now())

}

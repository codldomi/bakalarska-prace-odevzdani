package dcs.app.clock

import java.sql.Timestamp


/** Provides date and time.
  */
trait Clock {

  def now: Timestamp

}

package dcs.app

import dcs.app.clock.impl.SystemClock
import dcs.app.ioc.impl.{TypeSafeConfigLoader, UaResourceCreator1V04}
import dcs.app.layer.bl.service.impl.SyncTaskService
import dcs.app.layer.bl.service.impl.util.resource.impl.UnsafeResourceManager
import dcs.app.layer.dl.repository.impl.{SlickIdRepository, SlickTaskRepository}
import dcs.app.layer.pl.cli.controller.impl.TaskController
import dcs.app.layer.pl.cli.view.impl.TaskView
import dcs.app.logger.impl.EmptyLogger
import dcs.app.layer.dl.db.Tables

import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import java.sql.Timestamp
import java.time.Instant

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global


object Main {


  private def codegen(): Unit = {

    val outputDir = "./target/generatedSources/postgres"
    val url = "jdbc:postgresql://localhost:5432/dcs_database"
    val jdbcDriver = "org.postgresql.Driver"
    val slickDriver = "slick.jdbc.PostgresProfile"
    val pkg = "dcs.app.layers.dl.db"
    val user = "postgres"
    val password = ""

    val args = Array(slickDriver, jdbcDriver, url, outputDir, pkg, user, password)

    slick.codegen.SourceCodeGenerator.main(args)
  }


  def main(args: Array[String]): Unit = presentation(program(args))


  private def program(args: Array[String]): Unit = {

    //codegen()

    //  Loads configuration

    val configLoader = new TypeSafeConfigLoader("application.conf")

    val dbConfig = configLoader.loadDatabase()
    val dbSettings = configLoader.loadDatabaseSettings()
    val uaResource = configLoader.loadUaResource()

    //  Creates persistence layer

    val taskRepo = new SlickTaskRepository(dbConfig.profile, dbConfig.db, dbSettings.waitTime)
    val idRepo = new SlickIdRepository(dbConfig.profile, dbConfig.db, dbSettings.waitTime)

    //  Creates business layer

    val clientCreator = new UaResourceCreator1V04()
    val resources = clientCreator.create(uaResource)
    val resourceManager = new UnsafeResourceManager(resources)

    val service = new SyncTaskService(taskRepo, idRepo, resourceManager, EmptyLogger, SystemClock)

    //  Creates presentation layer

    val controller = new TaskController(taskRepo, service, SystemClock)
    val view = new TaskView(controller)

    //  Runs application

    view.run()
  }


  /** Presents the current state of the development. Provides test data.
    */
  private def presentation(program: => Unit): Unit = {

    println("The presentation of application.")

    val config = DatabaseConfig.forConfig[JdbcProfile]("dbConnection")
    val profile: JdbcProfile = config.profile
    val db = config.db

    import profile.api._
    import Tables._

    /** The current time.
      */
    def now = Timestamp.from(Instant.now())

    /** Synchronously executes an action.
      */
    def exec[T](action: DBIO[T]): T = Await.result(db.run(action), 2.seconds)

    def cleanup(): Unit =
      exec(
        TTaskState.delete
        andThen TResult.delete
        andThen TTask.delete
        andThen TVariableValue.delete
        andThen TVariable.delete
        andThen TDatatype.delete
        andThen TNode.delete
        andThen TId.delete
        andThen TNamespace.delete
        andThen TServer.delete
        andThen TObjectTree.delete)

    // initializes the test database

    cleanup()

    val action = for {

      objectTreeId <- {
        val row = TObjectTreeRow(id = 0, cAlias = "ObjectTree", cTimestamp = now)
        val objectTrees = TableQuery[TObjectTree]
        objectTrees returning objectTrees.map(_.id) += row
      }

      serverId <- {
        val row = TServerRow(id = 0, cAlias = "TEST localhost OPC UA server", cUrl = "opc.tcp://127.0.0.1:4840", tObjectTreeId = objectTreeId)
        val servers = TableQuery[TServer]
        servers returning  servers.map(_.id) += row
      }

      namespaceId <- {
        val row = TNamespaceRow(id = 0, cUri = "http://www.modemtec.cz/PD/", cTimestamp = now)
        val namespaces = TableQuery[TNamespace]
        namespaces returning  namespaces.map(_.id) += row
      }

      nodeId <- {
        val row = TIdRow(id = 0, cIndex = 6027, tNamespaceId = namespaceId)
        val ids = TableQuery[TId]
        ids returning  ids.map(_.id) += row
      }

      dataTypeId <- {
        val row = TIdRow(id = 0, cIndex = 3003, tNamespaceId = namespaceId)
        val ids = TableQuery[TId]
        ids returning ids.map(_.id) += row
      }

      _ <- {
        val row =
          TNodeRow(
            cBrowseName = "BrowseName",
            cDisplayName = "DisplayName",
            cDescription = None,
            cType = 1,
            tNodeId = None,
            tObjectTreeId = objectTreeId,
            id = nodeId)
        TableQuery[TNode] += row
      }

      _ <- {
        val row = TDatatypeRow(cIsAbstract = false, cType = 1, cName = "MtGridFreq", tDatatypeId = None, id = dataTypeId)
        TableQuery[TDatatype] += row
      }

      _ <- {
        val row =
          TVariableRow(
            cIsHistorizing = true,
            cMinimumSamplingInterval = 0,
            cDefaultValue = None,
            cAccessLevel = 2,
            tNodeId = nodeId,
            tDatatypeId = dataTypeId)
        TableQuery[TVariable] += row
      }

    } yield (serverId, nodeId)

    val (serverId, nodeId) = exec(action)

    println(
      s"""
         |The test database has been initialized.
         |Test data: Server ID = $serverId, Node ID = $nodeId.
         |The running OPC UA server is from the project Test Server.
         |""".stripMargin)

    // runs the program

    println("The program is starting...")

    program

    cleanup()

    println("The test database has been cleaned up.")
  }

}

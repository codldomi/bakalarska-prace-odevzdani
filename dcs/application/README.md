## Data Collection Application

verze 0.1.0

---

### Jazyk

- [Scala](https://www.scala-lang.org/) 2.13.7
- konvence dle [Scala Style Guide](https://docs.scala-lang.org/style/)

### Vývojové prostředí

- [IntelliJ IDEA](https://www.jetbrains.com/idea/) (Community nebo Ultimate) s [pluginem Scala](https://www.jetbrains.com/help/idea/discover-intellij-idea-for-scala.html)

### Použité knihovny

- [Scalable OPC UA](http://gitlab.intranet.modemtec.cz/products/server/pdds/scalable-opc-ua)
- [ScalaMock](https://scalamock.org/)
- [ScalaTest](https://www.scalatest.org/)
- [Lift-JSON](https://github.com/lift/lift/tree/master/framework/lift-base/lift-json)
- [PureConfig](https://pureconfig.github.io/)
- [Slick](https://scala-slick.org/)

### Zpracování požadavků

- uživatelské rozhraní skrze Command Line Interface (pro testovací účely)
- synchronní zpracování požadavků:
  - _UpdateTask_ - vyčte hodnotu proměnné ze zvoleného OPC UA serveru a uloží do databáze
  - _ReadInfoTask_ - zobrazí informace o zvoleném úkolu

### Běh aplikace

- podporuje příkazy:
  - ”help“ – zobrazí nápovědu.
  - ”stop“ – zastaví program.
  - ”read [id]“ – zobrazí informace o Task se zvoleným identifikátorem.
  - ”create update { "serverId" : [number], "nodeId" : [number] }“ – vytvoří UpdateTask pro zvolený server a uzel na základě dat ve formátu JSON. Ten obsahuje JSON Object s atributy nodeId a serverId. Nakonec zobrazí informace o vytvořeném Task.
- zobrazené úkoly jsou ve formátu JSON 
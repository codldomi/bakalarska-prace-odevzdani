
lazy val root = (project in file("."))
  .settings(
    name := "DataCollectionApp",
    scalaVersion := "2.13.8",
    version := "0.1",
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.11" % "test",
      "org.scalamock" %% "scalamock" % "5.2.0" % "test",
      "net.liftweb" %% "lift-json" % "3.5.0",
      "com.typesafe.slick" %% "slick" % "3.3.3",
      "com.typesafe.slick" %% "slick-codegen" % "3.3.3",
      "org.slf4j" % "slf4j-nop" % "1.7.36",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
      "org.postgresql" % "postgresql" % "42.3.3",
      "com.github.pureconfig" %% "pureconfig" % "0.17.1"
    ))
  .dependsOn(
      RootProject(
          uri(s"ssh://git@gitlab.intranet.modemtec.cz/products/server/pdds/scalable-opc-ua.git#78c483894435901490c3e86c7b337f8ee4744443")))
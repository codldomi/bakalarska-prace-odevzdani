## Testovací Server

verze 0.1.0

Jedná se o OPC UA server vytvořený pomocný knihovny [open62541](https://open62541.org/), který umožňuje projektům z DCS testování na běžícím serveru.

---

### Použité knihovny

- [open62541](https://open62541.org/)
- [OPC UA Nodes Extension](http://gitlab.intranet.modemtec.cz/libraries/pc/opcua_nodes_extension)

---

### Vývojové prostředí

- [CLion](https://www.jetbrains.com/clion/)

---

### Nástroje pro OPC UA

- [UaModeler](https://www.unified-automation.com/products/development-tools/uamodeler.html) - nástroj pro modelování informačních modelů a jejich export v NodeSet 2 XML
- [UaExpert](https://www.unified-automation.com/products/development-tools/uaexpert.html) - nástroj, resp. klient, pro připojování a průzkum OPC UA serverů.


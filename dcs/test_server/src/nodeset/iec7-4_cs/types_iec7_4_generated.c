/* Generated from Opc.Ua.IEC61850-7-4.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py
 * on host martin-dev4 by user martin at 2021-11-29 10:23:37 */

#include "types_iec7_4_generated.h"

/* BehaviourModeKind */
#define BehaviourModeKind_members NULL

/* HealthKind */
#define HealthKind_members NULL
const UA_DataType UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_COUNT] = {
/* BehaviourModeKind */
{
    {3, UA_NODEIDTYPE_NUMERIC, {5LU}}, /* .typeId */
    {3, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_BehaviourModeKind), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    BehaviourModeKind_members  /* .members */
    UA_TYPENAME("BehaviourModeKind") /* .typeName */
},
/* HealthKind */
{
    {3, UA_NODEIDTYPE_NUMERIC, {25LU}}, /* .typeId */
    {3, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_HealthKind), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    HealthKind_members  /* .members */
    UA_TYPENAME("HealthKind") /* .typeName */
},
};


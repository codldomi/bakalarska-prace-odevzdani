/* Generated from Opc.Ua.IEC61850-7-4.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py * on host martin-dev4 by user martin at 2021-11-29 10:23:37 */

#ifndef TYPES_IEC7_4_GENERATED_H_
#define TYPES_IEC7_4_GENERATED_H_

#ifdef UA_ENABLE_AMALGAMATION
#include "open62541.h"
#else
#include <open62541/types.h>
#include <open62541/types_generated.h>

#endif

_UA_BEGIN_DECLS


/**
 * Every type is assigned an index in an array containing the type descriptions.
 * These descriptions are used during type handling (copying, deletion,
 * binary encoding, ...). */
#define UA_TYPES_IEC7_4_COUNT 2
extern UA_EXPORT const UA_DataType UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_COUNT];

/**
 * BehaviourModeKind
 * ^^^^^^^^^^^^^^^^^
 */
typedef enum {
    UA_BEHAVIOURMODEKIND_ON = 1,
    UA_BEHAVIOURMODEKIND_BLOCKED = 2,
    UA_BEHAVIOURMODEKIND_TEST = 3,
    UA_BEHAVIOURMODEKIND_TESTBLOCKED = 4,
    UA_BEHAVIOURMODEKIND_OFF = 5,
    __UA_BEHAVIOURMODEKIND_FORCE32BIT = 0x7fffffff
} UA_BehaviourModeKind;
UA_STATIC_ASSERT(sizeof(UA_BehaviourModeKind) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_TYPES_IEC7_4_BEHAVIOURMODEKIND 0

/**
 * HealthKind
 * ^^^^^^^^^^
 */
typedef enum {
    UA_HEALTHKIND_OK = 1,
    UA_HEALTHKIND_WARNING = 2,
    UA_HEALTHKIND_ALARM = 3,
    __UA_HEALTHKIND_FORCE32BIT = 0x7fffffff
} UA_HealthKind;
UA_STATIC_ASSERT(sizeof(UA_HealthKind) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_TYPES_IEC7_4_HEALTHKIND 1


_UA_END_DECLS

#endif /* TYPES_IEC7_4_GENERATED_H_ */

/* Generated from Opc.Ua.IEC61850-7-4.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py
 * on host martin-dev4 by user martin at 2021-11-29 10:23:37 */

#ifndef TYPES_IEC7_4_GENERATED_ENCODING_BINARY_H_
#define TYPES_IEC7_4_GENERATED_ENCODING_BINARY_H_

#ifdef UA_ENABLE_AMALGAMATION
# include "open62541.h"
#else
# include "ua_types_encoding_binary.h"
# include "types_iec7_4_generated.h"
#endif



/* BehaviourModeKind */
static UA_INLINE size_t
UA_BehaviourModeKind_calcSizeBinary(const UA_BehaviourModeKind *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_BEHAVIOURMODEKIND]);
}
static UA_INLINE UA_StatusCode
UA_BehaviourModeKind_encodeBinary(const UA_BehaviourModeKind *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_BEHAVIOURMODEKIND], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_BehaviourModeKind_decodeBinary(const UA_ByteString *src, size_t *offset, UA_BehaviourModeKind *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_BEHAVIOURMODEKIND], NULL);
}

/* HealthKind */
static UA_INLINE size_t
UA_HealthKind_calcSizeBinary(const UA_HealthKind *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_HEALTHKIND]);
}
static UA_INLINE UA_StatusCode
UA_HealthKind_encodeBinary(const UA_HealthKind *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_HEALTHKIND], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_HealthKind_decodeBinary(const UA_ByteString *src, size_t *offset, UA_HealthKind *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_HEALTHKIND], NULL);
}

#endif /* TYPES_IEC7_4_GENERATED_ENCODING_BINARY_H_ */

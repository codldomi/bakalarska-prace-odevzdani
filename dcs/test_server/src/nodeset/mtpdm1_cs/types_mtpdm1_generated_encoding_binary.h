/* Generated from ModemTec.PDM1.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py
 * on host martin-dev4 by user martin at 2021-11-29 10:24:00 */

#ifndef TYPES_MTPDM1_GENERATED_ENCODING_BINARY_H_
#define TYPES_MTPDM1_GENERATED_ENCODING_BINARY_H_

#ifdef UA_ENABLE_AMALGAMATION
# include "open62541.h"
#else
# include "ua_types_encoding_binary.h"
# include "types_mtpdm1_generated.h"
#endif



/* MtGridFreq */
static UA_INLINE size_t
UA_MtGridFreq_calcSizeBinary(const UA_MtGridFreq *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTGRIDFREQ]);
}
static UA_INLINE UA_StatusCode
UA_MtGridFreq_encodeBinary(const UA_MtGridFreq *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTGRIDFREQ], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_MtGridFreq_decodeBinary(const UA_ByteString *src, size_t *offset, UA_MtGridFreq *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTGRIDFREQ], NULL);
}

/* MtLogLevel */
static UA_INLINE size_t
UA_MtLogLevel_calcSizeBinary(const UA_MtLogLevel *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTLOGLEVEL]);
}
static UA_INLINE UA_StatusCode
UA_MtLogLevel_encodeBinary(const UA_MtLogLevel *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTLOGLEVEL], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_MtLogLevel_decodeBinary(const UA_ByteString *src, size_t *offset, UA_MtLogLevel *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTLOGLEVEL], NULL);
}

/* MtMeasureStatus */
static UA_INLINE size_t
UA_MtMeasureStatus_calcSizeBinary(const UA_MtMeasureStatus *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTMEASURESTATUS]);
}
static UA_INLINE UA_StatusCode
UA_MtMeasureStatus_encodeBinary(const UA_MtMeasureStatus *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTMEASURESTATUS], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_MtMeasureStatus_decodeBinary(const UA_ByteString *src, size_t *offset, UA_MtMeasureStatus *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTMEASURESTATUS], NULL);
}

/* MtRequestResult */
static UA_INLINE size_t
UA_MtRequestResult_calcSizeBinary(const UA_MtRequestResult *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT]);
}
static UA_INLINE UA_StatusCode
UA_MtRequestResult_encodeBinary(const UA_MtRequestResult *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_MtRequestResult_decodeBinary(const UA_ByteString *src, size_t *offset, UA_MtRequestResult *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT], NULL);
}

#endif /* TYPES_MTPDM1_GENERATED_ENCODING_BINARY_H_ */

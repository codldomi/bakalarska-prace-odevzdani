/* Generated from ModemTec.PDM1.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py
 * on host martin-dev4 by user martin at 2021-11-29 10:24:00 */

#ifndef TYPES_MTPDM1_GENERATED_HANDLING_H_
#define TYPES_MTPDM1_GENERATED_HANDLING_H_

#include "types_mtpdm1_generated.h"

_UA_BEGIN_DECLS

#if defined(__GNUC__) && __GNUC__ >= 4 && __GNUC_MINOR__ >= 6
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wmissing-field-initializers"
# pragma GCC diagnostic ignored "-Wmissing-braces"
#endif


/* MtGridFreq */
static UA_INLINE void
UA_MtGridFreq_init(UA_MtGridFreq *p) {
    memset(p, 0, sizeof(UA_MtGridFreq));
}

static UA_INLINE UA_MtGridFreq *
UA_MtGridFreq_new(void) {
    return (UA_MtGridFreq*)UA_new(&UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTGRIDFREQ]);
}

static UA_INLINE UA_StatusCode
UA_MtGridFreq_copy(const UA_MtGridFreq *src, UA_MtGridFreq *dst) {
    return UA_copy(src, dst, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTGRIDFREQ]);
}

UA_DEPRECATED static UA_INLINE void
UA_MtGridFreq_deleteMembers(UA_MtGridFreq *p) {
    UA_clear(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTGRIDFREQ]);
}

static UA_INLINE void
UA_MtGridFreq_clear(UA_MtGridFreq *p) {
    UA_clear(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTGRIDFREQ]);
}

static UA_INLINE void
UA_MtGridFreq_delete(UA_MtGridFreq *p) {
    UA_delete(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTGRIDFREQ]);
}

/* MtLogLevel */
static UA_INLINE void
UA_MtLogLevel_init(UA_MtLogLevel *p) {
    memset(p, 0, sizeof(UA_MtLogLevel));
}

static UA_INLINE UA_MtLogLevel *
UA_MtLogLevel_new(void) {
    return (UA_MtLogLevel*)UA_new(&UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTLOGLEVEL]);
}

static UA_INLINE UA_StatusCode
UA_MtLogLevel_copy(const UA_MtLogLevel *src, UA_MtLogLevel *dst) {
    return UA_copy(src, dst, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTLOGLEVEL]);
}

UA_DEPRECATED static UA_INLINE void
UA_MtLogLevel_deleteMembers(UA_MtLogLevel *p) {
    UA_clear(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTLOGLEVEL]);
}

static UA_INLINE void
UA_MtLogLevel_clear(UA_MtLogLevel *p) {
    UA_clear(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTLOGLEVEL]);
}

static UA_INLINE void
UA_MtLogLevel_delete(UA_MtLogLevel *p) {
    UA_delete(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTLOGLEVEL]);
}

/* MtMeasureStatus */
static UA_INLINE void
UA_MtMeasureStatus_init(UA_MtMeasureStatus *p) {
    memset(p, 0, sizeof(UA_MtMeasureStatus));
}

static UA_INLINE UA_MtMeasureStatus *
UA_MtMeasureStatus_new(void) {
    return (UA_MtMeasureStatus*)UA_new(&UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTMEASURESTATUS]);
}

static UA_INLINE UA_StatusCode
UA_MtMeasureStatus_copy(const UA_MtMeasureStatus *src, UA_MtMeasureStatus *dst) {
    return UA_copy(src, dst, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTMEASURESTATUS]);
}

UA_DEPRECATED static UA_INLINE void
UA_MtMeasureStatus_deleteMembers(UA_MtMeasureStatus *p) {
    UA_clear(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTMEASURESTATUS]);
}

static UA_INLINE void
UA_MtMeasureStatus_clear(UA_MtMeasureStatus *p) {
    UA_clear(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTMEASURESTATUS]);
}

static UA_INLINE void
UA_MtMeasureStatus_delete(UA_MtMeasureStatus *p) {
    UA_delete(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTMEASURESTATUS]);
}

/* MtRequestResult */
static UA_INLINE void
UA_MtRequestResult_init(UA_MtRequestResult *p) {
    memset(p, 0, sizeof(UA_MtRequestResult));
}

static UA_INLINE UA_MtRequestResult *
UA_MtRequestResult_new(void) {
    return (UA_MtRequestResult*)UA_new(&UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT]);
}

static UA_INLINE UA_StatusCode
UA_MtRequestResult_copy(const UA_MtRequestResult *src, UA_MtRequestResult *dst) {
    return UA_copy(src, dst, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT]);
}

UA_DEPRECATED static UA_INLINE void
UA_MtRequestResult_deleteMembers(UA_MtRequestResult *p) {
    UA_clear(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT]);
}

static UA_INLINE void
UA_MtRequestResult_clear(UA_MtRequestResult *p) {
    UA_clear(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT]);
}

static UA_INLINE void
UA_MtRequestResult_delete(UA_MtRequestResult *p) {
    UA_delete(p, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT]);
}

#if defined(__GNUC__) && __GNUC__ >= 4 && __GNUC_MINOR__ >= 6
# pragma GCC diagnostic pop
#endif

_UA_END_DECLS

#endif /* TYPES_MTPDM1_GENERATED_HANDLING_H_ */

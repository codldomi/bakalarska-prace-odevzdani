/* WARNING: This is a generated file.
 * Any manual changes will be overwritten. */

#include "namespace_mtpdm1_generated.h"


/* MtRequestResult - ns=3;i=3002 */

static UA_StatusCode function_namespace_mtpdm1_generated_0_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_DataTypeAttributes attr = UA_DataTypeAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "MtRequestResult");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_DATATYPE,
UA_NODEID_NUMERIC(ns[3], 3002LU),
UA_NODEID_NUMERIC(ns[0], 29LU),
UA_NODEID_NUMERIC(ns[0], 45LU),
UA_QUALIFIEDNAME(ns[3], "MtRequestResult"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_DATATYPEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_0_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 3002LU)
);
}

/* EnumStrings - ns=3;i=6008 */

static UA_StatusCode function_namespace_mtpdm1_generated_1_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 6;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 21LU);
UA_LocalizedText variablenode_ns_3_i_6008_variant_DataContents[6];
variablenode_ns_3_i_6008_variant_DataContents[0] = UA_LOCALIZEDTEXT("", "Ok");
variablenode_ns_3_i_6008_variant_DataContents[1] = UA_LOCALIZEDTEXT("", "Error");
variablenode_ns_3_i_6008_variant_DataContents[2] = UA_LOCALIZEDTEXT("", "Busy");
variablenode_ns_3_i_6008_variant_DataContents[3] = UA_LOCALIZEDTEXT("", "InvalidArgument");
variablenode_ns_3_i_6008_variant_DataContents[4] = UA_LOCALIZEDTEXT("", "Timeout");
variablenode_ns_3_i_6008_variant_DataContents[5] = UA_LOCALIZEDTEXT("", "Access");
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6008_variant_DataContents, (UA_Int32) 6, &UA_TYPES[UA_TYPES_LOCALIZEDTEXT]);
attr.displayName = UA_LOCALIZEDTEXT("", "EnumStrings");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6008LU),
UA_NODEID_NUMERIC(ns[3], 3002LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "EnumStrings"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6008LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_1_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6008LU)
);
}

/* MtMeasureStatus - ns=3;i=3004 */

static UA_StatusCode function_namespace_mtpdm1_generated_2_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_DataTypeAttributes attr = UA_DataTypeAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "MtMeasureStatus");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_DATATYPE,
UA_NODEID_NUMERIC(ns[3], 3004LU),
UA_NODEID_NUMERIC(ns[0], 29LU),
UA_NODEID_NUMERIC(ns[0], 45LU),
UA_QUALIFIEDNAME(ns[3], "MtMeasureStatus"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_DATATYPEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_2_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 3004LU)
);
}

/* EnumStrings - ns=3;i=6019 */

static UA_StatusCode function_namespace_mtpdm1_generated_3_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 8;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 21LU);
UA_LocalizedText variablenode_ns_3_i_6019_variant_DataContents[8];
variablenode_ns_3_i_6019_variant_DataContents[0] = UA_LOCALIZEDTEXT("", "Ok");
variablenode_ns_3_i_6019_variant_DataContents[1] = UA_LOCALIZEDTEXT("", "Busy");
variablenode_ns_3_i_6019_variant_DataContents[2] = UA_LOCALIZEDTEXT("", "Storing");
variablenode_ns_3_i_6019_variant_DataContents[3] = UA_LOCALIZEDTEXT("", "Error");
variablenode_ns_3_i_6019_variant_DataContents[4] = UA_LOCALIZEDTEXT("", "Timeout");
variablenode_ns_3_i_6019_variant_DataContents[5] = UA_LOCALIZEDTEXT("", "SignalLow");
variablenode_ns_3_i_6019_variant_DataContents[6] = UA_LOCALIZEDTEXT("", "SignalOverfow");
variablenode_ns_3_i_6019_variant_DataContents[7] = UA_LOCALIZEDTEXT("", "Unknown");
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6019_variant_DataContents, (UA_Int32) 8, &UA_TYPES[UA_TYPES_LOCALIZEDTEXT]);
attr.displayName = UA_LOCALIZEDTEXT("", "EnumStrings");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6019LU),
UA_NODEID_NUMERIC(ns[3], 3004LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "EnumStrings"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6019LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_3_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6019LU)
);
}

/* MtLogLevel - ns=3;i=3005 */

static UA_StatusCode function_namespace_mtpdm1_generated_4_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_DataTypeAttributes attr = UA_DataTypeAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "MtLogLevel");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_DATATYPE,
UA_NODEID_NUMERIC(ns[3], 3005LU),
UA_NODEID_NUMERIC(ns[0], 29LU),
UA_NODEID_NUMERIC(ns[0], 45LU),
UA_QUALIFIEDNAME(ns[3], "MtLogLevel"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_DATATYPEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_4_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 3005LU)
);
}

/* EnumStrings - ns=3;i=6020 */

static UA_StatusCode function_namespace_mtpdm1_generated_5_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 8;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 21LU);
UA_LocalizedText variablenode_ns_3_i_6020_variant_DataContents[8];
variablenode_ns_3_i_6020_variant_DataContents[0] = UA_LOCALIZEDTEXT("", "Emergency");
variablenode_ns_3_i_6020_variant_DataContents[1] = UA_LOCALIZEDTEXT("", "Alert");
variablenode_ns_3_i_6020_variant_DataContents[2] = UA_LOCALIZEDTEXT("", "Critical");
variablenode_ns_3_i_6020_variant_DataContents[3] = UA_LOCALIZEDTEXT("", "Error");
variablenode_ns_3_i_6020_variant_DataContents[4] = UA_LOCALIZEDTEXT("", "Warning");
variablenode_ns_3_i_6020_variant_DataContents[5] = UA_LOCALIZEDTEXT("", "Notice");
variablenode_ns_3_i_6020_variant_DataContents[6] = UA_LOCALIZEDTEXT("", "Info");
variablenode_ns_3_i_6020_variant_DataContents[7] = UA_LOCALIZEDTEXT("", "Debug");
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6020_variant_DataContents, (UA_Int32) 8, &UA_TYPES[UA_TYPES_LOCALIZEDTEXT]);
attr.displayName = UA_LOCALIZEDTEXT("", "EnumStrings");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6020LU),
UA_NODEID_NUMERIC(ns[3], 3005LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "EnumStrings"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6020LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_5_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6020LU)
);
}

/* MtGridFreq - ns=3;i=3003 */

static UA_StatusCode function_namespace_mtpdm1_generated_6_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_DataTypeAttributes attr = UA_DataTypeAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "MtGridFreq");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_DATATYPE,
UA_NODEID_NUMERIC(ns[3], 3003LU),
UA_NODEID_NUMERIC(ns[0], 29LU),
UA_NODEID_NUMERIC(ns[0], 45LU),
UA_QUALIFIEDNAME(ns[3], "MtGridFreq"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_DATATYPEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_6_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 3003LU)
);
}

/* EnumStrings - ns=3;i=6018 */

static UA_StatusCode function_namespace_mtpdm1_generated_7_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 2;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 21LU);
UA_LocalizedText variablenode_ns_3_i_6018_variant_DataContents[2];
variablenode_ns_3_i_6018_variant_DataContents[0] = UA_LOCALIZEDTEXT("", "50Hz");
variablenode_ns_3_i_6018_variant_DataContents[1] = UA_LOCALIZEDTEXT("", "60Hz");
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6018_variant_DataContents, (UA_Int32) 2, &UA_TYPES[UA_TYPES_LOCALIZEDTEXT]);
attr.displayName = UA_LOCALIZEDTEXT("", "EnumStrings");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6018LU),
UA_NODEID_NUMERIC(ns[3], 3003LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "EnumStrings"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6018LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_7_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6018LU)
);
}

/* PDM1 - ns=3;i=5015 */

static UA_StatusCode function_namespace_mtpdm1_generated_8_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "PDM1");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[0], 85LU),
UA_NODEID_NUMERIC(ns[0], 35LU),
UA_QUALIFIEDNAME(ns[3], "PDM1"),
UA_NODEID_NUMERIC(ns[0], 58LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_8_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5015LU)
);
}

/* TPRS1 - ns=3;i=5048 */

static UA_StatusCode function_namespace_mtpdm1_generated_9_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "TPRS1");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5048LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "TPRS1"),
UA_NODEID_NUMERIC(ns[2], 5538LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_9_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5048LU)
);
}

/* PresSv - ns=3;i=5071 */

static UA_StatusCode function_namespace_mtpdm1_generated_10_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "PresSv");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5071LU),
UA_NODEID_NUMERIC(ns[3], 5048LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "PresSv"),
UA_NODEID_NUMERIC(ns[1], 362LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_10_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5071LU)
);
}

/* q - ns=3;i=6180 */

static UA_StatusCode function_namespace_mtpdm1_generated_11_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6180LU),
UA_NODEID_NUMERIC(ns[3], 5071LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_11_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6180LU)
);
}

/* instMag - ns=3;i=6179 */

static UA_StatusCode function_namespace_mtpdm1_generated_12_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "instMag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6179LU),
UA_NODEID_NUMERIC(ns[3], 5071LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "instMag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_12_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6179LU)
);
}

/* Beh - ns=3;i=5050 */

static UA_StatusCode function_namespace_mtpdm1_generated_13_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5050LU),
UA_NODEID_NUMERIC(ns[3], 5048LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_13_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5050LU)
);
}

/* stVal - ns=3;i=6089 */

static UA_StatusCode function_namespace_mtpdm1_generated_14_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6089LU),
UA_NODEID_NUMERIC(ns[3], 5050LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_14_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6089LU)
);
}

/* q - ns=3;i=6088 */

static UA_StatusCode function_namespace_mtpdm1_generated_15_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6088LU),
UA_NODEID_NUMERIC(ns[3], 5050LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_15_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6088LU)
);
}

/* SPDC8 - ns=3;i=5045 */

static UA_StatusCode function_namespace_mtpdm1_generated_16_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "SPDC8");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5045LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "SPDC8"),
UA_NODEID_NUMERIC(ns[2], 5184LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_16_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5045LU)
);
}

/* OpCnt - ns=3;i=5047 */

static UA_StatusCode function_namespace_mtpdm1_generated_17_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "OpCnt");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5047LU),
UA_NODEID_NUMERIC(ns[3], 5045LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "OpCnt"),
UA_NODEID_NUMERIC(ns[1], 203LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_17_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5047LU)
);
}

/* q - ns=3;i=6086 */

static UA_StatusCode function_namespace_mtpdm1_generated_18_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6086LU),
UA_NODEID_NUMERIC(ns[3], 5047LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_18_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6086LU)
);
}

/* stVal - ns=3;i=6087 */

static UA_StatusCode function_namespace_mtpdm1_generated_19_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 6LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6087LU),
UA_NODEID_NUMERIC(ns[3], 5047LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_19_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6087LU)
);
}

/* AppPaDsch - ns=3;i=5068 */

static UA_StatusCode function_namespace_mtpdm1_generated_20_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "AppPaDsch");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5068LU),
UA_NODEID_NUMERIC(ns[3], 5045LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "AppPaDsch"),
UA_NODEID_NUMERIC(ns[1], 315LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_20_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5068LU)
);
}

/* units - ns=3;i=6176 */

static UA_StatusCode function_namespace_mtpdm1_generated_21_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 133LU);
attr.displayName = UA_LOCALIZEDTEXT("", "units");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6176LU),
UA_NODEID_NUMERIC(ns[3], 5068LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "units"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_21_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6176LU)
);
}

/* mag - ns=3;i=6169 */

static UA_StatusCode function_namespace_mtpdm1_generated_22_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "mag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6169LU),
UA_NODEID_NUMERIC(ns[3], 5068LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "mag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_22_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6169LU)
);
}

/* q - ns=3;i=6170 */

static UA_StatusCode function_namespace_mtpdm1_generated_23_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6170LU),
UA_NODEID_NUMERIC(ns[3], 5068LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_23_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6170LU)
);
}

/* Beh - ns=3;i=5046 */

static UA_StatusCode function_namespace_mtpdm1_generated_24_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5046LU),
UA_NODEID_NUMERIC(ns[3], 5045LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_24_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5046LU)
);
}

/* q - ns=3;i=6084 */

static UA_StatusCode function_namespace_mtpdm1_generated_25_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6084LU),
UA_NODEID_NUMERIC(ns[3], 5046LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_25_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6084LU)
);
}

/* stVal - ns=3;i=6085 */

static UA_StatusCode function_namespace_mtpdm1_generated_26_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6085LU),
UA_NODEID_NUMERIC(ns[3], 5046LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_26_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6085LU)
);
}

/* SPDC4 - ns=3;i=5033 */

static UA_StatusCode function_namespace_mtpdm1_generated_27_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "SPDC4");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5033LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "SPDC4"),
UA_NODEID_NUMERIC(ns[2], 5184LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_27_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5033LU)
);
}

/* OpCnt - ns=3;i=5034 */

static UA_StatusCode function_namespace_mtpdm1_generated_28_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "OpCnt");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5034LU),
UA_NODEID_NUMERIC(ns[3], 5033LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "OpCnt"),
UA_NODEID_NUMERIC(ns[1], 203LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_28_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5034LU)
);
}

/* stVal - ns=3;i=6069 */

static UA_StatusCode function_namespace_mtpdm1_generated_29_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 6LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6069LU),
UA_NODEID_NUMERIC(ns[3], 5034LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_29_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6069LU)
);
}

/* q - ns=3;i=6068 */

static UA_StatusCode function_namespace_mtpdm1_generated_30_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6068LU),
UA_NODEID_NUMERIC(ns[3], 5034LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_30_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6068LU)
);
}

/* Beh - ns=3;i=5035 */

static UA_StatusCode function_namespace_mtpdm1_generated_31_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5035LU),
UA_NODEID_NUMERIC(ns[3], 5033LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_31_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5035LU)
);
}

/* stVal - ns=3;i=6071 */

static UA_StatusCode function_namespace_mtpdm1_generated_32_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6071LU),
UA_NODEID_NUMERIC(ns[3], 5035LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_32_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6071LU)
);
}

/* q - ns=3;i=6070 */

static UA_StatusCode function_namespace_mtpdm1_generated_33_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6070LU),
UA_NODEID_NUMERIC(ns[3], 5035LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_33_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6070LU)
);
}

/* AppPaDsch - ns=3;i=5062 */

static UA_StatusCode function_namespace_mtpdm1_generated_34_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "AppPaDsch");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5062LU),
UA_NODEID_NUMERIC(ns[3], 5033LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "AppPaDsch"),
UA_NODEID_NUMERIC(ns[1], 315LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_34_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5062LU)
);
}

/* mag - ns=3;i=6114 */

static UA_StatusCode function_namespace_mtpdm1_generated_35_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "mag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6114LU),
UA_NODEID_NUMERIC(ns[3], 5062LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "mag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_35_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6114LU)
);
}

/* q - ns=3;i=6115 */

static UA_StatusCode function_namespace_mtpdm1_generated_36_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6115LU),
UA_NODEID_NUMERIC(ns[3], 5062LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_36_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6115LU)
);
}

/* units - ns=3;i=6148 */

static UA_StatusCode function_namespace_mtpdm1_generated_37_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 133LU);
attr.displayName = UA_LOCALIZEDTEXT("", "units");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6148LU),
UA_NODEID_NUMERIC(ns[3], 5062LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "units"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_37_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6148LU)
);
}

/* SPDC6 - ns=3;i=5039 */

static UA_StatusCode function_namespace_mtpdm1_generated_38_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "SPDC6");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5039LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "SPDC6"),
UA_NODEID_NUMERIC(ns[2], 5184LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_38_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5039LU)
);
}

/* Beh - ns=3;i=5041 */

static UA_StatusCode function_namespace_mtpdm1_generated_39_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5041LU),
UA_NODEID_NUMERIC(ns[3], 5039LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_39_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5041LU)
);
}

/* q - ns=3;i=6078 */

static UA_StatusCode function_namespace_mtpdm1_generated_40_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6078LU),
UA_NODEID_NUMERIC(ns[3], 5041LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_40_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6078LU)
);
}

/* stVal - ns=3;i=6079 */

static UA_StatusCode function_namespace_mtpdm1_generated_41_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6079LU),
UA_NODEID_NUMERIC(ns[3], 5041LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_41_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6079LU)
);
}

/* AppPaDsch - ns=3;i=5064 */

static UA_StatusCode function_namespace_mtpdm1_generated_42_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "AppPaDsch");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5064LU),
UA_NODEID_NUMERIC(ns[3], 5039LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "AppPaDsch"),
UA_NODEID_NUMERIC(ns[1], 315LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_42_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5064LU)
);
}

/* mag - ns=3;i=6161 */

static UA_StatusCode function_namespace_mtpdm1_generated_43_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "mag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6161LU),
UA_NODEID_NUMERIC(ns[3], 5064LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "mag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_43_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6161LU)
);
}

/* q - ns=3;i=6162 */

static UA_StatusCode function_namespace_mtpdm1_generated_44_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6162LU),
UA_NODEID_NUMERIC(ns[3], 5064LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_44_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6162LU)
);
}

/* units - ns=3;i=6163 */

static UA_StatusCode function_namespace_mtpdm1_generated_45_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 133LU);
attr.displayName = UA_LOCALIZEDTEXT("", "units");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6163LU),
UA_NODEID_NUMERIC(ns[3], 5064LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "units"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_45_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6163LU)
);
}

/* OpCnt - ns=3;i=5040 */

static UA_StatusCode function_namespace_mtpdm1_generated_46_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "OpCnt");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5040LU),
UA_NODEID_NUMERIC(ns[3], 5039LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "OpCnt"),
UA_NODEID_NUMERIC(ns[1], 203LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_46_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5040LU)
);
}

/* stVal - ns=3;i=6077 */

static UA_StatusCode function_namespace_mtpdm1_generated_47_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 6LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6077LU),
UA_NODEID_NUMERIC(ns[3], 5040LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_47_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6077LU)
);
}

/* q - ns=3;i=6076 */

static UA_StatusCode function_namespace_mtpdm1_generated_48_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6076LU),
UA_NODEID_NUMERIC(ns[3], 5040LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_48_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6076LU)
);
}

/* SPDC1 - ns=3;i=5024 */

static UA_StatusCode function_namespace_mtpdm1_generated_49_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "SPDC1");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5024LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "SPDC1"),
UA_NODEID_NUMERIC(ns[2], 5184LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_49_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5024LU)
);
}

/* OpCnt - ns=3;i=5025 */

static UA_StatusCode function_namespace_mtpdm1_generated_50_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "OpCnt");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5025LU),
UA_NODEID_NUMERIC(ns[3], 5024LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "OpCnt"),
UA_NODEID_NUMERIC(ns[1], 203LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_50_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5025LU)
);
}

/* q - ns=3;i=6056 */

static UA_StatusCode function_namespace_mtpdm1_generated_51_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6056LU),
UA_NODEID_NUMERIC(ns[3], 5025LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_51_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6056LU)
);
}

/* stVal - ns=3;i=6057 */

static UA_StatusCode function_namespace_mtpdm1_generated_52_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 6LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6057LU),
UA_NODEID_NUMERIC(ns[3], 5025LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_52_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6057LU)
);
}

/* Beh - ns=3;i=5026 */

static UA_StatusCode function_namespace_mtpdm1_generated_53_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5026LU),
UA_NODEID_NUMERIC(ns[3], 5024LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_53_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5026LU)
);
}

/* stVal - ns=3;i=6059 */

static UA_StatusCode function_namespace_mtpdm1_generated_54_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6059LU),
UA_NODEID_NUMERIC(ns[3], 5026LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_54_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6059LU)
);
}

/* q - ns=3;i=6058 */

static UA_StatusCode function_namespace_mtpdm1_generated_55_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6058LU),
UA_NODEID_NUMERIC(ns[3], 5026LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_55_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6058LU)
);
}

/* AppPaDsch - ns=3;i=5059 */

static UA_StatusCode function_namespace_mtpdm1_generated_56_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "AppPaDsch");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5059LU),
UA_NODEID_NUMERIC(ns[3], 5024LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "AppPaDsch"),
UA_NODEID_NUMERIC(ns[1], 315LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_56_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5059LU)
);
}

/* mag - ns=3;i=6100 */

static UA_StatusCode function_namespace_mtpdm1_generated_57_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "mag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6100LU),
UA_NODEID_NUMERIC(ns[3], 5059LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "mag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_57_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6100LU)
);
}

/* q - ns=3;i=6101 */

static UA_StatusCode function_namespace_mtpdm1_generated_58_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6101LU),
UA_NODEID_NUMERIC(ns[3], 5059LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_58_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6101LU)
);
}

/* units - ns=3;i=6104 */

static UA_StatusCode function_namespace_mtpdm1_generated_59_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 133LU);
attr.displayName = UA_LOCALIZEDTEXT("", "units");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6104LU),
UA_NODEID_NUMERIC(ns[3], 5059LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "units"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_59_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6104LU)
);
}

/* TTMP1 - ns=3;i=5057 */

static UA_StatusCode function_namespace_mtpdm1_generated_60_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "TTMP1");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5057LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "TTMP1"),
UA_NODEID_NUMERIC(ns[2], 5571LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_60_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5057LU)
);
}

/* Beh - ns=3;i=5058 */

static UA_StatusCode function_namespace_mtpdm1_generated_61_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5058LU),
UA_NODEID_NUMERIC(ns[3], 5057LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_61_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5058LU)
);
}

/* stVal - ns=3;i=6098 */

static UA_StatusCode function_namespace_mtpdm1_generated_62_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6098LU),
UA_NODEID_NUMERIC(ns[3], 5058LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_62_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6098LU)
);
}

/* q - ns=3;i=6097 */

static UA_StatusCode function_namespace_mtpdm1_generated_63_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6097LU),
UA_NODEID_NUMERIC(ns[3], 5058LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_63_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6097LU)
);
}

/* TmpSv - ns=3;i=5072 */

static UA_StatusCode function_namespace_mtpdm1_generated_64_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "TmpSv");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5072LU),
UA_NODEID_NUMERIC(ns[3], 5057LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "TmpSv"),
UA_NODEID_NUMERIC(ns[1], 362LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_64_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5072LU)
);
}

/* q - ns=3;i=6182 */

static UA_StatusCode function_namespace_mtpdm1_generated_65_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6182LU),
UA_NODEID_NUMERIC(ns[3], 5072LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_65_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6182LU)
);
}

/* instMag - ns=3;i=6181 */

static UA_StatusCode function_namespace_mtpdm1_generated_66_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "instMag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6181LU),
UA_NODEID_NUMERIC(ns[3], 5072LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "instMag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_66_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6181LU)
);
}

/* THUM1 - ns=3;i=5054 */

static UA_StatusCode function_namespace_mtpdm1_generated_67_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "THUM1");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5054LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "THUM1"),
UA_NODEID_NUMERIC(ns[2], 5483LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_67_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5054LU)
);
}

/* Beh - ns=3;i=5055 */

static UA_StatusCode function_namespace_mtpdm1_generated_68_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5055LU),
UA_NODEID_NUMERIC(ns[3], 5054LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_68_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5055LU)
);
}

/* q - ns=3;i=6090 */

static UA_StatusCode function_namespace_mtpdm1_generated_69_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6090LU),
UA_NODEID_NUMERIC(ns[3], 5055LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_69_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6090LU)
);
}

/* stVal - ns=3;i=6096 */

static UA_StatusCode function_namespace_mtpdm1_generated_70_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6096LU),
UA_NODEID_NUMERIC(ns[3], 5055LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_70_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6096LU)
);
}

/* HumSv - ns=3;i=5070 */

static UA_StatusCode function_namespace_mtpdm1_generated_71_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "HumSv");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5070LU),
UA_NODEID_NUMERIC(ns[3], 5054LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "HumSv"),
UA_NODEID_NUMERIC(ns[1], 362LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_71_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5070LU)
);
}

/* q - ns=3;i=6178 */

static UA_StatusCode function_namespace_mtpdm1_generated_72_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6178LU),
UA_NODEID_NUMERIC(ns[3], 5070LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_72_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6178LU)
);
}

/* instMag - ns=3;i=6177 */

static UA_StatusCode function_namespace_mtpdm1_generated_73_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "instMag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6177LU),
UA_NODEID_NUMERIC(ns[3], 5070LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "instMag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_73_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6177LU)
);
}

/* SPDC3 - ns=3;i=5030 */

static UA_StatusCode function_namespace_mtpdm1_generated_74_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "SPDC3");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5030LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "SPDC3"),
UA_NODEID_NUMERIC(ns[2], 5184LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_74_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5030LU)
);
}

/* OpCnt - ns=3;i=5031 */

static UA_StatusCode function_namespace_mtpdm1_generated_75_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "OpCnt");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5031LU),
UA_NODEID_NUMERIC(ns[3], 5030LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "OpCnt"),
UA_NODEID_NUMERIC(ns[1], 203LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_75_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5031LU)
);
}

/* stVal - ns=3;i=6065 */

static UA_StatusCode function_namespace_mtpdm1_generated_76_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 6LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6065LU),
UA_NODEID_NUMERIC(ns[3], 5031LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_76_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6065LU)
);
}

/* q - ns=3;i=6064 */

static UA_StatusCode function_namespace_mtpdm1_generated_77_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6064LU),
UA_NODEID_NUMERIC(ns[3], 5031LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_77_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6064LU)
);
}

/* Beh - ns=3;i=5032 */

static UA_StatusCode function_namespace_mtpdm1_generated_78_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5032LU),
UA_NODEID_NUMERIC(ns[3], 5030LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_78_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5032LU)
);
}

/* stVal - ns=3;i=6067 */

static UA_StatusCode function_namespace_mtpdm1_generated_79_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6067LU),
UA_NODEID_NUMERIC(ns[3], 5032LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_79_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6067LU)
);
}

/* q - ns=3;i=6066 */

static UA_StatusCode function_namespace_mtpdm1_generated_80_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6066LU),
UA_NODEID_NUMERIC(ns[3], 5032LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_80_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6066LU)
);
}

/* AppPaDsch - ns=3;i=5060 */

static UA_StatusCode function_namespace_mtpdm1_generated_81_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "AppPaDsch");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5060LU),
UA_NODEID_NUMERIC(ns[3], 5030LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "AppPaDsch"),
UA_NODEID_NUMERIC(ns[1], 315LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_81_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5060LU)
);
}

/* units - ns=3;i=6109 */

static UA_StatusCode function_namespace_mtpdm1_generated_82_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 133LU);
attr.displayName = UA_LOCALIZEDTEXT("", "units");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6109LU),
UA_NODEID_NUMERIC(ns[3], 5060LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "units"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_82_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6109LU)
);
}

/* q - ns=3;i=6106 */

static UA_StatusCode function_namespace_mtpdm1_generated_83_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6106LU),
UA_NODEID_NUMERIC(ns[3], 5060LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_83_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6106LU)
);
}

/* mag - ns=3;i=6105 */

static UA_StatusCode function_namespace_mtpdm1_generated_84_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "mag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6105LU),
UA_NODEID_NUMERIC(ns[3], 5060LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "mag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_84_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6105LU)
);
}

/* LPHD - ns=3;i=5007 */

static UA_StatusCode function_namespace_mtpdm1_generated_85_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "LPHD");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5007LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "LPHD"),
UA_NODEID_NUMERIC(ns[2], 173LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_85_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5007LU)
);
}

/* Proxy - ns=3;i=5014 */

static UA_StatusCode function_namespace_mtpdm1_generated_86_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Proxy");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5014LU),
UA_NODEID_NUMERIC(ns[3], 5007LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Proxy"),
UA_NODEID_NUMERIC(ns[1], 177LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_86_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5014LU)
);
}

/* q - ns=3;i=6006 */

static UA_StatusCode function_namespace_mtpdm1_generated_87_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6006LU),
UA_NODEID_NUMERIC(ns[3], 5014LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_87_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6006LU)
);
}

/* stVal - ns=3;i=6007 */

static UA_StatusCode function_namespace_mtpdm1_generated_88_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 1LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6007LU),
UA_NODEID_NUMERIC(ns[3], 5014LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_88_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6007LU)
);
}

/* PhyNam - ns=3;i=5013 */

static UA_StatusCode function_namespace_mtpdm1_generated_89_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "PhyNam");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[3], 5007LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "PhyNam"),
UA_NODEID_NUMERIC(ns[1], 920LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_89_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5013LU)
);
}

/* latitude - ns=3;i=6013 */

static UA_StatusCode function_namespace_mtpdm1_generated_90_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 10LU);
attr.displayName = UA_LOCALIZEDTEXT("", "latitude");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6013LU),
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "latitude"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_90_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6013LU)
);
}

/* owner - ns=3;i=6022 */

static UA_StatusCode function_namespace_mtpdm1_generated_91_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
attr.displayName = UA_LOCALIZEDTEXT("", "owner");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6022LU),
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "owner"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_91_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6022LU)
);
}

/* serNum - ns=3;i=6023 */

static UA_StatusCode function_namespace_mtpdm1_generated_92_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
attr.displayName = UA_LOCALIZEDTEXT("", "serNum");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6023LU),
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "serNum"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_92_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6023LU)
);
}

/* longitude - ns=3;i=6015 */

static UA_StatusCode function_namespace_mtpdm1_generated_93_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 10LU);
attr.displayName = UA_LOCALIZEDTEXT("", "longitude");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6015LU),
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "longitude"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_93_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6015LU)
);
}

/* swRev - ns=3;i=6025 */

static UA_StatusCode function_namespace_mtpdm1_generated_94_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
attr.displayName = UA_LOCALIZEDTEXT("", "swRev");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6025LU),
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "swRev"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_94_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6025LU)
);
}

/* name - ns=3;i=6021 */

static UA_StatusCode function_namespace_mtpdm1_generated_95_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
attr.displayName = UA_LOCALIZEDTEXT("", "name");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6021LU),
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "name"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_95_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6021LU)
);
}

/* hwRev - ns=3;i=6009 */

static UA_StatusCode function_namespace_mtpdm1_generated_96_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
attr.displayName = UA_LOCALIZEDTEXT("", "hwRev");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6009LU),
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "hwRev"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_96_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6009LU)
);
}

/* model - ns=3;i=6017 */

static UA_StatusCode function_namespace_mtpdm1_generated_97_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
attr.displayName = UA_LOCALIZEDTEXT("", "model");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6017LU),
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "model"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_97_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6017LU)
);
}

/* location - ns=3;i=6014 */

static UA_StatusCode function_namespace_mtpdm1_generated_98_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
attr.displayName = UA_LOCALIZEDTEXT("", "location");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6014LU),
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "location"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_98_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6014LU)
);
}

/* vendor - ns=3;i=6005 */

static UA_StatusCode function_namespace_mtpdm1_generated_99_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
attr.displayName = UA_LOCALIZEDTEXT("", "vendor");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6005LU),
UA_NODEID_NUMERIC(ns[3], 5013LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "vendor"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_99_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6005LU)
);
}

/* PhyHealth - ns=3;i=5012 */

static UA_StatusCode function_namespace_mtpdm1_generated_100_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "PhyHealth");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5012LU),
UA_NODEID_NUMERIC(ns[3], 5007LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "PhyHealth"),
UA_NODEID_NUMERIC(ns[2], 104LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_100_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5012LU)
);
}

/* q - ns=3;i=6003 */

static UA_StatusCode function_namespace_mtpdm1_generated_101_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6003LU),
UA_NODEID_NUMERIC(ns[3], 5012LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_101_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6003LU)
);
}

/* stVal - ns=3;i=6004 */

static UA_StatusCode function_namespace_mtpdm1_generated_102_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 25LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6004LU),
UA_NODEID_NUMERIC(ns[3], 5012LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_102_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6004LU)
);
}

/* LLN0 - ns=3;i=5004 */

static UA_StatusCode function_namespace_mtpdm1_generated_103_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "LLN0");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5004LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "LLN0"),
UA_NODEID_NUMERIC(ns[2], 223LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_103_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5004LU)
);
}

/* Beh - ns=3;i=5001 */

static UA_StatusCode function_namespace_mtpdm1_generated_104_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5001LU),
UA_NODEID_NUMERIC(ns[3], 5004LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_104_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5001LU)
);
}

/* stVal - ns=3;i=6002 */

static UA_StatusCode function_namespace_mtpdm1_generated_105_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6002LU),
UA_NODEID_NUMERIC(ns[3], 5001LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_105_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6002LU)
);
}

/* q - ns=3;i=6001 */

static UA_StatusCode function_namespace_mtpdm1_generated_106_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6001LU),
UA_NODEID_NUMERIC(ns[3], 5001LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_106_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6001LU)
);
}

/* SPDC2 - ns=3;i=5027 */

static UA_StatusCode function_namespace_mtpdm1_generated_107_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "SPDC2");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5027LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "SPDC2"),
UA_NODEID_NUMERIC(ns[2], 5184LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_107_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5027LU)
);
}

/* OpCnt - ns=3;i=5028 */

static UA_StatusCode function_namespace_mtpdm1_generated_108_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "OpCnt");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5028LU),
UA_NODEID_NUMERIC(ns[3], 5027LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "OpCnt"),
UA_NODEID_NUMERIC(ns[1], 203LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_108_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5028LU)
);
}

/* q - ns=3;i=6060 */

static UA_StatusCode function_namespace_mtpdm1_generated_109_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6060LU),
UA_NODEID_NUMERIC(ns[3], 5028LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_109_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6060LU)
);
}

/* stVal - ns=3;i=6061 */

static UA_StatusCode function_namespace_mtpdm1_generated_110_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 6LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6061LU),
UA_NODEID_NUMERIC(ns[3], 5028LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_110_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6061LU)
);
}

/* AppPaDsch - ns=3;i=5066 */

static UA_StatusCode function_namespace_mtpdm1_generated_111_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "AppPaDsch");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5066LU),
UA_NODEID_NUMERIC(ns[3], 5027LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "AppPaDsch"),
UA_NODEID_NUMERIC(ns[1], 315LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_111_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5066LU)
);
}

/* units - ns=3;i=6147 */

static UA_StatusCode function_namespace_mtpdm1_generated_112_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 133LU);
attr.displayName = UA_LOCALIZEDTEXT("", "units");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6147LU),
UA_NODEID_NUMERIC(ns[3], 5066LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "units"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_112_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6147LU)
);
}

/* mag - ns=3;i=6145 */

static UA_StatusCode function_namespace_mtpdm1_generated_113_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "mag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6145LU),
UA_NODEID_NUMERIC(ns[3], 5066LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "mag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_113_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6145LU)
);
}

/* q - ns=3;i=6146 */

static UA_StatusCode function_namespace_mtpdm1_generated_114_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6146LU),
UA_NODEID_NUMERIC(ns[3], 5066LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_114_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6146LU)
);
}

/* Beh - ns=3;i=5029 */

static UA_StatusCode function_namespace_mtpdm1_generated_115_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5029LU),
UA_NODEID_NUMERIC(ns[3], 5027LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_115_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5029LU)
);
}

/* stVal - ns=3;i=6063 */

static UA_StatusCode function_namespace_mtpdm1_generated_116_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6063LU),
UA_NODEID_NUMERIC(ns[3], 5029LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_116_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6063LU)
);
}

/* q - ns=3;i=6062 */

static UA_StatusCode function_namespace_mtpdm1_generated_117_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6062LU),
UA_NODEID_NUMERIC(ns[3], 5029LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_117_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6062LU)
);
}

/* SPDC5 - ns=3;i=5036 */

static UA_StatusCode function_namespace_mtpdm1_generated_118_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "SPDC5");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5036LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "SPDC5"),
UA_NODEID_NUMERIC(ns[2], 5184LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_118_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5036LU)
);
}

/* OpCnt - ns=3;i=5037 */

static UA_StatusCode function_namespace_mtpdm1_generated_119_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "OpCnt");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5037LU),
UA_NODEID_NUMERIC(ns[3], 5036LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "OpCnt"),
UA_NODEID_NUMERIC(ns[1], 203LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_119_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5037LU)
);
}

/* stVal - ns=3;i=6073 */

static UA_StatusCode function_namespace_mtpdm1_generated_120_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 6LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6073LU),
UA_NODEID_NUMERIC(ns[3], 5037LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_120_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6073LU)
);
}

/* q - ns=3;i=6072 */

static UA_StatusCode function_namespace_mtpdm1_generated_121_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6072LU),
UA_NODEID_NUMERIC(ns[3], 5037LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_121_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6072LU)
);
}

/* Beh - ns=3;i=5038 */

static UA_StatusCode function_namespace_mtpdm1_generated_122_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5038LU),
UA_NODEID_NUMERIC(ns[3], 5036LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_122_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5038LU)
);
}

/* q - ns=3;i=6074 */

static UA_StatusCode function_namespace_mtpdm1_generated_123_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6074LU),
UA_NODEID_NUMERIC(ns[3], 5038LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_123_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6074LU)
);
}

/* stVal - ns=3;i=6075 */

static UA_StatusCode function_namespace_mtpdm1_generated_124_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6075LU),
UA_NODEID_NUMERIC(ns[3], 5038LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_124_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6075LU)
);
}

/* AppPaDsch - ns=3;i=5063 */

static UA_StatusCode function_namespace_mtpdm1_generated_125_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "AppPaDsch");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5063LU),
UA_NODEID_NUMERIC(ns[3], 5036LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "AppPaDsch"),
UA_NODEID_NUMERIC(ns[1], 315LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_125_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5063LU)
);
}

/* mag - ns=3;i=6149 */

static UA_StatusCode function_namespace_mtpdm1_generated_126_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "mag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6149LU),
UA_NODEID_NUMERIC(ns[3], 5063LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "mag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_126_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6149LU)
);
}

/* q - ns=3;i=6150 */

static UA_StatusCode function_namespace_mtpdm1_generated_127_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6150LU),
UA_NODEID_NUMERIC(ns[3], 5063LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_127_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6150LU)
);
}

/* units - ns=3;i=6151 */

static UA_StatusCode function_namespace_mtpdm1_generated_128_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 133LU);
attr.displayName = UA_LOCALIZEDTEXT("", "units");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6151LU),
UA_NODEID_NUMERIC(ns[3], 5063LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "units"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_128_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6151LU)
);
}

/* SPDC7 - ns=3;i=5042 */

static UA_StatusCode function_namespace_mtpdm1_generated_129_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "SPDC7");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5042LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "SPDC7"),
UA_NODEID_NUMERIC(ns[2], 5184LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_129_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5042LU)
);
}

/* OpCnt - ns=3;i=5044 */

static UA_StatusCode function_namespace_mtpdm1_generated_130_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "OpCnt");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5044LU),
UA_NODEID_NUMERIC(ns[3], 5042LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "OpCnt"),
UA_NODEID_NUMERIC(ns[1], 203LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_130_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5044LU)
);
}

/* q - ns=3;i=6082 */

static UA_StatusCode function_namespace_mtpdm1_generated_131_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6082LU),
UA_NODEID_NUMERIC(ns[3], 5044LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_131_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6082LU)
);
}

/* stVal - ns=3;i=6083 */

static UA_StatusCode function_namespace_mtpdm1_generated_132_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 6LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6083LU),
UA_NODEID_NUMERIC(ns[3], 5044LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_132_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6083LU)
);
}

/* AppPaDsch - ns=3;i=5067 */

static UA_StatusCode function_namespace_mtpdm1_generated_133_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "AppPaDsch");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5067LU),
UA_NODEID_NUMERIC(ns[3], 5042LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "AppPaDsch"),
UA_NODEID_NUMERIC(ns[1], 315LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_133_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5067LU)
);
}

/* mag - ns=3;i=6164 */

static UA_StatusCode function_namespace_mtpdm1_generated_134_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 117LU);
attr.displayName = UA_LOCALIZEDTEXT("", "mag");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6164LU),
UA_NODEID_NUMERIC(ns[3], 5067LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "mag"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_134_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6164LU)
);
}

/* q - ns=3;i=6167 */

static UA_StatusCode function_namespace_mtpdm1_generated_135_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6167LU),
UA_NODEID_NUMERIC(ns[3], 5067LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_135_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6167LU)
);
}

/* units - ns=3;i=6168 */

static UA_StatusCode function_namespace_mtpdm1_generated_136_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 133LU);
attr.displayName = UA_LOCALIZEDTEXT("", "units");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6168LU),
UA_NODEID_NUMERIC(ns[3], 5067LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[1], "units"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_136_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6168LU)
);
}

/* Beh - ns=3;i=5043 */

static UA_StatusCode function_namespace_mtpdm1_generated_137_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Beh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5043LU),
UA_NODEID_NUMERIC(ns[3], 5042LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[2], "Beh"),
UA_NODEID_NUMERIC(ns[2], 98LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_137_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5043LU)
);
}

/* q - ns=3;i=6080 */

static UA_StatusCode function_namespace_mtpdm1_generated_138_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[1], 17LU);
attr.displayName = UA_LOCALIZEDTEXT("", "q");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6080LU),
UA_NODEID_NUMERIC(ns[3], 5043LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "q"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_138_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6080LU)
);
}

/* stVal - ns=3;i=6081 */

static UA_StatusCode function_namespace_mtpdm1_generated_139_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[2], 5LU);
attr.displayName = UA_LOCALIZEDTEXT("", "stVal");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6081LU),
UA_NODEID_NUMERIC(ns[3], 5043LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[2], "stVal"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_139_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6081LU)
);
}

/* ModemtecBaseObjectType - ns=3;i=1004 */

static UA_StatusCode function_namespace_mtpdm1_generated_140_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectTypeAttributes attr = UA_ObjectTypeAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "ModemtecBaseObjectType");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECTTYPE,
UA_NODEID_NUMERIC(ns[3], 1004LU),
UA_NODEID_NUMERIC(ns[0], 58LU),
UA_NODEID_NUMERIC(ns[0], 45LU),
UA_QUALIFIEDNAME(ns[3], "ModemtecBaseObjectType"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTTYPEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_140_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 1004LU)
);
}

/* SCOPE - ns=3;i=1007 */

static UA_StatusCode function_namespace_mtpdm1_generated_141_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectTypeAttributes attr = UA_ObjectTypeAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "SCOPE");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECTTYPE,
UA_NODEID_NUMERIC(ns[3], 1007LU),
UA_NODEID_NUMERIC(ns[3], 1004LU),
UA_NODEID_NUMERIC(ns[0], 45LU),
UA_QUALIFIEDNAME(ns[3], "SCOPE"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTTYPEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_141_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 1007LU)
);
}

/* SCOPE - ns=3;i=5021 */

static UA_StatusCode function_namespace_mtpdm1_generated_142_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "SCOPE");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5021LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "SCOPE"),
UA_NODEID_NUMERIC(ns[3], 1007LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_142_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5021LU)
);
}

/* status - ns=3;i=6053 */

static UA_StatusCode function_namespace_mtpdm1_generated_143_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3004LU);
attr.displayName = UA_LOCALIZEDTEXT("", "status");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6053LU),
UA_NODEID_NUMERIC(ns[3], 5021LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "status"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_143_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6053LU)
);
}

/* start - ns=3;i=7022 */

static UA_StatusCode function_namespace_mtpdm1_generated_144_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "start");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7022LU),
UA_NODEID_NUMERIC(ns[3], 5021LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "start"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_144_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7022LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* InputArguments - ns=3;i=6051 */

static UA_StatusCode function_namespace_mtpdm1_generated_145_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6051_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6051_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6051_variant_DataContents[0].name = UA_STRING("channel");
variablenode_ns_3_i_6051_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6051_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6051_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6051LU),
UA_NODEID_NUMERIC(ns[3], 7022LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_145_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6051LU)
);
}

/* OutputArguments - ns=3;i=6052 */

static UA_StatusCode function_namespace_mtpdm1_generated_146_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6052_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6052_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6052_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6052_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6052_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6052_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6052LU),
UA_NODEID_NUMERIC(ns[3], 7022LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_146_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6052LU)
);
}

/* status - ns=3;i=6107 */

static UA_StatusCode function_namespace_mtpdm1_generated_147_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3004LU);
attr.displayName = UA_LOCALIZEDTEXT("", "status");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6107LU),
UA_NODEID_NUMERIC(ns[3], 1007LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "status"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6107LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_147_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6107LU)
);
}

/* start - ns=3;i=7013 */

static UA_StatusCode function_namespace_mtpdm1_generated_148_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "start");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7013LU),
UA_NODEID_NUMERIC(ns[3], 1007LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "start"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 7013LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_148_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7013LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* InputArguments - ns=3;i=6128 */

static UA_StatusCode function_namespace_mtpdm1_generated_149_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6128_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6128_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6128_variant_DataContents[0].name = UA_STRING("channel");
variablenode_ns_3_i_6128_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6128_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6128_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6128LU),
UA_NODEID_NUMERIC(ns[3], 7013LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6128LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_149_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6128LU)
);
}

/* OutputArguments - ns=3;i=6129 */

static UA_StatusCode function_namespace_mtpdm1_generated_150_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6129_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6129_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6129_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6129_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6129_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6129_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6129LU),
UA_NODEID_NUMERIC(ns[3], 7013LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6129LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_150_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6129LU)
);
}

/* PRPD - ns=3;i=1003 */

static UA_StatusCode function_namespace_mtpdm1_generated_151_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectTypeAttributes attr = UA_ObjectTypeAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "PRPD");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECTTYPE,
UA_NODEID_NUMERIC(ns[3], 1003LU),
UA_NODEID_NUMERIC(ns[3], 1004LU),
UA_NODEID_NUMERIC(ns[0], 45LU),
UA_QUALIFIEDNAME(ns[3], "PRPD"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTTYPEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_151_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 1003LU)
);
}

/* endTime - ns=3;i=6111 */

static UA_StatusCode function_namespace_mtpdm1_generated_152_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 8LU);
attr.displayName = UA_LOCALIZEDTEXT("", "endTime");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6111LU),
UA_NODEID_NUMERIC(ns[3], 1003LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "endTime"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6111LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_152_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6111LU)
);
}

/* PRPD - ns=3;i=5020 */

static UA_StatusCode function_namespace_mtpdm1_generated_153_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "PRPD");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5020LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "PRPD"),
UA_NODEID_NUMERIC(ns[3], 1003LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_153_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5020LU)
);
}

/* endTime - ns=3;i=6046 */

static UA_StatusCode function_namespace_mtpdm1_generated_154_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 8LU);
attr.displayName = UA_LOCALIZEDTEXT("", "endTime");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6046LU),
UA_NODEID_NUMERIC(ns[3], 5020LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "endTime"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_154_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6046LU)
);
}

/* stop - ns=3;i=7019 */

static UA_StatusCode function_namespace_mtpdm1_generated_155_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "stop");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7019LU),
UA_NODEID_NUMERIC(ns[3], 5020LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "stop"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_155_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7019LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* OutputArguments - ns=3;i=6050 */

static UA_StatusCode function_namespace_mtpdm1_generated_156_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6050_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6050_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6050_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6050_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6050_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6050_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6050LU),
UA_NODEID_NUMERIC(ns[3], 7019LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_156_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6050LU)
);
}

/* busy - ns=3;i=6044 */

static UA_StatusCode function_namespace_mtpdm1_generated_157_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 1LU);
attr.displayName = UA_LOCALIZEDTEXT("", "busy");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6044LU),
UA_NODEID_NUMERIC(ns[3], 5020LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "busy"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_157_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6044LU)
);
}

/* read - ns=3;i=7015 */

static UA_StatusCode function_namespace_mtpdm1_generated_158_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "read");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7015LU),
UA_NODEID_NUMERIC(ns[3], 5020LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "read"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_158_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7015LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* OutputArguments - ns=3;i=6047 */

static UA_StatusCode function_namespace_mtpdm1_generated_159_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 2;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6047_variant_DataContents[2];

UA_init(&variablenode_ns_3_i_6047_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6047_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6047_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6047_variant_DataContents[0].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6047_variant_DataContents[1], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6047_variant_DataContents[1].name = UA_STRING("data");
variablenode_ns_3_i_6047_variant_DataContents[1].dataType = UA_NODEID_NUMERIC(ns[0], 15LU);
variablenode_ns_3_i_6047_variant_DataContents[1].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6047_variant_DataContents, (UA_Int32) 2, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6047LU),
UA_NODEID_NUMERIC(ns[3], 7015LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);


return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_159_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6047LU)
);
}

/* channel - ns=3;i=6045 */

static UA_StatusCode function_namespace_mtpdm1_generated_160_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
attr.displayName = UA_LOCALIZEDTEXT("", "channel");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6045LU),
UA_NODEID_NUMERIC(ns[3], 5020LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "channel"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_160_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6045LU)
);
}

/* start - ns=3;i=7018 */

static UA_StatusCode function_namespace_mtpdm1_generated_161_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "start");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7018LU),
UA_NODEID_NUMERIC(ns[3], 5020LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "start"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_161_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7018LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* OutputArguments - ns=3;i=6049 */

static UA_StatusCode function_namespace_mtpdm1_generated_162_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6049_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6049_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6049_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6049_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6049_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6049_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6049LU),
UA_NODEID_NUMERIC(ns[3], 7018LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_162_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6049LU)
);
}

/* InputArguments - ns=3;i=6048 */

static UA_StatusCode function_namespace_mtpdm1_generated_163_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 2;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6048_variant_DataContents[2];

UA_init(&variablenode_ns_3_i_6048_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6048_variant_DataContents[0].name = UA_STRING("channel");
variablenode_ns_3_i_6048_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6048_variant_DataContents[0].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6048_variant_DataContents[1], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6048_variant_DataContents[1].name = UA_STRING("duration");
variablenode_ns_3_i_6048_variant_DataContents[1].dataType = UA_NODEID_NUMERIC(ns[0], 8LU);
variablenode_ns_3_i_6048_variant_DataContents[1].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6048_variant_DataContents, (UA_Int32) 2, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6048LU),
UA_NODEID_NUMERIC(ns[3], 7018LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);


return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_163_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6048LU)
);
}

/* read - ns=3;i=7010 */

static UA_StatusCode function_namespace_mtpdm1_generated_164_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "read");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7010LU),
UA_NODEID_NUMERIC(ns[3], 1003LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "read"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 7010LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_164_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7010LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* OutputArguments - ns=3;i=6125 */

static UA_StatusCode function_namespace_mtpdm1_generated_165_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 2;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6125_variant_DataContents[2];

UA_init(&variablenode_ns_3_i_6125_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6125_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6125_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6125_variant_DataContents[0].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6125_variant_DataContents[1], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6125_variant_DataContents[1].name = UA_STRING("data");
variablenode_ns_3_i_6125_variant_DataContents[1].dataType = UA_NODEID_NUMERIC(ns[0], 15LU);
variablenode_ns_3_i_6125_variant_DataContents[1].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6125_variant_DataContents, (UA_Int32) 2, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6125LU),
UA_NODEID_NUMERIC(ns[3], 7010LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);


retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6125LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_165_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6125LU)
);
}

/* stop - ns=3;i=7007 */

static UA_StatusCode function_namespace_mtpdm1_generated_166_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "stop");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7007LU),
UA_NODEID_NUMERIC(ns[3], 1003LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "stop"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 7007LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_166_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7007LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* OutputArguments - ns=3;i=6122 */

static UA_StatusCode function_namespace_mtpdm1_generated_167_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6122_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6122_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6122_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6122_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6122_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6122_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6122LU),
UA_NODEID_NUMERIC(ns[3], 7007LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6122LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_167_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6122LU)
);
}

/* busy - ns=3;i=6099 */

static UA_StatusCode function_namespace_mtpdm1_generated_168_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 1LU);
attr.displayName = UA_LOCALIZEDTEXT("", "busy");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6099LU),
UA_NODEID_NUMERIC(ns[3], 1003LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "busy"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6099LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_168_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6099LU)
);
}

/* channel - ns=3;i=6110 */

static UA_StatusCode function_namespace_mtpdm1_generated_169_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
attr.displayName = UA_LOCALIZEDTEXT("", "channel");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6110LU),
UA_NODEID_NUMERIC(ns[3], 1003LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "channel"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6110LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_169_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6110LU)
);
}

/* start - ns=3;i=7004 */

static UA_StatusCode function_namespace_mtpdm1_generated_170_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "start");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7004LU),
UA_NODEID_NUMERIC(ns[3], 1003LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "start"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 7004LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_170_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7004LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* InputArguments - ns=3;i=6116 */

static UA_StatusCode function_namespace_mtpdm1_generated_171_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 2;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6116_variant_DataContents[2];

UA_init(&variablenode_ns_3_i_6116_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6116_variant_DataContents[0].name = UA_STRING("channel");
variablenode_ns_3_i_6116_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6116_variant_DataContents[0].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6116_variant_DataContents[1], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6116_variant_DataContents[1].name = UA_STRING("duration");
variablenode_ns_3_i_6116_variant_DataContents[1].dataType = UA_NODEID_NUMERIC(ns[0], 8LU);
variablenode_ns_3_i_6116_variant_DataContents[1].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6116_variant_DataContents, (UA_Int32) 2, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6116LU),
UA_NODEID_NUMERIC(ns[3], 7004LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);


retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6116LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_171_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6116LU)
);
}

/* OutputArguments - ns=3;i=6117 */

static UA_StatusCode function_namespace_mtpdm1_generated_172_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6117_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6117_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6117_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6117_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6117_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6117_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6117LU),
UA_NODEID_NUMERIC(ns[3], 7004LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6117LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_172_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6117LU)
);
}

/* LOG - ns=3;i=1005 */

static UA_StatusCode function_namespace_mtpdm1_generated_173_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectTypeAttributes attr = UA_ObjectTypeAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "LOG");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECTTYPE,
UA_NODEID_NUMERIC(ns[3], 1005LU),
UA_NODEID_NUMERIC(ns[3], 1004LU),
UA_NODEID_NUMERIC(ns[0], 45LU),
UA_QUALIFIEDNAME(ns[3], "LOG"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTTYPEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_173_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 1005LU)
);
}

/* File - ns=3;i=5008 */

static UA_StatusCode function_namespace_mtpdm1_generated_174_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "File");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5008LU),
UA_NODEID_NUMERIC(ns[3], 1005LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "File"),
UA_NODEID_NUMERIC(ns[0], 58LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 5008LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 80LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_174_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5008LU)
);
}

/* level - ns=3;i=6095 */

static UA_StatusCode function_namespace_mtpdm1_generated_175_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3005LU);
attr.displayName = UA_LOCALIZEDTEXT("", "level");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6095LU),
UA_NODEID_NUMERIC(ns[3], 5008LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "level"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6095LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_175_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6095LU)
);
}

/* Serial - ns=3;i=5009 */

static UA_StatusCode function_namespace_mtpdm1_generated_176_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Serial");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5009LU),
UA_NODEID_NUMERIC(ns[3], 1005LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "Serial"),
UA_NODEID_NUMERIC(ns[0], 58LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 5009LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 80LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_176_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5009LU)
);
}

/* level - ns=3;i=6026 */

static UA_StatusCode function_namespace_mtpdm1_generated_177_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3005LU);
attr.displayName = UA_LOCALIZEDTEXT("", "level");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6026LU),
UA_NODEID_NUMERIC(ns[3], 5009LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "level"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6026LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_177_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6026LU)
);
}

/* LOG - ns=3;i=5019 */

static UA_StatusCode function_namespace_mtpdm1_generated_178_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "LOG");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5019LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "LOG"),
UA_NODEID_NUMERIC(ns[3], 1005LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_178_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5019LU)
);
}

/* Serial - ns=3;i=5023 */

static UA_StatusCode function_namespace_mtpdm1_generated_179_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Serial");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5023LU),
UA_NODEID_NUMERIC(ns[3], 5019LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "Serial"),
UA_NODEID_NUMERIC(ns[0], 58LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_179_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5023LU)
);
}

/* level - ns=3;i=6055 */

static UA_StatusCode function_namespace_mtpdm1_generated_180_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3005LU);
attr.displayName = UA_LOCALIZEDTEXT("", "level");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6055LU),
UA_NODEID_NUMERIC(ns[3], 5023LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "level"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_180_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6055LU)
);
}

/* File - ns=3;i=5022 */

static UA_StatusCode function_namespace_mtpdm1_generated_181_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "File");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5022LU),
UA_NODEID_NUMERIC(ns[3], 5019LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "File"),
UA_NODEID_NUMERIC(ns[0], 58LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_181_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5022LU)
);
}

/* level - ns=3;i=6054 */

static UA_StatusCode function_namespace_mtpdm1_generated_182_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3005LU);
attr.displayName = UA_LOCALIZEDTEXT("", "level");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6054LU),
UA_NODEID_NUMERIC(ns[3], 5022LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "level"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_182_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6054LU)
);
}

/* CONF - ns=3;i=1010 */

static UA_StatusCode function_namespace_mtpdm1_generated_183_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectTypeAttributes attr = UA_ObjectTypeAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "CONF");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECTTYPE,
UA_NODEID_NUMERIC(ns[3], 1010LU),
UA_NODEID_NUMERIC(ns[3], 1004LU),
UA_NODEID_NUMERIC(ns[0], 45LU),
UA_QUALIFIEDNAME(ns[3], "CONF"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTTYPEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_183_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 1010LU)
);
}

/* changePass - ns=3;i=7020 */

static UA_StatusCode function_namespace_mtpdm1_generated_184_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "changePass");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7020LU),
UA_NODEID_NUMERIC(ns[3], 1010LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "changePass"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 7020LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_184_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7020LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* OutputArguments - ns=3;i=6153 */

static UA_StatusCode function_namespace_mtpdm1_generated_185_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6153_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6153_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6153_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6153_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6153_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6153_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6153LU),
UA_NODEID_NUMERIC(ns[3], 7020LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6153LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_185_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6153LU)
);
}

/* InputArguments - ns=3;i=6152 */

static UA_StatusCode function_namespace_mtpdm1_generated_186_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 2;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6152_variant_DataContents[2];

UA_init(&variablenode_ns_3_i_6152_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6152_variant_DataContents[0].name = UA_STRING("username");
variablenode_ns_3_i_6152_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
variablenode_ns_3_i_6152_variant_DataContents[0].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6152_variant_DataContents[1], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6152_variant_DataContents[1].name = UA_STRING("password");
variablenode_ns_3_i_6152_variant_DataContents[1].dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
variablenode_ns_3_i_6152_variant_DataContents[1].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6152_variant_DataContents, (UA_Int32) 2, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6152LU),
UA_NODEID_NUMERIC(ns[3], 7020LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);


retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6152LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_186_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6152LU)
);
}

/* channels - ns=3;i=6156 */

static UA_StatusCode function_namespace_mtpdm1_generated_187_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
attr.displayName = UA_LOCALIZEDTEXT("", "channels");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6156LU),
UA_NODEID_NUMERIC(ns[3], 1010LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "channels"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6156LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_187_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6156LU)
);
}

/* Calib - ns=3;i=5002 */

static UA_StatusCode function_namespace_mtpdm1_generated_188_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Calib");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5002LU),
UA_NODEID_NUMERIC(ns[3], 1010LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "Calib"),
UA_NODEID_NUMERIC(ns[0], 58LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 5002LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_188_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5002LU)
);
}

/* setup - ns=3;i=7017 */

static UA_StatusCode function_namespace_mtpdm1_generated_189_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "setup");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7017LU),
UA_NODEID_NUMERIC(ns[3], 5002LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "setup"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 7017LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_189_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7017LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* OutputArguments - ns=3;i=6143 */

static UA_StatusCode function_namespace_mtpdm1_generated_190_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6143_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6143_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6143_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6143_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6143_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6143_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6143LU),
UA_NODEID_NUMERIC(ns[3], 7017LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6143LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_190_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6143LU)
);
}

/* InputArguments - ns=3;i=6142 */

static UA_StatusCode function_namespace_mtpdm1_generated_191_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 3;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6142_variant_DataContents[3];

UA_init(&variablenode_ns_3_i_6142_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6142_variant_DataContents[0].name = UA_STRING("channel");
variablenode_ns_3_i_6142_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6142_variant_DataContents[0].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6142_variant_DataContents[1], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6142_variant_DataContents[1].name = UA_STRING("attenuator");
variablenode_ns_3_i_6142_variant_DataContents[1].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6142_variant_DataContents[1].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6142_variant_DataContents[2], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6142_variant_DataContents[2].name = UA_STRING("reference");
variablenode_ns_3_i_6142_variant_DataContents[2].dataType = UA_NODEID_NUMERIC(ns[0], 7LU);
variablenode_ns_3_i_6142_variant_DataContents[2].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6142_variant_DataContents, (UA_Int32) 3, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6142LU),
UA_NODEID_NUMERIC(ns[3], 7017LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);



retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6142LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_191_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6142LU)
);
}

/* status - ns=3;i=6140 */

static UA_StatusCode function_namespace_mtpdm1_generated_192_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3004LU);
attr.displayName = UA_LOCALIZEDTEXT("", "status");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6140LU),
UA_NODEID_NUMERIC(ns[3], 5002LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "status"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6140LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_192_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6140LU)
);
}

/* coeffs - ns=3;i=6141 */

static UA_StatusCode function_namespace_mtpdm1_generated_193_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
attr.valueRank = 3;
attr.arrayDimensionsSize = 3;
UA_UInt32 arrayDimensions[3];
arrayDimensions[0] = 0;
arrayDimensions[1] = 4;
arrayDimensions[2] = 8;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 7LU);
attr.displayName = UA_LOCALIZEDTEXT("", "coeffs");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6141LU),
UA_NODEID_NUMERIC(ns[3], 5002LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "coeffs"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6141LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_193_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6141LU)
);
}

/* restart - ns=3;i=7021 */

static UA_StatusCode function_namespace_mtpdm1_generated_194_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "restart");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7021LU),
UA_NODEID_NUMERIC(ns[3], 1010LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "restart"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 7021LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_194_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7021LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* InputArguments - ns=3;i=6154 */

static UA_StatusCode function_namespace_mtpdm1_generated_195_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6154_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6154_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6154_variant_DataContents[0].name = UA_STRING("delay");
variablenode_ns_3_i_6154_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 7LU);
variablenode_ns_3_i_6154_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6154_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6154LU),
UA_NODEID_NUMERIC(ns[3], 7021LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6154LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_195_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6154LU)
);
}

/* OutputArguments - ns=3;i=6155 */

static UA_StatusCode function_namespace_mtpdm1_generated_196_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6155_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6155_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6155_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6155_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6155_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6155_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6155LU),
UA_NODEID_NUMERIC(ns[3], 7021LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6155LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_196_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6155LU)
);
}

/* bands - ns=3;i=6157 */

static UA_StatusCode function_namespace_mtpdm1_generated_197_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
attr.valueRank = 2;
attr.arrayDimensionsSize = 2;
UA_UInt32 arrayDimensions[2];
arrayDimensions[0] = 0;
arrayDimensions[1] = 8;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 7LU);
attr.displayName = UA_LOCALIZEDTEXT("", "bands");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6157LU),
UA_NODEID_NUMERIC(ns[3], 1010LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "bands"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6157LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_197_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6157LU)
);
}

/* NoiseTh - ns=3;i=5005 */

static UA_StatusCode function_namespace_mtpdm1_generated_198_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "NoiseTh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5005LU),
UA_NODEID_NUMERIC(ns[3], 1010LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "NoiseTh"),
UA_NODEID_NUMERIC(ns[0], 58LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 5005LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_198_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5005LU)
);
}

/* coeffs - ns=3;i=6137 */

static UA_StatusCode function_namespace_mtpdm1_generated_199_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
attr.valueRank = 3;
attr.arrayDimensionsSize = 3;
UA_UInt32 arrayDimensions[3];
arrayDimensions[0] = 0;
arrayDimensions[1] = 4;
arrayDimensions[2] = 9;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 7LU);
attr.displayName = UA_LOCALIZEDTEXT("", "coeffs");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6137LU),
UA_NODEID_NUMERIC(ns[3], 5005LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "coeffs"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6137LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_199_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6137LU)
);
}

/* status - ns=3;i=6136 */

static UA_StatusCode function_namespace_mtpdm1_generated_200_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3004LU);
attr.displayName = UA_LOCALIZEDTEXT("", "status");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6136LU),
UA_NODEID_NUMERIC(ns[3], 5005LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "status"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6136LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_200_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6136LU)
);
}

/* setup - ns=3;i=7016 */

static UA_StatusCode function_namespace_mtpdm1_generated_201_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "setup");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7016LU),
UA_NODEID_NUMERIC(ns[3], 5005LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "setup"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 7016LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_201_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7016LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* InputArguments - ns=3;i=6138 */

static UA_StatusCode function_namespace_mtpdm1_generated_202_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 2;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6138_variant_DataContents[2];

UA_init(&variablenode_ns_3_i_6138_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6138_variant_DataContents[0].name = UA_STRING("channel");
variablenode_ns_3_i_6138_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6138_variant_DataContents[0].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6138_variant_DataContents[1], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6138_variant_DataContents[1].name = UA_STRING("attenuator");
variablenode_ns_3_i_6138_variant_DataContents[1].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6138_variant_DataContents[1].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6138_variant_DataContents, (UA_Int32) 2, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6138LU),
UA_NODEID_NUMERIC(ns[3], 7016LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);


retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6138LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_202_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6138LU)
);
}

/* OutputArguments - ns=3;i=6139 */

static UA_StatusCode function_namespace_mtpdm1_generated_203_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6139_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6139_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6139_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6139_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6139_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6139_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6139LU),
UA_NODEID_NUMERIC(ns[3], 7016LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6139LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_203_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6139LU)
);
}

/* phaseComp - ns=3;i=6158 */

static UA_StatusCode function_namespace_mtpdm1_generated_204_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 0;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 4LU);
attr.displayName = UA_LOCALIZEDTEXT("", "phaseComp");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6158LU),
UA_NODEID_NUMERIC(ns[3], 1010LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "phaseComp"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6158LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_204_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6158LU)
);
}

/* gridFreq - ns=3;i=6024 */

static UA_StatusCode function_namespace_mtpdm1_generated_205_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3003LU);
attr.displayName = UA_LOCALIZEDTEXT("", "gridFreq");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6024LU),
UA_NODEID_NUMERIC(ns[3], 1010LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "gridFreq"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6024LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_205_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6024LU)
);
}

/* CONF - ns=3;i=5016 */

static UA_StatusCode function_namespace_mtpdm1_generated_206_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "CONF");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5016LU),
UA_NODEID_NUMERIC(ns[3], 5015LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "CONF"),
UA_NODEID_NUMERIC(ns[3], 1010LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_206_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5016LU)
);
}

/* restart - ns=3;i=7003 */

static UA_StatusCode function_namespace_mtpdm1_generated_207_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "restart");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7003LU),
UA_NODEID_NUMERIC(ns[3], 5016LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "restart"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_207_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7003LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* InputArguments - ns=3;i=6040 */

static UA_StatusCode function_namespace_mtpdm1_generated_208_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6040_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6040_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6040_variant_DataContents[0].name = UA_STRING("delay");
variablenode_ns_3_i_6040_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 7LU);
variablenode_ns_3_i_6040_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6040_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6040LU),
UA_NODEID_NUMERIC(ns[3], 7003LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_208_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6040LU)
);
}

/* OutputArguments - ns=3;i=6041 */

static UA_StatusCode function_namespace_mtpdm1_generated_209_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6041_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6041_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6041_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6041_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6041_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6041_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6041LU),
UA_NODEID_NUMERIC(ns[3], 7003LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_209_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6041LU)
);
}

/* changePass - ns=3;i=7009 */

static UA_StatusCode function_namespace_mtpdm1_generated_210_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "changePass");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7009LU),
UA_NODEID_NUMERIC(ns[3], 5016LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "changePass"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_210_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7009LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* InputArguments - ns=3;i=6042 */

static UA_StatusCode function_namespace_mtpdm1_generated_211_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 2;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6042_variant_DataContents[2];

UA_init(&variablenode_ns_3_i_6042_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6042_variant_DataContents[0].name = UA_STRING("username");
variablenode_ns_3_i_6042_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
variablenode_ns_3_i_6042_variant_DataContents[0].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6042_variant_DataContents[1], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6042_variant_DataContents[1].name = UA_STRING("password");
variablenode_ns_3_i_6042_variant_DataContents[1].dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
variablenode_ns_3_i_6042_variant_DataContents[1].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6042_variant_DataContents, (UA_Int32) 2, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6042LU),
UA_NODEID_NUMERIC(ns[3], 7009LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);


return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_211_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6042LU)
);
}

/* OutputArguments - ns=3;i=6043 */

static UA_StatusCode function_namespace_mtpdm1_generated_212_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6043_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6043_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6043_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6043_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6043_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6043_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6043LU),
UA_NODEID_NUMERIC(ns[3], 7009LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_212_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6043LU)
);
}

/* phaseComp - ns=3;i=6039 */

static UA_StatusCode function_namespace_mtpdm1_generated_213_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 0;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 4LU);
attr.displayName = UA_LOCALIZEDTEXT("", "phaseComp");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6039LU),
UA_NODEID_NUMERIC(ns[3], 5016LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "phaseComp"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_213_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6039LU)
);
}

/* bands - ns=3;i=6028 */

static UA_StatusCode function_namespace_mtpdm1_generated_214_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
attr.valueRank = 2;
attr.arrayDimensionsSize = 2;
UA_UInt32 arrayDimensions[2];
arrayDimensions[0] = 0;
arrayDimensions[1] = 8;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 7LU);
attr.displayName = UA_LOCALIZEDTEXT("", "bands");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6028LU),
UA_NODEID_NUMERIC(ns[3], 5016LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "bands"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_214_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6028LU)
);
}

/* NoiseTh - ns=3;i=5018 */

static UA_StatusCode function_namespace_mtpdm1_generated_215_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "NoiseTh");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5018LU),
UA_NODEID_NUMERIC(ns[3], 5016LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "NoiseTh"),
UA_NODEID_NUMERIC(ns[0], 58LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_215_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5018LU)
);
}

/* coeffs - ns=3;i=6035 */

static UA_StatusCode function_namespace_mtpdm1_generated_216_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
attr.valueRank = 3;
attr.arrayDimensionsSize = 3;
UA_UInt32 arrayDimensions[3];
arrayDimensions[0] = 0;
arrayDimensions[1] = 4;
arrayDimensions[2] = 9;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 7LU);
attr.displayName = UA_LOCALIZEDTEXT("", "coeffs");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6035LU),
UA_NODEID_NUMERIC(ns[3], 5018LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "coeffs"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_216_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6035LU)
);
}

/* status - ns=3;i=6038 */

static UA_StatusCode function_namespace_mtpdm1_generated_217_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3004LU);
attr.displayName = UA_LOCALIZEDTEXT("", "status");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6038LU),
UA_NODEID_NUMERIC(ns[3], 5018LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "status"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_217_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6038LU)
);
}

/* setup - ns=3;i=7002 */

static UA_StatusCode function_namespace_mtpdm1_generated_218_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "setup");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7002LU),
UA_NODEID_NUMERIC(ns[3], 5018LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "setup"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_218_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7002LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* OutputArguments - ns=3;i=6037 */

static UA_StatusCode function_namespace_mtpdm1_generated_219_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6037_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6037_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6037_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6037_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6037_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6037_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6037LU),
UA_NODEID_NUMERIC(ns[3], 7002LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_219_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6037LU)
);
}

/* InputArguments - ns=3;i=6036 */

static UA_StatusCode function_namespace_mtpdm1_generated_220_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 2;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6036_variant_DataContents[2];

UA_init(&variablenode_ns_3_i_6036_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6036_variant_DataContents[0].name = UA_STRING("channel");
variablenode_ns_3_i_6036_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6036_variant_DataContents[0].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6036_variant_DataContents[1], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6036_variant_DataContents[1].name = UA_STRING("attenuator");
variablenode_ns_3_i_6036_variant_DataContents[1].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6036_variant_DataContents[1].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6036_variant_DataContents, (UA_Int32) 2, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6036LU),
UA_NODEID_NUMERIC(ns[3], 7002LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);


return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_220_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6036LU)
);
}

/* Calib - ns=3;i=5017 */

static UA_StatusCode function_namespace_mtpdm1_generated_221_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Calib");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_NUMERIC(ns[3], 5017LU),
UA_NODEID_NUMERIC(ns[3], 5016LU),
UA_NODEID_NUMERIC(ns[1], 1LU),
UA_QUALIFIEDNAME(ns[3], "Calib"),
UA_NODEID_NUMERIC(ns[0], 58LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_221_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 5017LU)
);
}

/* setup - ns=3;i=7001 */

static UA_StatusCode function_namespace_mtpdm1_generated_222_begin(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_MethodAttributes attr = UA_MethodAttributes_default;
attr.executable = true;
attr.userExecutable = true;
attr.displayName = UA_LOCALIZEDTEXT("", "setup");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_METHOD,
UA_NODEID_NUMERIC(ns[3], 7001LU),
UA_NODEID_NUMERIC(ns[3], 5017LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "setup"),
 UA_NODEID_NULL,
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_METHODATTRIBUTES],NULL, NULL);
return retVal;
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

static UA_StatusCode function_namespace_mtpdm1_generated_222_finish(UA_Server *server, UA_UInt16* ns) {
#ifdef UA_ENABLE_METHODCALLS
return UA_Server_addMethodNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 7001LU)
, NULL, 0, NULL, 0, NULL);
#else
return UA_STATUSCODE_GOOD;
#endif /* UA_ENABLE_METHODCALLS */
}

/* OutputArguments - ns=3;i=6031 */

static UA_StatusCode function_namespace_mtpdm1_generated_223_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 1;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6031_variant_DataContents[1];

UA_init(&variablenode_ns_3_i_6031_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6031_variant_DataContents[0].name = UA_STRING("result");
variablenode_ns_3_i_6031_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[3], 3002LU);
variablenode_ns_3_i_6031_variant_DataContents[0].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6031_variant_DataContents, (UA_Int32) 1, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "OutputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6031LU),
UA_NODEID_NUMERIC(ns[3], 7001LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "OutputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);

return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_223_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6031LU)
);
}

/* InputArguments - ns=3;i=6030 */

static UA_StatusCode function_namespace_mtpdm1_generated_224_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 3;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 296LU);
UA_Argument variablenode_ns_3_i_6030_variant_DataContents[3];

UA_init(&variablenode_ns_3_i_6030_variant_DataContents[0], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6030_variant_DataContents[0].name = UA_STRING("channel");
variablenode_ns_3_i_6030_variant_DataContents[0].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6030_variant_DataContents[0].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6030_variant_DataContents[1], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6030_variant_DataContents[1].name = UA_STRING("attenuator");
variablenode_ns_3_i_6030_variant_DataContents[1].dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
variablenode_ns_3_i_6030_variant_DataContents[1].valueRank = (UA_Int32) -1;

UA_init(&variablenode_ns_3_i_6030_variant_DataContents[2], &UA_TYPES[UA_TYPES_ARGUMENT]);
variablenode_ns_3_i_6030_variant_DataContents[2].name = UA_STRING("reference");
variablenode_ns_3_i_6030_variant_DataContents[2].dataType = UA_NODEID_NUMERIC(ns[0], 7LU);
variablenode_ns_3_i_6030_variant_DataContents[2].valueRank = (UA_Int32) -1;
UA_Variant_setArray(&attr.value, &variablenode_ns_3_i_6030_variant_DataContents, (UA_Int32) 3, &UA_TYPES[UA_TYPES_ARGUMENT]);
attr.displayName = UA_LOCALIZEDTEXT("", "InputArguments");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6030LU),
UA_NODEID_NUMERIC(ns[3], 7001LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "InputArguments"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);



return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_224_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6030LU)
);
}

/* coeffs - ns=3;i=6029 */

static UA_StatusCode function_namespace_mtpdm1_generated_225_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
attr.valueRank = 3;
attr.arrayDimensionsSize = 3;
UA_UInt32 arrayDimensions[3];
arrayDimensions[0] = 0;
arrayDimensions[1] = 4;
arrayDimensions[2] = 8;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 7LU);
attr.displayName = UA_LOCALIZEDTEXT("", "coeffs");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6029LU),
UA_NODEID_NUMERIC(ns[3], 5017LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "coeffs"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_225_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6029LU)
);
}

/* status - ns=3;i=6032 */

static UA_StatusCode function_namespace_mtpdm1_generated_226_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3004LU);
attr.displayName = UA_LOCALIZEDTEXT("", "status");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6032LU),
UA_NODEID_NUMERIC(ns[3], 5017LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "status"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_226_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6032LU)
);
}

/* gridFreq - ns=3;i=6027 */

static UA_StatusCode function_namespace_mtpdm1_generated_227_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[3], 3003LU);
attr.displayName = UA_LOCALIZEDTEXT("", "gridFreq");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6027LU),
UA_NODEID_NUMERIC(ns[3], 5016LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "gridFreq"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_227_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6027LU)
);
}

/* channels - ns=3;i=6033 */

static UA_StatusCode function_namespace_mtpdm1_generated_228_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 3LU);
attr.displayName = UA_LOCALIZEDTEXT("", "channels");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6033LU),
UA_NODEID_NUMERIC(ns[3], 5016LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "channels"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_228_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6033LU)
);
}

/* extSync - ns=3;i=6034 */

static UA_StatusCode function_namespace_mtpdm1_generated_229_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 0;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 1LU);
attr.displayName = UA_LOCALIZEDTEXT("", "extSync");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6034LU),
UA_NODEID_NUMERIC(ns[3], 5016LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "extSync"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_229_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6034LU)
);
}

/* extSync - ns=3;i=6159 */

static UA_StatusCode function_namespace_mtpdm1_generated_230_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 3;
attr.valueRank = 1;
attr.arrayDimensionsSize = 1;
UA_UInt32 arrayDimensions[1];
arrayDimensions[0] = 0;
attr.arrayDimensions = &arrayDimensions[0];
attr.dataType = UA_NODEID_NUMERIC(ns[0], 1LU);
attr.displayName = UA_LOCALIZEDTEXT("", "extSync");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6159LU),
UA_NODEID_NUMERIC(ns[3], 1010LU),
UA_NODEID_NUMERIC(ns[1], 2LU),
UA_QUALIFIEDNAME(ns[3], "extSync"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
retVal |= UA_Server_addReference(server, UA_NODEID_NUMERIC(ns[3], 6159LU), UA_NODEID_NUMERIC(ns[0], 37LU), UA_EXPANDEDNODEID_NUMERIC(ns[0], 78LU), true);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_230_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6159LU)
);
}

/* TypeDictionary - ns=3;i=6012 */
static const UA_Byte variablenode_ns_3_i_6012_variant_DataContents_byteArray[3129] = {60, 120, 115, 58, 115, 99, 104, 101, 109, 97, 32, 101, 108, 101, 109, 101, 110, 116, 70, 111, 114, 109, 68, 101, 102, 97, 117, 108, 116, 61, 34, 113, 117, 97, 108, 105, 102, 105, 101, 100, 34, 32, 116, 97, 114, 103, 101, 116, 78, 97, 109, 101, 115, 112, 97, 99, 101, 61, 34, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 109, 111, 100, 101, 109, 116, 101, 99, 46, 99, 122, 47, 80, 68, 47, 84, 121, 112, 101, 115, 46, 120, 115, 100, 34, 32, 120, 109, 108, 110, 115, 58, 116, 110, 115, 61, 34, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 109, 111, 100, 101, 109, 116, 101, 99, 46, 99, 122, 47, 80, 68, 47, 84, 121, 112, 101, 115, 46, 120, 115, 100, 34, 32, 120, 109, 108, 110, 115, 58, 117, 97, 61, 34, 104, 116, 116, 112, 58, 47, 47, 111, 112, 99, 102, 111, 117, 110, 100, 97, 116, 105, 111, 110, 46, 111, 114, 103, 47, 85, 65, 47, 50, 48, 48, 56, 47, 48, 50, 47, 84, 121, 112, 101, 115, 46, 120, 115, 100, 34, 32, 120, 109, 108, 110, 115, 58, 120, 115, 61, 34, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 119, 51, 46, 111, 114, 103, 47, 50, 48, 48, 49, 47, 88, 77, 76, 83, 99, 104, 101, 109, 97, 34, 62, 10, 32, 60, 120, 115, 58, 105, 109, 112, 111, 114, 116, 32, 110, 97, 109, 101, 115, 112, 97, 99, 101, 61, 34, 104, 116, 116, 112, 58, 47, 47, 111, 112, 99, 102, 111, 117, 110, 100, 97, 116, 105, 111, 110, 46, 111, 114, 103, 47, 85, 65, 47, 50, 48, 48, 56, 47, 48, 50, 47, 84, 121, 112, 101, 115, 46, 120, 115, 100, 34, 47, 62, 10, 32, 60, 120, 115, 58, 115, 105, 109, 112, 108, 101, 84, 121, 112, 101, 32, 110, 97, 109, 101, 61, 34, 77, 116, 71, 114, 105, 100, 70, 114, 101, 113, 34, 62, 10, 32, 32, 60, 120, 115, 58, 114, 101, 115, 116, 114, 105, 99, 116, 105, 111, 110, 32, 98, 97, 115, 101, 61, 34, 120, 115, 58, 115, 116, 114, 105, 110, 103, 34, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 53, 48, 72, 122, 95, 48, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 54, 48, 72, 122, 95, 49, 34, 47, 62, 10, 32, 32, 60, 47, 120, 115, 58, 114, 101, 115, 116, 114, 105, 99, 116, 105, 111, 110, 62, 10, 32, 60, 47, 120, 115, 58, 115, 105, 109, 112, 108, 101, 84, 121, 112, 101, 62, 10, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 77, 116, 71, 114, 105, 100, 70, 114, 101, 113, 34, 32, 110, 97, 109, 101, 61, 34, 77, 116, 71, 114, 105, 100, 70, 114, 101, 113, 34, 47, 62, 10, 32, 60, 120, 115, 58, 99, 111, 109, 112, 108, 101, 120, 84, 121, 112, 101, 32, 110, 97, 109, 101, 61, 34, 76, 105, 115, 116, 79, 102, 77, 116, 71, 114, 105, 100, 70, 114, 101, 113, 34, 62, 10, 32, 32, 60, 120, 115, 58, 115, 101, 113, 117, 101, 110, 99, 101, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 109, 105, 110, 79, 99, 99, 117, 114, 115, 61, 34, 48, 34, 32, 109, 97, 120, 79, 99, 99, 117, 114, 115, 61, 34, 117, 110, 98, 111, 117, 110, 100, 101, 100, 34, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 77, 116, 71, 114, 105, 100, 70, 114, 101, 113, 34, 32, 110, 97, 109, 101, 61, 34, 77, 116, 71, 114, 105, 100, 70, 114, 101, 113, 34, 32, 110, 105, 108, 108, 97, 98, 108, 101, 61, 34, 116, 114, 117, 101, 34, 47, 62, 10, 32, 32, 60, 47, 120, 115, 58, 115, 101, 113, 117, 101, 110, 99, 101, 62, 10, 32, 60, 47, 120, 115, 58, 99, 111, 109, 112, 108, 101, 120, 84, 121, 112, 101, 62, 10, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 76, 105, 115, 116, 79, 102, 77, 116, 71, 114, 105, 100, 70, 114, 101, 113, 34, 32, 110, 97, 109, 101, 61, 34, 76, 105, 115, 116, 79, 102, 77, 116, 71, 114, 105, 100, 70, 114, 101, 113, 34, 32, 110, 105, 108, 108, 97, 98, 108, 101, 61, 34, 116, 114, 117, 101, 34, 47, 62, 10, 32, 60, 120, 115, 58, 115, 105, 109, 112, 108, 101, 84, 121, 112, 101, 32, 110, 97, 109, 101, 61, 34, 77, 116, 76, 111, 103, 76, 101, 118, 101, 108, 34, 62, 10, 32, 32, 60, 120, 115, 58, 114, 101, 115, 116, 114, 105, 99, 116, 105, 111, 110, 32, 98, 97, 115, 101, 61, 34, 120, 115, 58, 115, 116, 114, 105, 110, 103, 34, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 69, 109, 101, 114, 103, 101, 110, 99, 121, 95, 48, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 65, 108, 101, 114, 116, 95, 49, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 67, 114, 105, 116, 105, 99, 97, 108, 95, 50, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 69, 114, 114, 111, 114, 95, 51, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 87, 97, 114, 110, 105, 110, 103, 95, 52, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 78, 111, 116, 105, 99, 101, 95, 53, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 73, 110, 102, 111, 95, 54, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 68, 101, 98, 117, 103, 95, 55, 34, 47, 62, 10, 32, 32, 60, 47, 120, 115, 58, 114, 101, 115, 116, 114, 105, 99, 116, 105, 111, 110, 62, 10, 32, 60, 47, 120, 115, 58, 115, 105, 109, 112, 108, 101, 84, 121, 112, 101, 62, 10, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 77, 116, 76, 111, 103, 76, 101, 118, 101, 108, 34, 32, 110, 97, 109, 101, 61, 34, 77, 116, 76, 111, 103, 76, 101, 118, 101, 108, 34, 47, 62, 10, 32, 60, 120, 115, 58, 99, 111, 109, 112, 108, 101, 120, 84, 121, 112, 101, 32, 110, 97, 109, 101, 61, 34, 76, 105, 115, 116, 79, 102, 77, 116, 76, 111, 103, 76, 101, 118, 101, 108, 34, 62, 10, 32, 32, 60, 120, 115, 58, 115, 101, 113, 117, 101, 110, 99, 101, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 109, 105, 110, 79, 99, 99, 117, 114, 115, 61, 34, 48, 34, 32, 109, 97, 120, 79, 99, 99, 117, 114, 115, 61, 34, 117, 110, 98, 111, 117, 110, 100, 101, 100, 34, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 77, 116, 76, 111, 103, 76, 101, 118, 101, 108, 34, 32, 110, 97, 109, 101, 61, 34, 77, 116, 76, 111, 103, 76, 101, 118, 101, 108, 34, 32, 110, 105, 108, 108, 97, 98, 108, 101, 61, 34, 116, 114, 117, 101, 34, 47, 62, 10, 32, 32, 60, 47, 120, 115, 58, 115, 101, 113, 117, 101, 110, 99, 101, 62, 10, 32, 60, 47, 120, 115, 58, 99, 111, 109, 112, 108, 101, 120, 84, 121, 112, 101, 62, 10, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 76, 105, 115, 116, 79, 102, 77, 116, 76, 111, 103, 76, 101, 118, 101, 108, 34, 32, 110, 97, 109, 101, 61, 34, 76, 105, 115, 116, 79, 102, 77, 116, 76, 111, 103, 76, 101, 118, 101, 108, 34, 32, 110, 105, 108, 108, 97, 98, 108, 101, 61, 34, 116, 114, 117, 101, 34, 47, 62, 10, 32, 60, 120, 115, 58, 115, 105, 109, 112, 108, 101, 84, 121, 112, 101, 32, 110, 97, 109, 101, 61, 34, 77, 116, 77, 101, 97, 115, 117, 114, 101, 83, 116, 97, 116, 117, 115, 34, 62, 10, 32, 32, 60, 120, 115, 58, 114, 101, 115, 116, 114, 105, 99, 116, 105, 111, 110, 32, 98, 97, 115, 101, 61, 34, 120, 115, 58, 115, 116, 114, 105, 110, 103, 34, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 79, 107, 95, 48, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 66, 117, 115, 121, 95, 49, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 83, 116, 111, 114, 105, 110, 103, 95, 50, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 69, 114, 114, 111, 114, 95, 51, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 84, 105, 109, 101, 111, 117, 116, 95, 52, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 83, 105, 103, 110, 97, 108, 76, 111, 119, 95, 53, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 83, 105, 103, 110, 97, 108, 79, 118, 101, 114, 102, 111, 119, 95, 54, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 85, 110, 107, 110, 111, 119, 110, 95, 55, 34, 47, 62, 10, 32, 32, 60, 47, 120, 115, 58, 114, 101, 115, 116, 114, 105, 99, 116, 105, 111, 110, 62, 10, 32, 60, 47, 120, 115, 58, 115, 105, 109, 112, 108, 101, 84, 121, 112, 101, 62, 10, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 77, 116, 77, 101, 97, 115, 117, 114, 101, 83, 116, 97, 116, 117, 115, 34, 32, 110, 97, 109, 101, 61, 34, 77, 116, 77, 101, 97, 115, 117, 114, 101, 83, 116, 97, 116, 117, 115, 34, 47, 62, 10, 32, 60, 120, 115, 58, 99, 111, 109, 112, 108, 101, 120, 84, 121, 112, 101, 32, 110, 97, 109, 101, 61, 34, 76, 105, 115, 116, 79, 102, 77, 116, 77, 101, 97, 115, 117, 114, 101, 83, 116, 97, 116, 117, 115, 34, 62, 10, 32, 32, 60, 120, 115, 58, 115, 101, 113, 117, 101, 110, 99, 101, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 109, 105, 110, 79, 99, 99, 117, 114, 115, 61, 34, 48, 34, 32, 109, 97, 120, 79, 99, 99, 117, 114, 115, 61, 34, 117, 110, 98, 111, 117, 110, 100, 101, 100, 34, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 77, 116, 77, 101, 97, 115, 117, 114, 101, 83, 116, 97, 116, 117, 115, 34, 32, 110, 97, 109, 101, 61, 34, 77, 116, 77, 101, 97, 115, 117, 114, 101, 83, 116, 97, 116, 117, 115, 34, 32, 110, 105, 108, 108, 97, 98, 108, 101, 61, 34, 116, 114, 117, 101, 34, 47, 62, 10, 32, 32, 60, 47, 120, 115, 58, 115, 101, 113, 117, 101, 110, 99, 101, 62, 10, 32, 60, 47, 120, 115, 58, 99, 111, 109, 112, 108, 101, 120, 84, 121, 112, 101, 62, 10, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 76, 105, 115, 116, 79, 102, 77, 116, 77, 101, 97, 115, 117, 114, 101, 83, 116, 97, 116, 117, 115, 34, 32, 110, 97, 109, 101, 61, 34, 76, 105, 115, 116, 79, 102, 77, 116, 77, 101, 97, 115, 117, 114, 101, 83, 116, 97, 116, 117, 115, 34, 32, 110, 105, 108, 108, 97, 98, 108, 101, 61, 34, 116, 114, 117, 101, 34, 47, 62, 10, 32, 60, 120, 115, 58, 115, 105, 109, 112, 108, 101, 84, 121, 112, 101, 32, 110, 97, 109, 101, 61, 34, 77, 116, 82, 101, 113, 117, 101, 115, 116, 82, 101, 115, 117, 108, 116, 34, 62, 10, 32, 32, 60, 120, 115, 58, 114, 101, 115, 116, 114, 105, 99, 116, 105, 111, 110, 32, 98, 97, 115, 101, 61, 34, 120, 115, 58, 115, 116, 114, 105, 110, 103, 34, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 79, 107, 95, 48, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 69, 114, 114, 111, 114, 95, 49, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 66, 117, 115, 121, 95, 50, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 73, 110, 118, 97, 108, 105, 100, 65, 114, 103, 117, 109, 101, 110, 116, 95, 51, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 84, 105, 109, 101, 111, 117, 116, 95, 52, 34, 47, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 110, 117, 109, 101, 114, 97, 116, 105, 111, 110, 32, 118, 97, 108, 117, 101, 61, 34, 65, 99, 99, 101, 115, 115, 95, 53, 34, 47, 62, 10, 32, 32, 60, 47, 120, 115, 58, 114, 101, 115, 116, 114, 105, 99, 116, 105, 111, 110, 62, 10, 32, 60, 47, 120, 115, 58, 115, 105, 109, 112, 108, 101, 84, 121, 112, 101, 62, 10, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 77, 116, 82, 101, 113, 117, 101, 115, 116, 82, 101, 115, 117, 108, 116, 34, 32, 110, 97, 109, 101, 61, 34, 77, 116, 82, 101, 113, 117, 101, 115, 116, 82, 101, 115, 117, 108, 116, 34, 47, 62, 10, 32, 60, 120, 115, 58, 99, 111, 109, 112, 108, 101, 120, 84, 121, 112, 101, 32, 110, 97, 109, 101, 61, 34, 76, 105, 115, 116, 79, 102, 77, 116, 82, 101, 113, 117, 101, 115, 116, 82, 101, 115, 117, 108, 116, 34, 62, 10, 32, 32, 60, 120, 115, 58, 115, 101, 113, 117, 101, 110, 99, 101, 62, 10, 32, 32, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 109, 105, 110, 79, 99, 99, 117, 114, 115, 61, 34, 48, 34, 32, 109, 97, 120, 79, 99, 99, 117, 114, 115, 61, 34, 117, 110, 98, 111, 117, 110, 100, 101, 100, 34, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 77, 116, 82, 101, 113, 117, 101, 115, 116, 82, 101, 115, 117, 108, 116, 34, 32, 110, 97, 109, 101, 61, 34, 77, 116, 82, 101, 113, 117, 101, 115, 116, 82, 101, 115, 117, 108, 116, 34, 32, 110, 105, 108, 108, 97, 98, 108, 101, 61, 34, 116, 114, 117, 101, 34, 47, 62, 10, 32, 32, 60, 47, 120, 115, 58, 115, 101, 113, 117, 101, 110, 99, 101, 62, 10, 32, 60, 47, 120, 115, 58, 99, 111, 109, 112, 108, 101, 120, 84, 121, 112, 101, 62, 10, 32, 60, 120, 115, 58, 101, 108, 101, 109, 101, 110, 116, 32, 116, 121, 112, 101, 61, 34, 116, 110, 115, 58, 76, 105, 115, 116, 79, 102, 77, 116, 82, 101, 113, 117, 101, 115, 116, 82, 101, 115, 117, 108, 116, 34, 32, 110, 97, 109, 101, 61, 34, 76, 105, 115, 116, 79, 102, 77, 116, 82, 101, 113, 117, 101, 115, 116, 82, 101, 115, 117, 108, 116, 34, 32, 110, 105, 108, 108, 97, 98, 108, 101, 61, 34, 116, 114, 117, 101, 34, 47, 62, 10, 60, 47, 120, 115, 58, 115, 99, 104, 101, 109, 97, 62, 10};



static UA_StatusCode function_namespace_mtpdm1_generated_231_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 15LU);
UA_ByteString *variablenode_ns_3_i_6012_variant_DataContents =  UA_ByteString_new();
if (!variablenode_ns_3_i_6012_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_ByteString_init(variablenode_ns_3_i_6012_variant_DataContents);
variablenode_ns_3_i_6012_variant_DataContents->length = 3129;
variablenode_ns_3_i_6012_variant_DataContents->data = (UA_Byte *)(void*)(uintptr_t)variablenode_ns_3_i_6012_variant_DataContents_byteArray;
UA_Variant_setScalar(&attr.value, variablenode_ns_3_i_6012_variant_DataContents, &UA_TYPES[UA_TYPES_BYTESTRING]);
attr.displayName = UA_LOCALIZEDTEXT("", "TypeDictionary");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "Collects the data type descriptions of http://www.modemtec.cz/PD/");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6012LU),
UA_NODEID_NUMERIC(ns[0], 92LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "TypeDictionary"),
UA_NODEID_NUMERIC(ns[0], 72LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
variablenode_ns_3_i_6012_variant_DataContents->data = NULL;
variablenode_ns_3_i_6012_variant_DataContents->length = 0;
UA_ByteString_delete(variablenode_ns_3_i_6012_variant_DataContents);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_231_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6012LU)
);
}

/* NamespaceUri - ns=3;i=6016 */

static UA_StatusCode function_namespace_mtpdm1_generated_232_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
UA_String *variablenode_ns_3_i_6016_variant_DataContents =  UA_String_new();
if (!variablenode_ns_3_i_6016_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_String_init(variablenode_ns_3_i_6016_variant_DataContents);
*variablenode_ns_3_i_6016_variant_DataContents = UA_STRING_ALLOC("http://www.modemtec.cz/PD/Types.xsd");
UA_Variant_setScalar(&attr.value, variablenode_ns_3_i_6016_variant_DataContents, &UA_TYPES[UA_TYPES_STRING]);
attr.displayName = UA_LOCALIZEDTEXT("", "NamespaceUri");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6016LU),
UA_NODEID_NUMERIC(ns[3], 6012LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "NamespaceUri"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_String_delete(variablenode_ns_3_i_6016_variant_DataContents);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_232_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6016LU)
);
}

/* TypeDictionary - ns=3;i=6010 */
static const UA_Byte variablenode_ns_3_i_6010_variant_DataContents_byteArray[1876] = {60, 111, 112, 99, 58, 84, 121, 112, 101, 68, 105, 99, 116, 105, 111, 110, 97, 114, 121, 32, 120, 109, 108, 110, 115, 58, 120, 115, 105, 61, 34, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 119, 51, 46, 111, 114, 103, 47, 50, 48, 48, 49, 47, 88, 77, 76, 83, 99, 104, 101, 109, 97, 45, 105, 110, 115, 116, 97, 110, 99, 101, 34, 32, 120, 109, 108, 110, 115, 58, 116, 110, 115, 61, 34, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 109, 111, 100, 101, 109, 116, 101, 99, 46, 99, 122, 47, 80, 68, 47, 34, 32, 68, 101, 102, 97, 117, 108, 116, 66, 121, 116, 101, 79, 114, 100, 101, 114, 61, 34, 76, 105, 116, 116, 108, 101, 69, 110, 100, 105, 97, 110, 34, 32, 120, 109, 108, 110, 115, 58, 111, 112, 99, 61, 34, 104, 116, 116, 112, 58, 47, 47, 111, 112, 99, 102, 111, 117, 110, 100, 97, 116, 105, 111, 110, 46, 111, 114, 103, 47, 66, 105, 110, 97, 114, 121, 83, 99, 104, 101, 109, 97, 47, 34, 32, 120, 109, 108, 110, 115, 58, 117, 97, 61, 34, 104, 116, 116, 112, 58, 47, 47, 111, 112, 99, 102, 111, 117, 110, 100, 97, 116, 105, 111, 110, 46, 111, 114, 103, 47, 85, 65, 47, 34, 32, 84, 97, 114, 103, 101, 116, 78, 97, 109, 101, 115, 112, 97, 99, 101, 61, 34, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 109, 111, 100, 101, 109, 116, 101, 99, 46, 99, 122, 47, 80, 68, 47, 34, 62, 10, 32, 60, 111, 112, 99, 58, 73, 109, 112, 111, 114, 116, 32, 78, 97, 109, 101, 115, 112, 97, 99, 101, 61, 34, 104, 116, 116, 112, 58, 47, 47, 111, 112, 99, 102, 111, 117, 110, 100, 97, 116, 105, 111, 110, 46, 111, 114, 103, 47, 85, 65, 47, 34, 47, 62, 10, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 84, 121, 112, 101, 32, 76, 101, 110, 103, 116, 104, 73, 110, 66, 105, 116, 115, 61, 34, 51, 50, 34, 32, 78, 97, 109, 101, 61, 34, 77, 116, 71, 114, 105, 100, 70, 114, 101, 113, 34, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 53, 48, 72, 122, 34, 32, 86, 97, 108, 117, 101, 61, 34, 48, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 54, 48, 72, 122, 34, 32, 86, 97, 108, 117, 101, 61, 34, 49, 34, 47, 62, 10, 32, 60, 47, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 84, 121, 112, 101, 62, 10, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 84, 121, 112, 101, 32, 76, 101, 110, 103, 116, 104, 73, 110, 66, 105, 116, 115, 61, 34, 51, 50, 34, 32, 78, 97, 109, 101, 61, 34, 77, 116, 76, 111, 103, 76, 101, 118, 101, 108, 34, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 69, 109, 101, 114, 103, 101, 110, 99, 121, 34, 32, 86, 97, 108, 117, 101, 61, 34, 48, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 65, 108, 101, 114, 116, 34, 32, 86, 97, 108, 117, 101, 61, 34, 49, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 67, 114, 105, 116, 105, 99, 97, 108, 34, 32, 86, 97, 108, 117, 101, 61, 34, 50, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 69, 114, 114, 111, 114, 34, 32, 86, 97, 108, 117, 101, 61, 34, 51, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 87, 97, 114, 110, 105, 110, 103, 34, 32, 86, 97, 108, 117, 101, 61, 34, 52, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 78, 111, 116, 105, 99, 101, 34, 32, 86, 97, 108, 117, 101, 61, 34, 53, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 73, 110, 102, 111, 34, 32, 86, 97, 108, 117, 101, 61, 34, 54, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 68, 101, 98, 117, 103, 34, 32, 86, 97, 108, 117, 101, 61, 34, 55, 34, 47, 62, 10, 32, 60, 47, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 84, 121, 112, 101, 62, 10, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 84, 121, 112, 101, 32, 76, 101, 110, 103, 116, 104, 73, 110, 66, 105, 116, 115, 61, 34, 51, 50, 34, 32, 78, 97, 109, 101, 61, 34, 77, 116, 77, 101, 97, 115, 117, 114, 101, 83, 116, 97, 116, 117, 115, 34, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 79, 107, 34, 32, 86, 97, 108, 117, 101, 61, 34, 48, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 66, 117, 115, 121, 34, 32, 86, 97, 108, 117, 101, 61, 34, 49, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 83, 116, 111, 114, 105, 110, 103, 34, 32, 86, 97, 108, 117, 101, 61, 34, 50, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 69, 114, 114, 111, 114, 34, 32, 86, 97, 108, 117, 101, 61, 34, 51, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 84, 105, 109, 101, 111, 117, 116, 34, 32, 86, 97, 108, 117, 101, 61, 34, 52, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 83, 105, 103, 110, 97, 108, 76, 111, 119, 34, 32, 86, 97, 108, 117, 101, 61, 34, 53, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 83, 105, 103, 110, 97, 108, 79, 118, 101, 114, 102, 111, 119, 34, 32, 86, 97, 108, 117, 101, 61, 34, 54, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 85, 110, 107, 110, 111, 119, 110, 34, 32, 86, 97, 108, 117, 101, 61, 34, 55, 34, 47, 62, 10, 32, 60, 47, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 84, 121, 112, 101, 62, 10, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 84, 121, 112, 101, 32, 76, 101, 110, 103, 116, 104, 73, 110, 66, 105, 116, 115, 61, 34, 51, 50, 34, 32, 78, 97, 109, 101, 61, 34, 77, 116, 82, 101, 113, 117, 101, 115, 116, 82, 101, 115, 117, 108, 116, 34, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 79, 107, 34, 32, 86, 97, 108, 117, 101, 61, 34, 48, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 69, 114, 114, 111, 114, 34, 32, 86, 97, 108, 117, 101, 61, 34, 49, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 66, 117, 115, 121, 34, 32, 86, 97, 108, 117, 101, 61, 34, 50, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 73, 110, 118, 97, 108, 105, 100, 65, 114, 103, 117, 109, 101, 110, 116, 34, 32, 86, 97, 108, 117, 101, 61, 34, 51, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 84, 105, 109, 101, 111, 117, 116, 34, 32, 86, 97, 108, 117, 101, 61, 34, 52, 34, 47, 62, 10, 32, 32, 60, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 86, 97, 108, 117, 101, 32, 78, 97, 109, 101, 61, 34, 65, 99, 99, 101, 115, 115, 34, 32, 86, 97, 108, 117, 101, 61, 34, 53, 34, 47, 62, 10, 32, 60, 47, 111, 112, 99, 58, 69, 110, 117, 109, 101, 114, 97, 116, 101, 100, 84, 121, 112, 101, 62, 10, 60, 47, 111, 112, 99, 58, 84, 121, 112, 101, 68, 105, 99, 116, 105, 111, 110, 97, 114, 121, 62, 10};



static UA_StatusCode function_namespace_mtpdm1_generated_233_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 15LU);
UA_ByteString *variablenode_ns_3_i_6010_variant_DataContents =  UA_ByteString_new();
if (!variablenode_ns_3_i_6010_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_ByteString_init(variablenode_ns_3_i_6010_variant_DataContents);
variablenode_ns_3_i_6010_variant_DataContents->length = 1876;
variablenode_ns_3_i_6010_variant_DataContents->data = (UA_Byte *)(void*)(uintptr_t)variablenode_ns_3_i_6010_variant_DataContents_byteArray;
UA_Variant_setScalar(&attr.value, variablenode_ns_3_i_6010_variant_DataContents, &UA_TYPES[UA_TYPES_BYTESTRING]);
attr.displayName = UA_LOCALIZEDTEXT("", "TypeDictionary");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "Collects the data type descriptions of http://www.modemtec.cz/PD/");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6010LU),
UA_NODEID_NUMERIC(ns[0], 93LU),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[3], "TypeDictionary"),
UA_NODEID_NUMERIC(ns[0], 72LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
variablenode_ns_3_i_6010_variant_DataContents->data = NULL;
variablenode_ns_3_i_6010_variant_DataContents->length = 0;
UA_ByteString_delete(variablenode_ns_3_i_6010_variant_DataContents);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_233_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6010LU)
);
}

/* NamespaceUri - ns=3;i=6011 */

static UA_StatusCode function_namespace_mtpdm1_generated_234_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -1;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 12LU);
UA_String *variablenode_ns_3_i_6011_variant_DataContents =  UA_String_new();
if (!variablenode_ns_3_i_6011_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_String_init(variablenode_ns_3_i_6011_variant_DataContents);
*variablenode_ns_3_i_6011_variant_DataContents = UA_STRING_ALLOC("http://www.modemtec.cz/PD/");
UA_Variant_setScalar(&attr.value, variablenode_ns_3_i_6011_variant_DataContents, &UA_TYPES[UA_TYPES_STRING]);
attr.displayName = UA_LOCALIZEDTEXT("", "NamespaceUri");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_NUMERIC(ns[3], 6011LU),
UA_NODEID_NUMERIC(ns[3], 6010LU),
UA_NODEID_NUMERIC(ns[0], 46LU),
UA_QUALIFIEDNAME(ns[0], "NamespaceUri"),
UA_NODEID_NUMERIC(ns[0], 68LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_String_delete(variablenode_ns_3_i_6011_variant_DataContents);
return retVal;
}

static UA_StatusCode function_namespace_mtpdm1_generated_234_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_NUMERIC(ns[3], 6011LU)
);
}

static UA_DataTypeArray customUA_TYPES_MTPDM1 = {
    NULL,
    UA_TYPES_MTPDM1_COUNT,
    UA_TYPES_MTPDM1
};

static UA_DataTypeArray customUA_TYPES_IEC7_4 = {
    NULL,
    UA_TYPES_IEC7_4_COUNT,
    UA_TYPES_IEC7_4
};

static UA_DataTypeArray customUA_TYPES_IEC7_3 = {
    NULL,
    UA_TYPES_IEC7_3_COUNT,
    UA_TYPES_IEC7_3
};

UA_StatusCode namespace_mtpdm1_generated(UA_Server *server) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
/* Use namespace ids generated by the server */
UA_UInt16 ns[4];
ns[0] = UA_Server_addNamespace(server, "http://opcfoundation.org/UA/");
ns[1] = UA_Server_addNamespace(server, "http://opcfoundation.org/UA/IEC61850-7-3");
ns[2] = UA_Server_addNamespace(server, "http://opcfoundation.org/UA/IEC61850-7-4");
ns[3] = UA_Server_addNamespace(server, "http://www.modemtec.cz/PD/");

/* Load custom datatype definitions into the server */
if(UA_TYPES_MTPDM1_COUNT > 0) {
customUA_TYPES_MTPDM1.next = UA_Server_getConfig(server)->customDataTypes;
UA_Server_getConfig(server)->customDataTypes = &customUA_TYPES_MTPDM1;

}
if(UA_TYPES_IEC7_4_COUNT > 0) {
customUA_TYPES_IEC7_4.next = UA_Server_getConfig(server)->customDataTypes;
UA_Server_getConfig(server)->customDataTypes = &customUA_TYPES_IEC7_4;

}
if(UA_TYPES_IEC7_3_COUNT > 0) {
customUA_TYPES_IEC7_3.next = UA_Server_getConfig(server)->customDataTypes;
UA_Server_getConfig(server)->customDataTypes = &customUA_TYPES_IEC7_3;

}
bool dummy = (
!(retVal = function_namespace_mtpdm1_generated_0_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_1_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_2_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_3_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_4_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_5_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_6_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_7_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_8_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_9_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_10_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_11_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_12_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_13_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_14_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_15_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_16_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_17_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_18_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_19_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_20_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_21_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_22_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_23_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_24_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_25_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_26_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_27_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_28_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_29_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_30_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_31_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_32_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_33_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_34_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_35_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_36_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_37_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_38_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_39_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_40_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_41_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_42_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_43_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_44_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_45_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_46_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_47_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_48_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_49_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_50_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_51_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_52_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_53_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_54_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_55_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_56_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_57_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_58_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_59_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_60_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_61_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_62_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_63_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_64_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_65_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_66_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_67_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_68_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_69_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_70_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_71_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_72_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_73_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_74_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_75_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_76_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_77_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_78_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_79_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_80_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_81_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_82_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_83_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_84_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_85_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_86_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_87_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_88_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_89_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_90_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_91_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_92_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_93_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_94_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_95_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_96_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_97_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_98_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_99_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_100_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_101_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_102_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_103_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_104_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_105_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_106_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_107_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_108_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_109_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_110_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_111_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_112_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_113_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_114_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_115_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_116_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_117_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_118_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_119_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_120_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_121_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_122_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_123_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_124_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_125_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_126_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_127_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_128_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_129_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_130_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_131_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_132_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_133_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_134_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_135_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_136_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_137_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_138_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_139_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_140_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_141_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_142_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_143_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_144_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_145_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_146_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_147_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_148_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_149_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_150_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_151_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_152_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_153_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_154_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_155_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_156_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_157_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_158_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_159_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_160_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_161_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_162_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_163_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_164_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_165_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_166_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_167_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_168_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_169_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_170_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_171_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_172_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_173_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_174_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_175_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_176_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_177_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_178_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_179_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_180_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_181_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_182_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_183_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_184_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_185_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_186_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_187_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_188_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_189_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_190_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_191_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_192_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_193_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_194_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_195_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_196_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_197_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_198_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_199_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_200_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_201_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_202_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_203_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_204_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_205_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_206_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_207_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_208_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_209_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_210_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_211_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_212_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_213_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_214_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_215_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_216_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_217_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_218_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_219_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_220_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_221_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_222_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_223_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_224_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_225_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_226_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_227_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_228_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_229_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_230_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_231_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_232_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_233_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_234_begin(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_234_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_233_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_232_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_231_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_230_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_229_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_228_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_227_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_226_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_225_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_224_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_223_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_222_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_221_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_220_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_219_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_218_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_217_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_216_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_215_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_214_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_213_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_212_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_211_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_210_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_209_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_208_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_207_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_206_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_205_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_204_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_203_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_202_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_201_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_200_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_199_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_198_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_197_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_196_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_195_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_194_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_193_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_192_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_191_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_190_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_189_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_188_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_187_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_186_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_185_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_184_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_183_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_182_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_181_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_180_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_179_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_178_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_177_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_176_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_175_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_174_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_173_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_172_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_171_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_170_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_169_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_168_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_167_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_166_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_165_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_164_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_163_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_162_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_161_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_160_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_159_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_158_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_157_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_156_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_155_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_154_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_153_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_152_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_151_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_150_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_149_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_148_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_147_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_146_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_145_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_144_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_143_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_142_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_141_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_140_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_139_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_138_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_137_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_136_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_135_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_134_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_133_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_132_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_131_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_130_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_129_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_128_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_127_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_126_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_125_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_124_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_123_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_122_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_121_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_120_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_119_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_118_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_117_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_116_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_115_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_114_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_113_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_112_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_111_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_110_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_109_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_108_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_107_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_106_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_105_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_104_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_103_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_102_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_101_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_100_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_99_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_98_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_97_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_96_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_95_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_94_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_93_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_92_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_91_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_90_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_89_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_88_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_87_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_86_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_85_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_84_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_83_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_82_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_81_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_80_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_79_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_78_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_77_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_76_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_75_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_74_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_73_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_72_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_71_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_70_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_69_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_68_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_67_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_66_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_65_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_64_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_63_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_62_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_61_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_60_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_59_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_58_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_57_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_56_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_55_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_54_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_53_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_52_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_51_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_50_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_49_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_48_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_47_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_46_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_45_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_44_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_43_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_42_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_41_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_40_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_39_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_38_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_37_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_36_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_35_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_34_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_33_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_32_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_31_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_30_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_29_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_28_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_27_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_26_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_25_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_24_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_23_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_22_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_21_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_20_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_19_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_18_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_17_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_16_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_15_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_14_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_13_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_12_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_11_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_10_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_9_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_8_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_7_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_6_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_5_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_4_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_3_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_2_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_1_finish(server, ns))
&& !(retVal = function_namespace_mtpdm1_generated_0_finish(server, ns))
); (void)(dummy);
return retVal;
}

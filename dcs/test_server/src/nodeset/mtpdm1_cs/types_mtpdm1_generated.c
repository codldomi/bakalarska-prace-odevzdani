/* Generated from ModemTec.PDM1.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py
 * on host martin-dev4 by user martin at 2021-11-29 10:24:00 */

#include "types_mtpdm1_generated.h"

/* MtGridFreq */
#define MtGridFreq_members NULL

/* MtLogLevel */
#define MtLogLevel_members NULL

/* MtMeasureStatus */
#define MtMeasureStatus_members NULL

/* MtRequestResult */
#define MtRequestResult_members NULL
const UA_DataType UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_COUNT] = {
/* MtGridFreq */
{
    {4, UA_NODEIDTYPE_NUMERIC, {3003LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_MtGridFreq), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    MtGridFreq_members  /* .members */
    UA_TYPENAME("MtGridFreq") /* .typeName */
},
/* MtLogLevel */
{
    {4, UA_NODEIDTYPE_NUMERIC, {3005LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_MtLogLevel), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    MtLogLevel_members  /* .members */
    UA_TYPENAME("MtLogLevel") /* .typeName */
},
/* MtMeasureStatus */
{
    {4, UA_NODEIDTYPE_NUMERIC, {3004LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_MtMeasureStatus), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    MtMeasureStatus_members  /* .members */
    UA_TYPENAME("MtMeasureStatus") /* .typeName */
},
/* MtRequestResult */
{
    {4, UA_NODEIDTYPE_NUMERIC, {3002LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_MtRequestResult), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    MtRequestResult_members  /* .members */
    UA_TYPENAME("MtRequestResult") /* .typeName */
},
};


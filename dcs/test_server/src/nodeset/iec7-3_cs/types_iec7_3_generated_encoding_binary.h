/* Generated from Opc.Ua.IEC61850-7-3.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py
 * on host martin-dev4 by user martin at 2021-11-29 10:23:27 */

#ifndef TYPES_IEC7_3_GENERATED_ENCODING_BINARY_H_
#define TYPES_IEC7_3_GENERATED_ENCODING_BINARY_H_

#ifdef UA_ENABLE_AMALGAMATION
# include "open62541.h"
#else
# include "ua_types_encoding_binary.h"
# include "types_iec7_3_generated.h"
#endif



/* ValidityKind */
static UA_INLINE size_t
UA_ValidityKind_calcSizeBinary(const UA_ValidityKind *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_VALIDITYKIND]);
}
static UA_INLINE UA_StatusCode
UA_ValidityKind_encodeBinary(const UA_ValidityKind *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_VALIDITYKIND], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_ValidityKind_decodeBinary(const UA_ByteString *src, size_t *offset, UA_ValidityKind *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_VALIDITYKIND], NULL);
}

/* SourceKind */
static UA_INLINE size_t
UA_SourceKind_calcSizeBinary(const UA_SourceKind *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SOURCEKIND]);
}
static UA_INLINE UA_StatusCode
UA_SourceKind_encodeBinary(const UA_SourceKind *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SOURCEKIND], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_SourceKind_decodeBinary(const UA_ByteString *src, size_t *offset, UA_SourceKind *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SOURCEKIND], NULL);
}

/* SIUnitKind */
static UA_INLINE size_t
UA_SIUnitKind_calcSizeBinary(const UA_SIUnitKind *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SIUNITKIND]);
}
static UA_INLINE UA_StatusCode
UA_SIUnitKind_encodeBinary(const UA_SIUnitKind *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SIUNITKIND], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_SIUnitKind_decodeBinary(const UA_ByteString *src, size_t *offset, UA_SIUnitKind *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SIUNITKIND], NULL);
}

/* MultiplierKind */
static UA_INLINE size_t
UA_MultiplierKind_calcSizeBinary(const UA_MultiplierKind *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_MULTIPLIERKIND]);
}
static UA_INLINE UA_StatusCode
UA_MultiplierKind_encodeBinary(const UA_MultiplierKind *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_MULTIPLIERKIND], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_MultiplierKind_decodeBinary(const UA_ByteString *src, size_t *offset, UA_MultiplierKind *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_MULTIPLIERKIND], NULL);
}

/* DetailQual */
static UA_INLINE size_t
UA_DetailQual_calcSizeBinary(const UA_DetailQual *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_DETAILQUAL]);
}
static UA_INLINE UA_StatusCode
UA_DetailQual_encodeBinary(const UA_DetailQual *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_DETAILQUAL], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_DetailQual_decodeBinary(const UA_ByteString *src, size_t *offset, UA_DetailQual *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_DETAILQUAL], NULL);
}

/* Quality */
static UA_INLINE size_t
UA_Quality_calcSizeBinary(const UA_Quality *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY]);
}
static UA_INLINE UA_StatusCode
UA_Quality_encodeBinary(const UA_Quality *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_Quality_decodeBinary(const UA_ByteString *src, size_t *offset, UA_Quality *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY], NULL);
}

/* AnalogueValue */
static UA_INLINE size_t
UA_AnalogueValue_calcSizeBinary(const UA_AnalogueValue *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_ANALOGUEVALUE]);
}
static UA_INLINE UA_StatusCode
UA_AnalogueValue_encodeBinary(const UA_AnalogueValue *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_ANALOGUEVALUE], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_AnalogueValue_decodeBinary(const UA_ByteString *src, size_t *offset, UA_AnalogueValue *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_ANALOGUEVALUE], NULL);
}

/* Unit */
static UA_INLINE size_t
UA_Unit_calcSizeBinary(const UA_Unit *src) {
    return UA_calcSizeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_UNIT]);
}
static UA_INLINE UA_StatusCode
UA_Unit_encodeBinary(const UA_Unit *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_UNIT], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_Unit_decodeBinary(const UA_ByteString *src, size_t *offset, UA_Unit *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_UNIT], NULL);
}

#endif /* TYPES_IEC7_3_GENERATED_ENCODING_BINARY_H_ */

/* Generated from Opc.Ua.IEC61850-7-3.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py
 * on host martin-dev4 by user martin at 2021-11-29 10:23:27 */

#include "types_iec7_3_generated.h"

/* ValidityKind */
#define ValidityKind_members NULL

/* SourceKind */
#define SourceKind_members NULL

/* SIUnitKind */
#define SIUnitKind_members NULL

/* MultiplierKind */
#define MultiplierKind_members NULL

/* DetailQual */
static UA_DataTypeMember DetailQual_members[8] = {
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Overflow") /* .memberName */
},
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    offsetof(UA_DetailQual, outOfRange) - offsetof(UA_DetailQual, overflow) - sizeof(UA_Boolean), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("OutOfRange") /* .memberName */
},
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    offsetof(UA_DetailQual, badReference) - offsetof(UA_DetailQual, outOfRange) - sizeof(UA_Boolean), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("BadReference") /* .memberName */
},
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    offsetof(UA_DetailQual, oscillatory) - offsetof(UA_DetailQual, badReference) - sizeof(UA_Boolean), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Oscillatory") /* .memberName */
},
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    offsetof(UA_DetailQual, failure) - offsetof(UA_DetailQual, oscillatory) - sizeof(UA_Boolean), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Failure") /* .memberName */
},
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    offsetof(UA_DetailQual, oldData) - offsetof(UA_DetailQual, failure) - sizeof(UA_Boolean), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("OldData") /* .memberName */
},
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    offsetof(UA_DetailQual, inconsistent) - offsetof(UA_DetailQual, oldData) - sizeof(UA_Boolean), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Inconsistent") /* .memberName */
},
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    offsetof(UA_DetailQual, inaccurate) - offsetof(UA_DetailQual, inconsistent) - sizeof(UA_Boolean), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Inaccurate") /* .memberName */
},};

/* Quality */
static UA_DataTypeMember Quality_members[5] = {
{
    UA_TYPES_IEC7_3_VALIDITYKIND, /* .memberTypeIndex */
    0, /* .padding */
    false, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Validity") /* .memberName */
},
{
    UA_TYPES_IEC7_3_DETAILQUAL, /* .memberTypeIndex */
    offsetof(UA_Quality, detailQual) - offsetof(UA_Quality, validity) - sizeof(UA_ValidityKind), /* .padding */
    false, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("DetailQual") /* .memberName */
},
{
    UA_TYPES_IEC7_3_SOURCEKIND, /* .memberTypeIndex */
    offsetof(UA_Quality, source) - offsetof(UA_Quality, detailQual) - sizeof(UA_DetailQual), /* .padding */
    false, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Source") /* .memberName */
},
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    offsetof(UA_Quality, test) - offsetof(UA_Quality, source) - sizeof(UA_SourceKind), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Test") /* .memberName */
},
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    offsetof(UA_Quality, operatorBlocked) - offsetof(UA_Quality, test) - sizeof(UA_Boolean), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("OperatorBlocked") /* .memberName */
},};

/* AnalogueValue */
static UA_DataTypeMember AnalogueValue_members[2] = {
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("I") /* .memberName */
},
{
    UA_TYPES_FLOAT, /* .memberTypeIndex */
    offsetof(UA_AnalogueValue, f) - offsetof(UA_AnalogueValue, i) - sizeof(UA_Int32), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("F") /* .memberName */
},};

/* Unit */
static UA_DataTypeMember Unit_members[2] = {
{
    UA_TYPES_IEC7_3_SIUNITKIND, /* .memberTypeIndex */
    0, /* .padding */
    false, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("SIUnit") /* .memberName */
},
{
    UA_TYPES_IEC7_3_MULTIPLIERKIND, /* .memberTypeIndex */
    offsetof(UA_Unit, multiplier) - offsetof(UA_Unit, sIUnit) - sizeof(UA_SIUnitKind), /* .padding */
    false, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Multiplier") /* .memberName */
},};
const UA_DataType UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_COUNT] = {
/* ValidityKind */
{
    {2, UA_NODEIDTYPE_NUMERIC, {22LU}}, /* .typeId */
    {2, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_ValidityKind), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    ValidityKind_members  /* .members */
    UA_TYPENAME("ValidityKind") /* .typeName */
},
/* SourceKind */
{
    {2, UA_NODEIDTYPE_NUMERIC, {24LU}}, /* .typeId */
    {2, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_SourceKind), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    SourceKind_members  /* .members */
    UA_TYPENAME("SourceKind") /* .typeName */
},
/* SIUnitKind */
{
    {2, UA_NODEIDTYPE_NUMERIC, {97LU}}, /* .typeId */
    {2, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_SIUnitKind), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    SIUnitKind_members  /* .members */
    UA_TYPENAME("SIUnitKind") /* .typeName */
},
/* MultiplierKind */
{
    {2, UA_NODEIDTYPE_NUMERIC, {81LU}}, /* .typeId */
    {2, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_MultiplierKind), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    MultiplierKind_members  /* .members */
    UA_TYPENAME("MultiplierKind") /* .typeName */
},
/* DetailQual */
{
    {2, UA_NODEIDTYPE_NUMERIC, {23LU}}, /* .typeId */
    {2, UA_NODEIDTYPE_NUMERIC, {25LU}}, /* .binaryEncodingId */
    sizeof(UA_DetailQual), /* .memSize */
    UA_TYPES_IEC7_3_DETAILQUAL, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    true, /* .pointerFree */
    false, /* .overlayable */
    8, /* .membersSize */
    DetailQual_members  /* .members */
    UA_TYPENAME("DetailQual") /* .typeName */
},
/* Quality */
{
    {2, UA_NODEIDTYPE_NUMERIC, {17LU}}, /* .typeId */
    {2, UA_NODEIDTYPE_NUMERIC, {18LU}}, /* .binaryEncodingId */
    sizeof(UA_Quality), /* .memSize */
    UA_TYPES_IEC7_3_QUALITY, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    true, /* .pointerFree */
    false, /* .overlayable */
    5, /* .membersSize */
    Quality_members  /* .members */
    UA_TYPENAME("Quality") /* .typeName */
},
/* AnalogueValue */
{
    {2, UA_NODEIDTYPE_NUMERIC, {117LU}}, /* .typeId */
    {2, UA_NODEIDTYPE_NUMERIC, {158LU}}, /* .binaryEncodingId */
    sizeof(UA_AnalogueValue), /* .memSize */
    UA_TYPES_IEC7_3_ANALOGUEVALUE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    true, /* .pointerFree */
    false, /* .overlayable */
    2, /* .membersSize */
    AnalogueValue_members  /* .members */
    UA_TYPENAME("AnalogueValue") /* .typeName */
},
/* Unit */
{
    {2, UA_NODEIDTYPE_NUMERIC, {133LU}}, /* .typeId */
    {2, UA_NODEIDTYPE_NUMERIC, {134LU}}, /* .binaryEncodingId */
    sizeof(UA_Unit), /* .memSize */
    UA_TYPES_IEC7_3_UNIT, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    true, /* .pointerFree */
    false, /* .overlayable */
    2, /* .membersSize */
    Unit_members  /* .members */
    UA_TYPENAME("Unit") /* .typeName */
},
};


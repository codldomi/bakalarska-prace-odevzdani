
#include "iec_server.h"

#include "../nodeset/iec7-3_cs/types_iec7_3_generated.h"
#include "../nodeset/iec7-3_cs/ns_iec7_3_reduced.h"
#include "../nodeset/iec7-4_cs/ns_iec7_4_reduced.h"
#include "../nodeset/mtpdm1_cs/namespace_mtpdm1_generated.h"

#include <open62541/server_config_default.h>
#include <nex_macros.h>
#include <nex_utility.h>

#include <stdexcept>

using namespace std;


namespace iecserver
{
    namespace
    {
        const UA_DataTypeArray _dataTypeArray =
        {
            nullptr,
            UA_TYPES_IEC7_3_COUNT,
            UA_TYPES_IEC7_3
        };

        UA_Quality _quality =
        {
            .validity = UA_VALIDITYKIND_INVALID,
            .detailQual = UA_DetailQual
            {
                .overflow = UA_TRUE,
                .outOfRange = UA_FALSE,
                .badReference = UA_TRUE,
                .oscillatory = UA_FALSE,
                .failure = UA_TRUE,
                .oldData = UA_FALSE,
                .inconsistent = UA_TRUE,
                .inaccurate = UA_FALSE
            },
            .source = UA_SOURCEKIND_PROCESS,
            .test = UA_FALSE,
            .operatorBlocked = UA_TRUE
        };


        UA_BehaviourModeKind _behaviourModeKind = UA_BEHAVIOURMODEKIND_TEST;


        void initNamespaces(UA_Server * server)
        {
            UA_StatusCode retval = UA_STATUSCODE_GOOD;

            UA_NodeId root = UA_NODEID_NUMERIC(0, UA_NS0ID_ROOTFOLDER);

            retval = ns_iec7_3_reduced(server);
            NEX_CHECK_GOTO(retval, exit);

            retval = ns_iec7_4_reduced(server);
            NEX_CHECK_GOTO(retval, exit);

            retval = namespace_mtpdm1_generated(server);
            NEX_CHECK_GOTO(retval, exit);

            retval = NEX_Duplicates_remove(server, &root);

        exit:
            UA_NodeId_clear(&root);

            if (UA_StatusCode_isBad(retval))
            {
                throw logic_error("Error during namespaces initialization.");
            }
        }


        void initValues(UA_Server * server)
        {
            UA_StatusCode retval = UA_STATUSCODE_GOOD;

            UA_Variant value;
            UA_Variant_setScalar(&value, &_quality, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY]);

            retval = UA_Server_writeValue(server, UA_NODEID_NUMERIC(4, 6001), value);
            NEX_CHECK_GOTO(retval, exit)

            UA_Variant_setScalar(&value, &_behaviourModeKind, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_BEHAVIOURMODEKIND]);
            retval = UA_Server_writeValue(server, UA_NODEID_NUMERIC(4, 6002), value);

        exit:
            if (UA_StatusCode_isBad(retval))
            {
                throw logic_error("Error during values initialization.");
            }
        }
    }


    UA_Server * create()
    {
        UA_Server * server = UA_Server_new();
        UA_ServerConfig_setDefault(UA_Server_getConfig(server));

        initNamespaces(server);
        initValues(server);

        return server;
    }


    void run(UA_Server * server, bool * running)
    {
        UA_StatusCode retval = UA_Server_run(server, running);

        if (UA_StatusCode_isBad(retval))
        {
            throw logic_error("Error during server running.");
        }
    }


    void clean(UA_Server * server)
    {
        UA_Server_delete(server);
    }
}

#ifndef TEST_SERVER_IEC_SERVER_H
#define TEST_SERVER_IEC_SERVER_H

#include <open62541/server.h>


namespace iecserver
{
    UA_Server * create();

    void run(UA_Server * server, bool * running);

    void clean(UA_Server * server);
}


#endif //TEST_SERVER_IEC_SERVER_H

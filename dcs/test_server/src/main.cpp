#include <iostream>

#include "server/iec_server.h"

using namespace std;


void runServer()
{
    auto running = true;
    auto server = iecserver::create();

    iecserver::run(server, &running);

    iecserver::clean(server);
}


int main()
{
    runServer();
    return 0;
}




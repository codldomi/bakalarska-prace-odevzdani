cmake_minimum_required(VERSION 3.14)
project(test_server)
include(FetchContent)

set(CMAKE_CXX_STANDARD 17)


set(IEC7_3_DIR "${PROJECT_SOURCE_DIR}/src/nodeset/iec7-3_cs")
set(IEC7_4_DIR "${PROJECT_SOURCE_DIR}/src/nodeset/iec7-4_cs")
set(MTPDM1_DIR "${PROJECT_SOURCE_DIR}/src/nodeset/mtpdm1_cs")

FetchContent_Declare(
        open62541
        GIT_REPOSITORY https://github.com/open62541/open62541.git
        GIT_TAG        v1.2
)
FetchContent_MakeAvailable(open62541)



FetchContent_Declare(
        opcua_nodes_extension
        GIT_REPOSITORY git@gitlab.intranet.modemtec.cz:libraries/pc/opcua_nodes_extension.git
        GIT_TAG        edeba82e20ad8cec4812fb7f1ec56403cc6f106d
)
FetchContent_MakeAvailable(opcua_nodes_extension)


add_executable(
        test_server
        src/main.cpp
        ${IEC7_3_DIR}/types_iec7_3_generated.h
        ${IEC7_3_DIR}/types_iec7_3_generated.c
        ${IEC7_3_DIR}/types_iec7_3_generated_encoding_binary.h
        ${IEC7_3_DIR}/types_iec7_3_generated_handling.h
        ${IEC7_3_DIR}/ns_iec7_3_reduced.h
        ${IEC7_3_DIR}/ns_iec7_3_reduced.c
        ${IEC7_3_DIR}/iec7_3_nodeids.h
        ${IEC7_4_DIR}/types_iec7_4_generated.h
        ${IEC7_4_DIR}/types_iec7_4_generated.c
        ${IEC7_4_DIR}/types_iec7_4_generated_encoding_binary.h
        ${IEC7_4_DIR}/types_iec7_4_generated_handling.h
        ${IEC7_4_DIR}/ns_iec7_4_reduced.h
        ${IEC7_4_DIR}/ns_iec7_4_reduced.c
        ${IEC7_4_DIR}/iec7_4_nodeids.h
        ${MTPDM1_DIR}/types_mtpdm1_generated.h
        ${MTPDM1_DIR}/types_mtpdm1_generated.c
        ${MTPDM1_DIR}/types_mtpdm1_generated_encoding_binary.h
        ${MTPDM1_DIR}/types_mtpdm1_generated_handling.h
        ${MTPDM1_DIR}/mtpdm1_nodeids.h
        ${MTPDM1_DIR}/namespace_mtpdm1_generated.c
        ${MTPDM1_DIR}/namespace_mtpdm1_generated.h
        src/server/iec_server.cpp
        src/server/iec_server.h)


target_link_libraries(
        test_server
        open62541
        opcua_nodes_extension)

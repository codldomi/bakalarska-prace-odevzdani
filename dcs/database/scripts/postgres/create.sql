-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.3
-- PostgreSQL version: 13.0
-- Project Site: pgmodeler.io
-- Model Author: ---

-- Database creation must be performed outside a multi lined SQL file.
-- These commands were put in this file only as a convenience.
--
-- object: dcs_database | type: DATABASE --
-- DROP DATABASE IF EXISTS dcs_database;
CREATE DATABASE dcs_database;
-- ddl-end --


-- object: s_server | type: SCHEMA --
-- DROP SCHEMA IF EXISTS s_server CASCADE;
CREATE SCHEMA s_server;
-- ddl-end --
ALTER SCHEMA s_server OWNER TO postgres;
-- ddl-end --

-- object: s_task | type: SCHEMA --
-- DROP SCHEMA IF EXISTS s_task CASCADE;
CREATE SCHEMA s_task;
-- ddl-end --
ALTER SCHEMA s_task OWNER TO postgres;
-- ddl-end --

SET search_path TO pg_catalog,public,s_server,s_task;
-- ddl-end --

-- object: s_server.t_object | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_object CASCADE;
CREATE TABLE s_server.t_object (
	c_event_notifier integer NOT NULL,
	t_node_id integer NOT NULL,
	CONSTRAINT t_object_pk PRIMARY KEY (t_node_id)

);
-- ddl-end --
ALTER TABLE s_server.t_object OWNER TO postgres;
-- ddl-end --

-- object: s_server.t_method | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_method CASCADE;
CREATE TABLE s_server.t_method (
	c_is_executable bool NOT NULL,
	t_node_id integer NOT NULL,
	CONSTRAINT t_method_pk PRIMARY KEY (t_node_id)

);
-- ddl-end --
ALTER TABLE s_server.t_method OWNER TO postgres;
-- ddl-end --

-- object: s_server.t_argument | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_argument CASCADE;
CREATE TABLE s_server.t_argument (
	id serial NOT NULL,
	c_is_input bool NOT NULL,
	c_name text NOT NULL,
	t_method_id integer NOT NULL,
	t_datatype_id integer NOT NULL,
	CONSTRAINT t_argument_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE s_server.t_argument OWNER TO postgres;
-- ddl-end --

-- object: s_server.t_datatype | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_datatype CASCADE;
CREATE TABLE s_server.t_datatype (
	c_is_abstract bool NOT NULL,
	c_type integer NOT NULL,
	c_name text NOT NULL,
	t_datatype_id integer,
	id integer NOT NULL,
	CONSTRAINT t_datatype_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE s_server.t_datatype OWNER TO postgres;
-- ddl-end --

-- object: s_server.t_namespace | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_namespace CASCADE;
CREATE TABLE s_server.t_namespace (
	id serial NOT NULL,
	c_uri text NOT NULL,
	c_timestamp timestamp NOT NULL DEFAULT now(),
	CONSTRAINT t_namespace_pk PRIMARY KEY (id),
	CONSTRAINT t_namespace_uri_uq UNIQUE (c_uri)

);
-- ddl-end --
ALTER TABLE s_server.t_namespace OWNER TO postgres;
-- ddl-end --

-- object: s_server.t_variable | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_variable CASCADE;
CREATE TABLE s_server.t_variable (
	c_is_historizing bool NOT NULL,
	c_minimum_sampling_interval double precision NOT NULL,
	c_default_value text,
	c_access_level integer NOT NULL,
	t_node_id integer NOT NULL,
	t_datatype_id integer NOT NULL,
	CONSTRAINT t_variable_pk PRIMARY KEY (t_node_id)

);
-- ddl-end --
ALTER TABLE s_server.t_variable OWNER TO postgres;
-- ddl-end --

-- object: s_server.t_node | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_node CASCADE;
CREATE TABLE s_server.t_node (
	c_browse_name text NOT NULL,
	c_display_name text NOT NULL,
	c_description text,
	c_type integer NOT NULL,
	t_node_id integer,
	t_object_tree_id integer NOT NULL,
	id integer NOT NULL,
	CONSTRAINT t_node_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE s_server.t_node OWNER TO postgres;
-- ddl-end --

-- object: s_server.t_enum_field | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_enum_field CASCADE;
CREATE TABLE s_server.t_enum_field (
	id serial NOT NULL,
	c_name text NOT NULL,
	c_value integer NOT NULL,
	t_datatype_id integer NOT NULL,
	CONSTRAINT t_enum_field_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE s_server.t_enum_field OWNER TO postgres;
-- ddl-end --

-- object: s_server.t_struct_field | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_struct_field CASCADE;
CREATE TABLE s_server.t_struct_field (
	id serial NOT NULL,
	c_name text NOT NULL,
	c_is_array bool NOT NULL,
	c_is_optional bool NOT NULL,
	t_datatype_id integer NOT NULL,
	t_of_datatype_id integer NOT NULL,
	CONSTRAINT t_struct_field_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE s_server.t_struct_field OWNER TO postgres;
-- ddl-end --

-- object: s_server.t_object_tree | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_object_tree CASCADE;
CREATE TABLE s_server.t_object_tree (
	id serial NOT NULL,
	c_alias text NOT NULL,
	c_timestamp timestamp NOT NULL DEFAULT now(),
	CONSTRAINT t_object_tree_pk PRIMARY KEY (id),
	CONSTRAINT t_object_tree_alias_uq UNIQUE (c_alias)

);
-- ddl-end --
ALTER TABLE s_server.t_object_tree OWNER TO postgres;
-- ddl-end --

-- object: t_object_tree_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_node DROP CONSTRAINT IF EXISTS t_object_tree_fk CASCADE;
ALTER TABLE s_server.t_node ADD CONSTRAINT t_object_tree_fk FOREIGN KEY (t_object_tree_id)
REFERENCES s_server.t_object_tree (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: s_server.t_server | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_server CASCADE;
CREATE TABLE s_server.t_server (
	id serial NOT NULL,
	c_alias text NOT NULL,
	c_url text NOT NULL,
	t_object_tree_id integer NOT NULL,
	CONSTRAINT t_server_pk PRIMARY KEY (id),
	CONSTRAINT t_server_alias_uq UNIQUE (c_alias)

);
-- ddl-end --
ALTER TABLE s_server.t_server OWNER TO postgres;
-- ddl-end --

-- object: s_server.t_variable_value | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_variable_value CASCADE;
CREATE TABLE s_server.t_variable_value (
	id serial NOT NULL,
	c_source_timestamp timestamp NOT NULL,
	c_timestamp timestamp NOT NULL DEFAULT now(),
	c_value text NOT NULL,
	t_server_id integer NOT NULL,
	t_variable_id integer NOT NULL,
	CONSTRAINT t_variable_value_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE s_server.t_variable_value OWNER TO postgres;
-- ddl-end --

-- object: t_server_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_variable_value DROP CONSTRAINT IF EXISTS t_server_fk CASCADE;
ALTER TABLE s_server.t_variable_value ADD CONSTRAINT t_server_fk FOREIGN KEY (t_server_id)
REFERENCES s_server.t_server (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: t_object_tree_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_server DROP CONSTRAINT IF EXISTS t_object_tree_fk CASCADE;
ALTER TABLE s_server.t_server ADD CONSTRAINT t_object_tree_fk FOREIGN KEY (t_object_tree_id)
REFERENCES s_server.t_object_tree (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: s_task.t_task | type: TABLE --
-- DROP TABLE IF EXISTS s_task.t_task CASCADE;
CREATE TABLE s_task.t_task (
	id serial NOT NULL,
	c_type integer NOT NULL,
	t_server_id integer NOT NULL,
	t_node_id integer NOT NULL,
	CONSTRAINT t_request_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE s_task.t_task OWNER TO postgres;
-- ddl-end --

-- object: s_task.t_task_state | type: TABLE --
-- DROP TABLE IF EXISTS s_task.t_task_state CASCADE;
CREATE TABLE s_task.t_task_state (
	id serial NOT NULL,
	c_type integer NOT NULL,
	c_timestamp timestamp NOT NULL DEFAULT now(),
	c_description text,
	t_task_id integer NOT NULL,
	CONSTRAINT t_request_state_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE s_task.t_task_state OWNER TO postgres;
-- ddl-end --

-- object: t_task_fk | type: CONSTRAINT --
-- ALTER TABLE s_task.t_task_state DROP CONSTRAINT IF EXISTS t_task_fk CASCADE;
ALTER TABLE s_task.t_task_state ADD CONSTRAINT t_task_fk FOREIGN KEY (t_task_id)
REFERENCES s_task.t_task (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: t_server_fk | type: CONSTRAINT --
-- ALTER TABLE s_task.t_task DROP CONSTRAINT IF EXISTS t_server_fk CASCADE;
ALTER TABLE s_task.t_task ADD CONSTRAINT t_server_fk FOREIGN KEY (t_server_id)
REFERENCES s_server.t_server (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: s_server.t_id | type: TABLE --
-- DROP TABLE IF EXISTS s_server.t_id CASCADE;
CREATE TABLE s_server.t_id (
	id serial NOT NULL,
	c_index integer NOT NULL,
	t_namespace_id integer NOT NULL,
	CONSTRAINT t_id_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE s_server.t_id OWNER TO postgres;
-- ddl-end --

-- object: t_id_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_node DROP CONSTRAINT IF EXISTS t_id_fk CASCADE;
ALTER TABLE s_server.t_node ADD CONSTRAINT t_id_fk FOREIGN KEY (id)
REFERENCES s_server.t_id (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: t_id_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_datatype DROP CONSTRAINT IF EXISTS t_id_fk CASCADE;
ALTER TABLE s_server.t_datatype ADD CONSTRAINT t_id_fk FOREIGN KEY (id)
REFERENCES s_server.t_id (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: t_datatype_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_struct_field DROP CONSTRAINT IF EXISTS t_datatype_fk CASCADE;
ALTER TABLE s_server.t_struct_field ADD CONSTRAINT t_datatype_fk FOREIGN KEY (t_datatype_id)
REFERENCES s_server.t_datatype (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: t_datatype_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_enum_field DROP CONSTRAINT IF EXISTS t_datatype_fk CASCADE;
ALTER TABLE s_server.t_enum_field ADD CONSTRAINT t_datatype_fk FOREIGN KEY (t_datatype_id)
REFERENCES s_server.t_datatype (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: t_namespace_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_id DROP CONSTRAINT IF EXISTS t_namespace_fk CASCADE;
ALTER TABLE s_server.t_id ADD CONSTRAINT t_namespace_fk FOREIGN KEY (t_namespace_id)
REFERENCES s_server.t_namespace (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: t_node_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_method DROP CONSTRAINT IF EXISTS t_node_fk CASCADE;
ALTER TABLE s_server.t_method ADD CONSTRAINT t_node_fk FOREIGN KEY (t_node_id)
REFERENCES s_server.t_node (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: t_node_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_object DROP CONSTRAINT IF EXISTS t_node_fk CASCADE;
ALTER TABLE s_server.t_object ADD CONSTRAINT t_node_fk FOREIGN KEY (t_node_id)
REFERENCES s_server.t_node (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: t_node_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_variable DROP CONSTRAINT IF EXISTS t_node_fk CASCADE;
ALTER TABLE s_server.t_variable ADD CONSTRAINT t_node_fk FOREIGN KEY (t_node_id)
REFERENCES s_server.t_node (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: t_datatype_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_variable DROP CONSTRAINT IF EXISTS t_datatype_fk CASCADE;
ALTER TABLE s_server.t_variable ADD CONSTRAINT t_datatype_fk FOREIGN KEY (t_datatype_id)
REFERENCES s_server.t_datatype (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: t_method_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_argument DROP CONSTRAINT IF EXISTS t_method_fk CASCADE;
ALTER TABLE s_server.t_argument ADD CONSTRAINT t_method_fk FOREIGN KEY (t_method_id)
REFERENCES s_server.t_method (t_node_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: t_datatype_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_argument DROP CONSTRAINT IF EXISTS t_datatype_fk CASCADE;
ALTER TABLE s_server.t_argument ADD CONSTRAINT t_datatype_fk FOREIGN KEY (t_datatype_id)
REFERENCES s_server.t_datatype (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: t_variable_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_variable_value DROP CONSTRAINT IF EXISTS t_variable_fk CASCADE;
ALTER TABLE s_server.t_variable_value ADD CONSTRAINT t_variable_fk FOREIGN KEY (t_variable_id)
REFERENCES s_server.t_variable (t_node_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: t_node_fk | type: CONSTRAINT --
-- ALTER TABLE s_task.t_task DROP CONSTRAINT IF EXISTS t_node_fk CASCADE;
ALTER TABLE s_task.t_task ADD CONSTRAINT t_node_fk FOREIGN KEY (t_node_id)
REFERENCES s_server.t_node (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: s_task.t_result | type: TABLE --
-- DROP TABLE IF EXISTS s_task.t_result CASCADE;
CREATE TABLE s_task.t_result (
	t_task_id integer NOT NULL,
	t_variable_value_id integer,
	CONSTRAINT t_result_pk PRIMARY KEY (t_task_id)

);
-- ddl-end --
ALTER TABLE s_task.t_result OWNER TO postgres;
-- ddl-end --

-- object: t_task_fk | type: CONSTRAINT --
-- ALTER TABLE s_task.t_result DROP CONSTRAINT IF EXISTS t_task_fk CASCADE;
ALTER TABLE s_task.t_result ADD CONSTRAINT t_task_fk FOREIGN KEY (t_task_id)
REFERENCES s_task.t_task (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: t_variable_value_fk | type: CONSTRAINT --
-- ALTER TABLE s_task.t_result DROP CONSTRAINT IF EXISTS t_variable_value_fk CASCADE;
ALTER TABLE s_task.t_result ADD CONSTRAINT t_variable_value_fk FOREIGN KEY (t_variable_value_id)
REFERENCES s_server.t_variable_value (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: t_of_datatype_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_struct_field DROP CONSTRAINT IF EXISTS t_of_datatype_fk CASCADE;
ALTER TABLE s_server.t_struct_field ADD CONSTRAINT t_of_datatype_fk FOREIGN KEY (t_of_datatype_id)
REFERENCES s_server.t_datatype (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: t_datatype_id_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_datatype DROP CONSTRAINT IF EXISTS t_datatype_id_fk CASCADE;
ALTER TABLE s_server.t_datatype ADD CONSTRAINT t_datatype_id_fk FOREIGN KEY (t_datatype_id)
REFERENCES s_server.t_datatype (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: t_node_id_fk | type: CONSTRAINT --
-- ALTER TABLE s_server.t_node DROP CONSTRAINT IF EXISTS t_node_id_fk CASCADE;
ALTER TABLE s_server.t_node ADD CONSTRAINT t_node_id_fk FOREIGN KEY (t_node_id)
REFERENCES s_server.t_node (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

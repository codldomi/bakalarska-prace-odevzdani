## Data Collection Database

verze 0.1.0

Schémata a skripty pro tvorbu databáze. 

---

### Podporované databázové systémy

- [PostgreSQL](https://www.postgresql.org/)

### Vývojové prostředí

Vždy použít nejnovější verzi.

- [PgModeler](https://www.pgmodeler.io/) - tvorba databázového schéma
- [PgAdmin](https://www.pgadmin.org/) - správa databáze
- [Docker](https://www.docker.com/) - běh databáze v dockeru (image a další informace: https://hub.docker.com/_/postgres)

### Zprovoznění databáze (Windows)

Kde není uveden návod, očekává se oficiální postup podle webových stránek výše.

1. instalace Dockeru
1. instalace PostgreSQL image, instalace PgAdmina a inicializace databáze ([návod](https://elanderson.net/2018/02/setup-postgresql-on-windows-with-docker/))
1. instalace PgModeleru (narozdíl od Linuxu je třeba jít například podle návodu [zde](https://en.quillevere.net/programming/bdd/postgresql/compile-pgmodeler-windows_60618.htm))
1. zavolat create skript nad databází

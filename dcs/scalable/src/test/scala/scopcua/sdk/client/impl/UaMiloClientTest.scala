package scopcua.sdk.client.impl

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import pureconfig.generic.ProductHint
import pureconfig.generic.semiauto.deriveEnumerationReader
import pureconfig.{CamelCase, ConfigFieldMapping, ConfigReader, PascalCase}
import scopcua.data.UaDataType.{GeneralEnumeration, GeneralStructure}
import scopcua.data.UaDataType.GeneralStructure.Field
import scopcua.data.security.UaSecurityPolicy
import scopcua.data.{UaDataType, UaId}
import scopcua.data.value.{UaBoolean, UaString}
import scopcua.data.value.number.integer.UaSInteger.UaInt32
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.util.binarydecoder.impl.UaStdBinaryDecoder


class UaMiloClientTest extends AnyFunSuite with Matchers {

  protected val _behaviourModeKind: GeneralEnumeration =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("BehaviourModeKind"),
      UaId(UaUInt32(5), UaString("http://opcfoundation.org/UA/IEC61850-7-4")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      Map(
        UaString("on") -> UaInt32(1),
        UaString("blocked") -> UaInt32(2),
        UaString("test") -> UaInt32(3),
        UaString("test/blocked") -> UaInt32(4),
        UaString("off") -> UaInt32(5)))

  protected val _detailQual: GeneralStructure =
    GeneralStructure(
      UaBoolean(false),
      UaString("DetailQual"),
      UaId(UaUInt32(23), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
      Field(
        UaString("overflow"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("outOfRange"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("badReference"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("oscillatory"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("failure"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("oldData"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("inconsistent"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("inaccurate"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)))

  protected val _validityKind: GeneralEnumeration =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("ValidityKind"),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      Map(
        UaString("good") -> UaInt32(0),
        UaString("invalid") -> UaInt32(1),
        UaString("reserved") -> UaInt32(2),
        UaString("questionable") -> UaInt32(3)))

  protected val _sourceKind: GeneralEnumeration =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("SourceKind"),
      UaId(UaUInt32(24), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      Map(
        UaString("process") -> UaInt32(0),
        UaString("substituted") -> UaInt32(1)))

  protected val _quality: GeneralStructure =
    GeneralStructure(
      UaBoolean(false),
      UaString("Quality"),
      UaId(UaUInt32(17), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
      Field(
        UaString("validity"),
        UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("detailQual"),
        UaId(UaUInt32(23), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("source"),
        UaId(UaUInt32(24), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("test"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("operatorBlocked"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)))


  protected val _decoder: UaStdBinaryDecoder = {

    val types = Map(
      _quality.id -> _quality,
      _detailQual.id -> _detailQual,
      _validityKind.id -> _validityKind,
      _sourceKind.id -> _sourceKind,
      _behaviourModeKind.id -> _behaviourModeKind)

    new UaStdBinaryDecoder(UaDataType.builtIns ++ types, Map.empty)
  }


  protected def safe[A](client: UaMiloClient)(code: => A): A = {

    client.connect()

    try {
      code
    } finally {
      client.disconnect()
    }
  }


  protected implicit def hint[A]: ProductHint[A] =

    ProductHint[A](ConfigFieldMapping(CamelCase, CamelCase))


  protected implicit val _securityReader: ConfigReader[UaSecurityPolicy] =

    deriveEnumerationReader[UaSecurityPolicy](ConfigFieldMapping(PascalCase, PascalCase))

}

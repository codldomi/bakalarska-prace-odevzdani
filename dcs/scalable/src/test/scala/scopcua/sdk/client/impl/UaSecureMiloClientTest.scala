package scopcua.sdk.client.impl

import pureconfig._
import pureconfig.generic.auto._
import scopcua.data.UaId
import scopcua.data.security.UaSecurityConfig
import scopcua.data.value.{UaArray, UaString}
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.test.config.SecureClientConfig
import scopcua.test.tag.{IntegrationTest, NeedsRunningServer}

import java.security.cert.X509Certificate
import java.security.{KeyPair, KeyStore, PrivateKey}


class UaSecureMiloClientTest extends UaMiloClientTest {

  private val _client = {

    val source =
      ConfigSource
        .resources("client/SecureClient.conf")
        .at("Connection")
        .loadOrThrow[SecureClientConfig]

    val ks = KeyStore.getInstance("PKCS12")
    val is = getClass.getClassLoader.getResourceAsStream(source.keyStore)
    ks.load(is, source.keyStorePassword.toCharArray)

    val certificate = ks.getCertificate(source.alias).asInstanceOf[X509Certificate]
    val chain = ks.getCertificateChain(source.alias).map(_.asInstanceOf[X509Certificate])
    val clientPrivateKey = ks.getKey(source.alias, source.keyStorePassword.toCharArray)
    val serverPublicKey = certificate.getPublicKey
    val keyPair = new KeyPair(serverPublicKey, clientPrivateKey.asInstanceOf[PrivateKey])

    val config =
      UaSecurityConfig(
        source.username,
        source.password,
        source.appName,
        source.appUri,
        keyPair,
        certificate,
        chain,
        source.policy,
        source.trustDir)

    UaMiloClient.from(source.url, config, _decoder, source.timeout)
  }


  test("Read Namespace Array - node with i=2255;ns=“http://opcfoundation.org/UA/”", IntegrationTest, NeedsRunningServer) {

    val id = UaId(UaUInt32(2255), UaString("http://opcfoundation.org/UA/"))

    val result =
      safe(_client) {
        val (value, _) = _client.readValue(id)
        value
      }

    val expected =
      UaArray(
        UaString("http://opcfoundation.org/UA/"),
        UaString("urn:eclipse:milo:opcua:server:db520941-be4e-4934-8367-2b2a899762f1"),
        UaString("urn:eclipse:milo:opcua:server:demo"))

    result shouldBe expected
  }

/*
  todo: code to generate valid OPC UA self-signed certificate (valid cert contains app uri == client app uri)

  private def cs(): Unit = {

    val keyStore = KeyStore.getInstance("PKCS12")
    keyStore.load(null, "password".toCharArray)

    val keyPair = SelfSignedCertificateGenerator.generateRsaKeyPair(2048)
    val builder = new SelfSignedCertificateBuilder(keyPair)
      .setCommonName("Secure Scalable Client")
      .setOrganization("modemtec")
      .setOrganizationalUnit("dev")
      .setLocalityName("Prague")
      .setStateName("CZ")
      .setCountryCode("CZ")
      .setApplicationUri("urn:secure:scalable:client")
      .addDnsName("localhost")
      .addIpAddress("127.0.0.1")

    val certificate = builder.build();

    keyStore.setKeyEntry("secureClient", keyPair.getPrivate, "password".toCharArray, Array(certificate))

    val out = new FileOutputStream("ks.p12")
    keyStore.store(out, "password".toCharArray)
  }
*/

}

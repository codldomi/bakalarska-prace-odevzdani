package scopcua.sdk.client.impl

import pureconfig._
import pureconfig.generic.auto._

import scopcua.data.value.{UaBoolean, UaEnumeration, UaString, UaStructure}
import scopcua.data.value.number.integer.UaSInteger.UaInt32
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.data.UaId
import scopcua.test.config.AnonymousClientConfig
import scopcua.test.tag.{IntegrationTest, NeedsRunningServer}


class UaAnonymousMiloClientTest extends UaMiloClientTest {

  private val client = {

    val url =
      ConfigSource
        .resources("client/AnonymousClient.conf")
        .at("Connection")
        .loadOrThrow[AnonymousClientConfig]
        .url

    UaMiloClient.from(url, _decoder)
  }


  test("Read data of BehaviourModeKing[IEC74] - node with i=6002;ns=http://www.modemtec.cz/PD/", IntegrationTest, NeedsRunningServer) {

    val expected = UaEnumeration(UaInt32(3), UaString("test"))

    val result =
      safe(client) {
        val id = UaId(UaUInt32(6002), UaString("http://www.modemtec.cz/PD/"))
        val (value, _) = client.readValue(id)
        value
      }

    result shouldBe expected
  }


  test("Read data of Quality[IEC73] - node with i=6001;ns=http://www.modemtec.cz/PD/", IntegrationTest, NeedsRunningServer) {

    val expected =
      UaStructure(
        Map(
          UaString("validity") ->
            UaEnumeration(UaInt32(1), UaString("invalid")),
          UaString("detailQual") ->
            UaStructure(
              Map(
                UaString("overflow")     -> UaBoolean(true),
                UaString("outOfRange")   -> UaBoolean(false),
                UaString("badReference") -> UaBoolean(true),
                UaString("oscillatory")  -> UaBoolean(false),
                UaString("failure")      -> UaBoolean(true),
                UaString("oldData")      -> UaBoolean(false),
                UaString("inconsistent") -> UaBoolean(true),
                UaString("inaccurate")   -> UaBoolean(false))),
          UaString("source") ->
            UaEnumeration(UaInt32(0), UaString("process")),
          UaString("test") ->
            UaBoolean(false),
          UaString("operatorBlocked") ->
            UaBoolean(true)))

    val result =
      safe(client) {
        val id = UaId(UaUInt32(6001), UaString("http://www.modemtec.cz/PD/"))
        val (value, _) = client.readValue(id)
        value
      }

    result shouldBe expected
  }


}

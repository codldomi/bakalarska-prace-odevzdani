package scopcua.test.tag

import org.scalatest.Tag

/** A test is done independently on components' implementation.
  */
object UnitTest extends Tag("UnitTest")

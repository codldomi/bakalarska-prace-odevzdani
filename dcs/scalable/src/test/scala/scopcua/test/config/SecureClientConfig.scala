package scopcua.test.config

import scopcua.data.security.UaSecurityPolicy


case class SecureClientConfig(
  url: String,
  username: String,
  password: String,
  alias: String,
  appName: String,
  appUri: String,
  policy: UaSecurityPolicy,
  serverCert: String,
  trustDir: String,
  keyStorePassword: String,
  keyStore: String,
  timeout: Int
)

package scopcua.util.statuscodeparser.impl

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import scopcua.data.value.{UaStatusCode, UaString}
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.test.tag.UnitTest

import scala.io.Source


class UaCsvStatusCodeParserTest extends AnyFunSuite with Matchers {

  private val _parser: UaCsvStatusCodeParser = new UaCsvStatusCodeParser()

  private def loadText(): String = Source.fromResource("StatusCode.csv").mkString


  test("Selected status codes.", UnitTest) {

    val text = loadText()

    val result = _parser.parse(text)

    result(UaUInt32(0))           shouldBe UaStatusCode(UaUInt32(0), UaString("Good"))
    result(UaUInt32(2161442816L)) shouldBe UaStatusCode(UaUInt32(2161442816L), UaString("BadAggregateNotSupported"))
    result(UaUInt32(67633152L))   shouldBe UaStatusCode(UaUInt32(67633152L), UaString("GoodInitiateFaultState"))
  }

}

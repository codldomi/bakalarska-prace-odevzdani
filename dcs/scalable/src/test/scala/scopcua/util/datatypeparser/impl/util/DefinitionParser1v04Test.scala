package scopcua.util.datatypeparser.impl.util

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import scopcua.data.UaId
import scopcua.data.value.number.integer.UaSInteger.UaInt32
import scopcua.data.value.{UaBoolean, UaString}
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.test.tag.IntegrationTest
import scopcua.util.datatypeparser.impl.model.Definition
import scopcua.util.datatypeparser.impl.model.Definition.{EnumField, StructField}


class DefinitionParser1v04Test extends AnyFunSuite with Matchers {

  val _namespaces =
    Map(
      0 -> "http://opcfoundation.org/UA/",
      1 -> "http://opcfoundation.org/UA/IEC61850-7-3",
      2 -> "http://opcfoundation.org/UA/IEC61850-7-4",
      3 -> "CustomNamespace")

  val _aliases =
    Map(
      "Boolean"       -> "i=1",
      "SByte"         -> "i=2",
      "Byte"          -> "i=3",
      "Int16"         -> "i=4",
      "UInt16"        -> "i=5",
      "Int32"         -> "i=6",
      "UInt32"        -> "i=7",
      "Int64"         -> "i=8",
      "UInt64"        -> "i=9",
      "Float"         -> "i=10",
      "Double"        -> "i=11",
      "String"        -> "i=12",
      "ByteString"    -> "i=15",
      "Structure"     -> "i=22",
      "BaseDataType"  -> "i=24",
      "Enumeration"   -> "i=29")


  def parser: DefinitionParser1v04 = new DefinitionParser1v04(_namespaces, _aliases)


  test ("Type[IEC73]: Timestamp", IntegrationTest) {

    val xml =
      <Definition Name="Timestamp">
        <Field Name="SecondSinceEpoch" DataType="UInt32" />
        <Field Name="FractionOfSecond" DataType="UInt32" />
        <Field Name="TimeQuality" DataType="ns=1;i=12" />
      </Definition>

    val result = parser.parse(xml, isStruct = true)

    val expected =
      Definition(
        UaString("Timestamp"),
        UaString(""),
        Right(
          Seq(
            StructField(
              UaString("SecondSinceEpoch"),
              UaId(UaUInt32(7), UaString("http://opcfoundation.org/UA/")),
              UaBoolean(false),
              UaBoolean(false)),
            StructField(
              UaString("FractionOfSecond"),
              UaId(UaUInt32(7), UaString("http://opcfoundation.org/UA/")),
              UaBoolean(false),
              UaBoolean(false)),
            StructField(
              UaString("TimeQuality"),
              UaId(UaUInt32(12), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
              UaBoolean(false),
              UaBoolean(false)))))

    result shouldBe expected
  }


  test("Type[IEC74]: AdjustmentKind", IntegrationTest) {

    val xml =
      <Definition Name="AdjustmentKind">
        <Field Name="Completed" Value="1" />
        <Field Name="Cancelled" Value="2" />
        <Field Name="New adjustments" Value="3" />
        <Field Name="Under way" Value="4" />
      </Definition>

    val result =  parser.parse(xml, isStruct = false)

    val expected =
      Definition(
        UaString("AdjustmentKind"),
        UaString(""),
        Left(
          Seq(
            EnumField(UaString("Completed"), UaInt32(1)),
            EnumField(UaString("Cancelled"), UaInt32(2)),
            EnumField(UaString("New adjustments"), UaInt32(3)),
            EnumField(UaString("Under way"), UaInt32(4)))))

    result shouldBe expected
  }


  test ("Type[IEC73]: Quality", IntegrationTest) {

    val xml =
      <Definition Name="Quality">
        <Field Name="validity" DataType="ns=1;i=22" />
        <Field Name="detailQual" DataType="ns=1;i=23" />
        <Field Name="source" DataType="ns=1;i=24" />
        <Field Name="test" DataType="Boolean" />
        <Field Name="operatorBlocked" DataType="Boolean" />
      </Definition>

    val result =  parser.parse(xml, isStruct = true)

    val expected =
      Definition(
        UaString("Quality"),
        UaString(""),
        Right(
          Seq(
            StructField(
              UaString("validity"),
              UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
              UaBoolean(false),
              UaBoolean(false)),
            StructField(
              UaString("detailQual"),
              UaId(UaUInt32(23), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
              UaBoolean(false),
              UaBoolean(false)),
            StructField(
              UaString("source"),
              UaId(UaUInt32(24), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
              UaBoolean(false),
              UaBoolean(false)),
            StructField(
              UaString("test"),
              UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
              UaBoolean(false),
              UaBoolean(false)),
            StructField(
              UaString("operatorBlocked"),
              UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
              UaBoolean(false),
              UaBoolean(false)))))

    result shouldBe expected
  }


  test("Type[Custom]: RequestResult", IntegrationTest) {

    val xml =
      <Definition Name="3:RequestResult" SymbolicName="Result">
        <Field Name="Ok" Value="0"/>
        <Field Name="Error" Value="1"/>
        <Field Name="Busy" Value="2"/>
        <Field Name="InvalidArgument" Value="3"/>
        <Field Name="Timeout" Value="4"/>
        <Field Name="Access" Value="5"/>
      </Definition>

    val result = parser.parse(xml, isStruct = false)

    val expected =
      Definition(
        UaString("RequestResult"),
        UaString("Result"),
        Left(
          Seq(
            EnumField(UaString("Ok"), UaInt32(0)),
            EnumField(UaString("Error"), UaInt32(1)),
            EnumField(UaString("Busy"), UaInt32(2)),
            EnumField(UaString("InvalidArgument"), UaInt32(3)),
            EnumField(UaString("Timeout"), UaInt32(4)),
            EnumField(UaString("Access"), UaInt32(5)))))

    result shouldBe expected
  }


  test("Type[Custom]: RequestResult (missing name)", IntegrationTest) {

    val xml =
      <Definition Name="  " SymbolicName="Result">
        <Field Name="Ok" Value="0"/>
        <Field Name="Error" Value="1"/>
        <Field Name="Busy" Value="2"/>
        <Field Name="InvalidArgument" Value="3"/>
        <Field Name="Timeout" Value="4"/>
        <Field Name="Access" Value="5"/>
      </Definition>

    assertThrows[Exception] {
      parser.parse(xml, isStruct = false)
    }
  }


  test("Type[IEC73]: Quality (missing namespace)", IntegrationTest) {

    val xml =
      <Definition Name="Quality">
        <Field Name="validity" DataType="ns=1;i=22" />
        <Field Name="detailQual" DataType="ns=1;i=23" />
        <Field Name="source" DataType="ns=1;i=24" />
        <Field Name="test" DataType="Boolean" />
        <Field Name="operatorBlocked" DataType="Boolean" />
      </Definition>

    assertThrows[Exception] {
      parser.parse(xml, isStruct = false)
    }
  }


}

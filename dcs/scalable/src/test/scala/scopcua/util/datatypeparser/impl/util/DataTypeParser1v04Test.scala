package scopcua.util.datatypeparser.impl.util

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import scopcua.data.UaDataType.GeneralEnumeration
import scopcua.data.UaId
import scopcua.data.UaDataType.GeneralStructure.Field
import scopcua.data.UaDataType.GeneralStructure
import scopcua.data.value.{UaBoolean, UaString}
import scopcua.data.value.number.integer.UaSInteger.UaInt32
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.test.tag.UnitTest


class DataTypeParser1v04Test extends AnyFunSuite with Matchers {

  val _namespaces =
    Map(
      0 -> "http://opcfoundation.org/UA/",
      1 -> "http://opcfoundation.org/UA/IEC61850-7-3",
      2 -> "http://opcfoundation.org/UA/IEC61850-7-4",
      3 -> "CustomNamespace")

  val _aliases =
    Map(
      "Boolean"                 -> "i=1",
      "SByte"                   -> "i=2",
      "Byte"                    -> "i=3",
      "Int16"                   -> "i=4",
      "UInt16"                  -> "i=5",
      "Int32"                   -> "i=6",
      "UInt32"                  -> "i=7",
      "Int64"                   -> "i=8",
      "UInt64"                  -> "i=9",
      "Float"                   -> "i=10",
      "Double"                  -> "i=11",
      "String"                  -> "i=12",
      "ByteString"              -> "i=15",
      "Structure"               -> "i=22",
      "BaseDataType"            -> "i=24",
      "Enumeration"             -> "i=29",
      "HasSubtype"              -> "i=45",
      "Organizes"               -> "i=35",
      "HasModellingRule"        -> "i=37",
      "HasEncoding"             -> "i=38",
      "HasDescription"          -> "i=39",
      "HasTypeDefinition"       -> "i=40",
      "HasSubtype"              -> "i=45",
      "HasProperty"             -> "i=46",
      "HasComponent"            -> "i=47",
      "PropertyType"            -> "i=68",
      "Mandatory"               -> "i=78",
      "Optional"                -> "i=80",
      "OptionalPlaceholder"     -> "i=11508",
      "MandatoryPlaceholder"    -> "i=11510",
      "DefaultVariableRefType"  -> "ns=1;i=2",
      "DefaultObjectRefType"    -> "ns=1;i=1",
      "HasDataObject"           -> "ns=1;i=1",
      "HasDataAttribute"        -> "ns=1;i=2")


  def parser: DataTypeParser1v04 =

    new DataTypeParser1v04(
      _namespaces,
      _aliases,
      new DefinitionParser1v04(
        _namespaces,
        _aliases))


  test ("Type[Zero]: Decimal (unsupported inheritance)", UnitTest) {

    val xml =
      <UADataType NodeId="i=50" BrowseName="Decimal">
        <DisplayName>Decimal</DisplayName>
        <Documentation>https://reference.opcfoundation.org/v104/Core/docs/Part3/8.54</Documentation>
        <References>
          <Reference ReferenceType="HasSubtype" IsForward="false">i=26</Reference>
        </References>
      </UADataType>

    assertThrows[Exception] {
      parser.parse(xml)
    }
  }


  test ("Type[IEC73]: Timestamp", UnitTest) {

    val xml =
      <UADataType NodeId="ns=1;i=5" BrowseName="1:Timestamp">
        <DisplayName>Timestamp</DisplayName>
        <References>
          <Reference ReferenceType="HasSubtype" IsForward="false">Structure</Reference>
        </References>
        <Definition Name="Timestamp">
          <Field Name="SecondSinceEpoch" DataType="UInt32" />
          <Field Name="FractionOfSecond" DataType="UInt32" />
          <Field Name="TimeQuality" DataType="ns=1;i=12" />
        </Definition>
      </UADataType>

    val result = parser.parse(xml)

    val expected =
      GeneralStructure(
        UaBoolean(false),
        UaString("Timestamp"),
        UaId(UaUInt32(5), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
        Field(
          UaString("SecondSinceEpoch"),
          UaId(UaUInt32(7), UaString("http://opcfoundation.org/UA/")),
          UaBoolean(false),
          UaBoolean(false)),
        Field(
          UaString("FractionOfSecond"),
          UaId(UaUInt32(7), UaString("http://opcfoundation.org/UA/")),
          UaBoolean(false),
          UaBoolean(false)),
        Field(
          UaString("TimeQuality"),
          UaId(UaUInt32(12), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
          UaBoolean(false),
          UaBoolean(false)))

    result shouldBe expected
  }


  test("Type[IEC74]: AdjustmentKind", UnitTest) {

    val xml =
      <UADataType NodeId="ns=2;i=1" BrowseName="2:AdjustmentKind">
        <DisplayName>AdjustmentKind</DisplayName>
        <References>
          <Reference ReferenceType="HasSubtype" IsForward="false">Enumeration</Reference>
          <Reference ReferenceType="HasProperty">ns=2;i=2</Reference>
        </References>
        <Definition Name="AdjustmentKind">
          <Field Name="Completed" Value="1" />
          <Field Name="Cancelled" Value="2" />
          <Field Name="New adjustments" Value="3" />
          <Field Name="Under way" Value="4" />
        </Definition>
      </UADataType>

    val result = parser.parse(xml)

    val expected =
      GeneralEnumeration(
        UaBoolean(false),
        UaString("AdjustmentKind"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/IEC61850-7-4")),
        UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
        UaString("Completed") -> UaInt32(1),
        UaString("Cancelled") -> UaInt32(2),
        UaString("New adjustments") -> UaInt32(3),
        UaString("Under way") -> UaInt32(4))

    result shouldBe expected
  }


  test ("Type[IEC73]: Quality", UnitTest) {

    val xml =
      <UADataType NodeId="ns=1;i=17" BrowseName="1:Quality">
        <DisplayName>Quality</DisplayName>
        <References>
          <Reference ReferenceType="HasSubtype" IsForward="false">Structure</Reference>
        </References>
        <Definition Name="Quality">
          <Field Name="validity" DataType="ns=1;i=22" />
          <Field Name="detailQual" DataType="ns=1;i=23" />
          <Field Name="source" DataType="ns=1;i=24" />
          <Field Name="test" DataType="Boolean" />
          <Field Name="operatorBlocked" DataType="Boolean" />
        </Definition>
      </UADataType>

    val result = parser.parse(xml)

    val expected =
      GeneralStructure(
        UaBoolean(false),
        UaString("Quality"),
        UaId(UaUInt32(17), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
        Field(
          UaString("validity"),
          UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
          UaBoolean(false),
          UaBoolean(false)),
        Field(
          UaString("detailQual"),
          UaId(UaUInt32(23), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
          UaBoolean(false),
          UaBoolean(false)),
        Field(
          UaString("source"),
          UaId(UaUInt32(24), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
          UaBoolean(false),
          UaBoolean(false)),
        Field(
          UaString("test"),
          UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
          UaBoolean(false),
          UaBoolean(false)),
        Field(
          UaString("operatorBlocked"),
          UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
          UaBoolean(false),
          UaBoolean(false)))

    result shouldBe expected
  }


  test("Type[Custom]: RequestResult", UnitTest) {

    val xml =
      <UADataType NodeId="ns=3;i=3002" BrowseName="3:RequestResult">
        <DisplayName>RequestResult</DisplayName>
        <References>
          <Reference ReferenceType="HasProperty">ns=1;i=6008</Reference>
          <Reference ReferenceType="HasSubtype" IsForward="false">i=29</Reference>
        </References>
        <Definition Name="3:RequestResult">
          <Field Name="Ok" Value="0"/>
          <Field Name="Error" Value="1"/>
          <Field Name="Busy" Value="2"/>
          <Field Name="InvalidArgument" Value="3"/>
          <Field Name="Timeout" Value="4"/>
          <Field Name="Access" Value="5"/>
        </Definition>
      </UADataType>

    val result =  parser.parse(xml)

    val expected =
      GeneralEnumeration(
        UaBoolean(false),
        UaString("RequestResult"),
        UaId(UaUInt32(3002), UaString("CustomNamespace")),
        UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
        UaString("Ok") -> UaInt32(0),
        UaString("Error") -> UaInt32(1),
        UaString("Busy") -> UaInt32(2),
        UaString("InvalidArgument") -> UaInt32(3),
        UaString("Timeout") -> UaInt32(4),
        UaString("Access") -> UaInt32(5))

    result shouldBe expected
  }


  test("Type[Custom]: RequestResult (missing supertype)", UnitTest) {

    val xml =
      <UADataType NodeId="ns=3;i=3002" BrowseName="3:RequestResult">
        <DisplayName>RequestResult</DisplayName>
        <References>
          <Reference ReferenceType="HasProperty">ns=1;i=6008</Reference>
        </References>
        <Definition Name="3:RequestResult">
          <Field Name="Ok" Value="0"/>
          <Field Name="Error" Value="1"/>
          <Field Name="Busy" Value="2"/>
          <Field Name="InvalidArgument" Value="3"/>
          <Field Name="Timeout" Value="4"/>
          <Field Name="Access" Value="5"/>
        </Definition>
      </UADataType>

    assertThrows[Exception] {
      parser.parse(xml)
    }
  }


  test("Type[IEC73]: Quality (missing NodeId)", UnitTest) {

    val xml =
      <UADataType BrowseName="1:Quality">
        <DisplayName>Quality</DisplayName>
        <References>
          <Reference ReferenceType="HasSubtype" IsForward="false">Structure</Reference>
        </References>
        <Definition Name="Quality">
          <Field Name="validity" DataType="ns=1;i=22" />
          <Field Name="detailQual" DataType="ns=1;i=23" />
          <Field Name="source" DataType="ns=1;i=24" />
          <Field Name="test" DataType="Boolean" />
          <Field Name="operatorBlocked" DataType="Boolean" />
        </Definition>
      </UADataType>

    assertThrows[Exception] {
      parser.parse(xml)
    }
  }


}

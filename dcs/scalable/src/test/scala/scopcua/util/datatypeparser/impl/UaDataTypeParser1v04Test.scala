package scopcua.util.datatypeparser.impl

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import scopcua.data.{UaDataType, UaId}
import scopcua.data.UaDataType.GeneralStructure.Field
import scopcua.data.UaDataType.{GeneralEnumeration, GeneralStructure}
import scopcua.data.value.number.integer.UaSInteger.UaInt32
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.data.value.{UaBoolean, UaString}
import scopcua.test.tag.IntegrationTest

import scala.io.Source


class UaDataTypeParser1v04Test extends AnyFunSuite with Matchers {


  def parser: UaDataTypeParser1v04 = new UaDataTypeParser1v04()


  def loadText(path: String): String = Source.fromResource(path).mkString


  def testSelectedDataTypes(result: Map[UaId, UaDataType], selected: Seq[UaDataType]): Unit = {

    val selectedIds = selected.map(_.id)

    val expectedTypes =
      selected
        .map(dataType => dataType.id -> dataType)
        .toMap

    val resultTypes =
      result
        .filter{
          case (id, _) => selectedIds.contains(id)
        }

    resultTypes shouldBe expectedTypes
  }


  test("DataTypes from IEC-61850-7-3", IntegrationTest) {

    val typesCount = 51
    val text = loadText("nodeset/Opc.Ua.IEC61850-7-3.NodeSet2.xml")
    val result = parser.parse(text)

    result.toSeq.length shouldBe typesCount

    val selected =
      Seq(
        GeneralEnumeration(
          UaBoolean(false),
          UaString("SequenceKind"),
          UaId(UaUInt32(101), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
          UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
          UaString("pos-neg-zero") -> UaInt32(0),
          UaString("dir-quad-zero") -> UaInt32(1)),
        GeneralStructure(
          UaBoolean(false),
          UaString("ScaledValueConfig"),
          UaId(UaUInt32(107), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
          UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
          Field(
            UaString("scaleFactor"),
            UaId(UaUInt32(10), UaString("http://opcfoundation.org/UA/")),
            UaBoolean(false),
            UaBoolean(false)),
          Field(
            UaString("offset"),
            UaId(UaUInt32(10), UaString("http://opcfoundation.org/UA/")),
            UaBoolean(false),
            UaBoolean(false))))

    testSelectedDataTypes(result, selected)
  }


  test("DataTypes from IEC-61850-7-4", IntegrationTest) {

    val typesCount = 46
    val text = loadText("nodeset/Opc.Ua.IEC61850-7-4.NodeSet2.xml")
    val result = parser.parse(text)

    result.toSeq.length shouldBe typesCount
  }

  
}

package scopcua.util.jsonencoder.impl

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import net.liftweb.json._

import scopcua.data.value.{UaArray, UaBoolean, UaByteString, UaDataValue, UaDateTime, UaDiagnosticInfo, UaEnumeration, UaExpandedNodeId, UaGuid, UaLocalizedText, UaNull, UaQualifiedName, UaStatusCode, UaString, UaStructure, UaValue, UaVariant, UaXmlElement}
import scopcua.data.value.number.integer.UaSInteger.{UaInt16, UaInt32, UaInt64, UaSByte}
import scopcua.data.value.number.integer.UaUInteger.{UaByte, UaUInt16, UaUInt32, UaUInt64}
import scopcua.util.jsonencoder.UaJsonEncoder
import scopcua.data.value.UaExtensionObject.{UaByteExtensionObject, UaXmlExtensionObject}
import scopcua.data.value.UaNodeId.{UaNumericNodeId, UaStringNodeId}
import scopcua.data.value.UaVariant.UaBuiltInId
import scopcua.data.value.number.{UaDouble, UaFloat}
import scopcua.test.tag.UnitTest
import scopcua.util.binarydecoder.impl.util.DefaultValue


class UaSmplJsonEncoderTest extends AnyFunSuite with Matchers {

  private def encoder: UaJsonEncoder =
    new UaSmplJsonEncoder(
      Map(
        UaUInt16(0) -> UaString("null"),
        UaUInt16(1) -> UaString("first")))


  private def compare(result: String, expected: String): Unit = {

    val testableResult = parse(result)
    val testableExpected = parse(expected)

    testableResult shouldBe testableExpected
  }


  private def compare(result: Seq[String], expected: Seq[String]): Unit = {

    result
      .zip(expected)
      .foreach(pair => compare(pair._1, pair._2))
  }


  private def encode(value: UaValue): String = encoder.encode(value)


  private def encode(values: Seq[UaValue]): Seq[String] = values.map(encode)


  test("Boolean: true, false", UnitTest) {

    val values = Seq(UaBoolean(true), UaBoolean(false))
    val result = encode(values)

    val expected = Seq("true", "false")

    compare(result, expected)
  }


  test("Byte: 0, 10, 255", UnitTest) {

    val values = Seq(UaByte(0), UaByte(10), UaByte(255))
    val result = encode(values)

    val expected = Seq("0", "10", "255")

    compare(result, expected)
  }


  test("SByte: -128, 0, 127", UnitTest) {

    val values = Seq(UaSByte(-128), UaSByte(0), UaSByte(127))
    val result = encode(values)

    val expected = Seq("-128", "0", "127")

    compare(result, expected)
  }


  test("Int16: −32 768, 0, 32 767", UnitTest) {

    val values = Seq(UaInt16(-32768), UaInt16(0), UaInt16(32767))
    val result = encode(values)

    val expected = Seq("-32768", "0", "32767")

    compare(result, expected)
  }


  test("UInt16: 0, 65 535", UnitTest) {

    val values = Seq(UaUInt16(0), UaUInt16(65535))
    val result = encode(values)

    val expected = Seq("0", "65535")

    compare(result, expected)
  }


  test("Int32: -2 147 483 648, 0, 2 147 483 647", UnitTest) {

    val values = Seq(UaInt32(-2147483648), UaInt32(0), UaInt32(2147483647))
    val result = encode(values)

    val expected = Seq("-2147483648", "0", "2147483647")

    compare(result, expected)
  }


  test("UInt32: 0, 4 294 967 295", UnitTest) {

    val values = Seq(UaUInt32(0), UaUInt32(4294967295L))
    val result = encode(values)

    val expected = Seq("0", "4294967295")

    compare(result, expected)
  }


  test("Int64: -9 223 372 036 854 775 808, 0, 9 223 372 036 854 775 807", UnitTest) {

    val values = Seq(UaInt64(-9223372036854775808L), UaInt64(0), UaInt64(9223372036854775807L))
    val result = encode(values)

    val expected = Seq("-9223372036854775808", "0", "9223372036854775807")

    compare(result, expected)
  }


  test("UInt64: 0, 18446744073709551615", UnitTest) {

    val values = Seq(UaUInt64(0), UaUInt64(BigInt("18446744073709551615")))
    val result = encode(values)

    val expected = Seq("0", "18446744073709551615")

    compare(result, expected)
  }


  test("Float: 1.17549e-38, 0, 3.40282e38", UnitTest) {

    val values = Seq(UaFloat(1.17549e-38F), UaFloat(0), UaFloat(3.40282e38F))
    val result = encode(values)

    val expected = Seq("1.1754900067970481E-38", "0.0", "3.402820018375656E38")

    compare(result, expected)
  }


  test("Double: 2.3e-308, 0, 1.7e308", UnitTest) {

    val values = Seq(UaDouble(2.3e-308), UaDouble(0), UaDouble(1.7e308))
    val result = encode(values)

    val expected = Seq("2.3E-308", "0.0", "1.7E308")

    compare(result, expected)
  }


  test("String: {empty}, string", UnitTest) {

    val values = Seq(UaString(""), UaString("string"))
    val result = encode(values)

    val expected = Seq("""""""", """"string"""")

    compare(result, expected)
  }


  test("DateTime: 1601-01-01T00:00Z, 30828-09-14T02:48Z", UnitTest) {

    val values = Seq(UaDateTime(0), UaDateTime(9223372036854775807L))
    val result = encode(values)

    val expected = Seq(""""1601-01-01T00:00Z"""", """"30828-09-14T02:48Z"""")

    compare(result, expected)
  }


  test("ByteString: {empty}, string", UnitTest) {

    val values =
      Seq(
        UaByteString(Seq.empty),
        UaByteString(Seq(UaByte('s'), UaByte('t'), UaByte('r'), UaByte('i'), UaByte('n'), UaByte('g'))))

    val result = encode(values)
    val expected = Seq("""""""", """"c3RyaW5n"""")

    compare(result, expected)
  }


  test("Guid: {empty}, string", UnitTest) {

    val values =
      Seq(
        UaGuid(Seq.empty),
        UaGuid(Seq(UaByte('s'), UaByte('t'), UaByte('r'), UaByte('i'), UaByte('n'), UaByte('g'))))

    val result = encode(values)
    val expected = Seq("""""""", """"string"""")

    compare(result, expected)
  }


  test("XmlElement: {empty}, string", UnitTest) {

    val values = Seq(DefaultValue.xmlElement, UaXmlElement("string"))
    val result = encode(values)

    val expected = Seq("""""""", """"string"""")

    compare(result, expected)
  }


  test("StatusCode", UnitTest) {

    val values =
      Seq(
        UaStatusCode(UaUInt32(0), UaString("Good")),
        UaStatusCode(UaUInt32(1), UaString("Decent")))

    val result = encode(values)

    val expected =
      Seq(
        """{
          | "code"   : 0,
          | "symbol" : "Good"
          |}""".stripMargin,
        """{
          | "code"   : 1,
          | "symbol" : "Decent"
          |}""".stripMargin)

    compare(result, expected)
  }


  test("QualifiedName", UnitTest) {

    val values =
      Seq(
        UaQualifiedName(
          name           = UaString("someName"),
          namespaceIndex = UaUInt16(0)),
        UaQualifiedName(
          name           = UaString("nameSome"),
          namespaceIndex = UaUInt16(1)))

    val result = encode(values)

    val expected =
      Seq(
        """{
          | "name" : "someName",
          | "uri"  : "null"
          |}""".stripMargin,
        """{
          | "name" : "nameSome",
          | "uri"  : "first"
          |}""".stripMargin)

    compare(result, expected)
  }


  test("LocalizedText", UnitTest) {

    val values =
      Seq(
        UaLocalizedText(
          text   = UaString("firstText"),
          locale = UaString("firstLocale")),
        UaLocalizedText(
          text   = UaString("secondText"),
          locale = UaString("secondLocale")))

    val result = encode(values)

    val expected =
      Seq(
        """{
          | "locale" : "firstLocale",
          | "text"   : "firstText"
          |}""".stripMargin,
        """{
          | "locale" : "secondLocale",
          | "text"   : "secondText"
          |}""".stripMargin)

    compare(result, expected)
  }


  test("NodeId", UnitTest) {

    val values =
      Seq(
        UaNumericNodeId(
          namespaceIndex = UaUInt16(0),
          identifier     = UaUInt32(123456789)),
        UaStringNodeId(
          namespaceIndex = UaUInt16(1),
          identifier     = UaString("string")))

    val result = encode(values)

    val expected =
      Seq(
        """{
          | "idType"       : 0,
          | "namespaceUri" : "null",
          | "id"           : 123456789
          |}""".stripMargin,
        """{
          | "idType"       : 4,
          | "namespaceUri" : "first",
          | "id"           : "string"
          |}""".stripMargin)

    compare(result, expected)
  }


  test("ExtensionObject", UnitTest) {

    val values =
      Seq(
        UaByteExtensionObject(
          encodingId = UaNumericNodeId(UaUInt16(0), UaUInt32(123456789)),
          body       = UaByteString(Seq(UaByte('s'), UaByte('t'), UaByte('r'), UaByte('i'), UaByte('n'), UaByte('g')))),
        UaXmlExtensionObject(
          encodingId = UaStringNodeId(UaUInt16(1), UaString("string")),
          body       = UaXmlElement("xmlElement")))

    val result = encode(values)

    val expected =
      Seq(
        """{
          | "body"   : "c3RyaW5n",
          | "enType" : 0
          |}""".stripMargin,
        """{
          | "body"   : "xmlElement",
          | "enType" : 1
          |}""".stripMargin)

    compare(result, expected)
  }


  test("DiagnosticInfo", UnitTest) {

    val values =
      Seq(
        UaDiagnosticInfo(
          symbolicId          = UaInt32(-2147483648),
          namespaceUri        = UaInt32(-1),
          localizedText       = UaInt32(-2),
          locale              = UaInt32(-3),
          additionalInfo      = UaString(DefaultValue.string.value),
          innerStatusCode     = None,
          innerDiagnosticInfo = None),
        UaDiagnosticInfo(
          symbolicId          = UaInt32(-4),
          namespaceUri        = UaInt32(-5),
          localizedText       = UaInt32(-6),
          locale              = UaInt32(-7),
          additionalInfo      = UaString("string"),
          innerStatusCode     = None,
          innerDiagnosticInfo = None))

    val result = encode(values)

    val expected =
      Seq(
        """{
          | "innerDiagnosticInfo" : null,
          | "symbolicId"          : -2147483648,
          | "locale"              : -3,
          | "namespaceUri"        : -1,
          | "additionalInfo"      : "",
          | "innerStatusCode"     : null,
          | "localizedText"       : -2
          |}""".stripMargin,
        """{
          | "innerDiagnosticInfo" : null,
          | "symbolicId"          : -4,
          | "locale"              : -7,
          | "namespaceUri"        : -5,
          | "additionalInfo"      : "string",
          | "innerStatusCode"     : null,
          | "localizedText"       : -6
          |}""".stripMargin)

    compare(result, expected)
  }


  test("DataValue", UnitTest) {

    val values =
      Seq(
        UaDataValue(
          value             = UaVariant(UaBuiltInId.Null, UaNull),
          status            = UaStatusCode(UaUInt32(0), UaString("Good")),
          sourceTimestamp   = UaDateTime(0),
          sourcePicoSeconds = UaUInt16(0),
          serverTimestamp   = UaDateTime(9223372036854775807L),
          serverPicoSeconds = UaUInt16(65535)),
        UaDataValue(
          value             = UaVariant(UaBuiltInId.String, UaString("string")),
          status            = UaStatusCode(UaUInt32(123456), UaString("GoodNonCriticalTimeout")),
          sourceTimestamp   = UaDateTime(9223372036854775807L),
          sourcePicoSeconds = UaUInt16(32767),
          serverTimestamp   = UaDateTime(0),
          serverPicoSeconds = UaUInt16(32767)))

    val result = encode(values)

    val expected =
      Seq(
        """{
          | "serverPicoSeconds" : 65535,
          | "serverTimestamp"   : "30828-09-14T02:48Z",
          | "sourceTimestamp"   : "1601-01-01T00:00Z",
          | "status"            : { "code" : 0,"symbol" : "Good" },
          | "sourcePicoSeconds" : 0,
          | "value"             : null
          |}""".stripMargin,
        """{
          | "serverPicoSeconds" : 32767,
          | "serverTimestamp"   : "1601-01-01T00:00Z",
          | "sourceTimestamp"   : "30828-09-14T02:48Z",
          | "status"            : { "code" : 123456, "symbol" : "GoodNonCriticalTimeout" },
          | "sourcePicoSeconds" : 32767,
          | "value"             : "string"
          |}""".stripMargin)

    compare(result, expected)
  }


  test("ExpandedNodeId", UnitTest) {

    val values =
      Seq(
        UaExpandedNodeId(
          namespaceUri = UaString("someNamespace"),
          nodeId       = UaNumericNodeId(UaUInt16(0), UaUInt32(4294967295L)),
          serverIndex  = UaUInt32(0)),
        UaExpandedNodeId(
          namespaceUri = UaString("mySpace"),
          nodeId       = UaStringNodeId(UaUInt16(0), UaString("string")),
          serverIndex  = UaUInt32(65535)))

    val result = encode(values)

    val expected =
      Seq(
        """{
          | "idType"       : 0,
          | "namespaceUri" : "someNamespace",
          | "id"           : 4294967295,
          | "serverIndex"  : 0
          |}""".stripMargin,
        """{
          | "idType"       : 1,
          | "namespaceUri" : "mySpace",
          | "id"           : "string",
          | "serverIndex"  : 65535
          |}""".stripMargin)

    compare(result, expected)
  }


  test ("Quality[IEC73]", UnitTest) {

    val value =
      UaStructure(
        UaString("validity") -> UaEnumeration(UaInt32(1), UaString("invalid")),
        UaString("detailQual") ->
          UaStructure(
            UaString("overflow")     -> UaBoolean(true),
            UaString("outOfRange")   -> UaBoolean(false),
            UaString("badReference") -> UaBoolean(true),
            UaString("oscillatory")  -> UaBoolean(false),
            UaString("failure")      -> UaBoolean(true),
            UaString("oldData")      -> UaBoolean(false),
            UaString("inconsistent") -> UaBoolean(true),
            UaString("inaccurate")   -> UaBoolean(false)),
        UaString("source") -> UaEnumeration(UaInt32(0), UaString("process")),
        UaString("test") -> UaBoolean(false),
        UaString("operatorBlocked") -> UaBoolean(true))

    val result = encode(value)

    val expected =
      """{
        | "validity" : { "value": 1, "name": "invalid" },
        | "detailQual" : {
        |   "overflow"     : true,
        |   "outOfRange"   : false,
        |   "badReference" : true,
        |   "oscillatory"  : false,
        |   "failure"      : true,
        |   "oldData"      : false,
        |   "inconsistent" : true,
        |   "inaccurate"   : false
        | },
        | "source" : { "value": 0, "name": "process" },
        | "test"  : false,
        | "operatorBlocked" : true
        |}"""
        .stripMargin

    compare(result, expected)
  }


  test ("Variant with 3 dimensional array.", UnitTest) {

    val value =
      UaVariant(
        UaBuiltInId.Int32,
        UaArray(
          UaArray(
            UaArray(UaInt32(1)),
            UaArray(UaInt32(2)),
            UaArray(UaInt32(3))),
          UaArray(
            UaArray(UaInt32(4)),
            UaArray(UaInt32(5)),
            UaArray(UaInt32(6)))))

    val result = encode(value)
    val expected = "[[[1],[2],[3]],[[4],[5],[6]]]"

    compare(result, expected)
  }


  test("3 dimensional array", UnitTest) {

    val value: UaValue =
      UaArray(
        UaArray(
          UaArray(UaInt32(1)),
          UaArray(UaInt32(2)),
          UaArray(UaInt32(3))),
        UaArray(
          UaArray(UaInt32(4)),
          UaArray(UaInt32(5)),
          UaArray(UaInt32(6))))

    val result = encode(value)
    val expected = "[[[1],[2],[3]],[[4],[5],[6]]]"

    compare(result, expected)
  }


  test ("AnalogValue[IEC73]", UnitTest) {

    val value =
      UaStructure(
        UaString("i") -> UaInt32(-2147483648),
        UaString("f") -> UaFloat(12.34000015258789f))

    val result = encode(value)

    val expected =
      """{
        | "i" : -2147483648,
        | "f" : 12.34000015258789
        |}"""
        .stripMargin

    compare(result, expected)
  }


  test ("Unit[IEC73]", UnitTest) {

    val value =
      UaStructure(
        UaString("sIUnit") -> UaEnumeration(UaInt32(8), UaString("candela")),
        UaString("multiplier") -> UaEnumeration(UaInt32(1), UaString("deca")))

    val result = encode(value)
    val expected =
      """{
        | "sIUnit" : { "value" : 8, "name" : "candela" },
        | "multiplier" : { "value" : 1, "name" : "deca" }
        |}"""
        .stripMargin

    compare(result, expected)
  }

}

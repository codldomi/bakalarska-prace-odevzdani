package scopcua.util.binarydecoder.impl.util

import io.netty.buffer.UnpooledByteBufAllocator

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scopcua.data.value.UaExtensionObject.{UaByteExtensionObject, UaXmlExtensionObject}
import scopcua.data.value.UaNodeId.{UaGuidNodeId, UaNumericNodeId, UaOpaqueNodeId, UaStringNodeId}
import scopcua.data.value.UaVariant.UaBuiltInId
import scopcua.data.value.number.{UaDouble, UaFloat}
import scopcua.data.value.number.integer.UaSInteger.{UaInt16, UaInt32, UaInt64, UaSByte}
import scopcua.data.value.{UaArray, UaBoolean, UaByteString, UaDataValue, UaDateTime, UaDiagnosticInfo, UaExpandedNodeId, UaGuid, UaLocalizedText, UaNull, UaQualifiedName, UaStatusCode, UaString, UaVariant, UaXmlElement}
import scopcua.data.value.number.integer.UaUInteger.{UaByte, UaUInt16, UaUInt32, UaUInt64}
import scopcua.test.tag.UnitTest


class BinaryBufferTest extends AnyFunSuite with Matchers {


  private def createBufferWith(encodedValues: Seq[Int]*): BinaryBuffer = {

    val nettyBuffer =
      UnpooledByteBufAllocator.DEFAULT
        .buffer()

    val buffer = new BinaryBuffer(nettyBuffer, Map.empty)
    encodedValues.foreach(value => buffer.writeBytes(value))

    buffer
  }

  private def createBufferWithAndMap(encodedValues: Seq[Int]*): BinaryBuffer = {

    val nettyBuffer =
      UnpooledByteBufAllocator.DEFAULT
        .buffer()

    val statusCodesMap =
      Map(
        UaUInt32(1) -> UaStatusCode(UaUInt32(0), UaString("minimum")),
        UaUInt32(2) -> UaStatusCode(UaUInt32(2147483647), UaString("secondHighest")),
        UaUInt32(3) -> UaStatusCode(UaUInt32(4294967295L), UaString("maximum")),
        UaUInt32(11141120) -> UaStatusCode(UaUInt32(123456), UaString("GoodNonCriticalTimeout")))

    val buffer = new BinaryBuffer(nettyBuffer, statusCodesMap)
    encodedValues.foreach(value => buffer.writeBytes(value))

    buffer
  }


  test("Boolean: true, false", UnitTest) {

    val len = 2

    val a = Seq(1)
    val b = Seq(0)

    val buffer = createBufferWith(a, b)
    val expected = Seq(UaBoolean(true), UaBoolean(false))

    val result = for (_ <- 0 until len) yield buffer.readBoolean()

    result shouldBe expected
  }


  test("Byte: 0, 10, 255", UnitTest) {

    val len = 3

    val a = Seq(0)
    val b = Seq(10)
    val c = Seq(255)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaByte(0), UaByte(10), UaByte(255))

    val result = for (_ <- 0 until len) yield buffer.readByte()

    result shouldBe expected
  }


  test("SByte: -128, 0, 127", UnitTest) {

    val len = 3

    val a = Seq(-128)
    val b = Seq(0)
    val c = Seq(127)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaSByte(-128), UaSByte(0), UaSByte(127))

    val result = for (_ <- 0 until len) yield buffer.readSByte()

    result shouldBe expected
  }


  test("UInt16: 0, 65535, 32767", UnitTest) {

    val len = 3

    val a = Seq(0, 0)
    val b = Seq(255, 255)
    val c = Seq(255, 127)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaUInt16(0), UaUInt16(65535), UaUInt16(32767))

    val result = for (_ <- 0 until len) yield buffer.readUInt16()

    result shouldBe expected
  }


  test("Int16: -32768, 0, 32767", UnitTest) {

    val len = 3

    val a = Seq(0, 128)
    val b = Seq(0, 0)
    val c = Seq(255, 127)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaInt16(-32768), UaInt16(0), UaInt16(32767))

    val result = for (_ <- 0 until len) yield buffer.readInt16()

    result shouldBe expected
  }


  test("UInt32: 0, 4294967295, 2147483647", UnitTest) {

    val len = 3

    val a = Seq(0, 0, 0, 0)
    val b = Seq(255, 255, 255, 255)
    val c = Seq(255, 255, 255, 127)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaUInt32(0), UaUInt32(4294967295L), UaUInt32(2147483647L))

    val result = for (_ <- 0 until len) yield buffer.readUInt32()

    result shouldBe expected
  }


  test("Int32: -2147483648, 0, 2147483647", UnitTest) {

    val len = 3

    val a = Seq(0, 0, 0, 128)
    val b = Seq(0, 0, 0, 0)
    val c = Seq(255, 255, 255, 127)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaInt32(-2147483648), UaInt32(0), UaInt32(2147483647))

    val result = for (_ <- 0 until len) yield buffer.readInt32()

    result shouldBe expected
  }


  test("UInt64: 0, 18446744073709551615, 9223372036854775807", UnitTest) {

    val len = 3

    val a = Seq(0, 0, 0, 0, 0, 0, 0, 0)
    val b = Seq(255, 255, 255, 255, 255, 255, 255, 255)
    val c = Seq(255, 255, 255, 255, 255, 255, 255, 127)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaUInt64(0), UaUInt64(BigInt("18446744073709551615")), UaUInt64(BigInt("9223372036854775807")))

    val result = for (_ <- 0 until len) yield buffer.readUInt64()

    result shouldBe expected
  }


  test("Int64: -9223372036854775808, 0, 9223372036854775807", UnitTest) {

    val len = 3

    val a = Seq(0, 0, 0, 0, 0, 0, 0, 128)
    val b = Seq(0, 0, 0, 0, 0, 0, 0, 0)
    val c = Seq(255, 255, 255, 255, 255, 255, 255, 127)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaInt64(-9223372036854775808L), UaInt64(0), UaInt64(9223372036854775807L))

    val result = for (_ <- 0 until len) yield buffer.readInt64()

    result shouldBe expected
  }


  test("Float: 1.17549e-38, 0, 3.40282e38", UnitTest) {

    val len = 3

    val a = Seq(0xe1, 0xff, 0x7f, 0x00)
    val b = Seq(0, 0, 0, 0)
    val c = Seq(0xee, 0xff, 0x7f, 0x7f)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaFloat(1.17549e-38f), UaFloat(0), UaFloat(3.40282e38f))

    val result = for (_ <- 0 until len) yield buffer.readFloat()

    result shouldBe expected
  }


  test("Double: 2.3e-308, 0, 1.7e308", UnitTest) {

    val len = 3

    val a = Seq(0xb0, 0xca, 0x6e, 0x47, 0xed, 0x89, 0x10, 0x00)
    val b = Seq(0, 0, 0, 0, 0, 0, 0, 0)
    val c = Seq(0x76, 0x3b, 0x77, 0x30, 0xd1, 0x42, 0xee, 0x7f)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaDouble(2.3e-308d), UaDouble(0), UaDouble(1.7e308d))

    val result = for (_ <- 0 until len) yield buffer.readDouble()

    result shouldBe expected
  }


  test("String: string, {empty},  helloWorld", UnitTest) {

    val len = 3

    val a = Seq(6, 0, 0, 0, 115, 116, 114, 105, 110, 103)
    val b = Seq(0, 0, 0, 0)
    val c = Seq(10, 0, 0, 0, 104, 101, 108, 108, 111, 87, 111, 114, 108, 100)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaString("string"), UaString(""), UaString("helloWorld"))

    val result = for (_ <- 0 until len) yield buffer.readString()

    result shouldBe expected
  }


  test("DateTime (input as long long): -9223372036854775808, 0,  9223372036854775807", UnitTest) {

    val len = 3

    val a = Seq(0, 0, 0, 0, 0, 0, 0, 128)
    val b = Seq(0, 0, 0, 0, 0, 0, 0, 0)
    val c = Seq(255, 255, 255, 255, 255, 255, 255, 127)

    val buffer = createBufferWith(a, b, c)
    val expected = Seq(UaDateTime(-9223372036854775808L), UaDateTime(0), UaDateTime(9223372036854775807L))

    val result = for (_ <- 0 until len) yield buffer.readDateTime()

    result shouldBe expected
  }


  test("ByteString: string, {empty},  helloWorld", UnitTest) {

    val len = 3

    val a = Seq(6, 0, 0, 0, 115, 116, 114, 105, 110, 103)
    val b = Seq(0, 0, 0, 0)
    val c = Seq(10, 0, 0, 0, 104, 101, 108, 108, 111, 87, 111, 114, 108, 100)

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaByteString(Seq(UaByte(115), UaByte(116), UaByte(114), UaByte(105), UaByte(110), UaByte(103))),
        UaByteString(Seq.empty),
        UaByteString(Seq(UaByte(104), UaByte(101), UaByte(108), UaByte(108), UaByte(111), UaByte(87), UaByte(111), UaByte(114), UaByte(108), UaByte(100))))

    val result = for (_ <- 0 until len) yield buffer.readByteString()

    result shouldBe expected
  }


  test("Guid: all zeroes, MS bit in zero,  all ones", UnitTest) {

    val len = 3

    val a = Seq(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    val b = Seq(255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 127)
    val c = Seq(255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255)

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaGuid(Seq(UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0), UaByte(0))),
        UaGuid(Seq(UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(127))),
        UaGuid(Seq(UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255))))

    val result = for (_ <- 0 until len) yield buffer.readGuid()

    result shouldBe expected
  }


  test("XmlElement: string, {empty},  helloWorld", UnitTest) {

    val len = 3

    val a = Seq(6, 0, 0, 0, 115, 116, 114, 105, 110, 103)
    val b = Seq(0, 0, 0, 0)
    val c = Seq(10, 0, 0, 0, 104, 101, 108, 108, 111, 87, 111, 114, 108, 100)

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaXmlElement("string"),
        UaXmlElement(""),
        UaXmlElement("helloWorld"))

    val result = for (_ <- 0 until len) yield buffer.readXmlElement()

    result shouldBe expected
  }


  test("StatusCode: c=0;s=lowest, c=2147483647;s=secondHighest, c=4294967295;s=maximum", UnitTest) {

    val len = 3

    val a = Seq(1, 0, 0, 0)
    val b = Seq(2, 0, 0, 0)
    val c = Seq(3, 0, 0, 0)

    val buffer = createBufferWithAndMap(a, b, c)
    val expected =
      Seq(
        UaStatusCode(UaUInt32(0), UaString("minimum")),
        UaStatusCode(UaUInt32(2147483647), UaString("secondHighest")),
        UaStatusCode(UaUInt32(4294967295L), UaString("maximum")))

    val result = for (_ <- 0 until len) yield buffer.readStatusCode()

    result shouldBe expected
  }


  test("QualifiedName: i=0;n=lowest, i=32767;n=secondHighest, i=65535;n=maximum", UnitTest) {

    val len = 3

    val a = Seq(0, 0, 6, 0, 0, 0, 'l', 'o', 'w', 'e', 's', 't')
    val b = Seq(255, 127, 13, 0, 0, 0, 's', 'e', 'c', 'o', 'n', 'd', 'H', 'i', 'g', 'h', 'e', 's', 't')
    val c = Seq(255, 255, 7, 0, 0, 0, 'm', 'a', 'x', 'i', 'm', 'u', 'm')

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaQualifiedName(UaString("lowest"), UaUInt16(0)),
        UaQualifiedName(UaString("secondHighest"), UaUInt16(32767)),
        UaQualifiedName(UaString("maximum"), UaUInt16(65535)))

    val result = for (_ <- 0 until len) yield buffer.readQualifiedName()

    result shouldBe expected
  }


  test("LocalizedText: m=0x01;l=locale;t={default}, m=0x02;l={default};t=text", UnitTest) {

    val len = 3

    val a = Seq(0x01, 6, 0, 0, 0, 'l', 'o', 'c', 'a', 'l', 'e')
    val b = Seq(0x02, 4, 0, 0, 0, 't', 'e', 'x', 't')
    val c = Seq(0x10)

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaLocalizedText(UaString(""), UaString("locale")),
        UaLocalizedText(UaString("text"), UaString("")),
        UaLocalizedText(UaString(""), UaString("")))

    val result = for (_ <- 0 until len) yield buffer.readLocalizedText()

    result shouldBe expected
  }


  test("NumericNodeId: m=0x00,i=255; m=0x01,i16=255,i32=65535, m=0x02,i16=32767,i32=2147483647", UnitTest) {

    val len = 3

    val a = Seq(0x00, 255)
    val b = Seq(0x01, 255, 255, 255)
    val c = Seq(0x02, 255, 127, 255, 255, 255, 127)

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaNumericNodeId(UaUInt16(0), UaUInt32(255)),
        UaNumericNodeId(UaUInt16(255), UaUInt32(65535)),
        UaNumericNodeId(UaUInt16(32767), UaUInt32(2147483647)))

    val result = for (_ <- 0 until len) yield buffer.readNodeId()

    result shouldBe expected
  }


  test("NodeId: m=0x03,i=32767,s=string; m=0x04,i=65407,g=only LS zero, m=0x05,i=65535,bs=byteString", UnitTest) {

    val len = 3

    val a = Seq(0x03, 255, 127, 6, 0, 0, 0 , 's', 't', 'r', 'i', 'n', 'g')
    val b = Seq(0x04, 127, 255, 127, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255)
    val c = Seq(0x05, 255, 255, 10, 0, 0, 0, 'b', 'y', 't', 'e', 'S', 't', 'r', 'i', 'n', 'g')

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaStringNodeId(
          UaUInt16(32767),
          UaString("string")),
        UaGuidNodeId(
          UaUInt16(65407),
          UaGuid(Seq(UaByte(127), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255), UaByte(255)))),
        UaOpaqueNodeId(
          UaUInt16(65535),
          UaByteString(Seq(UaByte('b'), UaByte('y'), UaByte('t'), UaByte('e'), UaByte('S'), UaByte('t'), UaByte('r'), UaByte('i'), UaByte('n'), UaByte('g')))))

    val result = for (_ <- 0 until len) yield buffer.readNodeId()

    result shouldBe expected
  }


  test("NodeId: ns=5;i=10, ns=5;s=string, s=", UnitTest) {

    val len = 3

    val a = Seq(1, 5, 10, 0)
    val b = Seq(3, 5, 0, 6, 0, 0, 0, 115, 116, 114, 105, 110, 103)
    val c = Seq(3, 0, 0, 255, 255, 255, 255)

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaNumericNodeId(UaUInt16(5), UaUInt32(10)),
        UaStringNodeId(UaUInt16(5), UaString("string")),
        UaStringNodeId(UaUInt16(0), UaString("")))

    val result = for (_ <- 0 until len) yield buffer.readNodeId()

    result shouldBe expected
  }


  test("ExtensionObject: ei=string,i=32767, f=0,bs={empty},f=1,bs=byteString,f=2,x=xmlE", UnitTest) {

    val len = 3

    val a = Seq(0x03, 255, 127, 6, 0, 0, 0 , 's', 't', 'r', 'i', 'n', 'g', 0)
    val b = Seq(0x03, 255, 127, 6, 0, 0, 0 , 's', 't', 'r', 'i', 'n', 'g', 1, 10, 0, 0, 0, 'b', 'y', 't', 'e', 'S', 't', 'r', 'i', 'n', 'g')
    val c = Seq(0x03, 255, 127, 6, 0, 0, 0 , 's', 't', 'r', 'i', 'n', 'g', 2, 4, 0, 0, 0, 'x', 'm', 'l', 'E')

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaByteExtensionObject(
          UaStringNodeId(UaUInt16(32767), UaString("string")),
          UaByteString(Seq.empty)),
        UaByteExtensionObject(
          UaStringNodeId(UaUInt16(32767), UaString("string")),
          UaByteString(Seq[UaByte](UaByte('b'), UaByte('y'), UaByte('t'), UaByte('e'), UaByte('S'), UaByte('t'), UaByte('r'), UaByte('i'), UaByte('n'), UaByte('g')))),
        UaXmlExtensionObject(
          UaStringNodeId(UaUInt16(32767), UaString("string")),
          UaXmlElement("xmlE")))

    val result = for (_ <- 0 until len) yield buffer.readExtensionObject()

    result shouldBe expected
  }


  test("Variant: i32=-2147483648;s=string;arr=3x3{1-9}", UnitTest) {

    val len = 3

    val a = Seq(6, 0, 0, 0, 128)
    val b = Seq(12, 6, 0, 0, 0, 115, 116, 114, 105, 110, 103)
    val c = Seq(203, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 8, 64, 0, 0, 0, 0, 0, 0, 16, 64, 0, 0, 0, 0, 0, 0, 20, 64, 0, 0, 0, 0, 0, 0, 24, 64, 0, 0, 0, 0, 0, 0, 28, 64, 0, 0, 0, 0, 0, 0, 32, 64, 0, 0, 0, 0, 0, 0, 34, 64, 2, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0)

    val buffer = createBufferWithAndMap(a, b, c)

    val expected =
      Seq(
        UaVariant(
          UaBuiltInId.Int32,
          UaInt32(-2147483648)),
        UaVariant(
          UaBuiltInId.String,
          UaString("string")),
        UaVariant(
          UaBuiltInId.Double,
          UaArray(
            UaArray(UaDouble(1), UaDouble(2), UaDouble(3)),
            UaArray(UaDouble(4), UaDouble(5), UaDouble(6)),
            UaArray(UaDouble(7), UaDouble(8), UaDouble(9)))))

    val result = for (_ <- 0 until len) yield buffer.readVariant()

    result shouldBe expected
  }


  test("DiagnosticInfoInts: i=-2147483648; m=0x01; m=0x02; m=0x04; m=0x08", UnitTest) {

    val len = 4

    val a = Seq(0x01, 0, 0, 0, 128)
    val b = Seq(0x02, 0, 0, 0, 128)
    val c = Seq(0x04, 0, 0, 0, 128)
    val d = Seq(0x08, 0, 0, 0, 128)

    val buffer = createBufferWith(a, b, c, d)

    val expected =
      Seq(
        UaDiagnosticInfo(
          UaInt32(-2147483648),
          UaInt32(-1),
          UaInt32(-1),
          UaInt32(-1),
          UaString(""),
          None,
          None),
        UaDiagnosticInfo(
          UaInt32(-1),
          UaInt32(-2147483648),
          UaInt32(-1),
          UaInt32(-1),
          UaString(""),
          None,
          None),
        UaDiagnosticInfo(
          UaInt32(-1),
          UaInt32(-1),
          UaInt32(-2147483648),
          UaInt32(-1),
          UaString(""),
          None,
          None),
        UaDiagnosticInfo(
          UaInt32(-1),
          UaInt32(-1),
          UaInt32(-1),
          UaInt32(-2147483648),
          UaString(""),
          None,
          None))

    val result = for (_ <- 0 until len) yield buffer.readDiagnosticInfo()

    result shouldBe expected
  }


  test("DiagnosticInfo: m=0x10,s=string; m=0x20,i=0,s=minimum; m=0x40,i=2147483647", UnitTest) {

    val len = 3

    val a = Seq(0x10, 6, 0, 0, 0, 's', 't', 'r', 'i', 'n', 'g')
    val b = Seq(0x20, 1, 0, 0, 0)
    val c = Seq(0x40, 0x02, 255, 255, 255, 127)

    val buffer = createBufferWithAndMap(a, b, c)

    val expected =
      Seq(
        UaDiagnosticInfo(
          UaInt32(-1),
          UaInt32(-1),
          UaInt32(-1),
          UaInt32(-1),
          UaString("string"),
          None,
          None),
        UaDiagnosticInfo(
          UaInt32(-1),
          UaInt32(-1),
          UaInt32(-1),
          UaInt32(-1),
          UaString(""),
          Some(UaStatusCode(UaUInt32(0), UaString("minimum"))),
          None),
        UaDiagnosticInfo(
          UaInt32(-1),
          UaInt32(-1),
          UaInt32(-1),
          UaInt32(-1),
          UaString(""),
          None,
          Some(
            UaDiagnosticInfo(
              UaInt32(-1),
              UaInt32(2147483647),
              UaInt32(-1),
              UaInt32(-1),
              UaString(""),
              None,
              None))))

    val result = for (_ <- 0 until len) yield buffer.readDiagnosticInfo()

    result shouldBe expected
  }


  test("DataValue: srcD=10000000,srcP=65535; serD=10000000,serP=65535; vs=string,sc=11141120,srcD=10000,srcP=32767,serD=10000,serP=32767", UnitTest) {

    val len = 3

    val a = Seq(12, 128, 150, 152, 0, 0, 0, 0, 0, 255, 255)
    val b = Seq(48, 128, 150, 152, 0, 0, 0, 0, 0, 255, 255)
    val c = Seq(63, 12, 6, 0, 0, 0, 115, 116, 114, 105, 110, 103, 0, 0, 170, 0, 16, 39, 0, 0, 0, 0, 0, 0, 255, 127, 16, 39, 0, 0, 0, 0, 0, 0, 255, 127)

    val buffer = createBufferWithAndMap(a, b, c)

    val expected =
      Seq(
        UaDataValue(
          UaVariant(
            UaBuiltInId.Null,
            UaNull),
          UaStatusCode(UaUInt32(0), UaString("Good")),
          UaDateTime((10L * 1000L) * 1000L),
          UaUInt16(65535),
          UaDateTime(0),
          UaUInt16(0)),
        UaDataValue(
          UaVariant(
            UaBuiltInId.Null,
            UaNull),
          UaStatusCode(UaUInt32(0), UaString("Good")),
          UaDateTime(0),
          UaUInt16(0),
          UaDateTime((10L * 1000L) * 1000L),
          UaUInt16(65535)),
        UaDataValue(
          UaVariant(
            UaBuiltInId.String,
            UaString("string")),
          UaStatusCode(UaUInt32(123456), UaString("GoodNonCriticalTimeout")),
          UaDateTime(10L * 1000L),
          UaUInt16(32767),
          UaDateTime(10L * 1000L),
          UaUInt16(32767)))

    val result = for (_ <- 0 until len) yield buffer.readDataValue()

    result shouldBe expected
  }


  test("ExpandedNodeIsUri: ns=someNamespace,numId=65535,4294967295,sId=0; ns=mySpace,strId=32767,string,sId=4294967295; ns=othersSpace,numId=0,65535,sId=2147483647", UnitTest) {

    val len = 3

    val a = Seq(130, 255, 255, 255, 255, 13, 0, 0, 0, 115, 111, 109, 101, 78, 97, 109, 101, 115, 112, 97, 99, 101)
    val b = Seq(195, 6, 0, 0, 0, 115, 116, 114, 105, 110, 103, 7, 0, 0, 0, 109, 121, 83, 112, 97, 99, 101, 255, 255, 0, 0)
    val c = Seq(193, 255, 127, 11, 0, 0, 0, 111, 116, 104, 101, 114, 115, 83, 112, 97, 99, 101, 255, 255, 255, 127)

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaExpandedNodeId(
          UaString("someNamespace"),
          UaNumericNodeId(UaUInt16(0), UaUInt32(4294967295L)),
          UaUInt32(0)),
        UaExpandedNodeId(
          UaString("mySpace"),
          UaStringNodeId(UaUInt16(0), UaString("string")),
          UaUInt32(65535)),
        UaExpandedNodeId(
          UaString("othersSpace"),
          UaNumericNodeId(UaUInt16(0), UaUInt32(32767)),
          UaUInt32(2147483647)))

    val result = for (_ <- 0 until len) yield buffer.readExpandedNodeId()

    result shouldBe expected
  }


  test("ExpandedNodeIdNoUri: numId=65535,4294967295,sId=0; strId=32767,string,sId=4294967295; numId=0,65535,sId=2147483647", UnitTest) {

    val len = 3

    val a = Seq(2, 255, 255, 255, 255, 255, 255)
    val b = Seq(67, 255, 127, 6, 0, 0, 0, 115, 116, 114, 105, 110, 103, 255, 255, 255, 255)
    val c = Seq(65, 0, 255, 255, 255, 255, 255, 127)

    val buffer = createBufferWith(a, b, c)

    val expected =
      Seq(
        UaExpandedNodeId(
          UaString(""),
          UaNumericNodeId(UaUInt16(65535), UaUInt32(4294967295L)),
          UaUInt32(0)),
        UaExpandedNodeId(
          UaString(""),
          UaStringNodeId(UaUInt16(32767), UaString("string")),
          UaUInt32(4294967295L)),
        UaExpandedNodeId(
          UaString(""),
          UaNumericNodeId(UaUInt16(0), UaUInt32(65535)),
          UaUInt32(2147483647)))

    val result = for (_ <- 0 until len) yield buffer.readExpandedNodeId()

    result shouldBe expected
  }


  test("BehaviourModeKind: 3 (test)") {

    val bytes = Seq(6, 3, 0, 0, 0)

    val buffer = createBufferWith(bytes)
    val result = buffer.readVariant()

    val expected =
      UaVariant(
        UaBuiltInId.Int32,
        UaInt32(3))

    result shouldBe expected
  }
}

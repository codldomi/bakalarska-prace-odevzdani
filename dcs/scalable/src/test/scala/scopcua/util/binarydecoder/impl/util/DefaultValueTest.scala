package scopcua.util.binarydecoder.impl.util

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scopcua.data.UaDataType.{GeneralEnumeration, GeneralStructure}
import scopcua.data.UaDataType.GeneralStructure.Field
import scopcua.data.{UaDataType, UaId}
import scopcua.data.value._
import scopcua.data.value.number.integer.UaSInteger.UaInt32
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.data.value.{UaBoolean, UaString, UaStructure}
import scopcua.test.tag.UnitTest


class DefaultValueTest extends AnyFunSuite with Matchers {

  private val _detailQual =
    GeneralStructure(
      UaBoolean(false),
      UaString("DetailQual"),
      UaId(UaUInt32(23), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
      Field(
        UaString("overflow"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("outOfRange"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("badReference"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("oscillatory"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("failure"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("oldData"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("inconsistent"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("inaccurate"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)))

  private val _validityKind =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("ValidityKind"),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      UaString("good") -> UaInt32(0),
      UaString("invalid") -> UaInt32(1),
      UaString("reserved") -> UaInt32(2),
      UaString("questionable") -> UaInt32(3))

  private val _sourceKind =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("SourceKind"),
      UaId(UaUInt32(24), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      UaString("process") -> UaInt32(0),
      UaString("substituted") -> UaInt32(1))

  private val _quality =
    GeneralStructure(
      UaBoolean(false),
      UaString("Quality"),
      UaId(UaUInt32(17), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
      Field(
        UaString("validity"),
        UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("detailQual"),
        UaId(UaUInt32(23), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("source"),
        UaId(UaUInt32(24), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("test"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("operatorBlocked"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)))

  private val _types = {

    val customTypes =
      Map(
      _quality.id -> _quality,
      _detailQual.id -> _detailQual,
      _validityKind.id -> _validityKind,
      _sourceKind.id -> _sourceKind)

    UaDataType.builtIns ++ customTypes
  }


  test ("Quality[IEC73]", UnitTest) {

    val result = DefaultValue.structure(_quality, _types)

    val expected =
      UaStructure(
        UaString("validity") -> UaEnumeration(UaInt32(0), UaString("good")),
        UaString("detailQual") ->
          UaStructure(
            UaString("overflow") -> UaBoolean(false),
            UaString("outOfRange") -> UaBoolean(false),
            UaString("badReference") -> UaBoolean(false),
            UaString("oscillatory") -> UaBoolean(false),
            UaString("failure") -> UaBoolean(false),
            UaString("oldData") -> UaBoolean(false),
            UaString("inconsistent") -> UaBoolean(false),
            UaString("inaccurate") -> UaBoolean(false)),
        UaString("source") -> UaEnumeration(UaInt32(0), UaString("process")),
        UaString("test") -> UaBoolean(false),
        UaString("operatorBlocked") -> UaBoolean(false))

    result shouldBe expected
  }

}

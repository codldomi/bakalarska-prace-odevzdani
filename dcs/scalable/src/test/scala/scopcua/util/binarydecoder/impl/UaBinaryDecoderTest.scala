package scopcua.util.binarydecoder.impl

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import scopcua.data.UaId
import scopcua.data.value.UaValue
import scopcua.util.binarydecoder.UaBinaryDecoder


abstract class UaBinaryDecoderTest extends AnyFunSuite with Matchers {

  def decoder: UaBinaryDecoder

  final def decode(value: String, expected: UaValue, typeId: UaId): Unit = {

    val result = decoder.decode(value, typeId)
    result shouldBe expected
  }

  final def decode(values: Seq[String], expected: Seq[UaValue], typeId: UaId): Unit = {

    val result = values.map(value => decoder.decode(value, typeId))
    result shouldBe expected
  }

}

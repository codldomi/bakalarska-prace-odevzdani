package scopcua.util.binarydecoder.impl

import scopcua.data.{UaDataType, UaId}
import scopcua.data.UaDataType.{GeneralEnumeration, GeneralStructure}
import scopcua.data.UaDataType.GeneralStructure.Field
import scopcua.data.value.number.UaFloat
import scopcua.data.value.number.integer.UaSInteger.UaInt32
import scopcua.data.value.{UaBoolean, UaEnumeration,  UaString, UaStructure}
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.test.tag.IntegrationTest
import scopcua.util.binarydecoder.UaBinaryDecoder

import scala.language.implicitConversions


class UaStdBinaryDecoderTest extends UaBinaryDecoderTest {

  private val _detailQual =
    GeneralStructure(
      UaBoolean(false),
      UaString("DetailQual"),
      UaId(UaUInt32(23), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
      Field(
        UaString("overflow"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("outOfRange"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("badReference"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("oscillatory"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("failure"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("oldData"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("inconsistent"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("inaccurate"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)))

  private val _validityKind =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("ValidityKind"),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      UaString("good") -> UaInt32(0),
      UaString("invalid") -> UaInt32(1),
      UaString("reserved") -> UaInt32(2),
      UaString("questionable") -> UaInt32(3))

  private val _sourceKind =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("SourceKind"),
      UaId(UaUInt32(24), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      UaString("process") -> UaInt32(0),
      UaString("substituted") -> UaInt32(1))

  private val _quality =
    GeneralStructure(
      UaBoolean(false),
      UaString("Quality"),
      UaId(UaUInt32(17), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
      Field(
        UaString("validity"),
        UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("detailQual"),
        UaId(UaUInt32(23), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("source"),
        UaId(UaUInt32(24), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("test"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("operatorBlocked"),
        UaId(UaUInt32(1), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)))

  private val _timeStamp =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("TimeStamp"),
      UaId(UaUInt32(5), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      UaString("source") -> UaInt32(0),
      UaString("server") -> UaInt32(1),
      UaString("both") -> UaInt32(2),
      UaString("neither") -> UaInt32(3),
      UaString("invalid") -> UaInt32(4))

  private val _sIUnitKind =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("SIUnitKind"),
      UaId(UaUInt32(97), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      UaString("") -> UaInt32(1),
      UaString("meter") -> UaInt32(2),
      UaString("kilogram") -> UaInt32(3),
      UaString("second") -> UaInt32(4),
      UaString("amper") -> UaInt32(5),
      UaString("kelvin") -> UaInt32(6),
      UaString("mole") -> UaInt32(7),
      UaString("candela") -> UaInt32(8),
      UaString("degree") -> UaInt32(9),
      UaString("radian") -> UaInt32(10))

  private val _multiplierKind =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("MultiplierKind"),
      UaId(UaUInt32(81), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      UaString("mili") -> UaInt32(-3),
      UaString("centi") -> UaInt32(-2),
      UaString("deci") -> UaInt32(-1),
      UaString("") -> UaInt32(0),
      UaString("deca") -> UaInt32(1),
      UaString("hecto") -> UaInt32(2),
      UaString("kilo") -> UaInt32(3))

  private val _analogValue =
    GeneralStructure(
      UaBoolean(false),
      UaString("AnalogValue"),
      UaId(UaUInt32(117), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
      Field(
        UaString("i"),
        UaId(UaUInt32(6), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("f"),
        UaId(UaUInt32(10), UaString("http://opcfoundation.org/UA/")),
        UaBoolean(false),
        UaBoolean(false)))

  private val _unit =
    GeneralStructure(
      UaBoolean(false),
      UaString("AnalogValue"),
      UaId(UaUInt32(133), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
      UaId(UaUInt32(22), UaString("http://opcfoundation.org/UA/")),
      Field(
        UaString("sIUnit"),
        UaId(UaUInt32(97), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)),
      Field(
        UaString("multiplier"),
        UaId(UaUInt32(81), UaString("http://opcfoundation.org/UA/IEC61850-7-3")),
        UaBoolean(false),
        UaBoolean(false)))

  private val _behaviourModeKind =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("BehaviourModeKind"),
      UaId(UaUInt32(5), UaString("http://opcfoundation.org/UA/IEC61850-7-4")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      UaString("on") -> UaInt32(1),
      UaString("blocked") -> UaInt32(2),
      UaString("test") -> UaInt32(3),
      UaString("testBlocked") -> UaInt32(4),
      UaString("off") -> UaInt32(5))

  private val _healthKind =
    GeneralEnumeration(
      UaBoolean(false),
      UaString("HealthKind"),
      UaId(UaUInt32(25), UaString("http://opcfoundation.org/UA/IEC61850-7-4")),
      UaId(UaUInt32(29), UaString("http://opcfoundation.org/UA/")),
      UaString("ok") -> UaInt32(1),
      UaString("warning") -> UaInt32(2),
      UaString("alarm") -> UaInt32(3))


  private val _decoder: UaStdBinaryDecoder = {

    val types =
      Map(
        _quality.id -> _quality,
        _detailQual.id -> _detailQual,
        _validityKind.id -> _validityKind,
        _sourceKind.id -> _sourceKind,
        _timeStamp.id -> _timeStamp,
        _sIUnitKind.id -> _sIUnitKind,
        _multiplierKind.id -> _multiplierKind,
        _analogValue.id -> _analogValue,
        _unit.id -> _unit,
        _behaviourModeKind.id -> _behaviourModeKind,
        _healthKind.id -> _healthKind)

    new UaStdBinaryDecoder(UaDataType.builtIns ++ types, Map.empty)
  }

  override def decoder: UaBinaryDecoder = _decoder

  private implicit def bytes2Strings(encoded: Seq[Int]): String = encoded.map(_.toChar).mkString


  test("Quality[IEC73] - from variant", IntegrationTest) {

    val bytes = Seq(22, 1, 2, 18, 0, 1, 18, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1)

    val expected =
      UaStructure(
        UaString("validity") -> UaEnumeration(UaInt32(1), UaString("invalid")),
        UaString("detailQual") ->
          UaStructure(
            UaString("overflow") -> UaBoolean(true),
            UaString("outOfRange") -> UaBoolean(false),
            UaString("badReference") -> UaBoolean(true),
            UaString("oscillatory") -> UaBoolean(false),
            UaString("failure") -> UaBoolean(true),
            UaString("oldData") -> UaBoolean(false),
            UaString("inconsistent") -> UaBoolean(true),
            UaString("inaccurate") -> UaBoolean(false)),
        UaString("source") -> UaEnumeration(UaInt32(0), UaString("process")),
        UaString("test") -> UaBoolean(false),
        UaString("operatorBlocked") -> UaBoolean(true))

    val result = _decoder.decodeVariant(bytes, _quality.id)

    result shouldBe expected
  }


  test("Quality[IEC73] - direct", IntegrationTest) {

    val bytes = Seq(1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1)

    val expected =
      UaStructure(
        UaString("validity") -> UaEnumeration(UaInt32(1), UaString("invalid")),
        UaString("detailQual") ->
          UaStructure(
            UaString("overflow") -> UaBoolean(true),
            UaString("outOfRange") -> UaBoolean(false),
            UaString("badReference") -> UaBoolean(true),
            UaString("oscillatory") -> UaBoolean(false),
            UaString("failure") -> UaBoolean(true),
            UaString("oldData") -> UaBoolean(false),
            UaString("inconsistent") -> UaBoolean(true),
            UaString("inaccurate") -> UaBoolean(false)),
        UaString("source") -> UaEnumeration(UaInt32(0), UaString("process")),
        UaString("test") -> UaBoolean(false),
        UaString("operatorBlocked") -> UaBoolean(true))

    val result = _decoder.decode(bytes, _quality.id)

    result shouldBe expected
  }


  test("TimeStamp[IEC73] - from variant", IntegrationTest) {

    val bytes = Seq(6, 1, 0, 0, 0)

    val expected = UaEnumeration(UaInt32(1), UaString("server"))
    val result = _decoder.decodeVariant(bytes, _timeStamp.id)

    result shouldBe expected
  }


  test("TimeStamp[IEC73] - direct", IntegrationTest) {

    val bytes = Seq(1, 0, 0, 0)

    val expected = UaEnumeration(UaInt32(1), UaString("server"))
    val result = _decoder.decode(bytes, _timeStamp.id)

    result shouldBe expected
  }

  test("SIUnitKind[IEC73] - from variant", IntegrationTest) {

    val bytes = Seq(6, 5, 0, 0, 0)

    val expected = UaEnumeration(UaInt32(5), UaString("amper"))
    val result = _decoder.decodeVariant(bytes, _sIUnitKind.id)

    result shouldBe expected
  }


  test("SIUnitKind[IEC73] - direct", IntegrationTest) {

    val bytes = Seq(5, 0, 0, 0)

    val expected = UaEnumeration(UaInt32(5), UaString("amper"))
    val result = _decoder.decode(bytes, _sIUnitKind.id)

    result shouldBe expected
  }


  test("SIUnitKind[IEC73] - faulty", IntegrationTest) {

    val incorrectType = Seq(5, 5, 0, 0, 0)
    val bytesVariant = Seq(6, 5, 0, 0)
    val bytes = Seq(5, 0, 0)

    val expected = UaEnumeration(UaInt32(5), UaString("amper"))
    val result = _decoder.decode(incorrectType, _sIUnitKind.id)

    assert(result != expected)

    assertThrows[IndexOutOfBoundsException] {
      _decoder.decodeVariant(bytesVariant, _sIUnitKind.id)
    }

    assertThrows[IndexOutOfBoundsException] {
      _decoder.decode(bytes, _sIUnitKind.id)
    }
  }


  test("MultiplierKind[IEC73] - from variant", IntegrationTest) {

    val bytes = Seq(6, 253, 255, 255, 255)

    val expected = UaEnumeration(UaInt32(-3), UaString("mili"))
    val result = _decoder.decodeVariant(bytes, _multiplierKind.id)

    result shouldBe expected
  }


  test("MultiplierKind[IEC73] - direct", IntegrationTest) {

    val bytes = Seq(253, 255, 255, 255)

    val expected = UaEnumeration(UaInt32(-3), UaString("mili"))
    val result = _decoder.decode(bytes, _multiplierKind.id)

    result shouldBe expected
  }


  test("MultiplierKind[IEC73] - faulty", IntegrationTest) {

    val incorrectType = Seq(4, 253, 255, 255, 255)
    val bytesVariant = Seq(6, 253, 255, 255)
    val bytes = Seq(253, 255, 255)

    val expected = UaEnumeration(UaInt32(-3), UaString("mili"))
    val result = _decoder.decode(incorrectType, _multiplierKind.id)

    assert(result != expected)

    assertThrows[IndexOutOfBoundsException] {
      _decoder.decodeVariant(bytesVariant, _multiplierKind.id)
    }

    assertThrows[IndexOutOfBoundsException] {
      _decoder.decode(bytes, _multiplierKind.id)
    }
  }


  test("AnalogValue[IEC73] - from variant", IntegrationTest) {

    val bytes = Seq(22, 1, 2, 158, 0, 1, 8, 0, 0, 0, 0, 0, 0, 128, 164, 112, 69, 65)

    val expected =
      UaStructure(
        UaString("i") -> UaInt32(-2147483648),
        UaString("f") -> UaFloat(12.34f))

    val result = _decoder.decodeVariant(bytes, _analogValue.id)

    result shouldBe expected
  }

  test("AnalogValue[IEC73] - direct", IntegrationTest) {

    val bytes = Seq(0, 0, 0, 128, 164, 112, 69, 65)

    val expected =
      UaStructure(
        UaString("i") -> UaInt32(-2147483648),
        UaString("f") -> UaFloat(12.34f))

    val result = _decoder.decode(bytes, _analogValue.id)

    result shouldBe expected
  }


  test("AnalogValue[IEC73] - faulty", IntegrationTest) {

    val incorrectType = Seq(21, 1, 2, 158, 0, 1, 8, 0, 0, 0, 0, 0, 0, 128, 164, 112, 69, 65)
    val bytesVariant = Seq(22, 1, 2, 158, 0, 1, 8, 0, 0, 0, 0, 0, 128, 164, 112, 69, 65)
    val bytes = Seq(0, 0, 0, 128, 164, 112, 69)

    val expected =
      UaStructure(
        UaString("i") -> UaInt32(-2147483648),
        UaString("f") -> UaFloat(12.34f))

    val result = _decoder.decode(incorrectType, _analogValue.id)

    assert(result != expected)

    assertThrows[IndexOutOfBoundsException] {
      _decoder.decodeVariant(bytesVariant, _analogValue.id)
    }

    assertThrows[IndexOutOfBoundsException] {
      _decoder.decode(bytes, _analogValue.id)
    }
  }


  test("Unit[IEC73] - from variant", IntegrationTest) {

    val bytes = Seq(22, 1, 2, 134, 0, 1, 8, 0, 0, 0, 8, 0, 0, 0, 1, 0, 0, 0)

    val expected =
      UaStructure(
        UaString("sIUnit") -> UaEnumeration(UaInt32(8), UaString("candela")),
        UaString("multiplier") -> UaEnumeration(UaInt32(1), UaString("deca")))

    val result = _decoder.decodeVariant(bytes, _unit.id)

    result shouldBe expected
  }

  test("Unit[IEC73] - direct", IntegrationTest) {

    val bytes = Seq(8, 0, 0, 0, 1, 0, 0, 0)

    val expected =
      UaStructure(
        UaString("sIUnit") -> UaEnumeration(UaInt32(8), UaString("candela")),
        UaString("multiplier") -> UaEnumeration(UaInt32(1), UaString("deca")))

    val result = _decoder.decode(bytes, _unit.id)

    result shouldBe expected
  }


  test("Unit[IEC73] - faulty", IntegrationTest) {

    val incorrectType = Seq(21, 1, 2, 134, 0, 1, 8, 0, 0, 0, 8, 0, 0, 0, 1, 0, 0, 0)
    val bytesVariant = Seq(22, 1, 2, 134, 0, 1, 8, 0, 0, 0, 8, 0, 0, 0, 1, 0, 0)
    val bytes = Seq(8, 0, 0, 0, 1, 0, 0)
    val errorMatching = Seq(22, 1, 2, 134, 1, 8, 0, 0, 0, 8, 0, 0, 0, 1, 0, 0)

    val expected =
      UaStructure(
        UaString("sIUnit") -> UaEnumeration(UaInt32(8), UaString("candela")),
        UaString("multiplier") -> UaEnumeration(UaInt32(1), UaString("deca")))

    val result = _decoder.decode(incorrectType, _unit.id)

    assert(result != expected)

    assertThrows[IndexOutOfBoundsException] {
      _decoder.decodeVariant(bytesVariant, _unit.id)
    }

    assertThrows[IndexOutOfBoundsException] {
      _decoder.decode(bytes, _unit.id)
    }

    assertThrows[MatchError] {
      _decoder.decodeVariant(errorMatching, _unit.id)
    }
  }


  test("BehaviourModeKind[IEC74] - from variant", IntegrationTest) {

    val bytes = Seq(6, 3, 0, 0, 0)

    val expected = UaEnumeration(UaInt32(3), UaString("test"))
    val result = _decoder.decodeVariant(bytes, _behaviourModeKind.id)

    result shouldBe expected
  }

  test("BehaviourModeKind[IEC74] - direct", IntegrationTest) {

    val bytes = Seq(3, 0, 0, 0)

    val expected = UaEnumeration(UaInt32(3), UaString("test"))
    val result = _decoder.decode(bytes, _behaviourModeKind.id)

    result shouldBe expected
  }


  test("HealthKind[IEC74] - from variant", IntegrationTest) {

    val bytes = Seq(6, 3, 0, 0, 0)

    val expected = UaEnumeration(UaInt32(3), UaString("alarm"))
    val result = _decoder.decodeVariant(bytes, _healthKind.id)

    result shouldBe expected
  }

  test("HealthKind[IEC74] - direct", IntegrationTest) {

    val bytes = Seq(3, 0, 0, 0)

    val expected = UaEnumeration(UaInt32(3), UaString("alarm"))
    val result = _decoder.decode(bytes, _healthKind.id)

    result shouldBe expected
  }

}


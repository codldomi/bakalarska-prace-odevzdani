package scopcua.sdk.client

import scopcua.data.UaId
import scopcua.data.value.number.integer.UaUInteger.UaUInt16
import scopcua.data.value.{UaDateTime, UaString, UaValue}


/** A synchronous OPC UA client's interface.
  */
trait UaClient {

  /** Reads the current value.
    *
    * @param id Variable's identifier.
    * @return The value and its source time.
    */
  def readValue(id: UaId): (UaValue, UaDateTime)

  /** Reads the NamespaceArray.
    *
    * @return NamespaceIndexes mapped to NamespaceUris.
    */
  def readNamespaces(): Map[UaUInt16, UaString]

  /** Reads the current DataType's identifier.
    *
    * @param id Variable's identifier.
    * @return DataType's identifier.
    */
  def readDataTypeId(id: UaId): UaId

  /** Connects to a server.
    */
  def connect(): Unit

  /** Disconnects from a server.
    */
  def disconnect(): Unit

}

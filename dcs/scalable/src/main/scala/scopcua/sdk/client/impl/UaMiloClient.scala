package scopcua.sdk.client.impl

import org.eclipse.milo.opcua.sdk.client.OpcUaClient
import org.eclipse.milo.opcua.sdk.client.api.config.OpcUaClientConfigBuilder
import org.eclipse.milo.opcua.sdk.client.api.identity.UsernameProvider
import org.eclipse.milo.opcua.stack.client.security.DefaultClientCertificateValidator
import org.eclipse.milo.opcua.stack.core.security.{DefaultTrustListManager, SecurityPolicy}
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription

import scopcua.data.UaId
import scopcua.data.security.{UaSecurityConfig, UaSecurityPolicy}
import scopcua.data.value.number.integer.UaUInteger.UaUInt16
import scopcua.data.value.{UaDateTime, UaString, UaValue}
import scopcua.sdk.client.UaClient
import scopcua.sdk.client.impl.util.ValueConverter
import scopcua.util.binarydecoder.UaBinaryDecoder

import java.io.File
import java.util

import scala.jdk.FunctionConverters.{enrichAsJavaFunction, enrichAsJavaPredicate}


/** Encapsulates Milo's implementation of OPC UA.
  *
  * @see [[https://github.com/eclipse/milo Eclipse Milo, GitHub]]
  * @param client Milo's client.
  * @param converter An utility for converting between Milo types and Scalable OPC UA types.
  */
class UaMiloClient private (val client: OpcUaClient, val converter: ValueConverter) extends UaClient {

  /** Connects to a server.
    */
  override def connect(): Unit = client.connect().get()

  /** Disconnects from a server.
    */
  override def disconnect(): Unit = client.disconnect().get()

  /** Reads the current DataType's identifier.
    *
    * @param id Variable's identifier.
    * @return DataType's identifier.
    */
  override def readDataTypeId(id: UaId): UaId = {

    val nodeId = converter.uaId2NodeId(id)
    val typeNodeId =
      client
        .getAddressSpace
        .getVariableNode(nodeId)
        .readDataType()

    converter.nodeId2UaId(typeNodeId)
  }

  /** Reads the NamespaceArray.
    *
    * @return NamespaceIndexes mapped to NamespaceUris.
    */
  override def readNamespaces(): Map[UaUInt16, UaString] = {

    val namespaces =
      client
        .readNamespaceTable()
        .toArray

    namespaces
      .zipWithIndex
      .foldLeft(Map.empty[UaUInt16, UaString])((result, namespace) => {

        val uri = UaString(namespace._1)
        val index = UaUInt16(namespace._2)

        result + (index -> uri)
      })
  }

  /** Reads the current value.
    *
    * @param id Variable's identifier.
    * @return The value and its source time.
    */
  override def readValue(id: UaId): (UaValue, UaDateTime) = {

    val nodeId = converter.uaId2NodeId(id)
    val value =
      client
        .getAddressSpace
        .getVariableNode(nodeId)
        .readValue()

    val dateTime = value.getSourceTime
    val variant = value.getValue

    val typeId = readDataTypeId(id)

    val resultValue = converter.variant2UaValue(variant, typeId)
    val resultDateTime = converter.dateTime2UaDateTime(dateTime)

    (resultValue, resultDateTime)
  }

}


/** An factory object for creating clients.
  *
  * @note At this time, the factory creates clients using an OPC UA BinaryEncoding. The reason is that each OPC UA
  *       server supports at least binary encoding.
  */
object UaMiloClient {

  /** Creates an anonymous client.
    *
    * @param url Server's URL.
    * @param decoder A decoder for an OPC UA BinaryEncoding used by the server.
    * @return The initialized client.
    */
  def from(url: String, decoder: UaBinaryDecoder): UaMiloClient = {

    val client = OpcUaClient.create(url)
    val converter = new ValueConverter(client, decoder)

    new UaMiloClient(client, converter)
  }

  /** Creates a secured client.
    *
    * @param url Server's URL.
    * @param config A configuration for a secured session.
    * @param decoder A decoder for an OPC UA BinaryEncoding used by the server.
    * @return The initialized client.
    */
  def from(url: String, config: UaSecurityConfig, decoder: UaBinaryDecoder, timeout: Int = 5000): UaMiloClient = {

    val pkiDirectory = new File(config.pkiDirectory)
    val manager = new DefaultTrustListManager(pkiDirectory)
    val validator = new DefaultClientCertificateValidator(manager)
    val identity = new UsernameProvider(config.username, config.password)

    val build = (builder: OpcUaClientConfigBuilder) =>
      builder
        .setRequestTimeout(uint(timeout))
        .setApplicationName(LocalizedText.english(config.appName))
        .setApplicationUri(config.appUri)
        .setKeyPair(config.keyPair)
        .setCertificate(config.certificate)
        .setCertificateChain(config.chain.toArray)
        .setCertificateValidator(validator)
        .setIdentityProvider(identity)
        .build()

    val predicate = (endpoint: EndpointDescription) => {
      val policy = config.policy match {
        case UaSecurityPolicy.None                => SecurityPolicy.None
        case UaSecurityPolicy.Basic256            => SecurityPolicy.Basic256
        case UaSecurityPolicy.Basic256Sha256      => SecurityPolicy.Basic256Sha256
        case UaSecurityPolicy.Aes128Sha256RsaOaep => SecurityPolicy.Aes128_Sha256_RsaOaep
        case UaSecurityPolicy.Aes256Sha256RsaPss  => SecurityPolicy.Aes256_Sha256_RsaPss
      }

      policy.getUri == endpoint.getSecurityPolicyUri
    }

    val selectEndpoint = (endpoints: util.List[EndpointDescription]) =>
      endpoints
        .stream()
        .filter(predicate.asJavaPredicate)
        .findFirst()

    val client = OpcUaClient.create(url, selectEndpoint.asJavaFunction, build.asJavaFunction)
    val converter = new ValueConverter(client, decoder)

    new UaMiloClient(client, converter)
  }

}

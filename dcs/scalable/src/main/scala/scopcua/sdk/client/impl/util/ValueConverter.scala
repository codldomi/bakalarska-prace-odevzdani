package scopcua.sdk.client.impl.util

import io.netty.buffer.UnpooledByteBufAllocator

import org.eclipse.milo.opcua.sdk.client.OpcUaClient
import org.eclipse.milo.opcua.stack.core.serialization.OpcUaBinaryStreamEncoder
import org.eclipse.milo.opcua.stack.core.types.builtin.{DateTime, NodeId, Variant}
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger
import org.eclipse.milo.opcua.stack.core.types.enumerated.IdType

import scopcua.data.UaId
import scopcua.data.value.{UaDateTime, UaString, UaValue}
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.util.binarydecoder.UaBinaryDecoder


/** An utility for converting between Milo types and Scalable OPC UA types.
  *
  * @param client Milo's client.
  * @param decoder A decoder for an OPC UA BinaryEncoding used by the server.
  */
class ValueConverter(val client: OpcUaClient, val decoder: UaBinaryDecoder) {

  /** Converts UaId to NodeId.
    *
    * @param id Scalable OPC UA identifier.
    * @return Milo's NodeId.
    */
  def uaId2NodeId(id: UaId): NodeId = {

    val index = UInteger.valueOf(id.index.value)
    val nsIndex =
      client
        .getNamespaceTable
        .getIndex(id.namespaceUri.value)

    new NodeId(nsIndex, index)
  }

  /** Converts NodeId to UaId.
    *
    * @note Currently supports only Numeric NodeIds.
    * @param nodeId Milo's NodeId.
    * @return Scalable OPC UA identifier.
    */
  def nodeId2UaId(nodeId: NodeId): UaId = {

    val isNumeric = nodeId.getType match {
      case IdType.Numeric => true
      case _ => false
    }

    require(isNumeric, "NodeId is not a numeric one.")

    val nsIndex = nodeId.getNamespaceIndex
    val uri = client.getNamespaceTable.getUri(nsIndex)
    val index = nodeId.getIdentifier.asInstanceOf[UInteger].longValue()

    UaId(UaUInt32(index), UaString(uri))
  }

  /** Converts DateTime to UaDateTime.
    *
    * @param dateTime Eclipse Milo DateTime.
    * @return Scalable OPC UA UaDateTime.
    */
  def dateTime2UaDateTime(dateTime: DateTime): UaDateTime = {

    val nanoSecIntervals = dateTime.getUtcTime
    UaDateTime(nanoSecIntervals)
  }

  /** Converts Milo's Variant to a pure UaValue.
    *
    * @param variant Eclipse Milo Variant.
    * @param typeId DataType's identifier.
    * @return Scalable OPC UA UaValue.
    */
  def variant2UaValue(variant: Variant, typeId: UaId): UaValue = {

    val buffer =
      UnpooledByteBufAllocator.DEFAULT
        .buffer()

    val context = client.getStaticSerializationContext
    val writer = new OpcUaBinaryStreamEncoder(context).setBuffer(buffer)

    writer.writeVariant(variant)
    var encoded = ""

    while (buffer.isReadable) {
      val byte = buffer.readUnsignedByte()
      encoded += byte.toChar
    }

    decoder.decodeVariant(encoded, typeId)
  }

}

package scopcua.data.security


sealed trait UaSecurityPolicy


object UaSecurityPolicy {

  case object None extends UaSecurityPolicy
  case object Basic256 extends UaSecurityPolicy
  case object Basic256Sha256 extends UaSecurityPolicy
  case object Aes128Sha256RsaOaep extends UaSecurityPolicy
  case object Aes256Sha256RsaPss extends UaSecurityPolicy

}
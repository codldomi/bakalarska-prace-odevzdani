package scopcua.data.security

import java.security.KeyPair
import java.security.cert.X509Certificate


case class UaSecurityConfig(
  username: String,
  password: String,
  appName: String,
  appUri: String,
  keyPair: KeyPair,
  certificate: X509Certificate,
  chain: Seq[X509Certificate],
  policy: UaSecurityPolicy,
  pkiDirectory: String
)

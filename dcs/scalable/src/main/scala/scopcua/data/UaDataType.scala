package scopcua.data

import scopcua.data
import scopcua.data.UaDataType.GeneralStructure.Field
import scopcua.data.value.number.integer.UaSInteger.UaInt32
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.data.value.{UaBoolean, UaString}

import scala.language.implicitConversions


sealed trait UaDataType {

  def isAbstract: UaBoolean
  def id: UaId
  def name: UaString
  def superId: UaId

}


object UaDataType {

  private val _namespace = Constants.zeroNamespace

  def builtIns: Map[UaId, UaDataType] = {

    val dataTypes = Seq(
      Boolean, SByte, Byte, Int16, UInt16, Int32, UInt32, Int64, UInt64, Float, Double, String, DateTime, Guid, ByteString,
      XmlElement, NodeId, ExpandedNodeId, StatusCode, QualifiedName, LocalizedText, Structure, DataValue, BaseDataType,
      DiagnosticInfo, Number, Integer, UInteger, Enumeration)

    dataTypes
      .map(dataType => dataType.id -> dataType)
      .toMap
  }

  private implicit def indexToZeroType(index: Int): UaId =
    data.UaId(
      UaUInt32(index),
      UaString(_namespace))

  private implicit def stringToUaString(text: String): UaString = UaString(text)

  private implicit def booleanToUaBoolean(bool: Boolean): UaBoolean = UaBoolean(bool)


  case object Null extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 0
    override def name: UaString = "Null"
    override def superId: UaId = 0

  }

  case object Boolean extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 1
    override def name: UaString = "Boolean"
    override def superId: UaId = 24

  }

  case object SByte extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 2
    override def name: UaString = "SByte"
    override def superId: UaId = 27

  }

  case object Byte extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 3
    override def name: UaString = "Byte"
    override def superId: UaId = 28

  }

  case object Int16 extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 4
    override def name: UaString = "Int16"
    override def superId: UaId = 27

  }

  case object UInt16 extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 5
    override def name: UaString = "UInt16"
    override def superId: UaId = 28

  }

  case object Int32 extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 6
    override def name: UaString = "Int32"
    override def superId: UaId = 27

  }

  case object UInt32 extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 7
    override def name: UaString = "UInt32"
    override def superId: UaId = 28

  }

  case object Int64 extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 8
    override def name: UaString = "Int64"
    override def superId: UaId = 27

  }

  case object UInt64 extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 9
    override def name: UaString = "UInt64"
    override def superId: UaId = 28

  }

  case object Float extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 10
    override def name: UaString = "Float"
    override def superId: UaId = 26

  }

  case object Double extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 11
    override def name: UaString = "Double"
    override def superId: UaId = 26

  }

  case object String extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 12
    override def name: UaString = "String"
    override def superId: UaId = 24

  }

  case object DateTime extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 13
    override def name: UaString = "DateTime"
    override def superId: UaId = 24

  }

  case object Guid extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 14
    override def name: UaString = "Guid"
    override def superId: UaId = 24

  }

  case object ByteString extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 15
    override def name: UaString = "ByteString"
    override def superId: UaId = 24

  }

  case object XmlElement extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 16
    override def name: UaString = "XmlElement"
    override def superId: UaId = 24

  }

  case object NodeId extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 17
    override def name: UaString = "NodeId"
    override def superId: UaId = 24

  }

  case object ExpandedNodeId extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 18
    override def name: UaString = "ExpandedNodeId"
    override def superId: UaId = 24

  }

  case object StatusCode extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 19
    override def name: UaString = "StatusCode"
    override def superId: UaId = 24

  }

  case object QualifiedName extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 20
    override def name: UaString = "QualifiedName"
    override def superId: UaId = 24

  }

  case object LocalizedText extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 21
    override def name: UaString = "LocalizedText"
    override def superId: UaId = 24

  }

  case object Structure extends UaDataType {

    override def isAbstract: UaBoolean = true
    override def id: UaId = 22
    override def name: UaString = "Structure"
    override def superId: UaId = 24

  }

  case object DataValue extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 23
    override def name: UaString = "DataValue"
    override def superId: UaId = 24

  }

  case object BaseDataType extends UaDataType {

    override def isAbstract: UaBoolean = true
    override def id: UaId = 24
    override def name: UaString = "BaseDataType"
    override def superId: UaId = 0

  }

  case object DiagnosticInfo extends UaDataType {

    override def isAbstract: UaBoolean = false
    override def id: UaId = 25
    override def name: UaString = "DiagnosticInfo"
    override def superId: UaId = 24

  }

  case object Number extends UaDataType {

    override def isAbstract: UaBoolean = true
    override def id: UaId = 26
    override def name: UaString = "Number"
    override def superId: UaId = 24

  }

  case object Integer extends UaDataType {

    override def isAbstract: UaBoolean = true
    override def id: UaId = 27
    override def name: UaString = "Integer"
    override def superId: UaId = 26

  }

  case object UInteger extends UaDataType {

    override def isAbstract: UaBoolean = true
    override def id: UaId = 28
    override def name: UaString = "UInteger"
    override def superId: UaId = 27

  }

  case object Enumeration extends UaDataType {

    override def isAbstract: UaBoolean = true
    override def id: UaId = 29
    override def name: UaString = "Enumeration"
    override def superId: UaId = 24

  }


  case class GeneralEnumeration(
    isAbstract: UaBoolean,
    name: UaString,
    id: UaId,
    superId: UaId,
    values: Map[UaString, UaInt32]
  ) extends UaDataType


  object GeneralEnumeration {

    def apply(
      isAbstract: UaBoolean,
      name: UaString,
      id: UaId,
      superId: UaId,
      values: (UaString, UaInt32)*
    ): GeneralEnumeration = new GeneralEnumeration(isAbstract, name, id, superId, values.toMap)

  }


  case class GeneralStructure(
    isAbstract: UaBoolean,
    name: UaString,
    id: UaId,
    superId: UaId,
    fields: Vector[Field]
  ) extends UaDataType


  object GeneralStructure {

    def apply(
      isAbstract: UaBoolean,
      name: UaString,
      id: UaId,
      superId: UaId,
      fields: Field*
    ): GeneralStructure = new GeneralStructure(isAbstract, name, id, superId, fields.toVector)

    case class Field(name: UaString, typeId: UaId, isArray: UaBoolean, isOptional: UaBoolean)

  }


}
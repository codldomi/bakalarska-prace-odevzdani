package scopcua.data.value

import scopcua.data.value.number.integer.UaUInteger.UaByte


case class UaByteString(bytes: Seq[UaByte] = Seq.empty) extends UaValue
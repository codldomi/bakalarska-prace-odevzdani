package scopcua.data.value

import scopcua.data.value.UaVariant.UaBuiltInId


case class UaVariant(
  id: UaBuiltInId,
  value: UaValue
) extends UaValue


object UaVariant {

  sealed trait UaBuiltInId

  object UaBuiltInId {

    case object Null extends UaBuiltInId
    case object Boolean extends UaBuiltInId
    case object SByte extends UaBuiltInId
    case object Byte extends UaBuiltInId
    case object Int16 extends UaBuiltInId
    case object UInt16 extends UaBuiltInId
    case object Int32 extends UaBuiltInId
    case object UInt32 extends UaBuiltInId
    case object Int64 extends UaBuiltInId
    case object UInt64 extends UaBuiltInId
    case object Float extends UaBuiltInId
    case object Double extends UaBuiltInId
    case object String extends UaBuiltInId
    case object DateTime extends UaBuiltInId
    case object Guid extends UaBuiltInId
    case object ByteString extends UaBuiltInId
    case object XmlElement extends UaBuiltInId
    case object NodeId extends UaBuiltInId
    case object ExpandedNodeId extends UaBuiltInId
    case object StatusCode extends UaBuiltInId
    case object QualifiedName extends UaBuiltInId
    case object LocalizedText extends UaBuiltInId
    case object ExtensionObject extends UaBuiltInId
    case object DataValue extends UaBuiltInId
    case object Variant extends UaBuiltInId
    case object DiagnosticInfo extends UaBuiltInId

  }

}




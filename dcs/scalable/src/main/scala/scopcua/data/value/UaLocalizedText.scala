package scopcua.data.value


case class UaLocalizedText(text: UaString, locale: UaString) extends UaValue

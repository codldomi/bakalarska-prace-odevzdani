package scopcua.data.value


case class UaArray[+A](array: Vector[A]) extends UaValue {

  def isEmpty: Boolean = array.isEmpty

  def length: Int = array.length

  def apply(index: Int): A = array(index)

}


object UaArray {


  def apply[A <: UaValue](values: A*): UaArray[A] = new UaArray(values.toVector)

}

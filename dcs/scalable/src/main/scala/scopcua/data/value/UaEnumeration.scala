package scopcua.data.value

import scopcua.data.value.number.integer.UaSInteger.UaInt32


case class UaEnumeration(value: UaInt32, name: UaString) extends UaValue

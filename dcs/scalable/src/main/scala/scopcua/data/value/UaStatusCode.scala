package scopcua.data.value

import scopcua.data.value.number.integer.UaUInteger.UaUInt32


case class UaStatusCode(code: UaUInt32, symbol: UaString) extends UaValue


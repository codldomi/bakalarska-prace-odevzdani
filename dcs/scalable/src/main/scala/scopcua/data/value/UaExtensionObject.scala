package scopcua.data.value


sealed trait UaExtensionObject extends UaValue


object UaExtensionObject {

  case class UaByteExtensionObject(encodingId: UaNodeId, body: UaByteString) extends UaExtensionObject

  case class UaXmlExtensionObject(encodingId: UaNodeId, body: UaXmlElement) extends UaExtensionObject

}
package scopcua.data.value


case class UaStructure(fields: Map[UaString, UaValue]) extends UaValue


object UaStructure {

  def apply(pairs: (UaString, UaValue)*): UaStructure = new UaStructure(pairs.toMap)

}
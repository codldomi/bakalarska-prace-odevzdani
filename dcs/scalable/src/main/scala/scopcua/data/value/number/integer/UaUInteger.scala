package scopcua.data.value.number.integer


sealed trait UaUInteger extends UaInteger


object UaUInteger {

  case class UaByte(value: Short) extends UaUInteger

  case class UaUInt16(value: Int) extends UaUInteger

  case class UaUInt32(value: Long) extends UaUInteger

  case class UaUInt64(value: BigInt) extends UaUInteger

}
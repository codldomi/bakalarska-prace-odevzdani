package scopcua.data.value

import scopcua.data.value.number.integer.UaUInteger.UaByte


case class UaGuid(bytes: Seq[UaByte] = Seq.empty) extends UaValue

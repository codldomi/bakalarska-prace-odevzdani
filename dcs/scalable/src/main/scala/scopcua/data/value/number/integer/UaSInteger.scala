package scopcua.data.value.number.integer


sealed trait UaSInteger extends UaInteger


object UaSInteger {

  case class UaSByte(value: Byte) extends UaSInteger

  case class UaInt16(value: Short) extends UaSInteger

  case class UaInt32(value: Int) extends UaSInteger

  case class UaInt64(value: Long) extends UaSInteger

}

package scopcua.data.value

import scopcua.data.value.number.integer.UaSInteger.UaInt32


case class UaDiagnosticInfo(
  symbolicId: UaInt32,
  namespaceUri: UaInt32,
  localizedText: UaInt32,
  locale: UaInt32,
  additionalInfo: UaString,
  innerStatusCode: Option[UaStatusCode],
  innerDiagnosticInfo: Option[UaDiagnosticInfo]
) extends UaValue

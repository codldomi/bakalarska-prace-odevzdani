package scopcua.data.value

import scopcua.data.value.number.integer.UaUInteger.UaUInt16


case class UaDataValue(
  value: UaVariant,
  status: UaStatusCode,
  sourceTimestamp: UaDateTime,
  sourcePicoSeconds: UaUInt16,
  serverTimestamp: UaDateTime,
  serverPicoSeconds: UaUInt16
) extends UaValue

package scopcua.data.value

import scopcua.data.value.number.integer.UaUInteger.UaUInt32


case class UaExpandedNodeId(
  namespaceUri: UaString,
  nodeId: UaNodeId,
  serverIndex: UaUInt32
) extends UaValue

package scopcua.data.value

import scopcua.data.value.number.integer.UaUInteger.{UaUInt16, UaUInt32}


sealed trait UaNodeId extends UaValue


object UaNodeId {

  case class UaNumericNodeId(namespaceIndex: UaUInt16, identifier: UaUInt32) extends UaNodeId

  case class UaGuidNodeId(namespaceIndex: UaUInt16, identifier: UaGuid) extends UaNodeId

  case class UaStringNodeId(namespaceIndex: UaUInt16, identifier: UaString) extends UaNodeId

  case class UaOpaqueNodeId(namespaceIndex: UaUInt16, identifier: UaByteString) extends UaNodeId

}
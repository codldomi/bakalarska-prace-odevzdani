package scopcua.data.value

import scopcua.data.value.number.integer.UaUInteger.UaUInt16


case class UaQualifiedName(name: UaString, namespaceIndex: UaUInt16) extends UaValue

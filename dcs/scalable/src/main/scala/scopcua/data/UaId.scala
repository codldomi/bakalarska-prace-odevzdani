package scopcua.data

import scopcua.data.value.UaString
import scopcua.data.value.number.integer.UaUInteger.UaUInt32


/** A unique identifier across information models.
  *
  * @note Namespace Index can be different on each server. Depends on the given namespace array. But URI is a unique
  *       natural identifier. Also, Node's Index as Node's numeric identifier in a namespace is commonly used.
  *
  *       The library supports only numeric ones at this moment.
  * @param index Index in the given namespace.
  * @param namespaceUri Namespace URI.
  */
case class UaId(index: UaUInt32, namespaceUri: UaString)

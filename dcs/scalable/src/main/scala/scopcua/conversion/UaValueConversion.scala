package scopcua.conversion

import scopcua.data.value.UaNodeId.UaNumericNodeId
import scopcua.data.value.{UaBoolean, UaDateTime, UaString}
import scopcua.data.value.number.integer.UaSInteger.UaInt32
import scopcua.data.value.number.integer.UaUInteger.{UaUInt16, UaUInt32}

import java.sql.Timestamp

import scala.language.implicitConversions


/** Set of functions for implicit conversion between Scala/Java type and Scalable OPC UA type.
  */
object UaValueConversion {

  implicit def stringToUaString(value: String): UaString = UaString(value)

  implicit def intToUaUInt16(value: Int): UaUInt16 = UaUInt16(value)

  implicit def intToUaInt32(value: Int): UaInt32 = UaInt32(value)

  implicit def longToUaUInt32(value: Long): UaUInt32 = UaUInt32(value)

  /** Converts UaDateTime to Timestamp.
    *
    * @note A DateTime value is a 64-bit signed integer which represents the number of 100 nanosecond intervals since
    *       January 1, 1601 (UTC).
    * @see [[https://reference.opcfoundation.org/v105/Core/docs/Part6/5.2.2/#5.2.2.5 Part 6, Binary Data Encoding, DateTime]]
    * @param dateTime Scalable OPC UA UaDateTime.
    * @return Unix Timestamp.
    */
  implicit def uaDateTimeToTimestamp(dateTime: UaDateTime): Timestamp = {

    val usec = 10L
    val msec = usec * 1000L
    val sec = msec * 1000L
    val unixEpoch = 11644473600L * sec

    val unix = (dateTime.value - unixEpoch) / sec

    new Timestamp(unix * 1000)
  }

  /** Parses String and creates Numeric NodeId.
    *
    * @see [[https://reference.opcfoundation.org/v105/Core/docs/Part6/5.3.1/#5.3.1.10 Part 6, XML Data Encoding, NodeId]]
    * @param value Textual representation.
    * @return Scalable OPC UA UaNumericNodeId.
    */
  implicit def stringToNumericNodeId(value: String): UaNumericNodeId = {

    val nonZeroNsId = "ns=([0-9]+);i=([0-9]+)".r
    val zeroNsId = "i=([0-9]+)".r

    val (i, ns) = nonZeroNsId.findFirstMatchIn(value) match {
      case Some(result) => (result.group(2).toLong, result.group(1).toInt)
      case None => (zeroNsId.findFirstMatchIn(value).get.group(1).toLong, 0)
    }

    UaNumericNodeId(UaUInt16(ns), UaUInt32(i))
  }

  implicit def booleanToUaBoolean(value: Boolean): UaBoolean = UaBoolean(value)

}

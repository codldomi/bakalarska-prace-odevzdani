package scopcua.util.datatypeparser.impl.util

import scopcua.data
import scopcua.data.{UaDataType, UaId}
import scopcua.data.UaDataType.GeneralStructure.Field
import scopcua.data.value.UaNodeId.UaNumericNodeId
import scopcua.conversion.UaValueConversion.{booleanToUaBoolean, longToUaUInt32, stringToNumericNodeId, stringToUaString}
import scopcua.util.datatypeparser.impl.model.Definition.{EnumField, StructField}

import scala.xml.Node


/** A Parser for the DataType Node.
  *
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/F.11/ Part 6, Annex F, UADataType]]
  * @param namespaces Namespace table mapping (index -> URI).
  * @param aliases Aliases from the OpcUaNodeSet2.xml.
  * @param defParser Parser for the DataType Definition.
  */
class DataTypeParser1v04(
  val namespaces: Map[Int, String],
  val aliases: Map[String, String],
  val defParser: DefinitionParser1v04
) {

  private val _builtIns = UaDataType.builtIns

  /** A numeric node's identifier of HasSubtype ReferenceType. Used in the NodeSet for the modeling of relation between
    * DataType and its super type. Namespace is zero.
    */
  private val _hasSubtypeIndex = 45

  /** Parses XML storing DataType Node.
    *
    * @param node XML representation of the DataType Node.
    * @return DataType model.
    */
  def parse(node: Node): UaDataType = {

    val id = parseId((node \ "@NodeId").text)

    if (_builtIns.contains(id)) {
      return _builtIns(id)
    }

    val isAbstract = (node \ "@isAbstract").text.toBooleanOption.getOrElse(false)
    val superId = parseSuperId(node)
    val isStruct = UaDataType.Structure.id == superId

    if (!isStruct && UaDataType.Enumeration.id != superId) {
      throw new IllegalArgumentException("Unsupported data type's inheritance.")
    }

    val definition = defParser.parse((node \ "Definition").head, isStruct)
    val name = definition.name

    definition.fields match {

      case Left(fields) =>
        UaDataType.GeneralEnumeration(
          isAbstract,
          name,
          id,
          superId,
          fields.map{ case EnumField(name, value) => name -> value }.toMap)

      case Right(fields) =>
        UaDataType.GeneralStructure(
          isAbstract,
          name,
          id,
          superId,
          fields
            .map{ case StructField(name, dataTypeId, isArray, isOptional) => Field(name, dataTypeId, isArray, isOptional) }
            .toVector)
    }
  }


  private def parseId(text: String): UaId = {

    val textualId = aliases.getOrElse(text, text)
    val nodeId: UaNumericNodeId = textualId

    data.UaId(
      nodeId.identifier,
      namespaces(nodeId.namespaceIndex.value))
  }


  private def parseSuperId(node: Node): UaId = {

    val refs = node \ "References" \ "Reference"

    val superId =
      refs
        .find(
          ref => {
            val refTypeId = parseId((ref \ "@ReferenceType").text)
            val subtypeId = data.UaId(_hasSubtypeIndex, namespaces(0))

            refTypeId == subtypeId
          })
        .getOrElse(throw new IllegalArgumentException("DataType does not have super type."))
        .text

    parseId(superId)
  }

}

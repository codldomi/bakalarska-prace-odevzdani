package scopcua.util.datatypeparser

import scopcua.data.{UaDataType, UaId}


/** Parses OPC UA NodeSet with DataTypes.
  */
trait UaDataTypeParser {

  /** Parses String containing the whole OPC UA NodeSet.
    *
    * @param nodeSet Textual representation of the NodeSet.
    * @return Mapping (DataType's identifier -> DataType)
    */
  def parse(nodeSet: String): Map[UaId, UaDataType]

}

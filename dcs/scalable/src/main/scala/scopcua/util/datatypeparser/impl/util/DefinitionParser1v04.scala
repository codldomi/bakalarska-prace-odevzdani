package scopcua.util.datatypeparser.impl.util

import scopcua.data
import scopcua.data.value.UaNodeId.UaNumericNodeId
import scopcua.data.value.UaString
import scopcua.util.datatypeparser.impl.model.Definition
import scopcua.util.datatypeparser.impl.model.Definition.{EnumField, StructField}
import scopcua.conversion.UaValueConversion.{booleanToUaBoolean, intToUaInt32, stringToNumericNodeId, stringToUaString}

import scala.xml.Node


/** A Parser for DataType Definition from the NodeSet version 1.04.
  *
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/F.12/ Part 6, Annex F, DataTypeDefinition]]
  * @param namespaces Namespace table mapping (index -> URI).
  * @param aliases Aliases from the OpcUaNodeSet2.xml.
  */
class DefinitionParser1v04(
  val namespaces: Map[Int, String],
  val aliases: Map[String, String]
) {

  /** Parses XML and creates Definition.
    *
    * @param xml DataType Definition as XML.
    * @param isStruct True, if Definition describes structural DataType. Otherwise false.
    * @return Model for the DataType Definition.
    */
  def parse(xml: Node, isStruct: Boolean): Definition = {

    val name = nameFromQualifiedName((xml \ "@Name").text)
    val symbolicName = (xml \ "@SymbolicName").text

    val fields =
      if (isStruct)
        Right((xml \ "Field").map(structFieldFromXml))
      else
        Left((xml \ "Field").map(enumFieldFromXml))

    require(!name.isBlank, "DataType's name should be non blank.")
    Definition(name, symbolicName, fields)
  }


  private def enumFieldFromXml(field: Node): EnumField = {

    val name = (field \ "@Name").text
    val value = (field \ "@Value").text.toInt

    EnumField(name, value)
  }


  private def structFieldFromXml(field: Node): StructField = {

    val name: UaString = (field \ "@Name").text

    var textualNodeId = (field \ "@DataType").text
    textualNodeId = aliases.getOrElse(textualNodeId, textualNodeId)
    val nodeId: UaNumericNodeId = textualNodeId
    val dataTypeId = data.UaId(nodeId.identifier, namespaces(nodeId.namespaceIndex.value))

    val isArray =
      if ((field \ "@ValueRank").isEmpty) {
        false
      } else {
        val value = (field \ "@ValueRank").text.toInt
        if (value >= 1) true else false
      }

    val isOptional = (field \ "@IsOptional").text.toBooleanOption.getOrElse(false)

    StructField(name, dataTypeId, isArray, isOptional)
  }


  private def nameFromQualifiedName(qName: String): String = {

    val pattern = "[0-9]+:(.*)".r

    pattern.findFirstMatchIn(qName) match {
      case Some(result) => result.group(1)
      case None => qName
    }
  }

}


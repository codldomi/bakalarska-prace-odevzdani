package scopcua.util.datatypeparser.impl

import scopcua.data.{Constants, UaDataType, UaId}
import scopcua.util.datatypeparser.UaDataTypeParser
import scopcua.util.datatypeparser.impl.util.{DataTypeParser1v04, DefinitionParser1v04}

import scala.xml.{Node, XML}


/** Parses OPC UA NodeSet with DataTypes. The implemented version is OpcUaNodeSet2.xml, v1.04.
  *
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/F.1/ Part 6, Annex F (normative) Information Model XML Schema]]
  */
class UaDataTypeParser1v04() extends UaDataTypeParser {

  private val _namespace = Constants.zeroNamespace

  /** Parses XML containing the whole OPC UA NodeSet.
    *
    * @param textualNodeset XML NodeSet representation as String.
    * @return Mapping (DataType's identifier -> DataType)
    */
  override def parse(textualNodeset: String): Map[UaId, UaDataType] = {

    val nodeSet = parseNodeSet(XML.loadString(textualNodeset))
    val aliases = parseAliases(nodeSet)
    val namespaces = parseNamespaces(nodeSet)

    val defParser = new DefinitionParser1v04(namespaces, aliases)
    val typeParser = new DataTypeParser1v04(namespaces, aliases, defParser)

    (nodeSet \ "UADataType")
      .map(typeParser.parse)
      .foldLeft(Map.empty[UaId, UaDataType]){
        case (result, dataType) => result + (dataType.id -> dataType)
      }
  }


  private def parseNodeSet(doc: Node): Node = (doc \\ "UANodeSet").head


  private def parseAliases(nodeSet: Node): Map[String, String] = {

    (nodeSet \ "Aliases" \ "Alias")
      .map(
        alias => {
          val name = (alias \ "@Alias").text
          val value = alias.text
          name -> value
        })
      .toMap
  }


  private def parseNamespaces(nodeSet: Node): Map[Int, String] = {

    val uris = _namespace +: (nodeSet \ "NamespaceUris" \ "Uri").map(_.text)
    val indexes = uris.indices

    indexes
      .zip(uris)
      .toMap
  }

}
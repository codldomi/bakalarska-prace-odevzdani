package scopcua.util.binarydecoder.impl.util

import scopcua.data.value.UaExtensionObject.{UaByteExtensionObject, UaXmlExtensionObject}
import scopcua.data.value.UaNodeId.{UaGuidNodeId, UaNumericNodeId, UaOpaqueNodeId, UaStringNodeId}
import scopcua.data.value.UaVariant.UaBuiltInId
import scopcua.data.value.{UaArray, UaBoolean, UaByteString, UaDataValue, UaDateTime, UaDiagnosticInfo, UaExpandedNodeId, UaExtensionObject, UaGuid, UaLocalizedText, UaNodeId, UaNull, UaQualifiedName, UaStatusCode, UaString, UaValue, UaVariant, UaXmlElement}
import scopcua.data.value.number.{UaDouble, UaFloat}
import scopcua.data.value.number.integer.UaSInteger.{UaInt16, UaInt32, UaInt64, UaSByte}
import scopcua.data.value.number.integer.UaUInteger.{UaByte, UaUInt16, UaUInt32, UaUInt64}

import io.netty.buffer.ByteBuf

import java.nio.charset.StandardCharsets


/** Encapsulates reading/writing methods on Netty buffer.
  *
  * @note Standard Binary DataEncoding uses the little endian format. OPC UA byte is unsigned 8-bit integer. On the
  *       other hand Java's byte is signed 8-bit integer.
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/5.2.2/ Part 6, OPC UA Binary Data Encoding, Built-in Types]]
  * @param buffer Initialized Netty buffer.
  * @param nestingLevels Maximum of nesting levels for recursive structures.
  * @param statusCodes Supported StatusCodes.
  */
class BinaryBuffer(val buffer: ByteBuf, val statusCodes: Map[UaUInt32, UaStatusCode], val nestingLevels: Int = 100) {

  /** Adds bytes to the buffer.
    * @param bytes Sequence of bytes values.
    */
  def writeBytes(bytes: Seq[Int]): Unit = bytes.foreach(byte => buffer.writeByte(byte))

  /** Adds bytes in String representation to the buffer.
    * @param string Sequence of bytes encoded as characters.
    */
  def writeString(string: String): Unit = string.foreach(char => buffer.writeByte(char))

  /** Adds bytes in ByteString representation to the buffer.
    * @param byteString Sequence of bytes.
    */
  def writeByteString(byteString: UaByteString): Unit = {

    byteString
      .bytes
      .foreach(byte => buffer.writeByte(byte.value))
  }

  /** Gets Boolean value and removes 1 byte from the buffer.
    */
  def readBoolean(): UaBoolean = UaBoolean(buffer.readBoolean())

  /** Gets Byte (unsigned 8-bit integer) value and removes 1 byte from the buffer.
    */
  def readByte(): UaByte = UaByte(buffer.readUnsignedByte())

  /** Gets SByte (signed 8-bit integer) value and removes 1 byte from the buffer.
    */
  def readSByte(): UaSByte = UaSByte(buffer.readByte())

  /** Gets Unsigned 16-bit Integer value and removes 2 bytes from the buffer.
    */
  def readUInt16(): UaUInt16 = UaUInt16(buffer.readUnsignedShortLE())

  /** Gets Signed 16-bit Integer value and removes 2 bytes from the buffer.
    */
  def readInt16(): UaInt16 = UaInt16(buffer.readShortLE())

  /** Gets Unsigned 32-bit Integer value and removes 4 bytes from the buffer.
    */
  def readUInt32(): UaUInt32 = UaUInt32(buffer.readUnsignedIntLE())

  /** Gets Signed 32-bit Integer value and removes 4 bytes from the buffer.
    */
  def readInt32(): UaInt32 = UaInt32(buffer.readIntLE())

  /** Gets Unsigned 64-bit Integer value and removes 8 bytes from the buffer.
    */
  def readUInt64(): UaUInt64 = {

    val signedLong = buffer.readLongLE()
    val bigInt = (BigInt(signedLong >>> 1) << 1) + (signedLong & 1)

    UaUInt64(bigInt)
  }

  /** Gets Signed 64-bit Integer value and removes 8 bytes from the buffer.
    */
  def readInt64(): UaInt64 = UaInt64(buffer.readLongLE())

  /** Gets Floating Point value and removes 4 bytes from the buffer.
    */
  def readFloat(): UaFloat = UaFloat(buffer.readFloatLE())

  /** Gets Floating Point value with Double Precision and removes 8 bytes from the buffer.
    */
  def readDouble(): UaDouble = UaDouble(buffer.readDoubleLE())

  /** Gets UTF-8 character's sequence and removes (String's length + 4 as encoded Int32 length) bytes from
    * the buffer.
    */
  def readString(): UaString = {

    val length = readInt32().value

    if (length == -1) {
      return DefaultValue.string
    }

    val chars = buffer.readCharSequence(length, StandardCharsets.UTF_8)
    UaString(chars.toString)
  }

  /** Gets DateTime and removes 8 bytes from the buffer.
    */
  def readDateTime(): UaDateTime = UaDateTime(buffer.readLongLE())

  /** Gets OPC UA byte's sequence and removes (bytes' length + 4 as encoded Int32 length) bytes from the buffer.
    */
  def readByteString(): UaByteString = {

    val length = readInt32().value

    if (length == -1) {
      return UaByteString(Seq.empty)
    }

    val bytes = for (_ <- 0 until length) yield readByte()
    UaByteString(bytes)
  }

  /** Reads GUID value and removes 16 bytes from the buffer.
   */
  def readGuid(): UaGuid = {

    val length = 16
    val bytes = for (_ <- 0 until length) yield readByte()

    UaGuid(bytes)
  }

  /** Gets XmlElement as UTF-8 character's sequence and removes (String's length + 4 as encoded Int32 length) bytes from
   *  the buffer.
   */
  def readXmlElement(): UaXmlElement = {

    val str = readString()
    UaXmlElement(str.value)
  }

  /** Gets StatusCode value and removes 4 bytes from the buffer.
    */
  def readStatusCode(): UaStatusCode = {

    val code = readUInt32()
    statusCodes(code)
  }

  /** Gets QualifiedName value and removes bytes from the buffer.
    */
  def readQualifiedName(): UaQualifiedName = {

    val namespaceIndex = readUInt16()
    val name = readString()

    UaQualifiedName(name, namespaceIndex)
  }

  /** Gets LocalizedText value and removes bytes from the buffer.
    */
  def readLocalizedText(): UaLocalizedText = {

    val mask = readByte().value

    val locale = if ((mask & 0x01) == 0x01) readString() else DefaultValue.string
    val text   = if ((mask & 0x02) == 0x02) readString() else DefaultValue.string

    UaLocalizedText(text, locale)
  }

  /** Gets NodeId based on the encoded type and removes bytes from the buffer.
    */
  def readNodeId(): UaNodeId = {

    val mask = readByte()
    readNodeIdByMask(mask.value)
  }

  /** Gets ExtensionObject based on the encoded type and removes bytes from the buffer.
    */
  def readExtensionObject(): UaExtensionObject = {

    val encodingId = readNodeId()
    val format = readByte()

    format.value match {
      case 0 => UaByteExtensionObject(encodingId, UaByteString(Seq.empty))
      case 1 => UaByteExtensionObject(encodingId, readByteString())
      case 2 => UaXmlExtensionObject(encodingId, readXmlElement())
    }
  }

  /** Gets Variant and removes bytes from the buffer.
    * @param depth Current nesting level.
    */
  def readVariant(depth: Int = 0): UaVariant = {

    def toBuiltInType(id: Int): UaBuiltInId = id match {

      case 1  => UaBuiltInId.Boolean
      case 2  => UaBuiltInId.SByte
      case 3  => UaBuiltInId.Byte
      case 4  => UaBuiltInId.Int16
      case 5  => UaBuiltInId.UInt16
      case 6  => UaBuiltInId.Int32
      case 7  => UaBuiltInId.UInt32
      case 8  => UaBuiltInId.Int64
      case 9  => UaBuiltInId.UInt64
      case 10 => UaBuiltInId.Float
      case 11 => UaBuiltInId.Double
      case 12 => UaBuiltInId.String
      case 13 => UaBuiltInId.DateTime
      case 14 => UaBuiltInId.Guid
      case 15 => UaBuiltInId.ByteString
      case 16 => UaBuiltInId.XmlElement
      case 17 => UaBuiltInId.NodeId
      case 18 => UaBuiltInId.ExpandedNodeId
      case 19 => UaBuiltInId.StatusCode
      case 20 => UaBuiltInId.QualifiedName
      case 21 => UaBuiltInId.LocalizedText
      case 22 => UaBuiltInId.ExtensionObject
      case 23 => UaBuiltInId.DataValue
      case 24 => UaBuiltInId.Variant
      case 25 => UaBuiltInId.DiagnosticInfo

      case _ => throw new IllegalArgumentException("Bad built-in type ID.")
    }


    def readBuiltIn(id: UaBuiltInId): UaValue = id match {

      case UaBuiltInId.Null             => UaNull
      case UaBuiltInId.Boolean          => readBoolean()
      case UaBuiltInId.SByte            => readSByte()
      case UaBuiltInId.Byte             => readByte()
      case UaBuiltInId.Int16            => readInt16()
      case UaBuiltInId.UInt16           => readUInt16()
      case UaBuiltInId.Int32            => readInt32()
      case UaBuiltInId.UInt32           => readUInt32()
      case UaBuiltInId.Int64            => readInt64()
      case UaBuiltInId.UInt64           => readUInt64()
      case UaBuiltInId.Float            => readFloat()
      case UaBuiltInId.Double           => readDouble()
      case UaBuiltInId.String           => readString()
      case UaBuiltInId.DateTime         => readDateTime()
      case UaBuiltInId.Guid             => readGuid()
      case UaBuiltInId.ByteString       => readByteString()
      case UaBuiltInId.XmlElement       => readXmlElement()
      case UaBuiltInId.NodeId           => readNodeId()
      case UaBuiltInId.ExpandedNodeId   => readExpandedNodeId()
      case UaBuiltInId.StatusCode       => readStatusCode()
      case UaBuiltInId.QualifiedName    => readQualifiedName()
      case UaBuiltInId.LocalizedText    => readLocalizedText()
      case UaBuiltInId.ExtensionObject  => readExtensionObject()
      case UaBuiltInId.DataValue        => readDataValue()
      case UaBuiltInId.Variant          => readVariant(depth + 1)
      case UaBuiltInId.DiagnosticInfo   => readDiagnosticInfo()

      case _ => throw new RuntimeException("There should be all built-in possibilities.")
    }


    def readValues(isArray: Boolean, mask: Int): (UaValue, UaBuiltInId) = {

      val length = if (isArray) readInt32().value else 0

      if (length == -1) {
        return (UaNull, UaBuiltInId.Null)
      }

      val id = toBuiltInType(mask & 0x3F)

      if (length == 0) {
        return (readBuiltIn(id), id)
      }

      val values = for (_ <- 0 until length) yield readBuiltIn(id)

      (UaArray(values.toVector), id)
    }


    def readDimensions(): UaArray[UaInt32] = {

      val length = readInt32().value

      if (length == -1) {
        return UaArray()
      }

      val dimensions = for (_ <- 0 until length) yield readInt32()

      UaArray(dimensions.toVector)
    }


    require(depth <= nestingLevels, s"Depth of variant decoding should not be greater than $nestingLevels")


    val mask = readByte().value

    if (mask == 0) {
      return UaVariant(UaBuiltInId.Null, UaNull)
    }

    val isArray = (mask & 0x80) == 0x80
    val isDimensionEncoded = (mask & 0x40) == 0x40

    val (value, builtInId) = readValues(isArray, mask)

    if (isDimensionEncoded) {
      val dimensions = readDimensions()

      if (dimensions.isEmpty) {
        return UaVariant(builtInId, value)
      }

      var values = value match {
        case array: UaArray[UaValue] => array
        case _ => throw new IllegalArgumentException("Bad encoded variant.")
      }

      values = arrayToMultiDimArray(values, dimensions.array.map(_.value))
      UaVariant(builtInId, values)
    } else {
      UaVariant(builtInId, value)
    }
  }

  /** Gets DiagnosticInfo and removes bytes from the buffer.
    * @param depth Current nesting level.
    */
  def readDiagnosticInfo(depth: Int = 0): UaDiagnosticInfo = {

    require(depth <= nestingLevels, s"Depth of diagnostic info decoding should not be greater than $nestingLevels")

    val mask = readByte().value

    val symbolicId          = if ((mask & 0x01) == 0x01) readInt32() else UaInt32(-1)
    val namespaceUri        = if ((mask & 0x02) == 0x02) readInt32() else UaInt32(-1)
    val localizedText       = if ((mask & 0x04) == 0x04) readInt32() else UaInt32(-1)
    val locale              = if ((mask & 0x08) == 0x08) readInt32() else UaInt32(-1)
    val additionalInfo      = if ((mask & 0x10) == 0x10) readString() else DefaultValue.string
    val innerStatusCode     = if ((mask & 0x20) == 0x20) Some(readStatusCode()) else None
    val innerDiagnosticInfo = if ((mask & 0x40) == 0x40) Some(readDiagnosticInfo(depth + 1)) else None

    UaDiagnosticInfo(
      symbolicId,
      namespaceUri,
      localizedText,
      locale,
      additionalInfo,
      innerStatusCode,
      innerDiagnosticInfo)
  }

  /** Gets DataValue and removes bytes from the buffer.
    */
  def readDataValue(): UaDataValue = {

    val mask = readByte().value

    val value             = if ((mask & 0x01) == 0x01) readVariant()    else DefaultValue.variant
    val status            = if ((mask & 0x02) == 0x02) readStatusCode() else DefaultValue.statusCode
    val sourceTimestamp   = if ((mask & 0x04) == 0x04) readDateTime()   else DefaultValue.dateTime
    val sourcePicoSeconds = if ((mask & 0x08) == 0x08) readUInt16()     else DefaultValue.uint16
    val serverTimestamp   = if ((mask & 0x10) == 0x10) readDateTime()   else DefaultValue.dateTime
    val serverPicoSeconds = if ((mask & 0x20) == 0x20) readUInt16()     else DefaultValue.uint16

    UaDataValue(
      value,
      status,
      sourceTimestamp,
      sourcePicoSeconds,
      serverTimestamp,
      serverPicoSeconds)
  }

  /** Gets ExpandedNodeId and removes bytes from the buffer.
    */
  def readExpandedNodeId(): UaExpandedNodeId = {

    val mask = readByte()
    val uriIsSpecified = (mask.value & 0x80) == 0x80

    val nodeId =
      if (uriIsSpecified)
        readNodeIdWithoutNamespace(mask.value)
      else
        readNodeIdByMask(mask.value)

    val namespaceUri = if (uriIsSpecified) readString() else DefaultValue.string
    val serverIndex  = if ((mask.value & 0x40) == 0x40) readUInt32() else DefaultValue.uint32

    UaExpandedNodeId(namespaceUri, nodeId, serverIndex)
  }


  private def readNodeIdWithoutNamespace(mask: Short): UaNodeId = {

    val nsIndex = DefaultValue.uint16

    mask & 0x0F match {
      case 0x00 =>
        UaNumericNodeId(
          nsIndex,
          UaUInt32(readByte().value))
      case 0x01 =>
        UaNumericNodeId(
          nsIndex,
          UaUInt32(readUInt16().value))
      case 0x02 =>
        UaNumericNodeId(
          nsIndex,
          readUInt32())
      case 0x03 =>
        UaStringNodeId(
          nsIndex,
          readString())
      case 0x04 =>
        UaGuidNodeId(
          nsIndex,
          readGuid())
      case 0x05 =>
        UaOpaqueNodeId(
          nsIndex,
          readByteString())
      case _ => throw new IllegalArgumentException("Not supported NodeId format.")
    }
  }


  private def readNodeIdByMask(mask: Short): UaNodeId = mask & 0x0F match {
    case 0x00 =>
      UaNumericNodeId(
        UaUInt16(0),
        UaUInt32(readByte().value))
    case 0x01 =>
      UaNumericNodeId(
        UaUInt16(readByte().value),
        UaUInt32(readUInt16().value))
    case 0x02 =>
      UaNumericNodeId(
        readUInt16(),
        readUInt32())
    case 0x03 =>
      UaStringNodeId(
        readUInt16(),
        readString())
    case 0x04 =>
      UaGuidNodeId(
        readUInt16(),
        readGuid())
    case 0x05 =>
      UaOpaqueNodeId(
        readUInt16(),
        readByteString())
    case _ => throw new IllegalArgumentException("Not supported NodeId format.")
  }


  private def arrayToMultiDimArray(values: UaArray[UaValue], dimensions: Seq[Int]): UaArray[UaValue] = {

    if (dimensions.isEmpty) {
      return values
    }

    dimensions
      .tail
      .foldRight(values)((dim, result) => {
        val groupedArray: Vector[UaValue] =
          result
            .array
            .grouped(dim)
            .map(group => UaArray(group))
            .toVector
        UaArray(groupedArray)
      })
  }


}

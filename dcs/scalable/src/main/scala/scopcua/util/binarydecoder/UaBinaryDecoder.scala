package scopcua.util.binarydecoder

import scopcua.data.UaId
import scopcua.data.value.UaValue


/** A decoder from binary data encoding to the value.
  */
trait UaBinaryDecoder {

  /** Directly decodes value.
    * @param encoded Bytes as characters in string.
    * @param typeId DataType's identifier.
    * @return Decoded value.
    */
  def decode(encoded: String, typeId: UaId): UaValue

  /** Decodes value from variant.
    *
    * @note Servers/clients typically work with variants encapsulating a value. This method serves as a shortcut to
    *       decoding values from an encoded variant.
    * @param encoded Bytes as characters in string.
    * @param typeId DataType's identifier.
    * @return Decoded value.
    */
  def decodeVariant(encoded: String, typeId: UaId): UaValue

}

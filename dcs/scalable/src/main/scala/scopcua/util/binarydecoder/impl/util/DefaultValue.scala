package scopcua.util.binarydecoder.impl.util

import scopcua.data.{UaDataType, UaId}
import scopcua.data.value.UaExtensionObject.UaByteExtensionObject
import scopcua.data.value.UaNodeId.UaNumericNodeId
import scopcua.data.value.UaValue
import scopcua.data.value._
import scopcua.data.value.UaVariant.UaBuiltInId
import scopcua.data.value.number.{UaDouble, UaFloat}
import scopcua.data.value.number.integer.UaSInteger.{UaInt16, UaInt32, UaInt64, UaSByte}
import scopcua.data.value.number.integer.UaUInteger.{UaByte, UaUInt16, UaUInt32, UaUInt64}


/** Contains all default values for standard Binary DataEncoding.
  */
object DefaultValue {

  /** Generates default value for the selected DataType.
    *
    * @param dataType Selected DataType.
    * @param types Supported DataTypes.
    * @return Default value.
    */
  def of(dataType: UaDataType, types: Map[UaId, UaDataType] = Map.empty): UaValue = dataType match {

    case UaDataType.Boolean               => boolean
    case UaDataType.SByte                 => sbyte
    case UaDataType.Byte                  => byte
    case UaDataType.Int16                 => int16
    case UaDataType.UInt16                => uint16
    case UaDataType.Int32                 => int32
    case UaDataType.UInt32                => uint32
    case UaDataType.Int64                 => int64
    case UaDataType.UInt64                => uint64
    case UaDataType.Float                 => float
    case UaDataType.Double                => double
    case UaDataType.String                => string
    case UaDataType.DateTime              => dateTime
    case UaDataType.Guid                  => guid
    case UaDataType.ByteString            => byteString
    case UaDataType.XmlElement            => xmlElement
    case UaDataType.NodeId                => nodeId
    case UaDataType.ExpandedNodeId        => expandedNodeId
    case UaDataType.StatusCode            => statusCode
    case UaDataType.QualifiedName         => qualifiedName
    case UaDataType.LocalizedText         => localizedText
    case UaDataType.DataValue             => dataValue
    case UaDataType.BaseDataType          => variant
    case UaDataType.DiagnosticInfo        => diagnosticInfo

    case t: UaDataType.GeneralEnumeration => enumeration(t)
    case t: UaDataType.GeneralStructure   => structure(t, types)

    case _ => UaNull
  }

  def byte: UaByte = UaByte(0)

  def sbyte: UaSByte = UaSByte(0)

  def uint16: UaUInt16 = UaUInt16(0)

  def int16: UaInt16 = UaInt16(0)

  def uint32: UaUInt32 = UaUInt32(0)

  def int32: UaInt32 = UaInt32(0)

  def uint64: UaUInt64 = UaUInt64(0)

  def int64: UaInt64 = UaInt64(0)

  def float: UaFloat = UaFloat(0)

  def double: UaDouble = UaDouble(0)

  def boolean: UaBoolean = UaBoolean(false)

  def byteString: UaByteString = UaByteString(Seq.empty)

  def dataValue: UaDataValue =

    UaDataValue(
      value = UaVariant(UaBuiltInId.Null, UaNull),
      status = UaStatusCode(UaUInt32(0), UaString("Good")),
      sourceTimestamp = UaDateTime(0),
      sourcePicoSeconds = UaUInt16(0),
      serverTimestamp = UaDateTime(0),
      serverPicoSeconds = UaUInt16(0))

  def dateTime: UaDateTime = UaDateTime(0)

  def diagnosticInfo: UaDiagnosticInfo =

    UaDiagnosticInfo(
      symbolicId = UaInt32(0),
      namespaceUri = UaInt32(0),
      localizedText = UaInt32(0),
      locale = UaInt32(0),
      additionalInfo = UaString(""),
      innerStatusCode = None,
      innerDiagnosticInfo = None)

  def expandedNodeId: UaExpandedNodeId =

    UaExpandedNodeId(
      namespaceUri = UaString(""),
      nodeId = UaNumericNodeId(UaUInt16(0), UaUInt32(0)),
      serverIndex = UaUInt32(0))

  def extensionObject: UaExtensionObject =

    UaByteExtensionObject(
      encodingId = UaNumericNodeId(UaUInt16(0), UaUInt32(0)),
      body = UaByteString(Seq.empty))

  def guid: UaGuid = UaGuid(Seq.empty)

  def localizedText: UaLocalizedText =

    UaLocalizedText(
      text = UaString(""),
      locale = UaString(""))

  def nodeId: UaNodeId =

    UaNumericNodeId(
      namespaceIndex = UaUInt16(0),
      identifier = UaUInt32(0))

  def qualifiedName: UaQualifiedName =

    UaQualifiedName(
      name = UaString(""),
      namespaceIndex = UaUInt16(0))

  def statusCode: UaStatusCode = UaStatusCode(UaUInt32(0), UaString("Good"))

  def string: UaString = UaString("")

  def variant: UaVariant =

    UaVariant(
      id = UaBuiltInId.Null,
      value = UaNull)

  def xmlElement: UaXmlElement = UaXmlElement("")

  def array: UaArray[UaValue] = UaArray()

  def enumeration(dataType: UaDataType.GeneralEnumeration): UaEnumeration = {

    val first = dataType.values.head
    UaEnumeration(first._2, first._1)
  }

  def structure(dataType: UaDataType.GeneralStructure, types: Map[UaId, UaDataType]): UaStructure = {

    val fields =
      dataType
        .fields
        .foldLeft(Map.empty[UaString, UaValue])((result, field) => {
          val value = of(types(field.typeId), types)
          val fieldValue =
            if (field.isArray == UaBoolean(true))
              UaArray(value)
            else
              value

          result + (field.name -> fieldValue)
        })

    UaStructure(fields)
  }

}

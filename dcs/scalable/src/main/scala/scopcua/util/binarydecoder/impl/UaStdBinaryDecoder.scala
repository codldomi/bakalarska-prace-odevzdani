package scopcua.util.binarydecoder.impl

import scopcua.data.value.{UaArray, UaBoolean, UaEnumeration, UaExtensionObject, UaNull, UaStatusCode, UaString, UaStructure, UaValue, UaVariant}
import scopcua.data.value.number.integer.UaSInteger._
import scopcua.util.binarydecoder.UaBinaryDecoder
import scopcua.data.UaDataType.GeneralEnumeration
import scopcua.data.value.UaExtensionObject.UaByteExtensionObject
import scopcua.data.value.number.integer.UaUInteger.UaUInt32
import scopcua.data.{UaDataType, UaId}
import scopcua.util.binarydecoder.impl.util.{BinaryBuffer, DefaultValue}

import io.netty.buffer.UnpooledByteBufAllocator


/** Decoder for the standard OPC UA Binary DataEncoding.
  *
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/5.2.1/ Part 6, OPC UA Binary Data Encoding]]
  * @param types Supported DataTypes.
  * @param nestingLevels Maximum of nesting levels for recursive structures.
  * @param statusCodes Supported StatusCodes.
  */
class UaStdBinaryDecoder(
  val types: Map[UaId, UaDataType],
  val statusCodes: Map[UaUInt32, UaStatusCode],
  val nestingLevels: Int = 100
) extends UaBinaryDecoder {

  /** Directly decodes value.
    * @param encoded Bytes as characters in string.
    * @param typeId DataType's identifier.
    * @return Decoded value.
    */
  override def decode(encoded: String, typeId: UaId): UaValue = {

    val nettyBuffer =
      UnpooledByteBufAllocator.DEFAULT
        .buffer()

    val buffer = new BinaryBuffer(nettyBuffer, statusCodes, nestingLevels)

    try {
      buffer.writeString(encoded)
      decodeValue(buffer, types(typeId))
    } finally {
      nettyBuffer.release()
    }
  }


  private def decodeValue(buffer: BinaryBuffer, dataType: UaDataType): UaValue = dataType match {

    case UaDataType.Null                  => UaNull

    case UaDataType.Boolean               => buffer.readBoolean()
    case UaDataType.SByte                 => buffer.readSByte()
    case UaDataType.Byte                  => buffer.readByte()
    case UaDataType.Int16                 => buffer.readInt16()
    case UaDataType.UInt16                => buffer.readUInt16()
    case UaDataType.Int32                 => buffer.readInt32()
    case UaDataType.UInt32                => buffer.readUInt32()
    case UaDataType.Int64                 => buffer.readInt64()
    case UaDataType.UInt64                => buffer.readUInt64()
    case UaDataType.Float                 => buffer.readFloat()
    case UaDataType.Double                => buffer.readDouble()
    case UaDataType.String                => buffer.readString()
    case UaDataType.DateTime              => buffer.readDateTime()
    case UaDataType.Guid                  => buffer.readGuid()
    case UaDataType.ByteString            => buffer.readByteString()
    case UaDataType.XmlElement            => buffer.readXmlElement()
    case UaDataType.NodeId                => buffer.readNodeId()
    case UaDataType.ExpandedNodeId        => buffer.readExpandedNodeId()
    case UaDataType.StatusCode            => buffer.readStatusCode()
    case UaDataType.QualifiedName         => buffer.readQualifiedName()
    case UaDataType.LocalizedText         => buffer.readLocalizedText()
    case UaDataType.Structure             => buffer.readExtensionObject()
    case UaDataType.DataValue             => buffer.readDataValue()
    case UaDataType.BaseDataType          => buffer.readVariant()
    case UaDataType.DiagnosticInfo        => buffer.readDiagnosticInfo()
    case t: UaDataType.GeneralEnumeration => decodeEnumeration(buffer.readInt32(), t)
    case t: UaDataType.GeneralStructure   => decodeStructure(buffer, t)

    case _ => throw new RuntimeException("There should be decoding method for each data type class.")
  }


  private def decodeStructure(buffer: BinaryBuffer, dataType: UaDataType.GeneralStructure): UaValue = {

    val hasOptionalFields =
      dataType
        .fields
        .exists(_.isOptional == UaBoolean(true))

    val mask = if (hasOptionalFields) buffer.readByte().value.toInt else 0

    val bits = createBits(mask, dataType)
    val fieldDescs = dataType.fields.zip(bits)

    val fields =
      fieldDescs
        .foldLeft(Map.empty[UaString, UaValue]){ case (result, (desc, isPresent)) =>
          val fieldType = types(desc.typeId)
          val value =
            (isPresent, desc.isArray.value) match {
              case (true, true) => decodeArray(buffer, fieldType)
              case (true, false) => decodeValue(buffer, fieldType)
              case _ => DefaultValue.of(fieldType)
            }

          result + (desc.name -> value)
        }

    UaStructure(fields)
  }


  private def createBits(mask: Int, dataType: UaDataType.GeneralStructure): Seq[Boolean] = {

    var value = mask

    dataType
      .fields
      .foldLeft(Seq.empty[Boolean])((bits, field) => {
        var bit = true

        if (field.isOptional == UaBoolean(true)) {
          if (value % 2 == 0) bit = false
          value = value >>> 1
        }

        bits :+ bit
      })
  }


  private def decodeEnumeration(value: UaInt32, dataType: UaDataType.GeneralEnumeration): UaEnumeration = {

    val result =
      dataType
        .values
        .find{
          case (_, current) => current == value
        }

    val name = result match {
      case Some(found) => found._1
      case None => UaString("")
    }

    UaEnumeration(value, name)
  }


  private def decodeArray(buffer: BinaryBuffer, dataType: UaDataType): UaArray[UaValue] = {

    val length = buffer.readInt32().value

    if (length <= 0) {
      return UaArray()
    }

    val values = for (_ <- 0 until length) yield decodeValue(buffer, dataType)
    UaArray(values.toVector)
  }

  /** Decodes value from variant.
    *
    * @note Servers/clients typically work with variants encapsulating a value. This method serves as a shortcut to
    *       decoding values from an encoded variant.
    * @param encoded Bytes as characters in string.
    * @param typeId DataType's identifier.
    * @return Decoded value.
    */
  override def decodeVariant(encoded: String, typeId: UaId): UaValue = {

    val nettyBuffer =
      UnpooledByteBufAllocator.DEFAULT
        .buffer()

    val buffer = new BinaryBuffer(nettyBuffer, statusCodes, nestingLevels)

    try {
      buffer.writeString(encoded)
      val variant = buffer.readVariant()
      decodeToOrigin(variant.value, typeId)
    } finally {
      nettyBuffer.release()
    }
  }


  private def decodeToOrigin(value: UaValue, typeId: UaId): UaValue = value match {

    case array: UaArray[UaValue] =>
      val newArray =
        array
          .array
          .map(v => decodeToOrigin(v, typeId))
      UaArray(newArray)

    case obj: UaExtensionObject => obj match {
        case UaByteExtensionObject(_, body) => decode(body.bytes.map(_.value.toChar).mkString, typeId)
        case other => other
      }

    case i: UaInt32 => types(typeId) match {
      case en: GeneralEnumeration => decodeEnumeration(i, en)
      case _ => i
    }

    case variant: UaVariant => decodeToOrigin(variant.value, typeId)

    case other => other
  }

}

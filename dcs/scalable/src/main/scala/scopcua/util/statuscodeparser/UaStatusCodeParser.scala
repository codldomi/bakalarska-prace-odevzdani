package scopcua.util.statuscodeparser

import scopcua.data.value.UaStatusCode
import scopcua.data.value.number.integer.UaUInteger.UaUInt32


trait UaStatusCodeParser {

  /** Parses text and creates mapping (code value -> StatusCode).
    *
    * @param text Textual representation of StatusCodes.
    * @return Pairs of (code value, StatusCode).
    */
  def parse(text: String): Map[UaUInt32, UaStatusCode]

}

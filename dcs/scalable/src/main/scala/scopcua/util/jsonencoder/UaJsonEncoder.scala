package scopcua.util.jsonencoder

import scopcua.data.value.UaValue


/** A Encoder from Value to implemented JSON format.
  */
trait UaJsonEncoder {

  /** Encodes value to the JSON format.
    *
    * @param value Value representation.
    * @return JSON as String.
    */
  def encode(value: UaValue): String

}

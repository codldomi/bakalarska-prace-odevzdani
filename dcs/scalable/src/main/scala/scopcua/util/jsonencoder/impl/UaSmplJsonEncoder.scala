package scopcua.util.jsonencoder.impl

import scopcua.data.value.{UaArray, UaBoolean, UaByteString, UaDataValue, UaDateTime, UaDiagnosticInfo, UaEnumeration, UaExpandedNodeId, UaExtensionObject, UaGuid, UaLocalizedText, UaNodeId, UaQualifiedName, UaStatusCode, UaString, UaStructure, UaValue, UaVariant, UaXmlElement}
import scopcua.data.value.number.integer.UaInteger
import scopcua.data.value.number.{UaDouble, UaFloat}
import scopcua.data.value.number.integer.UaSInteger.{UaInt16, UaInt32, UaInt64, UaSByte}
import scopcua.data.value.number.integer.UaUInteger.{UaByte, UaUInt16, UaUInt32, UaUInt64}
import scopcua.util.jsonencoder.UaJsonEncoder
import scopcua.data.value.UaExtensionObject.{UaByteExtensionObject, UaXmlExtensionObject}
import scopcua.data.value.UaNodeId.{UaGuidNodeId, UaNumericNodeId, UaOpaqueNodeId, UaStringNodeId}
import scopcua.conversion.UaValueConversion.uaDateTimeToTimestamp

import net.liftweb.json.{JBool, JField, JNull, JsonAST}
import net.liftweb.json.JsonAST.{JArray, JDouble, JInt, JObject, JString, JValue}

import java.sql.Timestamp
import java.util.{Base64, Date, TimeZone}
import java.text.SimpleDateFormat


/** Encoder for the Simplified JSON.
  *
  * @note Simplified JSON is defined as OPC UA Irreversible JSON DataEncoding with these differences:
  *       1. All numbers (float, double, integers, ...) are encoded as JSON numbers.
  *       1. In the case of namespace "http://opcfoundation.org/UA/", an index or URI is not omitted.
  *       1. Option fields is not omitted, but they are encoded as JSON null.
  *
  *       The main goals are:
  *       1. Reduce the number of special cases during processing JSON.
  *       1. Keep the whole object's structure in JSON.
  *
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/5.4.1/ Part 6, JSON Data Encoding]]
  * @param namespaces Namespace table with pair (index, uri)
  * @param nestingLevels Maximum of nesting levels for recursive structures.
  */
class UaSmplJsonEncoder(
  val namespaces: Map[UaUInt16, UaString],
  val nestingLevels: Int = 100
) extends UaJsonEncoder {

  /** Encodes value to the JSON format.
    *
    * @param value Value representation.
    * @return JSON as String.
    */
  override def encode(value: UaValue): String = {

    val json = jsonValue(value)
    JsonAST.compactRender(json)
  }


  private def jsonValue(value: UaValue): JValue = value match {

    case v: UaInteger          => jsonInteger(v)
    case v: UaBoolean          => jsonBoolean(v)
    case v: UaFloat            => jsonFloat(v)
    case v: UaDouble           => jsonDouble(v)
    case v: UaString           => jsonString(v)
    case v: UaDateTime         => jsonDateTime(v)
    case v: UaGuid             => jsonGuid(v)
    case v: UaByteString       => jsonByteString(v)
    case v: UaXmlElement       => jsonXmlElement(v)
    case v: UaNodeId           => jsonNodeId(v)
    case v: UaExpandedNodeId   => jsonExpandedNodeId(v)
    case v: UaStatusCode       => jsonStatusCode(v)
    case v: UaQualifiedName    => jsonQualifiedName(v)
    case v: UaLocalizedText    => jsonLocalizedText(v)
    case v: UaExtensionObject  => jsonExtensionObject(v)
    case v: UaDataValue        => jsonDataValue(v)
    case v: UaVariant          => jsonVariant(v)
    case v: UaDiagnosticInfo   => jsonDiagnosticInfo(v)
    case v: UaEnumeration      => jsonEnumeration(v)
    case v: UaStructure        => jsonStructure(v)
    case v: UaArray[UaValue]   => jsonArray(v)

    case _ => JNull
  }


  private def jsonObject(fields: Map[String, JValue]): JObject = {

    val fieldSeq = fields
      .map{ case (name, value) => JField(name, value) }
      .toList

    JObject(fieldSeq)
  }


  private def jsonNamespaceUri(index: UaUInt16): JString = jsonString(namespaces(index))


  private def jsonBoolean(bool: UaBoolean): JBool = JBool(bool.value)


  private def jsonInteger(int: UaInteger): JInt = int match {

    case UaByte(value)   => JInt(value)
    case UaSByte(value)  => JInt(value)
    case UaUInt16(value) => JInt(value)
    case UaInt16(value)  => JInt(value)
    case UaUInt32(value) => JInt(value)
    case UaInt32(value)  => JInt(value)
    case UaUInt64(value) => JInt(value)
    case UaInt64(value)  => JInt(value)

    case _ => throw new RuntimeException("There should be JSON for all integers.")
  }

  private def jsonFloat(float: UaFloat): JDouble = JDouble(float.value)


  private def jsonDouble(double: UaDouble): JDouble = JDouble(double.value)


  private def jsonString(string: UaString): JString = JString(string.value)


  private def jsonDateTime(dateTime: UaDateTime): JString = {

    val unix: Timestamp = dateTime
    val date = new Date(unix.getTime)

    val df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
    df.setTimeZone(TimeZone.getTimeZone("UTC"))

    JString(df.format(date))
  }


  private def jsonByteString(byteString: UaByteString): JString = {

    val bytes = byteString
      .bytes
      .map{ case UaByte(value) => value.toByte }
      .toArray

    val base64String = Base64.getEncoder.encodeToString(bytes)
    JString(base64String)
  }


  private def jsonGuid(guid: UaGuid): JString = {

    val guidAsString = guid.bytes.map{ case UaByte(value) => value.toChar }.mkString
    JString(guidAsString)
  }


  private def jsonXmlElement(xml: UaXmlElement): JString = JString(xml.value)


  private def jsonStatusCode(statusCode: UaStatusCode): JObject = {

    val fields = Map(
      "code"   -> jsonInteger(statusCode.code),
      "symbol" -> jsonString(statusCode.symbol))

    jsonObject(fields)
  }


  private def jsonQualifiedName(qualifiedName: UaQualifiedName): JObject = {

    val fields = Map(
      "name" -> jsonString(qualifiedName.name),
      "uri"  -> jsonNamespaceUri(qualifiedName.namespaceIndex))

    jsonObject(fields)
  }


  private def jsonLocalizedText(localizedText: UaLocalizedText): JObject = {

    val fields = Map(
      "locale" -> jsonString(localizedText.locale),
      "text"   -> jsonString(localizedText.text))

    jsonObject(fields)
  }


  private def jsonNodeId(nodeId: UaNodeId): JObject = {

    val (namespaceUri, idType, id) = nodeId match {
      case UaNumericNodeId(nsIndex, id) => (jsonNamespaceUri(nsIndex), JInt(0), jsonInteger(id))
      case UaGuidNodeId(nsIndex, id)    => (jsonNamespaceUri(nsIndex), JInt(2), jsonGuid(id))
      case UaStringNodeId(nsIndex, id)  => (jsonNamespaceUri(nsIndex), JInt(4), jsonString(id))
      case UaOpaqueNodeId(nsIndex, id)  => (jsonNamespaceUri(nsIndex), JInt(1), jsonByteString(id))
      case _ => throw new IllegalArgumentException("Not supported datatype.")
    }

    val fields = Map(
      "idType"       -> idType,
      "namespaceUri" -> namespaceUri,
      "id"           -> id)

    jsonObject(fields)
  }


  private def jsonExtensionObject(obj: UaExtensionObject): JObject = {

    val (body, enType) = obj match {
      case UaByteExtensionObject(_, body) => (jsonByteString(body), JInt(0))
      case UaXmlExtensionObject(_, body)  => (jsonXmlElement(body), JInt(1))
    }

    val fields = Map(
      "body" -> body,
      "enType" -> enType)

    jsonObject(fields)
  }


  private def jsonVariant(variant: UaVariant): JValue = jsonValue(variant.value)


  private def jsonDiagnosticInfo(info: UaDiagnosticInfo, depth: Int = 0): JObject = {

    require(depth <= nestingLevels, s"Depth of diagnostic info decoding should not be greater than $nestingLevels")

    val innerStatusCode = info.innerStatusCode match {
      case Some(status) => jsonStatusCode(status)
      case None => JNull
    }
    val innerDiagnosticInfo = info.innerDiagnosticInfo match {
      case Some(innerInfo) => jsonDiagnosticInfo(innerInfo, depth + 1)
      case None => JNull
    }

    val fields = Map(
      "symbolicId"          -> jsonInteger(info.symbolicId),
      "namespaceUri"        -> jsonInteger(info.namespaceUri),
      "localizedText"       -> jsonInteger(info.localizedText),
      "locale"              -> jsonInteger(info.locale),
      "additionalInfo"      -> jsonString(info.additionalInfo),
      "innerStatusCode"     -> innerStatusCode,
      "innerDiagnosticInfo" -> innerDiagnosticInfo)

    jsonObject(fields)
  }


  private def jsonDataValue(dataValue: UaDataValue): JObject = {

    val fields = Map(
      "value"             -> jsonVariant(dataValue.value),
      "status"            -> jsonStatusCode(dataValue.status),
      "serverTimestamp"   -> jsonDateTime(dataValue.serverTimestamp),
      "serverPicoSeconds" -> jsonInteger(dataValue.serverPicoSeconds),
      "sourceTimestamp"   -> jsonDateTime(dataValue.sourceTimestamp),
      "sourcePicoSeconds" -> jsonInteger(dataValue.sourcePicoSeconds)
    )

    jsonObject(fields)
  }


  private def jsonExpandedNodeId(expNodeId: UaExpandedNodeId): JObject = {

    val (idType, id) = expNodeId.nodeId match {
      case UaNumericNodeId(_, id) => (JInt(0), jsonInteger(id))
      case UaGuidNodeId(_, id)    => (JInt(2), jsonGuid(id))
      case UaStringNodeId(_, id)  => (JInt(1), jsonString(id))
      case UaOpaqueNodeId(_, id)  => (JInt(3), jsonByteString(id))
      case _ => throw new IllegalArgumentException("Not supported datatype.")
    }

    val namespaceUri = jsonString(expNodeId.namespaceUri)
    val serverIndex = JInt(expNodeId.serverIndex.value)

    val fields = Map(
      "idType"       -> idType,
      "namespaceUri" -> namespaceUri,
      "id"           -> id,
      "serverIndex"  -> serverIndex)

    jsonObject(fields)
  }


  private def jsonStructure(struct: UaStructure): JObject = {

    val fields = struct.fields.transform((_, value) => jsonValue(value))
    val jsonFields = fields.map{ case (name, value) => (name.value, value)}

    jsonObject(jsonFields)
  }


  private def jsonEnumeration(enumeration: UaEnumeration): JObject = {

    val value = jsonInteger(enumeration.value)
    val name = jsonString(enumeration.name)

    val fields =
      Map(
        "value" -> value,
        "name" -> name)

    jsonObject(fields)
  }


  private def jsonArray[A <: UaValue](array: UaArray[A]): JArray = {

    val jsonValues =
      array
        .array
        .map(jsonValue)
        .toList

    JArray(jsonValues)
  }

}


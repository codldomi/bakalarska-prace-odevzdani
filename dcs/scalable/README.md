## Scalable OPC UA

verze 0.1.0

---

### Jazyk

- [Scala](https://www.scala-lang.org/) 2.13.7
- konvence dle [Scala Style Guide](https://docs.scala-lang.org/style/)

### Vývojové prostředí

- [IntelliJ IDEA](https://www.jetbrains.com/idea/) (Community nebo Ultimate) s [pluginem Scala](https://www.jetbrains.com/help/idea/discover-intellij-idea-for-scala.html)

### Použité knihovny

- [ScalaTest](https://www.scalatest.org/)
- [Lift-JSON](https://github.com/lift/lift/tree/master/framework/lift-base/lift-json)
- [Eclipse Milo](https://projects.eclipse.org/proposals/milo)
- [PureConfig](https://pureconfig.github.io/)
- [Netty](https://netty.io/)
- [LogBack](https://logback.qos.ch/)

### Funkcionality

- podpora OPC UA verze 1.04 
- sdk:
  - synchronní klient
  - anonymním přihlašení nebo pomocí jméno + heslo + PKI
- moduly:
  - JsonEncoder
  - BinaryDecoder
  - DataTypeParser
  - StatusCodeParser
- data:
  - datové typy (built-in, vlastní struktury a výčty)
  - hodnoty (obecná struktura a výčet bez jakéhokoliv generování kódu)
- konverze
  - implicitní převody mezi datovými typy

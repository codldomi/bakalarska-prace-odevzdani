
name := "ScalableOpcUa"
organization := "ModemTec"


version := "0.1"
scalaVersion := "2.13.7"


libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.9"

//  Test
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.10" % "test"

//  XML
libraryDependencies += "org.scala-lang" % "scala-xml" % "2.11.0-M4"

//  Binary
libraryDependencies += "io.netty" % "netty-all" % "4.1.17.Final"

//  JSON
libraryDependencies += "net.liftweb" %% "lift-json" % "3.5.0"

//  TypeSafe Config
libraryDependencies += "com.github.pureconfig" %% "pureconfig" % "0.17.1" % "test"

//  OPC UA
libraryDependencies += "org.eclipse.milo" % "sdk-client" % "0.6.3"
libraryDependencies += "org.eclipse.milo" % "sdk-server" % "0.6.3"

//  LOG
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.10" % "test"
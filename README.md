## Bakalářská práce

- autor: Dominik Codl
- vedoucí: Ing. Ladislav Šťastný, PhD.
- téma: Systém pro sběr s využitím OPC UA 
- rok: 2022

---

Obsah přílohy je rozdělen do dvou částí - adresář "/text" obsahující zdrojové soubory písemné části a adresář "/dcs" obsahující nepísemnou část práce.

---

### Adresář "text"

- _soubor "BP_Codl_Dominik_2022.pdf"_ - text práce ve formátu PDF
- _adresář "latex_zdroj"_ - zdrojové soubory práce v LaTeX
- _adresář "obrazky_zdroj"_ - zdrojové soubory obrázků, které jsou exportovány v PDF a použity v projektu LaTeX. Zdrojové soubory obrázků jsou tvořeny skrze projekt [draw.io](https://app.diagrams.net/)

---

### Adresář "dcs"

Adresář obsahuje projekty vytvořené v rámci bakalářské práce. Každý z nich vlastní readme.md soubor, kde se nachází informace o projektu a návod pro programátory ohledně zprovoznění vývojových prostředí či zvoléném jazyce a stylu. 

- _adresář "database"_ - projekt databáze (Data Collection Database) obsahující skripty, projekty, obrázky databázových schémat.
- _adresář "scalable"_ - projekt knihovny Scalable OPC UA v jazyce Scala
- _adresář "application"_ - projekt aplikace Data Collection Application v jazyce Scala
- _adresář "test_server"_ - testovací OPC UA server v jazyce C/C++

Ačkoliv je v písemné části (sekce 6.3 Testování, kapitola 6 Scalable OPC UA) zmíněn ještě projekt [Eclipse Milo Demo Server](https://github.com/digitalpetri/opc-ua-demo-server), který byl použit k testování, nenachází se v nepísemné části práci. Důvodem je, že se nejedná o tvorbu autora.

